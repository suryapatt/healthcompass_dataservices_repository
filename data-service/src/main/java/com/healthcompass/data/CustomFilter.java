package com.healthcompass.data;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class CustomFilter implements Filter {

	private static final ThreadLocal<String> _tz = new ThreadLocal<>();

    public static ThreadLocal<String> getUserTimeZone() {
        return _tz;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        //....
        //other code
        //get the date from request
    	HttpServletRequest httpRequest = (HttpServletRequest) request;
    	Enumeration<String> headerNames = httpRequest.getHeaderNames();
    	String userTimeZone = "Asia/Kolkata";

        if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                	String headerName = (String) headerNames.nextElement();
                	 System.out.println("Header Name: " +headerName);
                	String headerValue = httpRequest.getHeader(headerName);
                	 System.out.println("Header Value: " +headerValue);
                        if(headerName.equalsIgnoreCase("user-tz"))  userTimeZone = headerValue;
                }
        }
       
        //set the date to the local thread variable
        
        
        _tz.set(userTimeZone);
        chain.doFilter(request, response);
    }
}

