package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class ServiceBillHistorySearchPredicates {

	private static final QServiceBill serviceBill = QServiceBill.serviceBill;
	private static final BooleanExpression matchAll = serviceBill.isNotNull();
	
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : serviceBill.active.eq(true);
	}
	
	public BooleanExpression isPatientIdEq(UUID patientId) {
		
		return patientId == null ? matchAll : serviceBill.patient.id.eq(patientId);
	}
	
  	 public BooleanExpression isBillGenerated(Boolean billGenerated) {
			return billGenerated == null ? matchAll : serviceBill.billNumber.isNotNull();
	 }
	
	 public BooleanExpression isPractitionerRoleIdsInList(List<UUID> idList) {
			
			return ((idList == null) || (idList.isEmpty())) ? matchAll : serviceBill.practitionerRole.id.in(idList);
		}
}
