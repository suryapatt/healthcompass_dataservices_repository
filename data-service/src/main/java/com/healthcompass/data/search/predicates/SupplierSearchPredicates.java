package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.healthcompass.data.model.QSupplier;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class SupplierSearchPredicates {

	private static final QSupplier supplier = QSupplier.supplier;
	private static final BooleanExpression matchAll = supplier.isNotNull();
	
	public BooleanExpression isOrganizationIdEq(UUID orgnaizationId) {
		return orgnaizationId == null ? matchAll : supplier.organization.id.eq(orgnaizationId);
	}
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : supplier.active.eq(true);
	}
	
	public BooleanExpression isSupplierNameLike(String supplierName) {
		return supplierName == null ? matchAll : supplier.name.likeIgnoreCase("%"+supplierName+"%");
	}
	public BooleanExpression isIdsInList(List<UUID> idList) {
		
		return ((idList == null) || (idList.isEmpty())) ? matchAll : supplier.id.in(idList);
	}
	
	
	
	
	
	
	
}
