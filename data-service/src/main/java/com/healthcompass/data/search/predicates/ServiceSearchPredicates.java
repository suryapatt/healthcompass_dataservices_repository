package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QService;


import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class ServiceSearchPredicates {

	private static final QService service = QService.service;
	private static final BooleanExpression matchAll = service.isNotNull();
	
	public BooleanExpression isLocationIdEq(UUID locationId) {
		return locationId == null ? matchAll : service.locationId.eq(locationId);
	}
	
	public BooleanExpression isOrganizationIdEq(UUID organizationId) {
		
		return organizationId == null ? matchAll : service.organizationId.eq(organizationId);
	}
	
    public BooleanExpression isIdsInList(List<Integer> idList) {
		
		return ((idList == null) || (idList.isEmpty())) ? matchAll : service.id.in(idList);
	}
	
	
}
