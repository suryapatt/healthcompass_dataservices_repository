package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QMarketer;
import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.healthcompass.data.model.QSupplier;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class MarketerSearchPredicates {

	private static final QMarketer marketer = QMarketer.marketer;
	private static final BooleanExpression matchAll = marketer.isNotNull();
	
	public BooleanExpression isOrganizationIdEq(UUID orgnaizationId) {
		return orgnaizationId == null ? matchAll : marketer.organization.id.eq(orgnaizationId);
	}
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : marketer.active.eq(true);
	}
	
	public BooleanExpression isMarketerNameLike(String marketerName) {
		return marketerName == null ? matchAll : marketer.name.likeIgnoreCase("%"+marketerName+"%");
	}
	public BooleanExpression isIdsInList(List<Integer> idList) {
		
		return ((idList == null) || (idList.isEmpty())) ? matchAll : marketer.id.in(idList);
	}
	
	
	
	
	
	
	
}
