package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QInventoryItem;
import com.healthcompass.data.model.QInventorySummary;
import com.healthcompass.data.model.QRateCard;
import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class InventoryDetailsSearchPredicates {

	private static final QInventoryItem inventory = QInventoryItem.inventoryItem;
	private static final BooleanExpression matchAll = inventory.isNotNull();
	
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : inventory.product.active.eq(true);
	}
	public BooleanExpression isOrganizationIdEq(UUID organizationId) {
		return organizationId == null ? matchAll : inventory.organization.id.eq(organizationId);
	}
	
	public BooleanExpression isProductIdEq(Integer productId) {
		return productId == null ? matchAll : inventory.product.id.eq(productId);
	}
	
	public BooleanExpression isLocationIdEq(UUID locationId) {
		return locationId == null ? matchAll : inventory.location.id.eq(locationId);
	}
	
	public BooleanExpression isProductNameLike(String productName ) {
		return productName == null ? matchAll : inventory.product.name.likeIgnoreCase("'%"+productName+"%'");
	}
	
	





	public BooleanExpression isAvailableUnitsOperator(Integer count,String operator) {
			BooleanExpression result = matchAll;
			if(operator != null && !operator.isEmpty()) {
				switch (operator) {
				case "lt":
					result =  (count == null) ? matchAll : inventory.availableUnits.lt(count);
					break;
					
				case "lte":
					result =  (count == null) ? matchAll : inventory.availableUnits.loe(count);
					break;
				
				
				case "eq":
					result = (count == null) ? matchAll : inventory.availableUnits.eq(count);
					break;
					
				case "ne":
					result = (count == null) ? matchAll : inventory.availableUnits.ne(count);
					break;
				
				case "gt":
					result = (count == null) ? matchAll : inventory.availableUnits.gt(count);
					break;
					
				case "gte":
					result = (count == null) ? matchAll : inventory.availableUnits.goe(count);
					break;
				
				default:
					break;
				}
			}	
			return result;
			
		}
	
	
	
	
}
