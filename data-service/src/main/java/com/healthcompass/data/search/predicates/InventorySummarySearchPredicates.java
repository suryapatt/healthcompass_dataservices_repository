package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QInventorySummary;
import com.healthcompass.data.model.QRateCard;
import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class InventorySummarySearchPredicates {

	private static final QInventorySummary inventorySummary = QInventorySummary.inventorySummary;
	private static final BooleanExpression matchAll = inventorySummary.isNotNull();
	
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : inventorySummary.product.active.eq(true);
	}
	public BooleanExpression isOrganizationIdEq(UUID organizationId) {
		return organizationId == null ? matchAll : inventorySummary.organization.id.eq(organizationId);
	}
	
	public BooleanExpression isProductIdEq(Integer productId) {
		return productId == null ? matchAll : inventorySummary.product.id.eq(productId);
	}
	
	public BooleanExpression isLocationIdEq(UUID locationId) {
		return locationId == null ? matchAll : inventorySummary.location.id.eq(locationId);
	}
	
	public BooleanExpression isProductNameLike(String productName ) {
		return productName == null ? matchAll : inventorySummary.product.name.likeIgnoreCase("'%"+productName+"%'");
	}
	
	public BooleanExpression isAvailableUnitsOperator(Integer count,String operator) {
		BooleanExpression result = matchAll;
		if(operator != null && !operator.isEmpty()) {
			switch (operator) {
			case "lt":
				result =  (count == null) ? matchAll : inventorySummary.availableUnits.lt(count);
				break;
				
			case "lte":
				result =  (count == null) ? matchAll : inventorySummary.availableUnits.loe(count);
				break;
			
			
			case "eq":
				result = (count == null) ? matchAll : inventorySummary.availableUnits.eq(count);
				break;
				
			case "ne":
				result = (count == null) ? matchAll : inventorySummary.availableUnits.ne(count);
				break;
			
			case "gt":
				result = (count == null) ? matchAll : inventorySummary.availableUnits.gt(count);
				break;
				
			case "gte":
				result = (count == null) ? matchAll : inventorySummary.availableUnits.goe(count);
				break;
			
			default:
				break;
			}
		}	
		return result;
		
	}
	
	
	
	
}
