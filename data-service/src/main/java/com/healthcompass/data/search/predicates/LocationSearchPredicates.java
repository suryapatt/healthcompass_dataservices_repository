package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QLocation;
import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class LocationSearchPredicates {

	private static final QLocation location = QLocation.location;
	private static final BooleanExpression matchAll = location.isNotNull();
	
	public BooleanExpression isIdEq(UUID locationId) {
		return locationId == null ? matchAll : location.id.eq(locationId);
	}
	public BooleanExpression isOrganizationIdEq(UUID organizationId) {
		return organizationId == null ? matchAll : location.organization.id.eq(organizationId);
	}
	
	
	 public BooleanExpression isIdsInList(List<UUID> idList) {
			
			return ((idList == null) || (idList.isEmpty())) ? matchAll : location.id.in(idList);
		}
	
	
	
	
}
