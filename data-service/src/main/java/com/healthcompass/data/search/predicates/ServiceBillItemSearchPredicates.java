package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBillItem;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class ServiceBillItemSearchPredicates {

	private static final QServiceBillItem serviceBillItem = QServiceBillItem.serviceBillItem;
	private static final BooleanExpression matchAll = serviceBillItem.isNotNull();
	
	public BooleanExpression isIdEq(UUID serviceBillItemId) {
		return serviceBillItemId == null ? matchAll : serviceBillItem.id.eq(serviceBillItemId);
	}
	
	public BooleanExpression isServiceBillIdEq(UUID serviceBillId) {
		
		return serviceBillId == null ? matchAll : serviceBillItem.serviceBill.id.eq(serviceBillId);
	}
	
	 public BooleanExpression isIdsInList(List<UUID> idList) {
			
			return ((idList == null) || (idList.isEmpty())) ? matchAll : serviceBillItem.id.in(idList);
		}
	
    
	
	
}
