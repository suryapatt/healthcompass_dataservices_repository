package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class ServiceBillSearchPredicates {

	private static final QServiceBill serviceBill = QServiceBill.serviceBill;
	private static final BooleanExpression matchAll = serviceBill.isNotNull();
	
	public BooleanExpression isIdEq(UUID serviceBillId) {
		return serviceBillId == null ? matchAll : serviceBill.id.eq(serviceBillId);
	}
	
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : serviceBill.active.eq(true);
	}
	
	public BooleanExpression isEncounterIdEq(UUID encounterId) {
		return encounterId == null ? matchAll : serviceBill.encounter.id.eq(encounterId);
	}
	
	public BooleanExpression isPatientIdEq(UUID patientId) {
		
		return patientId == null ? matchAll : serviceBill.patient.id.eq(patientId);
	}
	
   public BooleanExpression isPractitionerRoleIdEq(UUID practitionerRoleId) {
		
		return practitionerRoleId == null ? matchAll : serviceBill.practitionerRole.id.eq(practitionerRoleId);
	}

	 public BooleanExpression isIdsInList(List<UUID> idList) {
			
			return ((idList == null) || (idList.isEmpty())) ? matchAll : serviceBill.id.in(idList);
		}
	
	
	
	
}
