package com.healthcompass.data.search.predicates;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.healthcompass.data.model.QRateCard;
import com.healthcompass.data.model.QService;
import com.healthcompass.data.model.QServiceBill;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class RateCardSearchPredicates {

	private static final QRateCard rateCard = QRateCard.rateCard;
	private static final BooleanExpression matchAll = rateCard.isNotNull();
	
	public BooleanExpression isIdEq(Integer id) {
		return id == null ? matchAll : rateCard.id.eq(id);
	}
	public BooleanExpression isActive(Boolean active) {
		return active == null ? matchAll : rateCard.active.eq(true);
	}
	public BooleanExpression isLocationIdEq(UUID locationId) {
		return locationId == null ? matchAll : rateCard.location.id.eq(locationId);
	}
	
	
	
	 public BooleanExpression isIdsInList(List<Integer> idList) {
			
			return ((idList == null) || (idList.isEmpty())) ? matchAll : rateCard.id.in(idList);
		}
	
	
	
	
}
