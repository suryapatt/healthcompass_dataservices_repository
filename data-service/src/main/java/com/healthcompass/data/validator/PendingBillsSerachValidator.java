package com.healthcompass.data.validator;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.PurchaseOrderService;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component

public class PendingBillsSerachValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return  BillPaymentsSearchParams.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			BillPaymentsSearchParams billPaymentsSearchParams = (BillPaymentsSearchParams) obj;
			if(billPaymentsSearchParams.getPatientId() == null && billPaymentsSearchParams.getPhoneNumber() == null && billPaymentsSearchParams.getBillNumber() == null ) {
				errors.rejectValue("", "", null, "At least one value of either Patient ID, Phone Number or Bill Number should be selected");
			}
			
				
			
			
			
			
			
		}
		
		
		public void customValidate(Object obj, Map<String,Object> errors,Errors errors1) {
			
			
			BillPaymentsSearchParams billPaymentsSearchParams = (BillPaymentsSearchParams) obj;
			if(billPaymentsSearchParams.getPatientId() == null && billPaymentsSearchParams.getPhoneNumber() == null && billPaymentsSearchParams.getBillNumber() == null ) {
				//errors.rejectValue("", "", null, "At least one value of either Patient ID, Phone Number or Bill Number should be selected");
				errors.put("billSearch","At least one value of either Patient ID, Phone Number or Bill Number should be selected\"");
				HealthCompassValidationUtils.markError(errors);
			}
			
				
			
			
			
			
			
		}


}
