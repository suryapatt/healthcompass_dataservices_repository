package com.healthcompass.data.validator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.CustomFilter;
import com.healthcompass.data.model.TimeZoneInformation;
import com.healthcompass.data.service.TimeZoneService;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
@Component

public class PurchaseOrderItemValidator implements Validator {
	
	    @Autowired
	    TimeZoneService timeZoneService;

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return PurchaseOrderItemViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName",""," Product Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mrp",""," MRP is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity",""," Quantity is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discount",""," Discount is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netItemValue",""," Net Item Value is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "expiryDate",""," Expiry Date is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "batchNumber",""," Batch Number is required field");
			
			String expiryDate = (String)errors.getFieldValue("expiryDate");
			String userTimeZone = CustomFilter.getUserTimeZone().get();
			TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
		    String userLocalDateFormatOnlyDate = timeZoneDateInfo.getTimeZoneDateFormat();
			
			try {
				
					
					DateFormat sourceDateFormat = new SimpleDateFormat(userLocalDateFormatOnlyDate);
					sourceDateFormat.setLenient(false);
					java.util.Date date = sourceDateFormat.parse(expiryDate);

				
			}catch(Exception e) {
				errors.rejectValue("expiryDate", "", null, "Invalid Expiry Date! Expected format:"+userLocalDateFormatOnlyDate);
				return;
			}
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netAmount",""," Net Amount is required field");
			boolean proceedWithAmountTotals=true;
			
			Double quantity = 0.0;
			Double mrp=0.0;
			Double discount = 0.0;
			Double netItemValue = 0.0;
			try {
				
				quantity = Double.valueOf((String)errors.getFieldValue("quantity"));
				mrp = Double.valueOf((String)errors.getFieldValue("mrp"));
				discount = Double.valueOf((String)errors.getFieldValue("discount"));
				netItemValue = Double.valueOf((String)errors.getFieldValue("netItemValue"));
				
				
						if(quantity < 0){
							errors.rejectValue("quantity", "", null, "Quantity should be positive");
							proceedWithAmountTotals=false;
						}
						if(mrp < 0){
							errors.rejectValue("mrp", "", null, "MRP should be positive");
							proceedWithAmountTotals=false;
						}
						if(discount < 0){
							errors.rejectValue("discount", "", null, "Discount should be positive");
							proceedWithAmountTotals=false;
						}
						
						if(netItemValue < 0){
							errors.rejectValue("netItemValue", "", null, "Net Item Value shoul be positive");
							proceedWithAmountTotals=false;
						}
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			
			
			
			
			
			if(proceedWithAmountTotals){
				
			
					if(mrp.compareTo(netItemValue) == -1) {
						errors.rejectValue("mrp", "", null, "MRP cannot be less than  net item value");
						proceedWithAmountTotals=false;
					}
				
			}	
				
				if(proceedWithAmountTotals) {
					if(discount.compareTo(mrp) > 0) {
						errors.rejectValue("discount", "", null, "Discount Amount cannot be more than MRP");
						proceedWithAmountTotals=false;
					}
				
				}
				
				if(!proceedWithAmountTotals) return;
				
				
				Double calculatedNetNetItemValue = mrp-discount;
				if(calculatedNetNetItemValue.compareTo(netItemValue) != 0) {
					errors.rejectValue("netItemValue", "", null, "Net Item Value is not mathcing mrp-discounty");
					
				}
				
			}
			
			
	
		
		public void customValidate(Object obj, Errors errors1,Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> orderItemsErrors = (ArrayList) errors.get("orderItems");
			
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active",""," Active flag is required field");
			Map<String,Object> orderItemErrorMap = new HashMap<String,Object>();
			orderItemsErrors.add(orderItemErrorMap);
			
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "productName",""," Product Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "mrp",""," MRP is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "quantity",""," Quantity is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "discount",""," Discount is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "netItemValue",""," Net Item Value is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "expiryDate",""," Expiry Date is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap, "batchNumber",""," Batch Number is required field");
			
			String expiryDate = (String)errors1.getFieldValue("expiryDate");
			String userTimeZone = CustomFilter.getUserTimeZone().get();
			TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
		    String userLocalDateFormatOnlyDate = timeZoneDateInfo.getTimeZoneDateFormat();
			
			try {
				
					
					DateFormat sourceDateFormat = new SimpleDateFormat(userLocalDateFormatOnlyDate);
					sourceDateFormat.setLenient(false);
					java.util.Date date = sourceDateFormat.parse(expiryDate);

				
			}catch(Exception e) {
				orderItemErrorMap.put("expiryDate", "Invalid Expiry Date! Expected format:"+userLocalDateFormatOnlyDate+"");
				HealthCompassValidationUtils.markError(orderItemErrorMap);
				return;
			}
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netAmount",""," Net Amount is required field");
			boolean proceedWithAmountTotals=true;
			
			Double quantity = 0.0;
			Double mrp=0.0;
			Double discount = 0.0;
			Double netItemValue = 0.0;
			try {
				
				quantity = Double.valueOf((String)errors1.getFieldValue("quantity"));
				mrp = Double.valueOf((String)errors1.getFieldValue("mrp"));
				discount = Double.valueOf((String)errors1.getFieldValue("discount"));
				netItemValue = Double.valueOf((String)errors1.getFieldValue("netItemValue"));
				
				
						if(quantity < 0){
							orderItemErrorMap.put("quantity", "Quantity should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						if(mrp < 0){
							orderItemErrorMap.put("mrp", "MRP should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						if(discount < 0){
							orderItemErrorMap.put("discount", "Discount should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						
						if(netItemValue < 0){
							orderItemErrorMap.put("netItemValue", "Net Item Value shoul be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			
			
			
			
			
			if(proceedWithAmountTotals){
				
			
					if(mrp.compareTo(netItemValue) == -1) {
						orderItemErrorMap.put("mrp", "MRP cannot be less than  net item value");
						HealthCompassValidationUtils.markError(orderItemErrorMap);
						proceedWithAmountTotals=false;
					}
				
			}	
				
				if(proceedWithAmountTotals) {
					if(discount.compareTo(mrp) > 0) {
						orderItemErrorMap.put("discount", "Discount Amount cannot be more than MRP");
						HealthCompassValidationUtils.markError(orderItemErrorMap);
						proceedWithAmountTotals=false;
					}
				
				}
				
				if(!proceedWithAmountTotals) return;
				
				
				Double calculatedNetNetItemValue = mrp-discount;
				if(calculatedNetNetItemValue.compareTo(netItemValue) != 0) {
					orderItemErrorMap.put("netItemValue", "Net Item Value is not mathcing mrp-discount");
					HealthCompassValidationUtils.markError(orderItemErrorMap);
					
					
					
				}
				
			}
			


}
