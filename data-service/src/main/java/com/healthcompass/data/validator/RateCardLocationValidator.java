package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component
@Qualifier("rateCardLocationValidator")
public class RateCardLocationValidator implements Validator {
	
	private final RateCardValidator rateCardValidator;

   public RateCardLocationValidator(RateCardValidator rateCardValidator) {
        if (rateCardValidator == null) {
            throw new IllegalArgumentException(
              "The supplied [Validator] is required and must not be null.");
        }
        if (!rateCardValidator.supports(LocationRateCardViewDTO.class)) {
            throw new IllegalArgumentException(
              "The supplied [Validator] must support the validation of [LocationRateCardViewDTO] instances.");
        }
        this.rateCardValidator = rateCardValidator;
    }
		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return LocationRateCardsViewDTO.class.isAssignableFrom(paramClass);
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			LocationRateCardsViewDTO locationRateCards = (LocationRateCardsViewDTO) obj;
			
			
			if(locationRateCards.getRateCards() == null || locationRateCards.getRateCards().isEmpty()) {
			
				errors.rejectValue("rateCards", "", null, "Items list is required");
				return;
			}
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "locationId", "","Location is required field");
			
			NestedPropertyValidator.invokeValidatorForNestedProperties(rateCardValidator, locationRateCards, "rateCards", errors);
		   
		}


	
		public void customValidate(Object obj,Map<String,Object> errors,Errors errors1) {
			
						
			LocationRateCardsViewDTO locationRateCards = (LocationRateCardsViewDTO) obj;
			
			
			if(locationRateCards.getRateCards() == null || locationRateCards.getRateCards().isEmpty()) {
			
				errors.put("rateCards","Items list is required");
				HealthCompassValidationUtils.markError(errors);
				
				return;
			}
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors,  "locationId", "","Location is required field");
			errors.put("rateCards",new ArrayList<Map<String,Object>>());
			
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(locationRateCards, "rateCards");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("rateCards" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				rateCardValidator.customValidate(elem, errors1, errors);
				errors1.popNestedPath();
				counter++;
			}
		  }	
			
		   
		
}
