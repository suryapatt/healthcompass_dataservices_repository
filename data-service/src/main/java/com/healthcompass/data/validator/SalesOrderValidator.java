package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.SalesOrderService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.SalesOrderItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
@Component
public class SalesOrderValidator implements Validator {
	
	@Autowired
	SalesOrderService salesOrderService;
	
	private final SalesOrderItemValidator salesOrderItemValidator;
	
	 public SalesOrderValidator(SalesOrderItemValidator salesOrderItemValidator) {
	        if (salesOrderItemValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied salesOrderItemValidator is required and must not be null.");
	        }
	        if (!salesOrderItemValidator.supports(SalesOrderItemViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied salesOrderItemValidator must support the validation of PurchaseOrderItemViewDTO instances.");
	        }
	        this.salesOrderItemValidator = salesOrderItemValidator;
	    }
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SalesOrderViewDTO.class.isAssignableFrom(paramClass);
			
		}

		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			
			
			
			
			
			
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orderValue","","Order Value is required field");
			
			Map<String,Object> orderMap = (Map) errors.get("order");
			
			boolean proceedWithCalculations = true;
			Double orderValue=0.0;
			Double discount = 0.0;
			Double netOrderValue=0.0;
			
			try {
				orderValue = Double.valueOf((String)errors1.getFieldValue("orderValue"));
				
				
				
				if(orderValue < 0){
					orderMap.put("orderValue","Order Value should be positive");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
				
				
				
				
				
				 						
				
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			
			SalesOrderViewDTO salesOrder = (SalesOrderViewDTO) obj;
			
			orderMap.put("orderItems",new ArrayList<Map<String,Object>>());
			
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(obj, "orderItems");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("orderItems" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				salesOrderItemValidator.customValidate(elem, errors1, orderMap);
				errors1.popNestedPath();
				counter++;
			}
			
			
			Double totalCalculatedOrderValueAmount = 0.0;
			Set<UUID> uniqueInventoryProductBatch = new HashSet<UUID>();
			List<UUID> inventoryIdList = new ArrayList<UUID>();
			
			try {
				for(SalesOrderItemViewDTO orderItem : salesOrder.getOrderItems()) {
					
					totalCalculatedOrderValueAmount+=orderItem.getItemValue();
					
					if(uniqueInventoryProductBatch.contains(orderItem.getInventory().getInventoryId())) {
						
						orderMap.put("orderItems","Duplicate Inevntory-Product combination in the same Order: Details Inventory:"+orderItem.getInventory().getInventoryId()
							+" Product:"+orderItem.getProduct().getProductId()+"");
						HealthCompassValidationUtils.markError(orderMap);
						
						
						return;
					}else {
						uniqueInventoryProductBatch.add(orderItem.getInventory().getInventoryId());
						inventoryIdList.add(orderItem.getInventory().getInventoryId());
					}
					
				}
				
				Map<UUID,Integer> latestAvailableUnits = salesOrderService.getLatestAvailableUnitsFromInventory(inventoryIdList);
				
				for(SalesOrderItemViewDTO orderItem : salesOrder.getOrderItems()) {
					
					
					
					if(latestAvailableUnits.containsKey(orderItem.getInventory().getInventoryId())) {
						Integer availableUnitsFromDB = (Integer) latestAvailableUnits.get(orderItem.getInventory().getInventoryId());
						if(availableUnitsFromDB.compareTo(orderItem.getQuantity()) == -1){
							orderMap.put("orderItems","Item Quantity is greater than available units: Details Qty: "+orderItem.getQuantity()+ " AvailableUnits: "+availableUnitsFromDB+ "");
								HealthCompassValidationUtils.markError(orderMap);
							//errors.rejectValue("orderItems", "", null, "Item Quantity is greater than available units: Details Qty: "+orderItem.getQuantity()+ " AvailableUnits: "+availableUnitsFromDB+ " ");
									
						}
						
						
					}
					
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(orderValue.compareTo(totalCalculatedOrderValueAmount) != 0) {
				orderMap.put("orderValue","Order Value not matching with individual item values");
				HealthCompassValidationUtils.markError(orderMap);
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			
			
						
		}
		
		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			
			
			
			
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orderValue","","Order Value is required field");
			
			
			boolean proceedWithCalculations = true;
			Double orderValue=0.0;
			Double discount = 0.0;
			Double netOrderValue=0.0;
			
			try {
				orderValue = Double.valueOf((String)errors.getFieldValue("orderValue"));
				
				
				
				if(orderValue < 0){
					errors.rejectValue("orderValue", "", null, "Order Value should be positive");
					proceedWithCalculations=false;
				}
				
				
				
				
				
				 						
				
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			
			SalesOrderViewDTO salesOrder = (SalesOrderViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(salesOrderItemValidator, salesOrder, "orderItems", errors);
			
			Double totalCalculatedOrderValueAmount = 0.0;
			Set<UUID> uniqueInventoryProductBatch = new HashSet<UUID>();
			List<UUID> inventoryIdList = new ArrayList<UUID>();
			
			try {
				for(SalesOrderItemViewDTO orderItem : salesOrder.getOrderItems()) {
					
					totalCalculatedOrderValueAmount+=orderItem.getItemValue();
					
					if(uniqueInventoryProductBatch.contains(orderItem.getInventory().getInventoryId())) {
						errors.rejectValue("orderItems", "", null, "Duplicate Inevntory-Product combination in the same Order: Details Inventory:"+orderItem.getInventory().getInventoryId()
								+" Product:"+orderItem.getProduct().getProductId()+"");
						return;
					}else {
						uniqueInventoryProductBatch.add(orderItem.getInventory().getInventoryId());
						inventoryIdList.add(orderItem.getInventory().getInventoryId());
					}
					
				}
				
				Map<UUID,Integer> latestAvailableUnits = salesOrderService.getLatestAvailableUnitsFromInventory(inventoryIdList);
				
				for(SalesOrderItemViewDTO orderItem : salesOrder.getOrderItems()) {
					
					
					
					if(latestAvailableUnits.containsKey(orderItem.getInventory().getInventoryId())) {
						Integer availableUnitsFromDB = (Integer) latestAvailableUnits.get(orderItem.getInventory().getInventoryId());
						if(availableUnitsFromDB.compareTo(orderItem.getQuantity()) == -1){
							errors.rejectValue("orderItems", "", null, "Item Quantity is greater than available units: Details Qty: "+orderItem.getQuantity()+ " AvailableUnits: "+availableUnitsFromDB+ " ");
									
						}
						
						
					}
					
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(orderValue.compareTo(totalCalculatedOrderValueAmount) != 0) {
				errors.rejectValue("orderValue", "", null, "Order Value not matching with individual item values");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			
			
						
		}


}
