package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component
@Qualifier("billPaymentMainViewValidator")
public class CreateNewBillPaymentMainViewValidator implements Validator {
	
	private final BillPaymentViewValidator billPaymentValidator;
	
	 public CreateNewBillPaymentMainViewValidator(BillPaymentViewValidator billPaymentValidator) {
	        if (billPaymentValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied BillPaymentViewValidator is required and must not be null.");
	        }
	        if (!billPaymentValidator.supports(CreateNewBillPaymentViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied v must support the validation of CreateNewBillPaymentViewDTO instances.");
	        }
	        this.billPaymentValidator = billPaymentValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return CreateNewBillPaymentMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "patientId", "","Patient is required field");
						
			CreateNewBillPaymentMainViewDTO billPaymentMainViewDTO = (CreateNewBillPaymentMainViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(billPaymentValidator, billPaymentMainViewDTO, "billPayments", errors);
			
			
			
		}
		
	
		public void customValidate(Object obj, Map<String,Object> errors,Errors errors1) {
			
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors,  "patientId", "","Patient is required field");
				
			CreateNewBillPaymentMainViewDTO billPaymentMainViewDTO = (CreateNewBillPaymentMainViewDTO) obj;
			//NestedPropertyValidator.invokeValidatorForNestedProperties(billPaymentValidator, billPaymentMainViewDTO, "billPayments", errors);
			
			ArrayList<Map<String,Object>> billPaymentsMap = new ArrayList<Map<String,Object>>();
			errors.put("billPayments",billPaymentsMap);
			//###
			
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(billPaymentMainViewDTO, "billPayments");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("billPayments" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				billPaymentValidator.customValidate(elem, errors1, errors);
				errors1.popNestedPath();
				counter++;
			}
		  }	
		
		
			//###
	       
			
			
			
		}



