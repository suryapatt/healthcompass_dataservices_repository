package com.healthcompass.data.validator;

import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
@Component
@Qualifier("rateCardCategoryValidator")
public class RateCardCategoryValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return LocationRateCardsCategoryViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rateCardCatgeoryDescription","","Category is required field");
			
			
		}
		
		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			Map<String,Object> rateCardCategoryMap = (Map) errors.get("rateCardCategory");
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, rateCardCategoryMap,"rateCardCatgeoryDescription", "","Category is required field");
			
			
			
		}


}
