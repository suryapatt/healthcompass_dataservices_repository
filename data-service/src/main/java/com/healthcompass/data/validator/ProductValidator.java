package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.ProductService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
@Component
public class ProductValidator implements Validator {
	
	
	
	private final MetaTagAttributeValidator metaTagAttributeValidator;
	
	 public ProductValidator(MetaTagAttributeValidator metaTagAttributeValidator) {
	        if (metaTagAttributeValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied metaTagAttributeValidator is required and must not be null.");
	        }
	        if (!metaTagAttributeValidator.supports(MetaTagAttributesViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied metaTagAttributeValidator must support the validation of MetaTagAttributesViewDTO instances.");
	        }
	        this.metaTagAttributeValidator = metaTagAttributeValidator;
	    }
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return ProductViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "marketerId", "","Marketer is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "","Product Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active","","Active flag is required field");
			Boolean  active = Boolean.valueOf((String)errors.getFieldValue("active"));		
			if(!active) {
				return;
			}
					
				
			/*Double gstRate = Double.valueOf((String)errors.getFieldValue("gstRate"));
				if(gstRate < 0){
					errors.rejectValue("gstRate", "", null, "GST Rate should be positive");
					
				}
				Double vatRate = Double.valueOf((String)errors.getFieldValue("vatRate"));
				if(vatRate < 0){
					errors.rejectValue("vatRate", "", null, "Vat Rate should be positive");
					
				}
				Double centralTaxRate = Double.valueOf((String)errors.getFieldValue("centralTaxRate"));
				
				if(centralTaxRate < 0){
					errors.rejectValue("centralTaxRate", "", null, "Central Tax Rate should be positive");
					
				}
				Double salesTaxRate = Double.valueOf((String)errors.getFieldValue("salesTaxRate"));
				
				if(salesTaxRate < 0){
					errors.rejectValue("salesTaxRate", "", null, "Sales Tax Rate should be positive");
					
				}*/
							
			
		
				ProductViewDTO productViewDTO = (ProductViewDTO) obj;
				NestedPropertyValidator.invokeValidatorForNestedProperties(metaTagAttributeValidator, obj, "metaTagAttributes",errors);


}
		
		
		public void customValidate(Object obj, Errors errors1,Map<String,Object> errors) {
			
			
			ArrayList<Map<String,Object>> productsErrors = (ArrayList) errors.get("products");
			
			Map<String,Object> productErrorMap = new HashMap<String,Object>();
			productsErrors.add(productErrorMap);
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, productErrorMap,"marketerId", "","Marketer is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, productErrorMap,"name", "","Product Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, productErrorMap,"active","","Active flag is required field");
			
			Boolean  active = Boolean.valueOf((String)errors1.getFieldValue("active"));		
			if(!active) {
				return;
			}
					
				
			/*Double gstRate = Double.valueOf((String)errors.getFieldValue("gstRate"));
				if(gstRate < 0){
					errors.rejectValue("gstRate", "", null, "GST Rate should be positive");
					
				}
				Double vatRate = Double.valueOf((String)errors.getFieldValue("vatRate"));
				if(vatRate < 0){
					errors.rejectValue("vatRate", "", null, "Vat Rate should be positive");
					
				}
				Double centralTaxRate = Double.valueOf((String)errors.getFieldValue("centralTaxRate"));
				
				if(centralTaxRate < 0){
					errors.rejectValue("centralTaxRate", "", null, "Central Tax Rate should be positive");
					
				}
				Double salesTaxRate = Double.valueOf((String)errors.getFieldValue("salesTaxRate"));
				
				if(salesTaxRate < 0){
					errors.rejectValue("salesTaxRate", "", null, "Sales Tax Rate should be positive");
					
				}*/
							
			productErrorMap.put("metaTagAttributes",new ArrayList<Map<String,Object>>());
			ProductViewDTO productViewDTO = (ProductViewDTO) obj;
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(productViewDTO, "metaTagAttributes");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("metaTagAttributes" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				metaTagAttributeValidator.customValidate(elem, errors1, productErrorMap);
				errors1.popNestedPath();
				counter++;
			}
		  }	
		
		
}

