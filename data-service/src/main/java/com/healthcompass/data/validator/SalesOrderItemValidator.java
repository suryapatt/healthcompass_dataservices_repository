package com.healthcompass.data.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
@Component

public class SalesOrderItemValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SalesOrderItemViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName","","Product Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "inventory.inventoryId",""," Inventtory ID is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "product.productId",""," Product is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity",""," Quantity is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "itemValue",""," Item Value is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate",""," Rate is required field");
			boolean proceedWithAmountTotals=true;
			
			Double quantity = 0.0;
			Double itemValue=0.0;
			Double rate=0.0;
			
			try {
				
				quantity = Double.valueOf((String)errors.getFieldValue("quantity"));
				itemValue = Double.valueOf((String)errors.getFieldValue("itemValue"));
				rate = Double.valueOf((String)errors.getFieldValue("rate"));
				
				
				
						if(quantity < 0){
							errors.rejectValue("quantity", "", null, "Quantity should be positive");
							proceedWithAmountTotals=false;
						}
						if(itemValue < 0){
							errors.rejectValue("itemValue", "", null, "Item Value should be positive");
							proceedWithAmountTotals=false;
						}
						if(rate < 0){
							errors.rejectValue("rate", "", null, "Rate should be positive");
							proceedWithAmountTotals=false;
						}
						
					
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			
			
			
			
			
							
				if(!proceedWithAmountTotals) return;
				
				
				Double calculatedItemValue = rate*quantity;
				if(calculatedItemValue.compareTo(itemValue) != 0) {
					errors.rejectValue("itemValue", "", null, "Item Value is not mathcing rate*qty");
					
				}
				
			}
			
			
		
		public void customValidate(Object obj, Errors errors1,Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> orderItemsErrors = (ArrayList) errors.get("orderItems");
			
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active",""," Active flag is required field");
			Map<String,Object> orderItemErrorMap = new HashMap<String,Object>();
			orderItemsErrors.add(orderItemErrorMap);
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "productName","","Product Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "inventory.inventoryId",""," Inventtory ID is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "product.productId",""," Product is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "quantity",""," Quantity is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "itemValue",""," Item Value is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, orderItemErrorMap,  "rate",""," Rate is required field");
			boolean proceedWithAmountTotals=true;
			
			Double quantity = 0.0;
			Double itemValue=0.0;
			Double rate=0.0;
			
			try {
				
				quantity = Double.valueOf((String)errors1.getFieldValue("quantity"));
				itemValue = Double.valueOf((String)errors1.getFieldValue("itemValue"));
				rate = Double.valueOf((String)errors1.getFieldValue("rate"));
				
				
				
						if(quantity < 0){
							orderItemErrorMap.put("quantity", "Quantity should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						if(itemValue < 0){
							orderItemErrorMap.put("itemValue", "Item Value should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						if(rate < 0){
							orderItemErrorMap.put("rate", "Rate should be positive");
							HealthCompassValidationUtils.markError(orderItemErrorMap);
							proceedWithAmountTotals=false;
						}
						
					
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			
			
			
			
			
							
				if(!proceedWithAmountTotals) return;
				
				
				Double calculatedItemValue = rate*quantity;
				if(calculatedItemValue.compareTo(itemValue) != 0) {
					orderItemErrorMap.put("itemValue", "Item Value is not mathcing rate*qty");
					HealthCompassValidationUtils.markError(orderItemErrorMap);
					
					
				}
				
			}
	


}
