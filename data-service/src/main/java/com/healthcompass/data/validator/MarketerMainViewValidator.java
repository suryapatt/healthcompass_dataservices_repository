package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component

public class MarketerMainViewValidator implements Validator {
	
	private final MarketerValidator marketerValidator;
	
	 public MarketerMainViewValidator(MarketerValidator marketerValidator) {
	        if (marketerValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied marketerValidator is required and must not be null.");
	        }
	        if (!marketerValidator.supports(MarketerViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied marketerValidator must support the validation of ServiceBillViewDTO instances.");
	        }
	        this.marketerValidator = marketerValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return MarketerMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
			
			
			
			MarketerMainViewDTO marketerMainViewDTO = (MarketerMainViewDTO) obj;
			NestedPropertyValidator.invokeNestedValidator(marketerValidator, marketerMainViewDTO,  errors,"marketer");
			
			
			
		}
		
		
		public void customValidate(Object obj,  Map<String,Object> errors,Errors errors1) {
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors,  "organizationId", "","Organization is required field");
				
			
			
			HashMap<String,Object> marketerMap = new HashMap<String,Object>();
			errors.put("marketer",marketerMap);
			MarketerMainViewDTO marketerMainViewDTO = (MarketerMainViewDTO) obj;
			try {
	            errors1.pushNestedPath("marketer");
	            try {
	            	Object object = (Object) PropertyUtils.getProperty(marketerMainViewDTO, "marketer");
	            	marketerValidator.customValidate(object, errors1, errors);
					//ValidationUtils.invokeValidator(validator, object, errors);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        } finally {
	            errors1.popNestedPath();
	        }
			
			
		}
		
		


}
