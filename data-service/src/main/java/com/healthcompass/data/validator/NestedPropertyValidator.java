package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;

public class NestedPropertyValidator {
	
	// For Nested properties that are collections
	public static void invokeValidatorForNestedProperties(Validator validator,
            Object obj,
            String collectionPath,
            Errors errors) {

		Collection collection;
		try {
			collection = (Collection) PropertyUtils.getProperty(obj, collectionPath);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

			throw new RuntimeException(e);
		}

		if (collection == null || collection.isEmpty()) return;
		int counter = 0;
		for (Object elem : collection) {
			errors.pushNestedPath(String.format(collectionPath + "[%d]", counter));
			ValidationUtils.invokeValidator(validator, elem, errors);
			errors.popNestedPath();
			counter++;
		}
	}	
		// For Nested properties 
	
	// For Nested properties 
	public static <TEntity> void invokeNestedValidator(Validator validator, TEntity entity, Errors errors, String subPath) {
		
		
		        try {
		            errors.pushNestedPath(subPath);
		            try {
		            	Object object = (Object) PropertyUtils.getProperty(entity, subPath);
						ValidationUtils.invokeValidator(validator, object, errors);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		            
		        } finally {
		            errors.popNestedPath();
		        }
    }
	
	// For Nested properties 
		public static <TEntity> void invokeCustomNestedValidator(Validator validator, TEntity entity, Errors errors1, Map<String,Object> errors,String subPath) {
			
			errors.put(subPath,new HashMap<String,Object>());
			        try {
			            errors1.pushNestedPath(subPath);
			            try {
			            	Object object = (Object) PropertyUtils.getProperty(entity, subPath);
			            	((GenerateBillValidator) validator).customValidate(object, errors1, errors);
							//ValidationUtils.invokeValidator(validator, object, errors);
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (NoSuchMethodException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            
			        } finally {
			            errors1.popNestedPath();
			        }
	    }
		
		
		public static void invokeCustomValidatorForNestedProperties(Validator validator,
	            Object obj,
	            String collectionPath,
	            Errors errors1,
	            Map<String,Object> errors) {

			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(obj, collectionPath);
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format(collectionPath + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				((ServiceBillItemValidator) validator).customValidate(elem, errors1, errors);
				errors1.popNestedPath();
				counter++;
			}
		}	
			// For Nested properties 


}
