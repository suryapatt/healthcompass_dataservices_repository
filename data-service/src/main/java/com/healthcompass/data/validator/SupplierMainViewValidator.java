package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component

public class SupplierMainViewValidator implements Validator {
	
	private final SupplierValidator supplierValidator;
	
	 public SupplierMainViewValidator(SupplierValidator supplierValidator) {
	        if (supplierValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied supplierValidator is required and must not be null.");
	        }
	        if (!supplierValidator.supports(SupplierViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied supplierValidator must support the validation of ServiceBillViewDTO instances.");
	        }
	        this.supplierValidator = supplierValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SupplierMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
			
			
			
			SupplierMainViewDTO supplierMainViewDTO = (SupplierMainViewDTO) obj;
			NestedPropertyValidator.invokeNestedValidator(supplierValidator, supplierMainViewDTO,  errors,"supplier");
			
			
			
		}
		
		
		public void customValidate(Object obj,  Map<String,Object> errors,Errors errors1) {
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors,  "organizationId", "","Organization is required field");
			
			SupplierMainViewDTO supplierMainViewDTO = (SupplierMainViewDTO) obj;
			
			HashMap<String,Object> supplierMap = new HashMap<String,Object>();
			errors.put("supplier",supplierMap);
			try {
	            errors1.pushNestedPath("supplier");
	            try {
	            	Object object = (Object) PropertyUtils.getProperty(supplierMainViewDTO, "supplier");
	            	supplierValidator.customValidate(object, errors1, errors);
					//ValidationUtils.invokeValidator(validator, object, errors);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        } finally {
	            errors1.popNestedPath();
	        }
			
			
			
			
		}
		


}
