package com.healthcompass.data.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
@Component
@Qualifier("serviceBillItemValidator")
public class ServiceBillItemValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return ServiceBillItemViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active",""," Active flag is required field");
			Boolean  active = Boolean.valueOf((String)errors.getFieldValue("active"));		
			if(!active) {
				return;
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate",""," Rate is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity",""," Quantity is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amount",""," Amount is required field");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netAmount",""," Net Amount is required field");
			boolean proceedWithAmountTotals=true;
			Double rate=0.0;
			Double quantity = 0.0;
			Double amount=0.0;
			try {
				rate = Double.valueOf((String)errors.getFieldValue("rate"));
				quantity = Double.valueOf((String)errors.getFieldValue("quantity"));
				amount = Double.valueOf((String)errors.getFieldValue("amount"));
				
				
						if(rate < 0){
							errors.rejectValue("rate", "", null, "Item Unit Price should be positive");
							proceedWithAmountTotals=false;
						}
						if(quantity < 0){
							errors.rejectValue("quantity", "", null, "Item quantity should be positive");
							proceedWithAmountTotals=false;
						}
						if(amount < 0){
							errors.rejectValue("amount", "", null, "Item amount should be positive");
							proceedWithAmountTotals=false;
						}
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			/*if(Double.valueOf((String)errors.getFieldValue("netAmount")) < 0){
				errors.rejectValue("netAmount", "", null, "Item net amount should be positive");
			}*/
			
			
			
			
			//Double netAmount = Double.valueOf((String)errors.getFieldValue("netAmount"));
			if(proceedWithAmountTotals){
				
				
				Double calculatedItemAmount = rate*quantity;
				if(calculatedItemAmount.compareTo(amount) != 0) {
					errors.rejectValue("amount", "", null, "Item Amount is not matching with rate*quantity");
					proceedWithAmountTotals=false;
				}
				
			}
			
			
		}


		
		public void customValidate(Object obj, Errors errors1,Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> billItemsErrors = (ArrayList) ((Map) errors.get("serviceBill")).get("serviceBillItems");
			
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active",""," Active flag is required field");
			Map<String,Object> itemErrorMap = new HashMap<String,Object>();
			billItemsErrors.add(itemErrorMap);
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, itemErrorMap, "rate",""," Rate is required field");
			Boolean  active = Boolean.valueOf((String)errors1.getFieldValue("active"));		
			if(!active) {
				return;
			}
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, itemErrorMap, "rate",""," Rate is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, itemErrorMap, "quantity",""," Quantity is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, itemErrorMap, "amount",""," Amount is required field");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netAmount",""," Net Amount is required field");
			boolean proceedWithAmountTotals=true;
			Double rate=0.0;
			Double quantity = 0.0;
			Double amount=0.0;
			try {
				rate = Double.valueOf((String)errors1.getFieldValue("rate"));
				quantity = Double.valueOf((String)errors1.getFieldValue("quantity"));
				amount = Double.valueOf((String)errors1.getFieldValue("amount"));
				
				
						if(rate < 0){
							itemErrorMap.put("rate", "Item Unit Price should be positive");
							HealthCompassValidationUtils.markError(itemErrorMap);
							//errors.rejectValue("rate", "", null, "Item Unit Price should be positive");
							proceedWithAmountTotals=false;
						}
						if(quantity < 0){
							itemErrorMap.put("quantity", "Item quantity should be positive");
							HealthCompassValidationUtils.markError(itemErrorMap);
							//errors.rejectValue("quantity", "", null, "Item quantity should be positive");
							proceedWithAmountTotals=false;
						}
						if(amount < 0){
							itemErrorMap.put("amount", "Item amount should be positive");
							HealthCompassValidationUtils.markError(itemErrorMap);
							//errors.rejectValue("amount", "", null, "Item amount should be positive");
							proceedWithAmountTotals=false;
						}
			}catch(Exception e) {
				proceedWithAmountTotals = false;
			}
			
			/*if(Double.valueOf((String)errors.getFieldValue("netAmount")) < 0){
				errors.rejectValue("netAmount", "", null, "Item net amount should be positive");
			}*/
			
			
			
			
			//Double netAmount = Double.valueOf((String)errors.getFieldValue("netAmount"));
			if(proceedWithAmountTotals){
				
				
				Double calculatedItemAmount = rate*quantity;
				if(calculatedItemAmount.compareTo(amount) != 0) {
					itemErrorMap.put("amount", "Item Amount is not matching with rate*quantity");
					HealthCompassValidationUtils.markError(itemErrorMap);
					//errors.rejectValue("amount", "", null, "Item Amount is not matching with rate*quantity");
					proceedWithAmountTotals=false;
				}
				
			}
			
			
		}
		
		
}
