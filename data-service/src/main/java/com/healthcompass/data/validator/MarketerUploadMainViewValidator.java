package com.healthcompass.data.validator;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component

public class MarketerUploadMainViewValidator implements Validator {
	
	private final MarketerValidator marketerValidator;
	
	 public MarketerUploadMainViewValidator(MarketerValidator marketerValidator) {
	        if (marketerValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied marketerValidator is required and must not be null.");
	        }
	        if (!marketerValidator.supports(MarketerViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied marketerValidator must support the validation of ServiceBillViewDTO instances.");
	        }
	        this.marketerValidator = marketerValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return MarketerUploadMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
					
			MarketerUploadMainViewDTO marketerUploadMainViewDTO = (MarketerUploadMainViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(marketerValidator, marketerUploadMainViewDTO, "marketers", errors);
			
			
			
		}
		
		


}
