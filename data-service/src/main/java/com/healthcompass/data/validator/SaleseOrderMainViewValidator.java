package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.PurchaseOrderService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.SalesOrderMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component

public class SaleseOrderMainViewValidator implements Validator {
	
	private final SalesOrderValidator salesOrderValidator;
	
	
	
	 public SaleseOrderMainViewValidator(SalesOrderValidator salesOrderValidator) {
	        if (salesOrderValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied salesOrderValidator is required and must not be null.");
	        }
	        if (!salesOrderValidator.supports(SalesOrderViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied salesOrderValidator must support the validation of PurchaseOrderViewDTO instances.");
	        }
	        this.salesOrderValidator = salesOrderValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SalesOrderMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "locationId", "","Location is required field");
			
			SalesOrderMainViewDTO salesOrderBasicCheck = (SalesOrderMainViewDTO) obj;
			if(salesOrderBasicCheck.getOrder() == null || salesOrderBasicCheck.getOrder().getOrderValue() ==  null) {
				errors.rejectValue("Order", "", null, "Product Details are required");
				return;
			}
			
						
			SalesOrderMainViewDTO salesOrderMainView = (SalesOrderMainViewDTO) obj;
			NestedPropertyValidator.invokeNestedValidator(salesOrderValidator, salesOrderMainView,  errors,"order");
			
			
			
		}
		
		
		
		public void customValidate(Object obj, Map<String,Object> errors,Errors errors1) {
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "organizationId", "","Organization is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "locationId", "","Location is required field");
			
			HashMap<String,Object> orderMap = new HashMap<String,Object>();
			
			SalesOrderMainViewDTO salesOrderBasicCheck = (SalesOrderMainViewDTO) obj;
			if(salesOrderBasicCheck.getOrder() == null || salesOrderBasicCheck.getOrder().getOrderValue() ==  null) {
				
				HealthCompassValidationUtils.markError(errors);
				orderMap.put("order","Product Details are required");
				errors.put("order",orderMap);
								
				return;
			}
			SalesOrderMainViewDTO salesOrderMainView = (SalesOrderMainViewDTO) obj;
			errors.put("order",orderMap);
			
			try {
	            errors1.pushNestedPath("order");
	            
	            try {
	            	Object object = (Object) PropertyUtils.getProperty(salesOrderMainView, "order");
	            	salesOrderValidator.customValidate(object, errors1, errors);
					//ValidationUtils.invokeValidator(validator, object, errors);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        } finally {
	            errors1.popNestedPath();
	        }
			
			
			
			
			
		}



}
