package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component
public class ProductListMainViewValidator implements Validator {
	
	private final ProductValidator productValidator;
	
	
	 public ProductListMainViewValidator(ProductValidator productValidator) {
	        if (productValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied productValidator is required and must not be null.");
	        }
	        if (!productValidator.supports(ProductViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied productValidator must support the validation of ProductViewDTO instances.");
	        }
	        this.productValidator = productValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return ProductListMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
			
			
			
			
			ProductListMainViewDTO productListMainViewDTO = (ProductListMainViewDTO) obj;
			if(productListMainViewDTO.getProducts() == null || productListMainViewDTO.getProducts().isEmpty()) {
				
				errors.rejectValue("products", "", null, "Products list is required");
				return;
			}
			
			NestedPropertyValidator.invokeValidatorForNestedProperties(productValidator, productListMainViewDTO, "products", errors);
			
			
			
		}


	
		public void customValidate(Object obj,Map<String,Object> errors,Errors errors1) {
			
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "organizationId", "","Organization is required field");
			
			
			
			ProductListMainViewDTO productListMainViewDTO = (ProductListMainViewDTO) obj;
			if(productListMainViewDTO.getProducts() == null || productListMainViewDTO.getProducts().isEmpty()) {
				errors.put("products","Products list is required");
				HealthCompassValidationUtils.markError(errors);
				
				return;
			}
			errors.put("products",new ArrayList<Map<String,Object>>());
			
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(productListMainViewDTO, "products");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("products" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				productValidator.customValidate(elem, errors1, errors);
				errors1.popNestedPath();
				counter++;
			}
		  }	
		
			
			
			
			
		}

