package com.healthcompass.data.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
@Component

public class MetaTagAttributeValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return MetaTagAttributesViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active",""," Active flag is required field");
			Boolean  active = Boolean.valueOf((String)errors.getFieldValue("active"));		
			if(!active) {
				return;
			}
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "metaTag.metaTagId",""," Meta Tag Name is required field");
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "attrubuteValue",""," Value is required field");
					
			
			
			
		
			
		}

		
		public void customValidate(Object obj,Errors errors1,Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> metaTagAttributesErrors = (ArrayList) errors.get("metaTagAttributes");
			
			Map<String,Object> metaTagErrorMap = new HashMap<String,Object>();
			metaTagAttributesErrors.add(metaTagErrorMap);
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, metaTagErrorMap,"active","","Active flag is required field");
			
			Boolean  active = Boolean.valueOf((String)errors1.getFieldValue("active"));		
			if(!active) {
				return;
			}
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, metaTagErrorMap,"metaTag.metaTagId","","Meta Tag Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, metaTagErrorMap,"attrubuteValue","","Value is required field");
			
				
			
		}

}
