package com.healthcompass.data.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
@Component
@Qualifier("serviceBillValidator")
public class ServiceBillValidator implements Validator {
	
	@Autowired
	BillService  billService;
	private final ServiceBillItemValidator serviceBillItemValidator;
	
	 public ServiceBillValidator(ServiceBillItemValidator serviceBillItemValidator) {
	        if (serviceBillItemValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied serviceBillValidator is required and must not be null.");
	        }
	        if (!serviceBillItemValidator.supports(ServiceBillItemViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied serviceBillItemValidator must support the validation of ServiceBillItemViewDTO instances.");
	        }
	        this.serviceBillItemValidator = serviceBillItemValidator;
	    }
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return ServiceBillViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active","","Active flag is required field");
			Boolean  active = Boolean.valueOf((String)errors.getFieldValue("active"));		
			if(!active) {
				return;
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountBilled","","Amount Billed is required field");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountPaid","","Amount Paid is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subTotal","","Sub Total is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discount","","Discount is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discountRate","","Discount rate is required field");
			boolean proceedWithCalculations = true;
			Double amountBilled=0.0;
			Double amountPaid = 0.0;
			Double amountDue=0.0;
			Double subTotal=0.0;
			try {
				amountBilled = Double.valueOf((String)errors.getFieldValue("amountBilled"));
				try {
					amountPaid = Double.valueOf((String)errors.getFieldValue("amountPaid"));
				}catch(Exception e) {
					amountPaid = 0.0;
				}
				try {
					amountDue = Double.valueOf((String)errors.getFieldValue("amountDue"));
				}catch(Exception e) {
					amountDue = 0.0;
				}
				
				
				subTotal = Double.valueOf((String)errors.getFieldValue("subTotal"));
				
				if(subTotal < 0){
					errors.rejectValue("subTotal", "", null, "Sub Total should be positive");
					proceedWithCalculations=false;
				}
				
				if(amountBilled < 0){
					errors.rejectValue("amountBilled", "", null, "Amount Billed should be positive");
					proceedWithCalculations=false;
				}
				
				if(amountPaid <0){
					errors.rejectValue("amountPaid", "", null, "Amount Paid should be positive");
					proceedWithCalculations=false;
				}
				
				 						
				if( amountDue < 0){
					errors.rejectValue("amountDue", "", null, "Amount Due should be positive");
					proceedWithCalculations=false;
				}
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			Double discountRate = 0.0;
			Double discount = 0.0;
			
			boolean proceedWithDiscountCalculation=true;
			try {
				discountRate = Double.valueOf((String)errors.getFieldValue("discountRate"));
				if(discountRate < 0){
					proceedWithDiscountCalculation=false;
					errors.rejectValue("discountRate", "", null, "Discount Rate should be positive");
				}	
				discount = Double.valueOf((String)errors.getFieldValue("discount"));
				if(discount < 0){
					proceedWithDiscountCalculation=false;
					errors.rejectValue("discount", "", null, "Discount should be positive");
				}
			}catch(Exception e) {
				proceedWithDiscountCalculation=false;
			}
			
			if(proceedWithCalculations) {
				if(subTotal.compareTo(amountBilled) == -1) {
					errors.rejectValue("subTotal", "", null, "Sub Total cannot be less than than aamount billed");
					proceedWithCalculations=false;
				}
			
			}			
			
			if(proceedWithCalculations) {
				if(amountBilled.compareTo(amountPaid) == -1) {
					errors.rejectValue("amountPaid", "", null, "Amount Paid cannot be more than aamount billed");
					proceedWithCalculations=false;
				}
			
			}
			if(!proceedWithCalculations) return;
			
			ServiceBillViewDTO serviceBill = (ServiceBillViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(serviceBillItemValidator, serviceBill, "serviceBillItems", errors);
			
			Double totalCalculatedAmount = 0.0;
			try {
				for(ServiceBillItemViewDTO billItem :serviceBill.getServiceBillItems() ) {
					if(!billItem.getActive()) continue;
					Double itemAmount = billItem.getAmount();
					totalCalculatedAmount+=itemAmount;
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(subTotal.compareTo(totalCalculatedAmount) != 0) {
				errors.rejectValue("subTotal", "", null, "SubTotal is not matching with individual items total");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			Double calculatedDiscountAmount = 0.0;
			Double finalBillAmount = 0.0;
			if(proceedWithDiscountCalculation) { 
				calculatedDiscountAmount = totalCalculatedAmount*(discountRate/100);
				calculatedDiscountAmount=Math.floor(calculatedDiscountAmount*100) / 100;
				if(calculatedDiscountAmount.compareTo(discount) != 0 ) {
					errors.rejectValue("discount", "", null, "Discount amount total is not properlyy calculated");
					proceedWithCalculations=false;
				}
				if(!proceedWithCalculations) return;
				finalBillAmount = subTotal-discount;
			}else {
				finalBillAmount = subTotal;
			}
						
			
			
			if(amountBilled.compareTo(finalBillAmount) != 0) {
				errors.rejectValue("amountBilled", "", null, "Amount Billed is not matching up with the individual item totals and discounts");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			Double calculatedAmountDue= amountBilled-amountPaid;
			calculatedAmountDue=Math.floor(calculatedAmountDue*100) / 100;
			if(calculatedAmountDue.compareTo(amountDue) != 0) {
				errors.rejectValue("amountDue", "", null, "Amount Due is not matching up with Amount Billed - Amount Paid");
				proceedWithCalculations=false;
			}
			
		}
		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
		  
			Map<String,Object> serviceBillMap = (Map) errors.get("serviceBill");
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,serviceBillMap, "active","","Active flag is required field");
			Boolean  active = Boolean.valueOf((String)errors1.getFieldValue("active"));		
			if(!active) {
				return;
			}
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,serviceBillMap, "amountBilled","","Amount Billed is required field");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountPaid","","Amount Paid is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,serviceBillMap, "subTotal","","Sub Total is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,serviceBillMap, "discount","","Discount is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,serviceBillMap, "discountRate","","Discount rate is required field");
			boolean proceedWithCalculations = true;
			Double amountBilled=0.0;
			Double amountPaid = 0.0;
			Double amountDue=0.0;
			Double subTotal=0.0;
			try {
				amountBilled = Double.valueOf((String)errors1.getFieldValue("amountBilled"));
				try {
					amountPaid = Double.valueOf((String)errors1.getFieldValue("amountPaid"));
				}catch(Exception e) {
					amountPaid = 0.0;
				}
				try {
					amountDue = Double.valueOf((String)errors1.getFieldValue("amountDue"));
				}catch(Exception e) {
					amountDue = 0.0;
				}
				
				
				subTotal = Double.valueOf((String)errors1.getFieldValue("subTotal"));
				
				if(subTotal < 0){
					//errors.rejectValue("subTotal", "", null, "Sub Total should be positive");
					serviceBillMap.put("subTotal","Sub Total should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
				
				if(amountBilled < 0){
					//errors.rejectValue("amountBilled", "", null, "Amount Billed should be positive");
					serviceBillMap.put("amountBilled","Amount Billed should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
				
				if(amountPaid <0){
					//errors.rejectValue("amountPaid", "", null, "Amount Paid should be positive");
					serviceBillMap.put("amountPaid","Amount Paid should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
				
				 						
				if( amountDue < 0){
					//errors.rejectValue("amountDue", "", null, "Amount Due should be positive");
					serviceBillMap.put("amountDue","Amount Due should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			Double discountRate = 0.0;
			Double discount = 0.0;
			
			boolean proceedWithDiscountCalculation=true;
			try {
				discountRate = Double.valueOf((String)errors1.getFieldValue("discountRate"));
				if(discountRate < 0){
					proceedWithDiscountCalculation=false;
					//errors.rejectValue("discountRate", "", null, "Discount Rate should be positive");
					serviceBillMap.put("discountRate","Discount Rate should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
				}	
				discount = Double.valueOf((String)errors1.getFieldValue("discount"));
				if(discount < 0){
					proceedWithDiscountCalculation=false;
					serviceBillMap.put("discount","Discount should be positive");
					HealthCompassValidationUtils.markError(serviceBillMap);
					//errors.rejectValue("discount", "", null, "Discount should be positive");
				}
			}catch(Exception e) {
				proceedWithDiscountCalculation=false;
			}
			
			if(proceedWithCalculations) {
				if(subTotal.compareTo(amountBilled) == -1) {
					//errors.rejectValue("subTotal", "", null, "Sub Total cannot be less than than aamount billed");
					serviceBillMap.put("subTotal","Sub Total cannot be less than than aamount billed");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
			
			}			
			
			if(proceedWithCalculations) {
				if(amountBilled.compareTo(amountPaid) == -1) {
					//errors.rejectValue("amountPaid", "", null, "Amount Paid cannot be more than aamount billed");
					serviceBillMap.put("amountPaid","Amount Paid cannot be more than aamount billed");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
			
			}
			if(!proceedWithCalculations) return;
			
			ServiceBillViewDTO serviceBill = (ServiceBillViewDTO) obj;
			serviceBillMap.put("serviceBillItems",new ArrayList<Map<String,Object>>());
			//NestedPropertyValidator.invokeValidatorForNestedProperties(serviceBillItemValidator, serviceBill, "serviceBillItems", errors);
			NestedPropertyValidator.invokeCustomValidatorForNestedProperties(serviceBillItemValidator, serviceBill, "serviceBillItems", errors1,errors);
			
			Double totalCalculatedAmount = 0.0;
			try {
				for(ServiceBillItemViewDTO billItem :serviceBill.getServiceBillItems() ) {
					if(!billItem.getActive()) continue;
					Double itemAmount = billItem.getAmount();
					totalCalculatedAmount+=itemAmount;
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(subTotal.compareTo(totalCalculatedAmount) != 0) {
				serviceBillMap.put("subTotal","SubTotal is not matching with individual items total");
				HealthCompassValidationUtils.markError(serviceBillMap);
				//errors.rejectValue("subTotal", "", null, "SubTotal is not matching with individual items total");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			Double calculatedDiscountAmount = 0.0;
			Double finalBillAmount = 0.0;
			if(proceedWithDiscountCalculation) { 
				calculatedDiscountAmount = totalCalculatedAmount*(discountRate/100);
				calculatedDiscountAmount=Math.floor(calculatedDiscountAmount*100) / 100;
				if(calculatedDiscountAmount.compareTo(discount) != 0 ) {
					//errors.rejectValue("discount", "", null, "Discount amount total is not properlyy calculated");
					serviceBillMap.put("discount","Discount amount total is not properlyy calculated");
					HealthCompassValidationUtils.markError(serviceBillMap);
					proceedWithCalculations=false;
				}
				if(!proceedWithCalculations) return;
				finalBillAmount = subTotal-discount;
			}else {
				finalBillAmount = subTotal;
			}
						
			
			
			if(amountBilled.compareTo(finalBillAmount) != 0) {
				serviceBillMap.put("amountBilled","Amount Billed is not matching up with the individual item totals and discounts");;
				HealthCompassValidationUtils.markError(serviceBillMap);
				//errors.rejectValue("amountBilled", "", null, "Amount Billed is not matching up with the individual item totals and discounts");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			Double calculatedAmountDue= amountBilled-amountPaid;
			calculatedAmountDue=Math.floor(calculatedAmountDue*100) / 100;
			if(calculatedAmountDue.compareTo(amountDue) != 0) {
				serviceBillMap.put("amountDue","Amount Due is not matching up with the individual item totals and discounts");;
				HealthCompassValidationUtils.markError(serviceBillMap);
				//errors.rejectValue("amountDue", "", null, "Amount Due is not matching up with Amount Billed - Amount Paid");
				proceedWithCalculations=false;
			}
		}


}
