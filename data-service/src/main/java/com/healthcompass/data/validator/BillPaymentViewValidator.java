package com.healthcompass.data.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
@Component
@Qualifier("billPaymentViewValidator")
public class BillPaymentViewValidator implements Validator {
	
	
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return CreateNewBillPaymentViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billNumber","","Bill Number is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountBilled","","Amount Billed is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentAmount","","Payment Amount is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountDue","","Amount Due is required field");
			
			boolean proceedWithCalculations = true;
			Double amountBilled=0.0;
			Double paymentAmount = 0.0;
			Double amountDue=0.0;
			try {
				amountBilled = Double.valueOf((String)errors.getFieldValue("amountBilled"));
				paymentAmount = Double.valueOf((String)errors.getFieldValue("paymentAmount"));
				amountDue = Double.valueOf((String)errors.getFieldValue("amountDue"));
				
				if(amountBilled < 0){
					errors.rejectValue("amountBilled", "", null, "Amount Billed should be positive");
					proceedWithCalculations=false;
				}
				
				if(paymentAmount <0){
					errors.rejectValue("paymentAmount", "", null, "Payment Amount should be positive");
					proceedWithCalculations=false;
				}
				
				 						
				if( amountDue < 0){
					errors.rejectValue("amountDue", "", null, "Amount Due should be positive");
					proceedWithCalculations=false;
				}
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			   if(amountBilled.compareTo(paymentAmount) == -1) {
					errors.rejectValue("paymentAmount", "", null, "Payment Amount cannot be more than Amount billed");
					
				}
			   if(amountDue.compareTo(paymentAmount) == -1) {
					errors.rejectValue("paymentAmount", "", null, "Payment Amount cannot be more than Amount Due");
					
				}
			
			
			
		}

		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> billPaymentErrors = (ArrayList) errors.get("billPayments");
			
			Map<String,Object> billPaymentErrorMap = new HashMap<String,Object>();
			billPaymentErrors.add(billPaymentErrorMap);
			
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,billPaymentErrorMap, "billNumber","","Bill Number is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,billPaymentErrorMap, "amountBilled","","Amount Billed is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,billPaymentErrorMap, "paymentAmount","","Payment Amount is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,billPaymentErrorMap, "amountDue","","Amount Due is required field");
			
			boolean proceedWithCalculations = true;
			Double amountBilled=0.0;
			Double paymentAmount = 0.0;
			Double amountDue=0.0;
			
			try {
				amountBilled = Double.valueOf((String)errors1.getFieldValue("amountBilled"));
				paymentAmount = Double.valueOf((String)errors1.getFieldValue("paymentAmount"));
				amountDue = Double.valueOf((String)errors1.getFieldValue("amountDue"));
				
				if(amountBilled < 0){
					//errors.rejectValue("amountBilled", "", null, "Amount Billed should be positive");
					billPaymentErrorMap.put("amountBilled","Amount Billed should be positive");
					HealthCompassValidationUtils.markError(errors);
					proceedWithCalculations=false;
				}
				
				if(paymentAmount <0){
					//errors.rejectValue("paymentAmount", "", null, "Payment Amount should be positive");
					billPaymentErrorMap.put("paymentAmount","Payment Amount should be positive");
					HealthCompassValidationUtils.markError(errors);
					proceedWithCalculations=false;
				}
				
				 						
				if( amountDue < 0){
					//errors.rejectValue("amountDue", "", null, "Amount Due should be positive");
					billPaymentErrorMap.put("amountDue","Amount Due should be positive");
					HealthCompassValidationUtils.markError(errors);
					proceedWithCalculations=false;
				}
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			   if(amountBilled.compareTo(paymentAmount) == -1) {
					//errors.rejectValue("paymentAmount", "", null, "Payment Amount cannot be more than Amount billed");
					
				}
			   if(amountDue.compareTo(paymentAmount) == -1) {
					//errors.rejectValue("paymentAmount", "", null, "Payment Amount cannot be more than Amount Due");
				   billPaymentErrorMap.put("paymentAmount","Payment Amount cannot be more than Amount Due");
				   HealthCompassValidationUtils.markError(errors);
					
				}
			
			
			
		}

}
