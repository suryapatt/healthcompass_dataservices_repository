package com.healthcompass.data.validator;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component

public class SupplierUploadMainViewValidator implements Validator {
	
	private final SupplierValidator supplierValidator;
	
	 public SupplierUploadMainViewValidator(SupplierValidator supplierValidator) {
	        if (supplierValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied supplierValidator is required and must not be null.");
	        }
	        if (!supplierValidator.supports(SupplierViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied supplierValidator must support the validation of ServiceBillViewDTO instances.");
	        }
	        this.supplierValidator = supplierValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SupplierUploadMainViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizationId", "","Organization is required field");
					
			SupplierUploadMainViewDTO supplierUploadMainViewDTO = (SupplierUploadMainViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(supplierValidator, supplierUploadMainViewDTO, "suppliers", errors);
			
			
			
		}
		
		


}
