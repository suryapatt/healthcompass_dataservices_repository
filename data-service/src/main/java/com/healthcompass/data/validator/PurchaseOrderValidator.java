package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
@Component
public class PurchaseOrderValidator implements Validator {
	
	
	private final PurchaseOrderItemValidator purchaseOrderItemValidator;
	
	 public PurchaseOrderValidator(PurchaseOrderItemValidator purchaseOrderItemValidator) {
	        if (purchaseOrderItemValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied purchaseOrderItemValidator is required and must not be null.");
	        }
	        if (!purchaseOrderItemValidator.supports(PurchaseOrderItemViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied purchaseOrderItemValidator must support the validation of PurchaseOrderItemViewDTO instances.");
	        }
	        this.purchaseOrderItemValidator = purchaseOrderItemValidator;
	    }
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return PurchaseOrderViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			
			
						
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orderValue","","Order Value is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discount","","Discount is required field");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "netOrderValue","","Net Order Value is required field");
			
			boolean proceedWithCalculations = true;
			Double orderValue=0.0;
			Double discount = 0.0;
			Double netOrderValue=0.0;
			
			try {
				orderValue = Double.valueOf((String)errors.getFieldValue("orderValue"));
				discount = Double.valueOf((String)errors.getFieldValue("discount"));
				netOrderValue = Double.valueOf((String)errors.getFieldValue("netOrderValue"));
				
				
				if(orderValue < 0){
					errors.rejectValue("orderValue", "", null, "Order Value should be positive");
					proceedWithCalculations=false;
				}
				
				if(discount < 0){
					errors.rejectValue("discount", "", null, "Discount should be greater than or zero");
					proceedWithCalculations=false;
				}
				
				if(netOrderValue <0){
					errors.rejectValue("netOrderValue", "", null, "Net Order Value should be positive");
					proceedWithCalculations=false;
				}
				
				 						
				
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			
			
			
			
			
			if(proceedWithCalculations) {
				if(orderValue.compareTo(netOrderValue) == -1) {
					errors.rejectValue("orderValue", "", null, "Order Value cannot be less than than net order value");
					proceedWithCalculations=false;
				}
			
			}			
			
			if(proceedWithCalculations) {
				if(discount.compareTo(orderValue) > 0) {
					errors.rejectValue("discount", "", null, "Discount Amount cannot be more than order value");
					proceedWithCalculations=false;
				}
			
			}
			
			if(!proceedWithCalculations) return;
			
			if(netOrderValue.compareTo((orderValue-discount)) != 0) {
				errors.rejectValue("netOrderValue", "", null, "Net Order value is not matching OrderValue - Discount");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			PurchaseOrderViewDTO purchaseOrder = (PurchaseOrderViewDTO) obj;
			NestedPropertyValidator.invokeValidatorForNestedProperties(purchaseOrderItemValidator, purchaseOrder, "orderItems", errors);
			
			Double totalCalculatedOrderValueAmount = 0.0;
			Double totalCalculatedDiscountAmount = 0.0;
			Set<String> uniqueProductBatch = new HashSet<String>();
			
			try {
				for(PurchaseOrderItemViewDTO orderItem : purchaseOrder.getOrderItems()) {
					
					totalCalculatedOrderValueAmount+=orderItem.getMrp();
					totalCalculatedDiscountAmount+=orderItem.getDiscount();
					String productBatchString = orderItem.getProduct().getProductId()+"-"+orderItem.getBatchNumber();
					if(uniqueProductBatch.contains(productBatchString)) {
						errors.rejectValue("orderItems", "", null, "Duplicate Product-Batch combination in the same Order: Product:"+orderItem.getProduct().getProductId()
								+" Batch:"+orderItem.getBatchNumber()+"");
						return;
					}else {
						uniqueProductBatch.add(productBatchString);
					}
					
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(orderValue.compareTo(totalCalculatedOrderValueAmount) != 0) {
				errors.rejectValue("orderValue", "", null, "Order Value not matching with individual item mrps");
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			if(discount.compareTo(totalCalculatedDiscountAmount) != 0) {
				errors.rejectValue("discount", "", null, "Discount not matching with individual item discounts");
				
			}
			
						
		}

		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			
			Map<String,Object> orderMap = (Map) errors.get("order");
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,orderMap, "discount","","Discount is required field");			
			
			
			boolean proceedWithCalculations = true;
			Double orderValue=0.0;
			Double discount = 0.0;
			Double netOrderValue=0.0;
			
			try {
				orderValue = Double.valueOf((String)errors1.getFieldValue("orderValue"));
				discount = Double.valueOf((String)errors1.getFieldValue("discount"));
				netOrderValue = Double.valueOf((String)errors1.getFieldValue("netOrderValue"));
				
				
				if(orderValue < 0){
					orderMap.put("orderValue","Order Value should be positive");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
				
				if(discount < 0){
					orderMap.put("discount","Discount should be greater than or zero");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
				
				if(netOrderValue <0){
					orderMap.put("netOrderValue","Net Order Value should be positive");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
				
				 						
				
			}catch(Exception e) {
				 proceedWithCalculations = false;
			}
			if(!proceedWithCalculations) return;
			
			
			
			
			
			
			if(proceedWithCalculations) {
				if(orderValue.compareTo(netOrderValue) == -1) {
					orderMap.put("orderValue","Order Value cannot be less than than net order value");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
			
			}			
			
			if(proceedWithCalculations) {
				if(discount.compareTo(orderValue) > 0) {
					orderMap.put("discount","Discount Amount cannot be more than order value");
					HealthCompassValidationUtils.markError(orderMap);
					proceedWithCalculations=false;
				}
			
			}
			
			if(!proceedWithCalculations) return;
			
			if(netOrderValue.compareTo((orderValue-discount)) != 0) {
				
				orderMap.put("netOrderValue","Net Order value is not matching OrderValue - Discount");
				HealthCompassValidationUtils.markError(orderMap);
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			PurchaseOrderViewDTO purchaseOrder = (PurchaseOrderViewDTO) obj;
			orderMap.put("orderItems",new ArrayList<Map<String,Object>>());
			
			Collection collection;
			try {
				collection = (Collection) PropertyUtils.getProperty(obj, "orderItems");
			} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

				throw new RuntimeException(e);
			}

			if (collection == null || collection.isEmpty()) return;
			int counter = 0;
			for (Object elem : collection) {
				errors1.pushNestedPath(String.format("orderItems" + "[%d]", counter));
				//ValidationUtils.invokeValidator(validator, elem, errors);
				purchaseOrderItemValidator.customValidate(elem, errors1, orderMap);
				errors1.popNestedPath();
				counter++;
			}
			
			// For Nested properties
		
			
			
			
			Double totalCalculatedOrderValueAmount = 0.0;
			Double totalCalculatedDiscountAmount = 0.0;
			Set<String> uniqueProductBatch = new HashSet<String>();
			
			try {
				for(PurchaseOrderItemViewDTO orderItem : purchaseOrder.getOrderItems()) {
					
					totalCalculatedOrderValueAmount+=orderItem.getMrp();
					totalCalculatedDiscountAmount+=orderItem.getDiscount();
					String productBatchString = orderItem.getProduct().getProductId()+"-"+orderItem.getBatchNumber();
					if(uniqueProductBatch.contains(productBatchString)) {
						
						orderMap.put("orderItems","Duplicate Product-Batch combination in the same Order: Product:"+orderItem.getProduct().getProductId()+" Batch:"+orderItem.getBatchNumber()+"");
						HealthCompassValidationUtils.markError(orderMap);
						
						
						return;
					}else {
						uniqueProductBatch.add(productBatchString);
					}
					
				}
				
			}catch(Exception e) {
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			if(orderValue.compareTo(totalCalculatedOrderValueAmount) != 0) {
				orderMap.put("orderValue","Order Value not matching with individual item mrps");
				HealthCompassValidationUtils.markError(orderMap);
				proceedWithCalculations=false;
			}
			if(!proceedWithCalculations) return;
			
			if(discount.compareTo(totalCalculatedDiscountAmount) != 0) {
				orderMap.put("discount","Discount not matching with individual item discounts");
				HealthCompassValidationUtils.markError(orderMap);
				
				
			}
			
						
		}

       
}
