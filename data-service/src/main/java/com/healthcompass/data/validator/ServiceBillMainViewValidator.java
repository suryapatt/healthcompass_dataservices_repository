package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component
@Qualifier("serviceBillMainViewValidator")
public class ServiceBillMainViewValidator implements Validator {
	
	private final ServiceBillValidator serviceBillValidator;
	
	@Autowired
	BillService billService;
	 public ServiceBillMainViewValidator(ServiceBillValidator serviceBillValidator) {
	        if (serviceBillValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied serviceBillValidator is required and must not be null.");
	        }
	        if (!serviceBillValidator.supports(ServiceBillViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied serviceBillValidator must support the validation of ServiceBillViewDTO instances.");
	        }
	        this.serviceBillValidator = serviceBillValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return ServiceBillViewMainDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "encounterId", "","Encounter is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "patientId", "","Patient is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "practitionerId", "","Practitioner is required field");
			
			UUID patientId = UUID.fromString((String)errors.getFieldValue("patientId"));
			UUID encounterId = UUID.fromString((String)errors.getFieldValue("encounterId"));
			UUID practitionerId = UUID.fromString((String)errors.getFieldValue("practitionerId"));
			boolean isBillExists = billService.isBillExistsForPatientEncounterAndPractitioner(patientId, encounterId, practitionerId);
			
			if(isBillExists) { // bill is created  for same unique combination
				errors.rejectValue("serviceBill", "", null, "Bill Already exists for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
				return;
				
			}
				/*try { // check if it is update case
					UUID serviceBillId = UUID.fromString((String) errors.getFieldValue("serviceBill.serviceBillId"));
					if(serviceBillId != null && !isBillExists) {
						errors.rejectValue("serviceBill", "", null, "Bill does not exist for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
						return;
					}
				}catch(NullPointerException e) {
					// implies serviceBillId is null and new service bill is being created 
					
					
					if(isBillExists) { // bill is created  for same unique combination
						errors.rejectValue("serviceBill", "", null, "Bill Already exists for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
						return;
					}
					
				}*/
				
				
				ServiceBillViewMainDTO serviceBillBasicCheck = (ServiceBillViewMainDTO) obj;
				if(serviceBillBasicCheck.getServiceBill() == null || serviceBillBasicCheck.getServiceBill().getServiceBillItems() == null 
						|| serviceBillBasicCheck.getServiceBill().getServiceBillItems().isEmpty()) {
				
					errors.rejectValue("serviceBill", "", null, "Bill Item Details are required");
					return;
				}
			
			
			ServiceBillViewMainDTO serviceBillMainView = (ServiceBillViewMainDTO) obj;
			NestedPropertyValidator.invokeNestedValidator(serviceBillValidator, serviceBillMainView,  errors,"serviceBill");
			
			
			
		}
		
		
		public void customValidate(Object obj, Map<String,Object> errors,Errors errors1) {
			
			
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "encounterId", "","Encounter is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "patientId", "","Patient is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,errors, "practitionerId", "","Practitioner is required field");
			
			UUID patientId = UUID.fromString((String)errors1.getFieldValue("patientId"));
			UUID encounterId = UUID.fromString((String)errors1.getFieldValue("encounterId"));
			UUID practitionerId = UUID.fromString((String)errors1.getFieldValue("practitionerId"));
			boolean isBillExists = billService.isBillExistsForPatientEncounterAndPractitioner(patientId, encounterId, practitionerId);
			
			HashMap<String,Object> serviceBillMap = new HashMap<String,Object>();
			
			if(isBillExists) { // bill is created  for same unique combination
				//errors.rejectValue("serviceBill", "", null, "Bill Already exists for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
				//return;
				
				HealthCompassValidationUtils.markError(errors);
				serviceBillMap.put("serviceBill","Bill Already exists for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
				errors.put("serviceBill",serviceBillMap);
				return;
			}
				/*try { // check if it is update case
					UUID serviceBillId = UUID.fromString((String) errors.getFieldValue("serviceBill.serviceBillId"));
					if(serviceBillId != null && !isBillExists) {
						errors.rejectValue("serviceBill", "", null, "Bill does not exist for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
						return;
					}
				}catch(NullPointerException e) {
					// implies serviceBillId is null and new service bill is being created 
					
					
					if(isBillExists) { // bill is created  for same unique combination
						errors.rejectValue("serviceBill", "", null, "Bill Already exists for this combination of Patient:"+patientId+" Encounter:"+encounterId+" Practitioner Role:"+practitionerId+"");
						return;
					}
					
				}*/
				
				
				ServiceBillViewMainDTO serviceBillBasicCheck = (ServiceBillViewMainDTO) obj;
				if(serviceBillBasicCheck.getServiceBill() == null || serviceBillBasicCheck.getServiceBill().getServiceBillItems() == null 
						|| serviceBillBasicCheck.getServiceBill().getServiceBillItems().isEmpty()) {
				
					//errors.rejectValue("serviceBill", "", null, "Bill Item Details are required");
					//return;
					
					HealthCompassValidationUtils.markError(errors);
					serviceBillMap.put("serviceBill","Bill Item Details are required");
					errors.put("serviceBill",serviceBillMap);
					return;
				}
			
			
			ServiceBillViewMainDTO serviceBillMainView = (ServiceBillViewMainDTO) obj;
			
			errors.put("serviceBill",serviceBillMap);
	        try {
	            errors1.pushNestedPath("serviceBill");
	            
	            try {
	            	Object object = (Object) PropertyUtils.getProperty(serviceBillMainView, "serviceBill");
	            	serviceBillValidator.customValidate(object, errors1, errors);
					//ValidationUtils.invokeValidator(validator, object, errors);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        } finally {
	            errors1.popNestedPath();
	        }
			
			
			//NestedPropertyValidator.invokeNestedValidator(serviceBillValidator, serviceBillMainView,  errors,"serviceBill");
			
			
			
		}
		
		


}
