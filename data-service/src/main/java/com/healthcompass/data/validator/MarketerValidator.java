package com.healthcompass.data.validator;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.MarketerService;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component

public class MarketerValidator implements Validator {
	
	
	@Autowired
	MarketerService marketerService;
	
	
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return MarketerViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name","","Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active","","Active flag is required field");
			
			String name = (String) errors.getFieldValue("name");
			
			/*boolean isMarketerExists = marketerService.isMarketerExistsWithSameName(name);
			
			if(isMarketerExists) { // bill is created  for same unique combination
				errors.rejectValue("marketer", "", null, "Marketer already exists with the name :"+name+"");
				return;
			}*/
						
		}


		
		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			Map<String,Object> marketerMap = (Map) errors.get("marketer");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,marketerMap, "name","","Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,marketerMap, "active","","Active flag is required field");
			
						
		}
}
