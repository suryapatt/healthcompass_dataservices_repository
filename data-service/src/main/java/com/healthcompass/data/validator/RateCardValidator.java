package com.healthcompass.data.validator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component
@Qualifier("rateCardValidator")
public class RateCardValidator implements Validator {
	
	private final RateCardCategoryValidator rateCardCategoryValidator;
	
	 public RateCardValidator(RateCardCategoryValidator rateCardCategoryValidator) {
	        if (rateCardCategoryValidator == null) {
	            throw new IllegalArgumentException(
	              "The supplied [Validator] is required and must not be null.");
	        }
	        if (!rateCardCategoryValidator.supports(LocationRateCardsCategoryViewDTO.class)) {
	            throw new IllegalArgumentException(
	              "The supplied [Validator] must support the validation of [LocationRateCardsCategoryViewDTO] instances.");
	        }
	        this.rateCardCategoryValidator = rateCardCategoryValidator;
	    }
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return LocationRateCardViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate", "","Rate is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "display", "","Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description","","Description is required fieldk");
			if(Double.valueOf((String)errors.getFieldValue("rate")) <=0){
				errors.rejectValue("rate", "", null, "Rate should be greated than 0");
			}
			
			NestedPropertyValidator.invokeNestedValidator(rateCardCategoryValidator, obj , errors, "rateCardCategory");
			
			
		}
		
		
		public void customValidate(Object obj, Errors errors1,Map<String,Object> errors) {
			
			ArrayList<Map<String,Object>> rateCardsErrors = (ArrayList) errors.get("rateCards");
			
			Map<String,Object> rateCardErrorMap = new HashMap<String,Object>();
			rateCardsErrors.add(rateCardErrorMap);
						
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, rateCardErrorMap,"rate", "","Rate is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, rateCardErrorMap,"display", "","Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1, rateCardErrorMap,"description", "","Description is required field");
			
			if(Double.valueOf((String)errors1.getFieldValue("rate")) <=0){
				
				rateCardErrorMap.put("rate","Rate should be greated than 0");
				HealthCompassValidationUtils.markError(rateCardErrorMap);
				
			}
			
			LocationRateCardViewDTO rateCardView = (LocationRateCardViewDTO) obj;
			HashMap<String,Object> rateCardCategoryMap = new HashMap<String,Object>();
			rateCardErrorMap.put("rateCardCategory",rateCardCategoryMap);
	        try {
	            errors1.pushNestedPath("rateCardCategory");
	            
	            try {
	            	Object object = (Object) PropertyUtils.getProperty(rateCardView, "rateCardCategory");
	            	rateCardCategoryValidator.customValidate(object, errors1, rateCardErrorMap);
					//ValidationUtils.invokeValidator(validator, object, errors);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	        } finally {
	            errors1.popNestedPath();
	        }
			
			
			
			
			
		}


}
