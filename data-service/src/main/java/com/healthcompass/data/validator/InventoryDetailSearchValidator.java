package com.healthcompass.data.validator;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.service.BillService;
import com.healthcompass.data.service.PurchaseOrderService;
import com.healthcompass.data.service.params.InventoryDetailsSearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
@Component

public class InventoryDetailSearchValidator implements Validator {
	
	

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return  InventoryDetailsSearchParams.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			InventoryDetailsSearchParams inventoryDetailsSearchParams = (InventoryDetailsSearchParams) obj;
			if(inventoryDetailsSearchParams.getLocationId() == null && inventoryDetailsSearchParams.getProductId() == null && inventoryDetailsSearchParams.getAvailableUnits() == null ) {
				errors.rejectValue("", "", null, "At least one value of either Location, Product or Available Units should be selected");
			}
			
				
			
			
			
			
			
		}


}
