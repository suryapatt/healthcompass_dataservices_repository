package com.healthcompass.data.validator;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsCategoryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
@Component
@Qualifier("serviceBillValidator")
public class SupplierValidator implements Validator {
	
	
	
	
	
	

	 

		//which objects can be validated by this validator
		@Override
		public boolean supports(Class<?> paramClass) {
			return SupplierViewDTO.class.isAssignableFrom(paramClass);
			
		}

		@Override
		public void validate(Object obj, Errors errors) {
			
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name","","Name is required field");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active","","Active flag is required field");
						
		}



		public void customValidate(Object obj, Errors errors1, Map<String,Object> errors) {
			
			Map<String,Object> supplierMap = (Map) errors.get("supplier");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,supplierMap, "name","","Name is required field");
			HealthCompassValidationUtils.rejectIfEmptyOrWhitespace(errors1,supplierMap, "active","","Active flag is required field");
			
						
		}
}
