package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ObjectType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;
import org.hibernate.usertype.UserType;

public class AddressType implements UserType, Serializable {

	private String use;
	private String type;
    private String text;
    private String line;
    private String city;
    private String district;
    private String state;
    private String postalCode;
    private String country;
    private Object period;

	//private final int[] sqlTypesSupported = new int[] { Types.OTHER };
	
	public AddressType(String compositeAddress) {
		
		System.out.println("CompositeAddress = " + compositeAddress+"");
		
		if(compositeAddress == null) return ;
		
		String[] values = compositeAddress.replace("(", "").replace(")", "").split(",");
		System.out.println("values length = " + values.length);
		
		for(int i = 0; i < values.length; i++) {
			switch(i) {
			case 0:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					use = String.valueOf(values[i]);
				break;

			case 1:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					type = String.valueOf(values[i]);
				break;

			case 2:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					text = String.valueOf(values[i]);
				break;

			case 3:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					line = String.valueOf(values[i]);
				break;

			case 4:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					city = String.valueOf(values[i]);
				break;

			case 5:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					district = String.valueOf(values[i]);
				break;
			case 6:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					state = values[i];
				break;
			case 7:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					postalCode = values[i];
				break;
			case 8:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					country = values[i];
				break;
			case 9:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					period = values[i];
				break;
			default:
				System.out.println("Index " + i + " unknown!");
				//return null;
				break;
					
			}
		}
	}
	
	

	public AddressType(String use, String type, String text, String line, String city, String district, String state,
			String postalCode, String country, Object period) {
		super();
		this.use = use;
		this.type = type;
		this.text = text;
		this.line = line;
		this.city = city;
		this.district = district;
		this.state = state;
		this.postalCode = postalCode;
		this.country = country;
		this.period = period;
	}



	

   

    public String getUse() {
		return use;
	}



	public void setUse(String use) {
		this.use = use;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getText() {
		return text;
	}



	public void setText(String text) {
		this.text = text;
	}



	public String getLine() {
		return line;
	}



	public void setLine(String line) {
		this.line = line;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getDistrict() {
		return district;
	}



	public void setDistrict(String district) {
		this.district = district;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getPostalCode() {
		return postalCode;
	}



	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public Object getPeriod() {
		return period;
	}



	public void setPeriod(Object period) {
		this.period = period;
	}



	@Override
    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        if (!(cached instanceof AddressType)) {
			//logger.error("invalid argument");
            throw new HibernateException("invalid argument");
        }
        AddressType f = (AddressType) cached;
        return new AddressType(f.getUse(),f.getType(),f.getText(),f.getLine(),
        		f.getCity(),f.getDistrict(),f.getState(),f.getPostalCode(),f.getCountry()
        		,f.getPeriod());
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        if (!(value instanceof AddressType)) {
			//logger.error("invalid argument");
            throw new HibernateException("invalid argument");
        }
        return (AddressType) value;
    }

	@Override
	public int[] sqlTypes() {
		return new int[] { Types.OTHER };
		//return sqlTypesSupported;
	}

	@Override
	public Class<AddressType> returnedClass() {
		return AddressType.class;
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		return 0;
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {		
		System.out.println("names[0] = " + names[0] + " value = " + rs.getString(names[0]) + " length = " + names.length);
		
		if(rs.wasNull()) {
			return null;
		}
		
		String[] values = rs.getString(names[0]).replace("(", "").replace(")", "").split(",");
		System.out.println("values length = " + values.length);
		
		for(int i = 0; i < values.length; i++) {
			switch(i) {
			case 0:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					use = String.valueOf(values[i]);
				break;

			case 1:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					type = String.valueOf(values[i]);
				break;

			case 2:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					text = String.valueOf(values[i]);
				break;

			case 3:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					line = String.valueOf(values[i]);
				break;

			case 4:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					city = String.valueOf(values[i]);
				break;

			case 5:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					district = String.valueOf(values[i]);
				break;
			case 6:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					state = values[i];
				break;
			case 7:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					postalCode = values[i];
				break;
			case 8:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					country = values[i];
				break;
			case 9:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					period = values[i];
				break;
			default:
				System.out.println("Index " + i + " unknown!");
				//return null;
				break;
					
			}
		}
		
		AddressType name = new AddressType(use, type,text, line, city, district, state,postalCode,country, period);
		return name;
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
			throws HibernateException, SQLException {
		String postgresSpecific;
		
		if(value == null) {
			System.out.println("Custom field is NULL");
			st.setNull(index, Types.OTHER);
		}
		else if (!(value instanceof AddressType)) {
			System.out.println("invalid argument");
            throw new HibernateException("invalid argument");
		}
		else {
			
			final AddressType f = (AddressType) value;
			postgresSpecific = "(" + (f.getUse() == null? "" : f.getUse()) + "," + (f.getType() == null? "" : f.getType()) + "," +
					(f.getLine() == null? "" : f.getLine()) + "," + (f.getCity()== null? "" : f.getCity()) + "," +
					(f.getDistrict() == null? "" : f.getDistrict()) + "," + (f.getState()== null? "" : f.getState()) + "," + (f.getPostalCode()== null? "" : f.getPostalCode())+ "," + (f.getCountry()== null? "" : f.getCountry())+ ","+ (f.getPeriod()== null? "" : f.getPeriod())+ ")";
			System.out.println("Setting custom field (PostgreSQL specific) to: " + postgresSpecific);
			st.setObject(index, postgresSpecific, Types.OTHER);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		if(value == null)
			return null;

		if (!(value instanceof AddressType)) {
			System.out.println("invalid argument");
            throw new HibernateException("invalid argument");
        }
		
		AddressType f = (AddressType) value;
        return new AddressType(f.getUse(), f.getType(),f.getText(),
        		f.getLine(), f.getCity(),f.getDistrict(), f.getState(),f.getPostalCode(),f.getCountry(),f.getPeriod());
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}
	
	@Override
	public boolean equals(Object x, Object y)  throws HibernateException {
		return x.equals(y);
	}
    
}
