package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="order_items",schema="inventory")
public class OrderItem extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "order_id",updatable=false)
	private Order order;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="product_id")
	private Product product;
	
	@Column(name = "product_name")
	private String productName;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="inventory_id", nullable=true)
	private InventoryItem inventory;
	
	@Column(name = "batch_number")
	private String batchNumber;
	
	@Column(name = "expiry_date")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private java.sql.Date expiryDate;
	
	
	@Column(name = "rate")
	private Double rate;
	
	@Column(name = "qty")
	private Integer quantity;
	
	@Column(name = "mrp")
	private Double mrp;
	
		
	@Column(name = "gst_rate")
	private Double gstRate;
	
	@Column(name = "sales_tax_rate")
	private Double salesTaxRate;
	
	@Column(name = "central_tax_rate")
	private Double centralTaxRate;
	
	@Column(name = "vat_rate")
	private Double vatRate;
	
	@Column(name = "discounted_rate")
	private Double discountedRate;
	
	@Column(name = "discount")
	private Double discount;
	
	@Column(name = "net_item_value")
	private Double netItemValue;
	
	
	
	/*@Column(name = "units")
	private Integer units;*/
	
	
	
	
	
	public OrderItem() {
		
	}
	
	
}
