package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="section", schema="project_asclepius")
public class HelpPageSection implements Serializable {
  
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "section_name")
	private String sectionName;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "padded")
	private String padded;
	
	@Column(name = "sno")
	private Integer serialNumber;
	
		
	@OneToMany(mappedBy = "section", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<HelpPageSectionDefinition> sectionDefinitions;
	
	@OneToMany(mappedBy = "parentSection", cascade = CascadeType.ALL,orphanRemoval=true)
	@OrderBy("position ASC")
	private List<HelpPageSectionDetail> sectionDetail;
	

	public HelpPageSection() {
		
	}
	
	
}
