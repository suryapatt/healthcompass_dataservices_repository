package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="value_set", schema = "project_asclepius")
public class ValueSet extends Auditable<String>{
  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // use custom sequence  from db
	private int id;
	
	@Column(name = "user_group")
	private String userGroup;
	
	@Column(name = "level")
	private Integer level;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "system")
	private String system;
	
	@Column(name = "display")
	private String display;
	
	@Column(name = "definition")
	private String definition;
	
	@Column(name = "sort_order")
	private Integer sortOrder;
	
	@Column(name = "active")
	private String isActive;
	
	@Column(name = "common_name")
	private String commonName;
	
	@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "value_set_type_id", updatable=false)
	private ValueSetType valueSetType;
	
	

	public ValueSet() {
		
	}
	
	
}
