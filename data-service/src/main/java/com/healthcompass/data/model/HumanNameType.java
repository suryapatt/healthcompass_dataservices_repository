package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ObjectType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;
import org.hibernate.usertype.UserType;

public class HumanNameType implements  Serializable {

	public Object getPeriod() {
		return period;
	}

	public void setPeriod(Object period) {
		this.period = period;
	}

	//private final int[] sqlTypesSupported = new int[] { Types.OTHER };
	
	public HumanNameType(String compositeName) {
		System.out.println("CompositeName = " + compositeName+"");
		
		if(compositeName==null) return;
		
		String[] values = compositeName.replace("(", "").replace(")", "").split(",");
		System.out.println("values length = " + values.length);
		
		for(int i = 0; i < values.length; i++) {
			switch(i) {
			case 0:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					use = String.valueOf(values[i]);
				break;

			case 1:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					text = String.valueOf(values[i]);
				break;

			case 2:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					family = String.valueOf(values[i]);
				break;

			case 3:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					given = String.valueOf(values[i]);
				break;

			case 4:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					prefix = String.valueOf(values[i]);
				break;

			case 5:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					suffix = String.valueOf(values[i]);
				break;
			case 6:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					period = values[i];
				break;
			default:
				System.out.println("Index " + i + " unknown!");
				//return null;
				break;
					
			}
		}
		
		
	}
	
	public HumanNameType(String use, String text, String family, String given, String prefix, String suffix,
			Object period) {
		super();
		this.use = use;
		this.text = text;
		this.family = family;
		this.given = given;
		this.prefix = prefix;
		this.suffix = suffix;
		this.period = period;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getGiven() {
		return given;
	}

	public void setGiven(String given) {
		this.given = given;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	private String use;
    private String text;
    private String family;
    private String given;
    private String prefix;
    private String suffix;
    private Object period;
   

    
	
	

	

	
	
    
}
