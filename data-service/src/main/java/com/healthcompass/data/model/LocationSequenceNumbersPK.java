package com.healthcompass.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class LocationSequenceNumbersPK implements Serializable {

	private String locationCode;
	private String sequenceName;
	
	
	public LocationSequenceNumbersPK() {
		
    }
	
	public LocationSequenceNumbersPK(String locationCode, String sequenceName) {
		super();
		this.locationCode = locationCode;
		this.sequenceName = sequenceName;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getSequenceName() {
		return sequenceName;
	}
	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}
	
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationSequenceNumbersPK that = (LocationSequenceNumbersPK) o;

        if (!locationCode.equals(that.locationCode)) return false;
        return sequenceName.equals(that.sequenceName);
    }

    @Override
    public int hashCode() {
        int result = locationCode.hashCode();
        result = 31 * result + sequenceName.hashCode();
        return result;
    }
}
