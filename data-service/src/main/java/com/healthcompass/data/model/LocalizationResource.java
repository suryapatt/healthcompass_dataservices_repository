package com.healthcompass.data.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="resource", schema="project_asclepius")
public class LocalizationResource implements Serializable {
  
	@Id
	@Column(name = "resource_id")
	private String resourceId;
	
	
	@Generated(GenerationTime.INSERT) // while updating this value is being ste to null. So removed from entity,as this is not used anywhere
	private Integer id;
	
		
	@Column(name = "resource_group_code")
	private String resourceGroupCode;
	
	@Column(name = "resource_code")
	private String resourceCode;
	
	@Column(name = "resource_sub_category")
	private String resourceSubCategory;
	
	@Column(name = "resource_name")
	private String resourceName;
	
	
	@Column(name = "resource_category_id")
	private String  resourceCategory;
	
	@OneToMany(mappedBy = "resource", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<LocalizationResourceSection> resourceSections;
	
		

	public LocalizationResource() {
		
	}
	
	
}
