package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="resource_category", schema="project_asclepius")
public class LocalizationResourceCategory implements Serializable {
  
	@Id
	@Column(name = "resource_category_id")
	private String resourceCategoryId;
	
	@Generated(GenerationTime.INSERT) // while updating this value is being ste to null. So removed from entity,as this is not used anywhere
	private Integer id;
	
	
	@Column(name = "resource_category_code")
	private String resourceCategoryCode;
	
	@Column(name = "resource_category_name")
	private String resourceCategoryName;
	
	@Column(name = "resource_category_level")
	private Integer resourceCategoryLevel;

	@Column(name = "resource_group_code")
	private String resourceGroup;
	
	@OneToMany(mappedBy = "resourceCategory", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<LocalizationResource> resources;
	
	
	public LocalizationResourceCategory() {
		
	}
	
	
}
