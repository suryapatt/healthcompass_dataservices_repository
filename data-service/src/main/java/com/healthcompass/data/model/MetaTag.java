package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="meta_tag", schema="inventory")
public class MetaTag extends Auditable<String> implements Serializable {
  
	@Id
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", nullable=false)
	private MetaTag parent;
		
	@OneToMany(mappedBy = "metaTag", fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval=true)
	private List<MetaTagAttributes> metaTagAttributes;
	
	
	
	

	public MetaTag() {
		
	}
	
	
}
