package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="encounter", schema = "project_asclepius")
public class Encounter extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	@JoinColumn(name = "encounter_number")
	private Integer encounterNumber;
	
	//@OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subject")
	private Patient patient;
	
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "participant")
	private PractitionerRole participant;*/
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appointment")
	private Appointment appointment; //  not sure if many to one or one to one. so no relation is specified
	
	@OneToOne
    @JoinColumn(name = "status")
	private ValueSet status;
	
	@Column(name = "arrive_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	private java.sql.Timestamp admissionTime;
	
	@Column(name = "discharge_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	private java.sql.Timestamp dischargeTime;
	
	//@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "service_provider")
	private Organization organization;
	
	//@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location")
	private Location location;
	
	
	
   @OneToMany(mappedBy = "encounter", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
   private List<ServiceBill> serviceBills;
	

	public Encounter() {
		
	}
	
	
}
