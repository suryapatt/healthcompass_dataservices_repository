package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="service_bill_item")
public class ServiceBillItem extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	// @GeneratedValue(strategy = GenerationType.IDENTITY) // use custom sequence  from db
	private UUID id;
	
	@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@JoinColumn(name = "bill_id",updatable=false)
	private ServiceBill serviceBill;
	
	
	@OneToOne
	@JoinColumn(name="rate_card_id")
	private RateCard RateCard;
	
	@Column(name = "rate")
	private Double rate;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "amount")
	private Double amount;
	
	
	
	/*@Column(name = "gst_rate")
	private Double gstRate;
	@Column(name = "sales_tax_rate")
	private Double salesTaxRate;
	@Column(name = "central_tax_rate")
	private Double centralTaxRate;
	@Column(name = "vat_rate")
	private Double vatRate;
	@Column(name = "discount_rate")
	private Double discountRate;
		@Column(name = "discount")
	private Double discount;
	@Column(name = "net_amount")
	private Double netAmount;
	*/
	
	
	
	@Column(name = "active")
	private Boolean active;
	
	
	public ServiceBillItem() {
		
	}
	
	
}
