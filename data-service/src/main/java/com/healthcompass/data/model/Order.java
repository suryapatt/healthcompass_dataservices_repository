package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="order", schema="inventory")
public class Order extends Auditable<String> implements Serializable {
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	@Column(name = "order_number")
	private String orderNumber;
	
	@Column(name = "order_type")
	private String orderType;
	
	@Column(name = "order_comment")
	private String orderComment;
	
	
	@Column(name = "order_date")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private java.sql.Timestamp orderDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private Organization organization;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	private Location location;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplier_id", nullable=true)
	private Supplier supplier;
	
	@Column(name = "order_value")
	private Double orderValue;
	
	@Column(name = "discount")
	private Double discount;
	
	@Column(name = "net_order_value")
	private Double netOrderValue;
	
	@Column(name = "attachment_id")
	private UUID attachmentId;
	
	
	
	
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<OrderItem> orderItems;
	
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval=true)
	private List<InventoryItem> inventoryItems;
	
	
	/*
	 
	@ManyToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;
	@ManyToOne
	@JoinColumn(name = "practitioner_role_id")
	private PractitionerRole practitionerRole;
	@ManyToOne
	@JoinColumn(name = "encounter_id")
	private Encounter encounter;
	@Column(name = "invoice_number")
	private String invoiceNumber
	@Column(name = "amount_paid")
	private Double amountPaid;
	@Column(name = "counter_no")
	private Integer counterNumber;
	*/
	

	public Order() {
		
	}
	
	
}
