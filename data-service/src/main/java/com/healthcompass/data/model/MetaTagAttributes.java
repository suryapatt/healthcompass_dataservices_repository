package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="meta_tag_attributes", schema="inventory")
public class MetaTagAttributes extends Auditable<String> implements Serializable {
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "meta_tag_attributes_pk_generator")
    @SequenceGenerator(name="meta_tag_attributes_pk_generator", sequenceName = "inventory.meta_tag_attributes_id_seq",allocationSize=1,initialValue=10)
	private Integer id;
	
	@Column(name = "value")
	private String value;
	
	
	
	@Column(name = "active")
	private Boolean active;	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable=false)
	private Product product;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "meta_tag_id", nullable=false)
	private MetaTag metaTag;
	
	
	
	

	public MetaTagAttributes() {
		
	}
	
	
}
