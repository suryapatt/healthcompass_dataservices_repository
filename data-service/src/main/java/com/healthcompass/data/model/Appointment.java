package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="appointment", schema = "project_asclepius")
public class Appointment extends Auditable<String>{
  
	@Id
	@GeneratedValue
    private Integer id;
	
	
	
	@Column(name = "description")
	private String description;
	
	@OneToOne
    @JoinColumn(name = "specialty", nullable=true)
    private ValueSet speciality;
	
	
	@Column(name = "created")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	private java.sql.Timestamp created;
	

	public Appointment() {
		
	}
	
	
}
