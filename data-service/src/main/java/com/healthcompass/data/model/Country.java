package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="countries", schema="project_asclepius")
public class Country implements Serializable {
  
	@Id
	private Integer id;
	
	@Column(name = "short_name")
	private String shortName;
	
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "phone_code")
	private Integer phoneCode;
	
		

	public Country() {
		
	}
	
	
}
