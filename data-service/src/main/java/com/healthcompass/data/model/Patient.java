package com.healthcompass.data.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Columns;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthcompass.data.util.HealthCompassConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="patient", schema = "project_asclepius")
/*@TypeDef(
		name="name",
		typeClass = com.healthcompass.data.model.HumanNameType.class,
		defaultForType=HumanName.class
	)*/
public class Patient extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	//@ManyToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "managing_organization")
	private Organization organization;
	
	@Column(name = "name")
	private String name;
	
	   
	@Column(name = "address")
	private String address;
		
	@Column(name = "telecom")
	private String phone;
	
	

	@Column(name = "birth_date")
	private String dateOfBirth;
	
	@OneToOne
	@JoinColumn(name="userid")
	private AuthUser authUser;
	
	 private transient HumanNameType compositeName;
	 private transient AddressType compositeAddress;
	 private transient ContactPointType compositePhone;
	 private transient ContactPointType compositeEmail;
	    
	
	@OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Encounter> encounters;
	
	@OneToMany(mappedBy = "patient", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ServiceBill> serviceBills;
	
	@OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<BillPayment> billPayments;
	
	public Patient() {
		
	}
	
	public HumanNameType getCompositeName() {
		
		
		if (this.compositeName == null){
			this.compositeName=new HumanNameType(this.name);
		}
		return this.compositeName;
	}
	
	public void setCompositeName(HumanNameType compositeName) {
		
		this.compositeName=compositeName;
	}
	
	public ContactPointType getCompositePhone() {
		
		if (this.compositePhone == null){
			this.compositePhone=new ContactPointType(this.phone);
		}
		return this.compositePhone;
	}
	
	public void setCompositePhone(ContactPointType compositePhone) {
		
		this.compositePhone=compositePhone;
	}
	
	public ContactPointType getCompositeEmail() {
		
		if (this.compositeEmail == null){
			this.compositeEmail=new ContactPointType(this.phone,4);
		}
		return this.compositeEmail;
	}
	
	public void setCompositeEmail(ContactPointType compositeEmail) {
		
		this.compositeEmail=compositeEmail;
	}
	
	public AddressType getCompositeAddress() {
		
		if (this.compositeAddress == null){
			this.compositeAddress=new AddressType(this.address);
		}
		return this.compositeAddress;
	}
	
	public void setCompositeAddress(AddressType compositeAddress) {
		
		this.compositeAddress=compositeAddress;
	}
	
	
	public String getDecodedName() {
	
	// return null;
			return  HealthCompassConstants.getDecodedValue("Patient Name",this.getCompositeName().getText());
	
	}
	public String getDecodedPhone() {
		
			return  HealthCompassConstants.getDecodedValue("Phone Number",this.getCompositePhone().getValue());
		
		
		 
	}
	
	public String getDecodedAddress() {
		
			return HealthCompassConstants.getDecodedValue("Address Text",this.getCompositeAddress().getText()) +" ";
			
		
		
		
	   		
	}
	public String getDecodedDateOfBirth() {
		
		return HealthCompassConstants.getDecodedValue("Date of Birth",this.dateOfBirth);
	}
	
	public String getDecodedEmail() {
		
		return  HealthCompassConstants.getDecodedValue("Email",this.getCompositeEmail().getValue());
		
	}
	public String getDecodedEmail(String compositeName) {
		String name = "";
		String[] multipleStrings = compositeName.split(",");
		 name = multipleStrings[2];
		 return name;
	}
	
	public int  getAge(String dob) {
		int age=0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			Instant instant = sdf.parse(dob).toInstant();
		    ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
		    LocalDate dobDate = zone.toLocalDate();
		      //Calculating the difference between given date to current date.
		    Period period = Period.between(dobDate, LocalDate.now());
		    age=period.getYears();
		}catch(Exception e) {
			
		}
		
		return age;
	}
	
	public static void main(String args[]) {
		String phone="{\"(138,gAAAAABgacGWgoNs_kW5f2lfH-zZmdjuDi0ipeNSlR6XCGdDzvc_F9Xokh5j5iJpd0mawB1h1hIAeUogoE2KPQfHPXk-Bb0IFw==,144,1)\",\"(133,gAAAAABgacGWE_P0nDfFpAGbk0BQsD7Tx1VjFQwazox-HmY4RVO8Df-v26osoDQO1QMB3h8gSh8vN45pFKKL_vJYiGgVGh3KlQ==,144,1)\"}";
		Patient p = new Patient();
		p.setPhone(phone);
		p.getDecodedPhone();
		p.getDecodedEmail();
		
		return;
	}
	
	
}
