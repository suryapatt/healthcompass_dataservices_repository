package com.healthcompass.data.model;

import java.util.Objects;

public class HumanName {

	public int getUse() {
		return use;
	}

	public void setUse(int use) {
		this.use = use;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getGiven() {
		return given;
	}

	public void setGiven(String given) {
		this.given = given;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	private int use;
    private String text;
    private String family;
    private String given;
    private String prefix;
    private String suffix;
    private Object period;
   

    

    public Object getPeriod() {
		return period;
	}

	public void setPeriod(Object period) {
		this.period = period;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanName name = (HumanName) o;
        return use == name.use &&
                Objects.equals(text, name.text) &&
                Objects.equals(family, name.family) &&
                Objects.equals(given, name.given) &&
                Objects.equals(prefix, name.prefix) &&
                Objects.equals(suffix, name.suffix) ;
               
    }

    @Override
    public int hashCode() {
        return Objects.hash(use, text, family, given, prefix,suffix);
    }
}
