package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="value_set_type", schema = "project_asclepius")
public class ValueSetType extends Auditable<String>{
  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // use custom sequence  from db
	private int id;
	
	@Column(name = "referred_from")
	private String referredFrom;
	
	@Column(name = "source")
	private String source;
	
	@Column(name = "active")
	private boolean isActive;
	
		
	@Column(name = "sort_order")
	private Integer sortOrder;
	
	@Column(name = "standard_level")
	private String standardLevel;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "definition")
	private String definition;
	
	@Column(name = "system")
	private String system;
	
	@OneToMany(mappedBy = "valueSetType",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ValueSet> valueSets;
	
	

	public ValueSetType() {
		
	}
	
	
}
