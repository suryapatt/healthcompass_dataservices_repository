package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="rate_card_category")
public class RateCardCategory extends Auditable<String>{
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rate_card_cat_pk_generator")
    @SequenceGenerator(name="rate_card_cat_pk_generator", sequenceName = "rate_card_cat_id_new_seq",allocationSize=1,initialValue=10)
	private Integer id;
	
	@Column(name = "code")
	private String code;
	
	//@NotEmpty
	@Column(name = "description")
	private String description;
	
	@Column(name = "active")
	private Boolean active;
	

	public RateCardCategory() {
		
	}
	
	
}
