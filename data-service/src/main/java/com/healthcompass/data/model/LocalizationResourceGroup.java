package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="resource_group", schema="project_asclepius")
public class LocalizationResourceGroup implements Serializable {
  
	@Id
	@Column(name = "resource_group_code")
	private String resourceGroupCode;
	
	
	@Generated(GenerationTime.INSERT) // while updating this value is being ste to null. So removed from entity,as this is not used anywhere
	private Integer id;
	
	
	
	
	@Column(name = "resource_group_name")
	private String resourceGroupName;
	
	
	@OneToMany(mappedBy = "resourceGroup", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<LocalizationResourceCategory> resourceCategories;
	
		

	public LocalizationResourceGroup() {
		
	}
	
	
}
