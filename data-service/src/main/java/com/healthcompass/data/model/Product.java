package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="product", schema="inventory")
public class Product extends Auditable<String> implements Serializable {
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_pk_generator")
    @SequenceGenerator(name="product_pk_generator", sequenceName = "inventory.product_id_seq",allocationSize=1,initialValue=10)
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "marketer_id", nullable=true)
	private Marketer marketer;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manifacturer_id", nullable=true)
	private Marketer manufacturer;
	
	@Column(name = "molecule_id")
	private Integer molecule;
	
	@Column(name = "hsn_code_id")
	private Integer hsnCode;
	
	@Column(name = "pack")
	private Integer pack;
	
	@Column(name = "units")
	private Integer units;
	
	@Column(name = "schedule_drug_code")
	private String scheduleDrugCode;
	
	@Column(name = "gst_rate")
	private Double gstRate;
	
	@Column(name = "sales_tax_rate")
	private Double salesTaxRate;
	
	@Column(name = "central_tax_rate")
	private Double centralTaxRate;
	
	@Column(name = "vat_rate")
	private Double vatRate;
	
	@Column(name = "reorder_level")
	private Integer reOrderLevel;
	
	@Column(name = "lead_time")
	private Integer leadTime;
	
	
	
	@Column(name = "active")
	private Boolean active;	
	
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<MetaTagAttributes> metaTagAttributes;
	
	

	public Product() {
		
	}
	
	
}
