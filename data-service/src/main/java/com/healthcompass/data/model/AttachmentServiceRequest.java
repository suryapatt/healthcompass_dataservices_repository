package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



public class AttachmentServiceRequest implements Serializable {
  
	
	
	
	private int contentType ;
	private java.sql.Timestamp creation; 
  	private byte[] data;
    private String hash;
    private int language;
    private long size;
    private String title;
    private String updatedBy;
    private java.sql.Timestamp updatedOn; 
    private String url;
	
    public AttachmentServiceRequest() {
		
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		try {
			//this.put("contentType",contentType);
			this.contentType = contentType;
		}catch(Exception e) {
			
		}
		
				
	}

	  public java.sql.Timestamp getCreation() {
			return creation;
		}

		public void setCreation(java.sql.Timestamp creation) {
			try {
				//this.put("creation",creation);
				this.creation = creation;
			}catch(Exception e) {
				
			}	
		}
	

	

		public byte[] getData() {
			return data;
		}

		public void setData(byte[] data) {
			try {
				//this.put("data",data);
				this.data = data;
			}catch(Exception e) {
				
			}
		}

		public String getHash() {
			return hash;
		}

	public void setHash(String hash) {
		try {
			//this.put("hash",hash);
			this.hash = hash;
		}catch(Exception e) {
			
		}	
	}

	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		try {
			//this.put("language",language);
			this.language = language;
		}catch(Exception e) {
			
		}
		
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		try {
			//this.put("size",size);
			this.size = size;
		}catch(Exception e) {
			
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		try {
			//this.put("title",title);
			this.title = title;
		}catch(Exception e) {
			
		}
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		try {
			//this.put("updatedBy",updatedBy);
			this.updatedBy = updatedBy;
		}catch(Exception e) {
			
		}
	}

	public java.sql.Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(java.sql.Timestamp updatedOn) {
		try {
			//this.put("updatedOn",updatedOn);
			this.updatedOn = updatedOn;
		}catch(Exception e) {
			
		}
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		try {
			//this.put("url",url);
			this.url = url;
		}catch(Exception e) {
			
		}	
	}
	
	
}
