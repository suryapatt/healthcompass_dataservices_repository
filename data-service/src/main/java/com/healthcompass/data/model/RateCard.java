package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="rate_card")
public class RateCard extends Auditable<String>{
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rate_card_pk_generator")
    @SequenceGenerator(name="rate_card_pk_generator", sequenceName = "rate_card_id_new_seq",allocationSize=1,initialValue=10)
	private Integer id;
	
	//@NotEmpty
	@Column(name = "display")
	private String display;
	
	//@NotEmpty	
	@Column(name = "description")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "rate_card_category_id")
	private RateCardCategory rateCardCategory;
	
	//@NotNull	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	private Location location;
	
	
	//@Positive
	@Column(name = "rate")
	private Double rate;
	
	@Column(name = "active")
	private Boolean active;

	public RateCard() {
		
	}
	
	public boolean equals(RateCard other){
	    boolean result;
	    if((other == null) || (getClass() != other.getClass())){
	        result = false;
	    } // end if
	    else{
	        RateCard otherRateCard = (RateCard)other;
	        result = id.equals(other.id);
	    } // end else

	    return result;
	} // end eq 
	
	
}
