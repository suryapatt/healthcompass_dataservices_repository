package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ObjectType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;
import org.hibernate.usertype.UserType;

public class ContactPointType implements  Serializable {


	//private final int[] sqlTypesSupported = new int[] { Types.OTHER };
	
	
	private String system;
    private String value;
    private String use;
    private String rank;
    
    public ContactPointType(String compositePhone) {
    	System.out.println("CompositePhone = " + compositePhone+"");
		
		if(compositePhone==null) return;
		
		String[] values = compositePhone.replace("(", "").replace(")", "").split(",");
		System.out.println("values length = " + values.length);
		
		for(int i = 0; i < values.length; i++) {
			switch(i) {
			case 0:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					system = String.valueOf(values[i]);
				break;

			case 1:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					value = String.valueOf(values[i]);
				break;

			case 2:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					use = String.valueOf(values[i]);
				break;

			case 3:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					rank = String.valueOf(values[i]);
				break;

			
			default:
				System.out.println("Index " + i + " unknown!");
				//return null;
				break;
					
			}
		}
		
		
    	
		
  	}
    
    public ContactPointType(String compositePhone,int startIndexToConsider) {
    	System.out.println("CompositePhone = " + compositePhone+"");
		
		if(compositePhone==null) return;
		
		String[] values = compositePhone.replace("(", "").replace(")", "").split(",");
		System.out.println("values length = " + values.length);
		
		for(int i = 4; i < values.length; i++) {
			switch(i) {
			case 4:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					system = String.valueOf(values[i]);
				break;

			case 5:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					value = String.valueOf(values[i]);
				break;

			case 6:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					use = String.valueOf(values[i]);
				break;

			case 7:
				System.out.println("values[" + i + "] = " + values[i]);
				if(values[i].length() > 0)
					rank = String.valueOf(values[i]);
				break;

			
			default:
				System.out.println("Index " + i + " unknown!");
				//return null;
				break;
					
			}
		}
		
		
  	}
  	
  	public ContactPointType(String system, String value, String use, String rank) {
  		super();
  		this.system = system;
  		this.value = value;
  		this.use = use;
  		this.rank = rank;
  		
  	}

    
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	    
}
