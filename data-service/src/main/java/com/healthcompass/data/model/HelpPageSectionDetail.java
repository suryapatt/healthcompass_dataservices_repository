package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="section_detail", schema="project_asclepius")
public class HelpPageSectionDetail implements Serializable {
  
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "section_id")
	private String parentSection;
	
	@Column(name = "section_name")
	private String sectionName;
	
	@Column(name = "field_name")
	private String fieldName;
	
	@Column(name = "padded_position")
	private String paddedPosition;
	
	@Column(name = "position")
	private Integer position;
	
	@Column(name = "self_referrence")
	private String selfReferrence;
	
	@Column(name = "self_referrence_id")
	private String selfReferrenceId;
	
	
	@Column(name = "display")
	private Boolean isDisplay;
	

	@OneToMany(mappedBy = "sectionDetail", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<HelpPageSectionDetailDefinition> sectionDetailDefinitions;
	
	
		

	public HelpPageSectionDetail() {
		
	}
	
	
}
