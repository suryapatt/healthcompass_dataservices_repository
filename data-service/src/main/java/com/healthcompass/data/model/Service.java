package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="service")
public class Service extends Auditable<String>{
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE) // use custom sequence  from db
	private int id;
	
	@Column(name = "service_code")
	private String serviceCode;
	
	@ManyToOne
	@JoinColumn(name = "service_type_id", updatable=false)
	private ValueSet serviceType;
	
	@Column(name = "organization_id")
	private UUID organizationId;
	
	@Column(name = "location_id")
	private UUID locationId;
	
	@Column(name = "practitioner_role_id")
	private UUID practitionarRoleId;
	
	@Column(name = "service_rate")
	private Double serviceRate;
	
	

	public Service() {
		
	}
	
	
}
