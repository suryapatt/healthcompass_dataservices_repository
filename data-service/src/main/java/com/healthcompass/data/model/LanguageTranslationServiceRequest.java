package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



public class LanguageTranslationServiceRequest implements Serializable {
  
	
	
	
	private int contentType ;
	private String languageTranslationServiceURL;
	
    public LanguageTranslationServiceRequest() {
		
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		try {
			//this.put("contentType",contentType);
			this.contentType = contentType;
		}catch(Exception e) {
			
		}
		
				
	}

	public String getLanguageTranslationServiceURL() {
		return languageTranslationServiceURL;
	}

	public void setLanguageTranslationServiceURL(String languageTranslationServiceURL) {
		this.languageTranslationServiceURL = languageTranslationServiceURL;
	}

	 
	

	
	
	
}
