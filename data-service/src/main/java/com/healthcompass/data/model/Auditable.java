package com.healthcompass.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthcompass.data.service.HealthCompassAuditingEntityListener;

@Data
@MappedSuperclass
@EntityListeners(HealthCompassAuditingEntityListener.class)
public class Auditable<U> {
	
	//@CreatedDate
	@Column(name = "CREATED_ON")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	private java.sql.Timestamp createdOn;
	
	@CreatedBy
	@Column(name = "CREATED_BY")
	private U createdBy;
	
	//@LastModifiedDate
	@Column(name = "UPDATED_ON")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss z")
	private java.sql.Timestamp updatedOn;
	
	@LastModifiedBy
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	

}
