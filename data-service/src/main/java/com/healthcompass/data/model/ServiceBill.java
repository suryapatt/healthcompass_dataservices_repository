package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="service_bill")
public class ServiceBill extends Auditable<String> implements Serializable {
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	@Column(name = "bill_number")
	private String billNumber;
	
	@Column(name = "bill_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyyTHH:mm:ss")
	private java.sql.Timestamp billDate;
	
	
	
	//@ManyToOne(cascade = CascadeType.ALL)
	@ManyToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;
	
	//@ManyToOne(cascade = CascadeType.ALL)
	@ManyToOne
	@JoinColumn(name = "practitioner_role_id")
	private PractitionerRole practitionerRole;
	
	//@ManyToOne(cascade = CascadeType.ALL)
    @ManyToOne
	@JoinColumn(name = "encounter_id")
	private Encounter encounter;
	
	@Column(name = "billing_done_by")
	private String billingDoneBy;
	
	@OneToOne
    @JoinColumn(name = "payment_mode_id", nullable=true)
    private ValueSet paymentMode;
	
	
	@Column(name = "discount_rate")
	private Double discountRate;
	
	@Column(name = "discount")
	private Double discount;
	
	
	@Column(name = "amount_billed")
	private Double amountBilled;
	
	@Column(name = "amount_paid")
	private Double amountPaid;
	
	@Column(name = "amount_due")
	private Double amountDue;
	
	@Column(name = "sub_total")
	private Double subTotal;
	
	@OneToOne
    @JoinColumn(name = "status_id",nullable=true)
	private ValueSet status;
	
	@OneToOne
    @JoinColumn(name = "payment_status_id",nullable=true)
	private ValueSet paymentStatus;
	
	@Column(name = "attachment_id")
	private UUID attachmentId;
	
	@Column(name = "active")
	private Boolean active;	
	
	
	@OneToMany(mappedBy = "serviceBill", cascade = CascadeType.ALL,orphanRemoval=true)
	private List<ServiceBillItem> serviceBillItems;
	
	@OneToMany(mappedBy = "serviceBill", fetch = FetchType.LAZY, cascade = CascadeType.ALL,orphanRemoval=true)
	private List<BillPayment> billPayments;
	
	private transient Double outstandingPendingBillsAmount;

	public ServiceBill() {
		
	}
	
	
}
