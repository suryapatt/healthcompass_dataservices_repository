package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthcompass.data.util.HealthCompassConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="practitioner", schema = "project_asclepius")
public class Practitioner extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	
	@Column(name = "name")
	private String name;
	
	 private transient HumanNameType compositeName;
	 
	
	
	@Column(name = "address")
	private String address;
	
	@OneToMany(mappedBy = "practitioner", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PractitionerRole> practitionerRoles;

	public Practitioner() {
		
	}
	
	public HumanNameType getCompositeName() {
		
		
		if (this.compositeName == null){
			this.compositeName=new HumanNameType(this.name);
		}
		return this.compositeName;
	}
	
	public void setCompositeName(HumanNameType compositeName) {
		
		this.compositeName=compositeName;
	}
	
	public String getDecodedName() {
		
		// return null;
				return  HealthCompassConstants.getDecodedValue("Practitioner Name",this.getCompositeName().getText());
		
		}
}
