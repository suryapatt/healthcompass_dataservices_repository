package com.healthcompass.data.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Entity
@Table(name="marketer", schema="inventory")
public class Marketer extends Auditable<String> implements Serializable {
  
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marketer_pk_generator")
    @SequenceGenerator(name="marketer_pk_generator", sequenceName = "inventory.marketer_id_seq", allocationSize=1,initialValue=10)
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	
	@Column(name = "description")
	private String description;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private Organization organization;
			
	@Column(name = "active")
	private Boolean active;


	public Marketer() {
		
	}
	
	
}
