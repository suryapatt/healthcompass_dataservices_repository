package com.healthcompass.data.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Data
@Entity
@Table(name="organization", schema = "project_asclepius")
public class Organization extends Auditable<String>{
  
	@Id
    @GenericGenerator(name = "UUIDGenerator", strategy = "uuid2")
    @GeneratedValue(generator = "UUIDGenerator")
	@Type(type="pg-uuid")
	private UUID id;
	
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private List<Location> locations;
	
	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<PractitionerRole> practitionerRoles;
	
	@OneToMany(mappedBy = "organization",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Patient> patients;

	public Organization() {
		
	}
	
	
}
