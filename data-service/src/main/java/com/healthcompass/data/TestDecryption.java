package com.healthcompass.data;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.boot.SpringApplication;

/*import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;*/
import com.macasaet.fernet.Key;
import com.macasaet.fernet.StringValidator;
import com.macasaet.fernet.Token;
import com.macasaet.fernet.Validator;



import java.time.Duration;
import java.time.temporal.TemporalAmount;

public class TestDecryption {

public static void Oldmain1(String[] args) {
		
		final Key key = new Key("CSMMNtPnLo9aE64V6XLuOZO-bYNslmFO3oFXF6RA-K8=");
		String encodedPatientName = getName("{\"(1145,t)\",\"(1186,t)\"}");
		final Token token = Token.fromString(encodedPatientName);
		
       
        final Validator<String> validator = new StringValidator() {
    		public TemporalAmount getTimeToLive() {
                return Duration.ofSeconds(Instant.MAX.getEpochSecond());
            }
    		};
        final String payload = token.validateAndDecrypt(key, validator);
        System.out.println("Payload is " + payload);
        
		
		
		SpringApplication.run(HealthCompassDataServicesApplication.class, args);
	}

public static String getName(String compositeName) {
	String name = "";
String[] multipleStrings = compositeName.split(",");
 name = multipleStrings[1];
 return name;
 //return "{"(1164,t)"";
}

/*public static void main(String[] args) {
	//"2021-05-28T00:00:00.000+00:00",
	//Instant eventOccurred = Instant.now();
	//System.out.println(eventOccurred);
	//eve
	//java.sql.Timestamp new = new java.sql.Timestamp(eventOccurred.getLong(field));
	//String startDateString = "2021-05-28T00:00:00.000+00:00";
	try {
	//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	
   
    //LocalDate dobDate = zone.toLocalDate();
    
	
	
	//ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
		//java.sql.Date d1 = new java.sql.Date(System.currentTimeMillis());
		//System.out.println(d1);
	//String s = d1.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		//System.out.println(s);
		//Date d2 =	new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTimeInMillis());	
			//long l = .toInstant()).toEpochMilli());
		//java.sql.Date d2 = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(s));
		//Date date = new SimpleDateFormat("dd-MM-yyyy").parse(d1.toLocalDate().);
		//String formattedDate = new SimpleDateFormat("yyyyMMdd").format(date);
	}catch (Exception e) {
		e.printStackTrace();
	}
}*/

//public static void Oldmain1(String[] args) {
	//Storage storage = StorageOptions.getDefaultInstance().getService();
	//BlobId blobId = BlobId.of("inventory_product_upload_health-compass-302720", "first_upload_again");
	//BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build();
	//storage.create(blobInfo, "Hello World!!!".getBytes(StandardCharsets.UTF_8));
	//Blob blob = storage.get(blobId);
	
	//String value = new String(blob.getContent());
	
	//List<BlobId> blobNamesInOutputBucket = new ArrayList<>();
	//storage.list("inventory_product_upload_health-compass-302720").iterateAll()
	//.forEach((b) -> blobNamesInOutputBucket.add(b.getBlobId()));
	//for (BlobId blobId : blobNamesInOutputBucket ) {
	//	Blob blob = storage.get(blobId);
		//System.out.println(blob.getContent());
	//}
	
	
//}

public static void Oldmain(String[] args) {
	final Validator<String> validator = new StringValidator() {
		public TemporalAmount getTimeToLive() {
            return Duration.ofSeconds(Instant.MAX.getEpochSecond());
        }
		};
	final Key key = new Key("CSMMNtPnLo9aE64V6XLuOZO-bYNslmFO3oFXF6RA-K8=");
	String phone="+919652236229";
	Token encodedPhone = Token.generate(key, phone);
	
	System.out.println("encoded phone:"+encodedPhone.serialise()+"");
	//Token decodedPhoneToken = Token.fromString("gAAAAABgecdA7BSxdx1097fyaAXGPRER5o-Yb0c2E683f7Mnz-8nZPoR4CcgLESJkPOUfi58-kUM07ny4LedHma7VEku4GPE7A==");
	
	Token decodedPhoneToken = Token.fromString(encodedPhone.serialise());
	
	String phonePayload = decodedPhoneToken.validateAndDecrypt(key, validator);
	System.out.println("decode phone:"+phonePayload+"");
}


}
