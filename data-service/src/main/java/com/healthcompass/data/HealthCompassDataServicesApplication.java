package com.healthcompass.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableConfigurationProperties
public class HealthCompassDataServicesApplication {

@Value("#{'${cors-allowed-origins}'.split(', ')}")
private List<String> allowedOrigins;
	

	public static void main(String[] args) {
		SpringApplication.run(HealthCompassDataServicesApplication.class, args);
	}
	
	
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistration() {

		// http://stackoverflow.com/questions/31724994/spring-data-rest-and-cors
		CorsConfiguration config = new CorsConfiguration();
		//config.setAllowCredentials(true);
		config.setAllowedOrigins(allowedOrigins);
		config.addAllowedOrigin("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("PATCH");
		config.addAllowedHeader("AUTHORIZATION");
		config.addAllowedHeader("X-Requested-With");
		config.addAllowedHeader("X-Auth-Token");
		config.addAllowedHeader("X-CLIENT-ID");
		config.addAllowedHeader("X-CLIENT_SECRET");
		config.addAllowedHeader("Content-Type");
		config.addAllowedHeader("User-tz");
		
		
		

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}
	
	@Bean
	public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
	    ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
	    arrayHttpMessageConverter.setSupportedMediaTypes(getSupportedMediaTypes());
	    return arrayHttpMessageConverter;
	}
	
	

	private List<MediaType> getSupportedMediaTypes() {
	    List<MediaType> list = new ArrayList<MediaType>();
	    list.add(MediaType.APPLICATION_OCTET_STREAM);
	    list.add(MediaType.APPLICATION_PDF);
	    list.add(MediaType.parseMediaType("application/pdf"));
	    return list;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate(); 
	}

	

	
	

}
