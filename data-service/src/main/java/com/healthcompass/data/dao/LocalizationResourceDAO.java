package com.healthcompass.data.dao;

import java.math.BigInteger;
import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dto.entity.assembler.LocalizationResourceEntityAsesmbler;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.HelpPageDefinition;
import com.healthcompass.data.model.HelpPageDetail;
import com.healthcompass.data.model.HelpPageDetailDefinition;
import com.healthcompass.data.model.HelpPageSection;
import com.healthcompass.data.model.HelpPageSectionDefinition;
import com.healthcompass.data.model.HelpPageSectionDetail;
import com.healthcompass.data.model.HelpPageSectionDetailDefinition;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.HelpPageRepository;
import com.healthcompass.data.repository.HelpPageSectionRepository;
import com.healthcompass.data.repository.LocalizationResourceGroupRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.MetaTagAttributesRepository;
import com.healthcompass.data.repository.MetaTagRepository;
import com.healthcompass.data.repository.ProductRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocalizedResourceSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentHistoryViewResultsetExtractor;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.HelpPageSectionDefinitionViewDTO;
import com.healthcompass.data.view.dto.HelpPageSectionDetailDefinitionViewDTO;
import com.healthcompass.data.view.dto.HelpPageSectionDetailViewDTO;
import com.healthcompass.data.view.dto.HelpPageSectionViewDTO;
import com.healthcompass.data.view.dto.LocalizationPageViewDTO;
import com.healthcompass.data.view.dto.LocalizationResourcePageResultsetExtractor;
import com.healthcompass.data.view.dto.LocalizedTextResultSetExtractor;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationViewResultSetExtrator;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewResultsetExtractor;
import com.healthcompass.data.view.dto.ResourceViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;



@Component
public class LocalizationResourceDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	
		
	@Autowired
	LocalizedTextResultSetExtractor localizedTextResultSetExtractor;
	
	@Autowired
	HelpPageRepository helpPageRepository;
	
	@Autowired
	HelpPageSectionRepository helpPageSectionRepository;
	
	
	@Autowired
	LocalizationResourceEntityAsesmbler localizationResourceEntityAsesmbler;

	@Autowired
	LocalizationResourcePageResultsetExtractor localizationResourcePageResultsetExtractor;
	
	
public List<ResourceViewDTO> getLocalizedTextForResources(LocalizedResourceSearchParams resourceSearchParams){
		
		
		Integer countryId = null;
		String languageCode=null;
		List<String> resourceIds=null;
		String countryCode=null;
		String andWhere = " ";
		boolean includeCountryIdInSearch=false;
		boolean includeLanguageCodeInSearch=false;
		boolean includeCountryCodeInSearch=false;
		boolean includeResourceIdsInSearch=false;
		
		countryId = resourceSearchParams.getCountryId();
		countryCode = resourceSearchParams.getCountryCode();	
		languageCode = resourceSearchParams.getLanguageCode();
		
		// let us do some conditional operations to derive on which key the search is performed out of country id, country ocde and language code
		try {
			if(countryId == null) { // check if country code is provided
				if(countryCode == null || countryCode.trim().length()== 0){ // check if language code is provided
					if(languageCode==null || languageCode.trim().length()== 0){  // none of country and language given. fall back to default US
						countryId=231;
						countryCode="US";
						languageCode="en";
						includeCountryIdInSearch=true;
						includeLanguageCodeInSearch=true;
						includeCountryCodeInSearch=true;
					}else { // language code is given but country id and country code are not given. so we search by language code
						includeLanguageCodeInSearch=true;
						includeCountryCodeInSearch=false;
						includeCountryIdInSearch=false;
					}
				}else { // country code is specified but not country id
					
					includeCountryCodeInSearch=true;
					
					if(languageCode==null || languageCode.trim().length()== 0){  // check if language code is given
						//only country code is given, country id and language code is not given
						includeLanguageCodeInSearch=false;
						includeCountryCodeInSearch=true;
						includeCountryIdInSearch=false;
						
					}else { // language code and country code is given but not country id
						
						includeLanguageCodeInSearch=true;
						includeCountryCodeInSearch=true;
						includeCountryIdInSearch=false;
					}
				}
			}else {  //  country id is given
				
				includeCountryIdInSearch=true;
				if(countryCode == null || countryCode.trim().length()== 0){ // country id is given, country code is not given
					if(languageCode==null || languageCode.trim().length()== 0){  
						// only country id given, languae code and country code not given
						includeLanguageCodeInSearch=false;
						includeCountryCodeInSearch=false;
						includeCountryIdInSearch=true;
						
					}else { // all country id, country code and language code given
						includeLanguageCodeInSearch=true;
						includeCountryCodeInSearch=false;
						includeCountryIdInSearch=true;
					}
				}else { // country code and country id given
					
					includeCountryCodeInSearch=true; 
					
					if(languageCode==null || languageCode.trim().length()== 0){  // none of country and language given. fall back to default US
						// country id and country code is given, labguage  code is not given
						includeCountryCodeInSearch=true; 
						includeCountryIdInSearch=true;
						includeLanguageCodeInSearch=false;
					}else { // language code, country code and country id given
						includeCountryCodeInSearch=true; 
						includeCountryIdInSearch=true;
						includeLanguageCodeInSearch=true;
					}
				}
				
			}
			
		}catch(Exception e) { // log the error and fall back to default US
			e.printStackTrace();
			countryId=231;
			countryCode="US";
			languageCode="en";
			includeCountryIdInSearch=true;
			includeLanguageCodeInSearch=true;
			includeCountryCodeInSearch=true;
		}
		
		
		
		try {
		    resourceIds = resourceSearchParams.getResourceIds();
			if(resourceIds != null && !resourceIds.isEmpty()) includeResourceIdsInSearch=true;
		}catch(Exception e) {
			resourceIds=null;
		}
		
		
		String LOCALIZED_TEXT_QUERY = "select res.id as id, res.resource_id as resourceId, lang.language_code as languageCode, lang.language as language\r\n" + 
				" , count.short_name as countryCode, count.name as country, res.text as text \r\n" + 
				" from project_asclepius.localization_resource res\r\n" + 
				" inner join project_asclepius.country_language lang on res.language_id=lang.id\r\n" + 
				" inner join project_asclepius.countries count on lang.country_id=count.id " ;
		
			
		 
		
		 if(includeResourceIdsInSearch){
			 String resourceIdInClause="( ";
			 int j = 1;
			 for(String resourceId : resourceIds) {
	    			
				 resourceIdInClause+="'"+resourceId+"' ";
	    			
    			if(j<resourceIds.size()) {
    				resourceIdInClause+=",";
    				j++;
    			}
    			
	    			
	    			
	    	}
			 resourceIdInClause+=")";
			 andWhere=" where ";
			 LOCALIZED_TEXT_QUERY += " " + andWhere +" res.resource_id in " + resourceIdInClause+" ";
			 andWhere = " and ";
			 
		 }else {
			 andWhere = " where ";
		 }
		         
         if(includeLanguageCodeInSearch){
			
			 LOCALIZED_TEXT_QUERY += " " + andWhere +" lang.language_code  = '" + languageCode +"' ";
			 andWhere = " and ";
			 
		 }
         
         if(includeCountryCodeInSearch){
			
			 LOCALIZED_TEXT_QUERY += " " + andWhere +" count.short_name  = '" + countryCode +"' ";
			 andWhere = " and ";
			 
		 }
         
         if(includeCountryIdInSearch){
			
			 LOCALIZED_TEXT_QUERY += " " + andWhere +" lang.country_id  = " + countryId +" ";
			
			 
		 }
         
         LOCALIZED_TEXT_QUERY += " order by countryCode asc,languageCode asc, resourceId asc";
		
				 
		
		
		return (List<ResourceViewDTO>) jdbcTemplate.query(LOCALIZED_TEXT_QUERY,localizedTextResultSetExtractor);
		
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<HelpPage> saveAllHelpPageFields(List<HelpPage> helpPages)
	{
		ObjectMapper mapper = new ObjectMapper();
		for(HelpPage helpPage : helpPages) {
			
			Optional<HelpPage> optHelpPage = helpPageRepository.findById(helpPage.getId());
			if(optHelpPage.isPresent()) {
				HelpPage existingHelpPage = optHelpPage.get();
				existingHelpPage = localizationResourceEntityAsesmbler.toHelpPageEntity(helpPage, existingHelpPage);
				helpPageRepository.save(existingHelpPage);
			}else {
				helpPageRepository.save(helpPage);
			}
			
			
			
			
		}
		
    	
    return helpPages ;
    	
	
	
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<HelpPageSection> saveAllHelpPageSections(List<HelpPageSection> helpPageSections)
	{
		ObjectMapper mapper = new ObjectMapper();
		for(HelpPageSection helpPageSection : helpPageSections) {
			
			Optional<HelpPageSection> optHelpPageSection = helpPageSectionRepository.findById(helpPageSection.getId());
			if(optHelpPageSection.isPresent()) {
				HelpPageSection existingHelpPageSection = optHelpPageSection.get();
				existingHelpPageSection = localizationResourceEntityAsesmbler.toHelpSectionEntity(helpPageSection, existingHelpPageSection);
				helpPageSectionRepository.save(existingHelpPageSection);
			}else {
				helpPageSectionRepository.save(helpPageSection);
			}
			
			
			
			
		}
		
    	
    return helpPageSections ;
    	
	
	
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public HelpPage findAll(String languageCode, String helpPageId)
	{
		
		Optional<HelpPage> optHelpPage = helpPageRepository.findById(helpPageId);
		HelpPage helpPage = optHelpPage.get();
		return helpPage;
	
	}
	
	
	public List<LocalizationPageViewDTO> getPersistedResourceGroups(Set<BigInteger> languageIds, Set<String> resourceIds,Set<String> categoryIds, Set<String> resourceGroupCodes){
		
		String newQuery =  "select a.* ,b.* from (select pg.id as pageId,pg.page_name as pageName,pg.sno as pageSerialNumber, \r\n" + 
				"	pd.language as language, pd.display_name as pageDisplayName, pd.description as pageDescription, \r\n" + 
				"	pdet.field_name as pageFieldName, pdet.position as pageFieldPosition, pdet.section_id as pageSectionLink,\r\n" + 
				"	pdetdef.display_name as pageFileldDisplayName, pdetdef.description as pageFieldDescription\r\n" + 
				" \r\n" + 
				"from\r\n" + 
				"project_asclepius.page pg\r\n" + 
				"inner join project_asclepius.page_definition pd on pd.page_id=pg.id\r\n" + 
				"inner join project_asclepius.page_detail pdet on pdet.page_id=pg.id\r\n" + 
				"left outer join project_asclepius.page_detail_definition pdetdef on pdet.id=pdetdef.page_detail_id ) a\r\n" + 
				"left outer join \r\n" + 
				"( select sec.id as sectionId,sec.section_name as sectionName,sec.sno as sectionSerialNumber, \r\n" + 
				" secdef.language as Sectionlanguage, secdef.display_name as sectionDisplayName, \r\n" + 
				" secdef.description as sectionDescription, secdet.field_name as sectionFieldName, secdet.position as sectionFieldPosition, secdetdef.display_name as sectionFileldDisplayName, secdetdef.description as sectionFieldDescription\r\n" + 
				"from project_asclepius.section sec\r\n" + 
				"inner join project_asclepius.section_definition secdef on secdef.section_id=sec.id\r\n" + 
				"inner join project_asclepius.section_detail secdet on secdet.section_id=sec.id\r\n" + 
				"inner join project_asclepius.section_detail_definition secdetdef on secdetdef.section_detail_id=secdet.id ) b\r\n" + 
				" on a.pageSectionLink=b.sectionId\r\n" + 
				" order by pageId asc, pageFieldPosition asc,sectionId asc,sectionFieldPosition asc\r\n" + 
				"\r\n" + 
				"";
		String languageIdInClause="( ";
		 int j = 1;
		 for(BigInteger id : languageIds) {
   			
			 languageIdInClause+=id+" ";
   			
			if(j<languageIds.size()) {
				languageIdInClause+=",";
				j++;
			}
			
   			
   			
		 }
		 languageIdInClause+=")";
		 
		 
		 String resourceIdInClause="( ";
		 int l = 1;
		 for(String resourceId : resourceIds) {
   			
			 resourceIdInClause+="'"+resourceId+"' ";
   			
			if(l<resourceIds.size()) {
				resourceIdInClause+=",";
				l++;
			}
			
   			
   			
		 }
		 resourceIdInClause+=")";
		 
		 String categoryIdInClause="( ";
		 int k = 1;
		 for(String resourceCategoryId : categoryIds) {
   			
			 categoryIdInClause+="'"+resourceCategoryId+"' ";
   			
			if(k<categoryIds.size()) {
				categoryIdInClause+=",";
				k++;
			}
			
   			
   			
		 }
		 categoryIdInClause+=")";
		 
		 String resourceGroupCodeInClause="( ";
		 int m = 1;
		 for(String resourceGroupCode : resourceGroupCodes) {
   			
			 resourceGroupCodeInClause+="'"+resourceGroupCode+"' ";
   			
			if(m<resourceGroupCodes.size()) {
				resourceGroupCodeInClause+=",";
				m++;
			}
			
   			
   			
		 }
		 resourceGroupCodeInClause+=")";
		 
		 String SELECT_PERSISTED_RESOURCES = "select field.resource_section_field_id as fieldId,field.text as text, lang.id as languageId,\r\n" + 
		 		"lang.language_code as languageCode, field.position as fieldPosition, "
		 		+ " section1.resource_section_id as resourceSectionId,section1. resource_section_code as resourceSectionCode, section1.resource_section_name as resourceSectionName, "
		 		+ "section1.position as resourceSectionPosition,res.resource_id as resourceId, "
		 		+ " res.resource_name as resourceName,res.resource_group_code as resourceGroupCode, "
				+ " res.resource_category_id as resourceCategoryId, cat.resource_category_name as resourceCategoryName, cat.resource_category_code as resourceCategoryCode, "
				+"  resgroup.resource_group_name as resourceGroupName "
				+ " from   project_asclepius.resource_section_field field "
				+" inner join   project_asclepius.country_language lang on field.language_id=lang.id "
				+" inner join  project_asclepius.resource_section section1 on "
				+" field.resource_section_id=section1.resource_section_id inner join "
				+ " project_asclepius.resource res on "
				+ "  section1.resource_id=res.resource_id "
				 +"  inner join project_asclepius.resource_category cat on res.resource_category_id=cat.resource_category_id "
				+" inner join project_asclepius.resource_group resgroup on res.resource_group_code=resgroup.resource_group_code "
				+" where res.language_id in "+languageIdInClause+" and cat.resource_category_id in "+categoryIdInClause+" and "
				+ "resgroup.resource_group_code in "+resourceGroupCodeInClause+" and res.resource_id in "+resourceIdInClause+ " " 
				+ " order by languageId asc,resourceGroupCode asc,resourceCategoryId asc,resourceId asc, resourceSectionPosition asc, fieldPosition asc ";


		 
		 
		

		
		return (List<LocalizationPageViewDTO>) jdbcTemplate.query(SELECT_PERSISTED_RESOURCES,localizationResourcePageResultsetExtractor);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String,Map<String,HelpPage>> getPersistedPagesForDisplay(Set<String> languageCodes, Set<String> helpPageIds)
	{
		//Map<languageCode, Map<page id, pageObject>>
		Map<String,Map<String,HelpPage>> helpPagesMemoryMap = new HashMap<String,Map<String,HelpPage>>();
		for(String langCode : languageCodes) {
			helpPagesMemoryMap.put(langCode, new HashMap<String,HelpPage>());
		}
		List<HelpPage> persistedDBHelpPages = (List<HelpPage>) helpPageRepository.findAllById(helpPageIds);
		
		String languageCode = null;
		for(HelpPage helpDBPage : persistedDBHelpPages) {
		//for(String pageId : helpPageIds) {
			//Optional<HelpPage> optHelpDBPage = helpPageRepository.findById(pageId);
			//HelpPage helpDBPage = optHelpDBPage.get();
			for(HelpPageDefinition pageDBDef : helpDBPage.getPageDefinitions()) {
				
				// first getht elanguage of page def
				String pageDefDBlanguageCode = pageDBDef.getLanguage();
				if(!helpPagesMemoryMap.containsKey(pageDefDBlanguageCode)) continue;
				
				 // check if the page object of that language is alreday inmain map
				 Map<String,HelpPage> languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(pageDefDBlanguageCode);
				 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
				 if(languageMemoryPage == null) { //  if the page specific to language does not exist, create it
					 languageMemoryPage = new HelpPage();
					 languageMemoryPage.setId(helpDBPage.getId());
					 languageMemoryPage.setPageName(helpDBPage.getPageName());
					 languageMemoryPage.setType(helpDBPage.getType());
					 languageMemoryPage.setSerialNumber(helpDBPage.getSerialNumber());
					 languageMemoryPage.setPageDefinitions(new ArrayList<HelpPageDefinition>());
					 languageMemoryMap.put(languageMemoryPage.getId(),languageMemoryPage);
				 }
				 // add only page definitions specific to labguage,
				 HelpPageDefinition pageMemoryDefinition = new HelpPageDefinition();
				 pageMemoryDefinition.setId(pageDBDef.getId());
				 pageMemoryDefinition.setDisplayName(pageDBDef.getDisplayName());
				 pageMemoryDefinition.setDescription(pageDBDef.getDescription());
				 pageMemoryDefinition.setLanguage(pageDBDef.getLanguage());
				 languageMemoryPage.getPageDefinitions().add(pageMemoryDefinition);
			} // end page definition	 
				 
			
			for(HelpPageDetail pageDBDet : helpDBPage.getDetails()) {
				boolean isSection = checkAndPopulateSectionForDisplay(pageDBDet, helpDBPage, helpPagesMemoryMap);
				if(isSection) continue;
				if(!pageDBDet.getIsDisplay()) continue; // we do not include the fields in help page whose display property is false
				for(HelpPageDetailDefinition detDBDef : pageDBDet.getDetailDefinitions()) {
						String detDefDBLanguageCode = detDBDef.getLanguage();
						if(!helpPagesMemoryMap.containsKey(detDefDBLanguageCode)) continue;
						
						 // check if the page object of that language is alreday inmain map
						 Map<String,HelpPage> languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(detDefDBLanguageCode);
						 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
						 
						 
						 List<HelpPageDetail> pageDetailMemoryList = languageMemoryPage.getDetails();
						 if(pageDetailMemoryList == null) {
							 pageDetailMemoryList = new ArrayList<HelpPageDetail>();
							 languageMemoryPage.setDetails(pageDetailMemoryList);
						 }
						 HelpPageDetail newPageDetail = new HelpPageDetail();
						 newPageDetail.setId(pageDBDet.getId());
						 newPageDetail.setFieldName(pageDBDet.getFieldName());
						 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
						 newPageDetail.setPosition(pageDBDet.getPosition());
						 newPageDetail.setDataType(pageDBDet.getDataType());
						 newPageDetail.setSectionId("DUMMY");
						 newPageDetail.setDetailDefinitions(new ArrayList<HelpPageDetailDefinition>());
						// add only page definitions specific to labguage,
						 HelpPageDetailDefinition newHelPageDetDefinition = new HelpPageDetailDefinition();
						 newHelPageDetDefinition.setId(detDBDef.getId());
						 newHelPageDetDefinition.setDisplayName(detDBDef.getDisplayName());
						 newHelPageDetDefinition.setDescription(detDBDef.getDescription());
						 newHelPageDetDefinition.setLanguage(detDBDef.getLanguage());
						 newPageDetail.getDetailDefinitions().add(newHelPageDetDefinition);
						 languageMemoryPage.getDetails().add(newPageDetail);
						 
						
				} //end page details definitions
			}	// end page details	
			// commeneted section will be handled in separate method in order to save sort order
			/*for(HelpPageDetail pageDBDet : helpDBPage.getDetails()) {
				 if(pageDBDet.getSectionId() != null && !pageDBDet.getSectionId().isEmpty()) {
					 
					 
					 
					 Optional<HelpPageSection> optDBSection = helpPageSectionRepository.findById(pageDBDet.getSectionId());
					 
					 HelpPageSection dbSection = optDBSection.get();
					 for(HelpPageSectionDefinition sectionDBDef : dbSection.getSectionDefinitions()) {
						 String sectionDeflanguageDBCode = sectionDBDef.getLanguage();
						 Map<String,HelpPage>  languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(sectionDeflanguageDBCode);
						 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
						 HelpPageDetail newPageDetail = new HelpPageDetail();
						 newPageDetail.setId(pageDBDet.getId());
						 newPageDetail.setFieldName(pageDBDet.getFieldName());
						 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
						 newPageDetail.setPosition(pageDBDet.getPosition());
						 newPageDetail.setSectionId(pageDBDet.getSectionId());
						 newPageDetail.setSectionName(pageDBDet.getSectionName());
						 languageMemoryPage.getDetails().add(newPageDetail);
						 //for(HelpPageDetail pageDetMemory : languageMemoryPage.getDetails()) {
							 //if(pageDetMemory.getId().equals(pageDBDet.getId())) {
								 
								 HelpPageSectionViewDTO newMemorySection = new HelpPageSectionViewDTO();
								 newMemorySection.setId(dbSection.getId());
								 newMemorySection.setSectionName(dbSection.getSectionName());
								 newMemorySection.setSerialNumber(dbSection.getSerialNumber());
								 newMemorySection.setSectionDefinitions(new ArrayList<HelpPageSectionDefinitionViewDTO>());
								 
								 HelpPageSectionDefinitionViewDTO newHelPageSectionMemoryDefinition = new HelpPageSectionDefinitionViewDTO();
								 newHelPageSectionMemoryDefinition.setId(sectionDBDef.getId());
								 newHelPageSectionMemoryDefinition.setDisplayName(sectionDBDef.getDisplayName());
								 newHelPageSectionMemoryDefinition.setDescription(sectionDBDef.getDescription());
								 newHelPageSectionMemoryDefinition.setLanguage(sectionDBDef.getLanguage());
								 
								 newMemorySection.getSectionDefinitions().add(newHelPageSectionMemoryDefinition);
								 newMemorySection.setSectionDetail(new ArrayList<HelpPageSectionDetailViewDTO>());
								 newPageDetail.setSection(newMemorySection);
								 
								 for(HelpPageSectionDetail sectionDBDet : dbSection.getSectionDetail()) {
									 for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet .getSectionDetailDefinitions()) {
										 String sectionDetDefDBLanguageCode = sectionDetDBDef .getLanguage();
										 if(sectionDetDefDBLanguageCode.equals(sectionDeflanguageDBCode)) {
											 
											 HelpPageSectionDetailViewDTO newPageSectionDetailMemory = new HelpPageSectionDetailViewDTO();
											 newPageSectionDetailMemory.setId(sectionDBDet.getId());
											 newPageSectionDetailMemory.setFieldName(sectionDBDet.getFieldName());
											 newPageSectionDetailMemory.setIsDisplay(sectionDBDet.getIsDisplay());
											 newPageSectionDetailMemory.setPosition(sectionDBDet.getPosition());
											 newPageSectionDetailMemory.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinitionViewDTO>());
											 
											// add only page definitions specific to labguage,
											 HelpPageSectionDetailDefinitionViewDTO newHelPageSectionDetDefinitionMemory = new HelpPageSectionDetailDefinitionViewDTO();
											 newHelPageSectionDetDefinitionMemory.setId(sectionDetDBDef.getId());
											 newHelPageSectionDetDefinitionMemory.setDisplayName(sectionDetDBDef.getDisplayName());
											 newHelPageSectionDetDefinitionMemory.setDescription(sectionDetDBDef.getDescription());
											 newHelPageSectionDetDefinitionMemory.setLanguage(sectionDetDBDef.getLanguage());
											 newPageSectionDetailMemory.getSectionDetailDefinitions().add(newHelPageSectionDetDefinitionMemory);
											 newMemorySection.getSectionDetail().add(newPageSectionDetailMemory);
											 
										 }
										 
									 }
											
									 
								 }
								 
							// }
						// }
						 
					 }
				 }
				
			}*/ //  commented untill here as section will be handled in separate method to save the order
						 
						
					
			
		}
		return helpPagesMemoryMap;
	
	}
	

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String,Map<String,HelpPage>> getPersistedPagesForDownload(Set<String> languageCodes, Set<String> helpPageIds)
	{
		//Map<languageCode, Map<page id, pageObject>>
		Map<String,Map<String,HelpPage>> helpPagesMemoryMap = new HashMap<String,Map<String,HelpPage>>();
		for(String langCode : languageCodes) {
			helpPagesMemoryMap.put(langCode, new HashMap<String,HelpPage>());
		}
		List<HelpPage> persistedDBHelpPages = (List<HelpPage>) helpPageRepository.findAllById(helpPageIds);
		
		String languageCode = null;
		for(HelpPage helpDBPage : persistedDBHelpPages) {
		//for(String pageId : helpPageIds) {
			//Optional<HelpPage> optHelpDBPage = helpPageRepository.findById(pageId);
			//HelpPage helpDBPage = optHelpDBPage.get();
			for(HelpPageDefinition pageDBDef : helpDBPage.getPageDefinitions()) {
				
				// first getht elanguage of page def
				String pageDefDBlanguageCode = pageDBDef.getLanguage();
				if(!helpPagesMemoryMap.containsKey(pageDefDBlanguageCode)) continue;
				
				 // check if the page object of that language is alreday inmain map
				 Map<String,HelpPage> languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(pageDefDBlanguageCode);
				 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
				 if(languageMemoryPage == null) { //  if the page specific to language does not exist, create it
					 languageMemoryPage = new HelpPage();
					 languageMemoryPage.setId(helpDBPage.getId());
					 languageMemoryPage.setPageName(helpDBPage.getPageName());
					 languageMemoryPage.setType(helpDBPage.getType());
					 languageMemoryPage.setSerialNumber(helpDBPage.getSerialNumber());
					 languageMemoryPage.setPageDefinitions(new ArrayList<HelpPageDefinition>());
					 languageMemoryMap.put(languageMemoryPage.getId(),languageMemoryPage);
				 }
				 // add only page definitions specific to labguage,
				 HelpPageDefinition pageMemoryDefinition = new HelpPageDefinition();
				 pageMemoryDefinition.setId(pageDBDef.getId());
				 pageMemoryDefinition.setDisplayName(pageDBDef.getDisplayName());
				 pageMemoryDefinition.setDescription(pageDBDef.getDescription());
				 pageMemoryDefinition.setLanguage(pageDBDef.getLanguage());
				 languageMemoryPage.getPageDefinitions().add(pageMemoryDefinition);
			} // end page definition	 
				 
			
			for(HelpPageDetail pageDBDet : helpDBPage.getDetails()) {
				boolean isSection = checkAndPopulateSectionForDownload(pageDBDet, helpDBPage, helpPagesMemoryMap);
				if(isSection) continue;
				for(HelpPageDetailDefinition detDBDef : pageDBDet.getDetailDefinitions()) {
						String detDefDBLanguageCode = detDBDef.getLanguage();
						if(!helpPagesMemoryMap.containsKey(detDefDBLanguageCode)) continue;
						
						 // check if the page object of that language is alreday inmain map
						 Map<String,HelpPage> languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(detDefDBLanguageCode);
						 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
						 
						 
						 List<HelpPageDetail> pageDetailMemoryList = languageMemoryPage.getDetails();
						 if(pageDetailMemoryList == null) {
							 pageDetailMemoryList = new ArrayList<HelpPageDetail>();
							 languageMemoryPage.setDetails(pageDetailMemoryList);
						 }
						 HelpPageDetail newPageDetail = new HelpPageDetail();
						 newPageDetail.setId(pageDBDet.getId());
						 newPageDetail.setFieldName(pageDBDet.getFieldName());
						 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
						 newPageDetail.setPosition(pageDBDet.getPosition());
						 newPageDetail.setDataType(pageDBDet.getDataType());
						 newPageDetail.setSectionId("DUMMY");
						 newPageDetail.setDetailDefinitions(new ArrayList<HelpPageDetailDefinition>());
						// add only page definitions specific to labguage,
						 HelpPageDetailDefinition newHelPageDetDefinition = new HelpPageDetailDefinition();
						 newHelPageDetDefinition.setId(detDBDef.getId());
						 newHelPageDetDefinition.setDisplayName(detDBDef.getDisplayName());
						 newHelPageDetDefinition.setDescription(detDBDef.getDescription());
						 newHelPageDetDefinition.setLanguage(detDBDef.getLanguage());
						 newPageDetail.getDetailDefinitions().add(newHelPageDetDefinition);
						 languageMemoryPage.getDetails().add(newPageDetail);
						 
						
				} //end page details definitions
			}	// end page details	
			// commeneted section will be handled in separate method in order to save sort order
			/*for(HelpPageDetail pageDBDet : helpDBPage.getDetails()) {
				 if(pageDBDet.getSectionId() != null && !pageDBDet.getSectionId().isEmpty()) {
					 
					 
					 
					 Optional<HelpPageSection> optDBSection = helpPageSectionRepository.findById(pageDBDet.getSectionId());
					 
					 HelpPageSection dbSection = optDBSection.get();
					 for(HelpPageSectionDefinition sectionDBDef : dbSection.getSectionDefinitions()) {
						 String sectionDeflanguageDBCode = sectionDBDef.getLanguage();
						 Map<String,HelpPage>  languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(sectionDeflanguageDBCode);
						 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
						 HelpPageDetail newPageDetail = new HelpPageDetail();
						 newPageDetail.setId(pageDBDet.getId());
						 newPageDetail.setFieldName(pageDBDet.getFieldName());
						 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
						 newPageDetail.setPosition(pageDBDet.getPosition());
						 newPageDetail.setSectionId(pageDBDet.getSectionId());
						 newPageDetail.setSectionName(pageDBDet.getSectionName());
						 languageMemoryPage.getDetails().add(newPageDetail);
						 //for(HelpPageDetail pageDetMemory : languageMemoryPage.getDetails()) {
							 //if(pageDetMemory.getId().equals(pageDBDet.getId())) {
								 
								 HelpPageSectionViewDTO newMemorySection = new HelpPageSectionViewDTO();
								 newMemorySection.setId(dbSection.getId());
								 newMemorySection.setSectionName(dbSection.getSectionName());
								 newMemorySection.setSerialNumber(dbSection.getSerialNumber());
								 newMemorySection.setSectionDefinitions(new ArrayList<HelpPageSectionDefinitionViewDTO>());
								 
								 HelpPageSectionDefinitionViewDTO newHelPageSectionMemoryDefinition = new HelpPageSectionDefinitionViewDTO();
								 newHelPageSectionMemoryDefinition.setId(sectionDBDef.getId());
								 newHelPageSectionMemoryDefinition.setDisplayName(sectionDBDef.getDisplayName());
								 newHelPageSectionMemoryDefinition.setDescription(sectionDBDef.getDescription());
								 newHelPageSectionMemoryDefinition.setLanguage(sectionDBDef.getLanguage());
								 
								 newMemorySection.getSectionDefinitions().add(newHelPageSectionMemoryDefinition);
								 newMemorySection.setSectionDetail(new ArrayList<HelpPageSectionDetailViewDTO>());
								 newPageDetail.setSection(newMemorySection);
								 
								 for(HelpPageSectionDetail sectionDBDet : dbSection.getSectionDetail()) {
									 for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet .getSectionDetailDefinitions()) {
										 String sectionDetDefDBLanguageCode = sectionDetDBDef .getLanguage();
										 if(sectionDetDefDBLanguageCode.equals(sectionDeflanguageDBCode)) {
											 
											 HelpPageSectionDetailViewDTO newPageSectionDetailMemory = new HelpPageSectionDetailViewDTO();
											 newPageSectionDetailMemory.setId(sectionDBDet.getId());
											 newPageSectionDetailMemory.setFieldName(sectionDBDet.getFieldName());
											 newPageSectionDetailMemory.setIsDisplay(sectionDBDet.getIsDisplay());
											 newPageSectionDetailMemory.setPosition(sectionDBDet.getPosition());
											 newPageSectionDetailMemory.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinitionViewDTO>());
											 
											// add only page definitions specific to labguage,
											 HelpPageSectionDetailDefinitionViewDTO newHelPageSectionDetDefinitionMemory = new HelpPageSectionDetailDefinitionViewDTO();
											 newHelPageSectionDetDefinitionMemory.setId(sectionDetDBDef.getId());
											 newHelPageSectionDetDefinitionMemory.setDisplayName(sectionDetDBDef.getDisplayName());
											 newHelPageSectionDetDefinitionMemory.setDescription(sectionDetDBDef.getDescription());
											 newHelPageSectionDetDefinitionMemory.setLanguage(sectionDetDBDef.getLanguage());
											 newPageSectionDetailMemory.getSectionDetailDefinitions().add(newHelPageSectionDetDefinitionMemory);
											 newMemorySection.getSectionDetail().add(newPageSectionDetailMemory);
											 
										 }
										 
									 }
											
									 
								 }
								 
							// }
						// }
						 
					 }
				 }
				
			}*/ //  commented untill here as section will be handled in separate method to save the order
						 
						
					
			
		}
		return helpPagesMemoryMap;
	
	}
	 
	 public boolean checkAndPopulateSectionForDisplay (HelpPageDetail pageDBDet,HelpPage helpDBPage,Map<String,Map<String,HelpPage>> helpPagesMemoryMap) {
		 boolean isSection=false;
		 if(pageDBDet.getSectionId() != null && !pageDBDet.getSectionId().isEmpty()) {
			 isSection=true;
		 }else {
			 return isSection;
		 }
		 Optional<HelpPageSection> optDBSection = helpPageSectionRepository.findById(pageDBDet.getSectionId());
		 
		 HelpPageSection dbSection = optDBSection.get();
		 for(HelpPageSectionDefinition sectionDBDef : dbSection.getSectionDefinitions()) {
			 String sectionDeflanguageDBCode = sectionDBDef.getLanguage();
			 if(!helpPagesMemoryMap.containsKey(sectionDeflanguageDBCode)) continue;
			 Map<String,HelpPage>  languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(sectionDeflanguageDBCode);
			 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
			 HelpPageDetail newPageDetail = new HelpPageDetail();
			 newPageDetail.setId(pageDBDet.getId());
			 newPageDetail.setFieldName(pageDBDet.getFieldName());
			 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
			 newPageDetail.setPosition(pageDBDet.getPosition());
			 newPageDetail.setSectionId(pageDBDet.getSectionId());
			 newPageDetail.setSectionName(pageDBDet.getSectionName());
			 List<HelpPageDetail> pageDetailMemoryList = languageMemoryPage.getDetails();
			 if(pageDetailMemoryList == null) {
				 pageDetailMemoryList = new ArrayList<HelpPageDetail>();
				 languageMemoryPage.setDetails(pageDetailMemoryList);
			 }
			 languageMemoryPage.getDetails().add(newPageDetail);
			 //for(HelpPageDetail pageDetMemory : languageMemoryPage.getDetails()) {
				 //if(pageDetMemory.getId().equals(pageDBDet.getId())) {
					 
					 HelpPageSectionViewDTO newMemorySection = new HelpPageSectionViewDTO();
					 newMemorySection.setId(dbSection.getId());
					 newMemorySection.setSectionName(dbSection.getSectionName());
					 newMemorySection.setSerialNumber(dbSection.getSerialNumber());
					 newMemorySection.setType(dbSection.getType());
					 newMemorySection.setSectionDefinitions(new ArrayList<HelpPageSectionDefinitionViewDTO>());
					 
					 HelpPageSectionDefinitionViewDTO newHelPageSectionMemoryDefinition = new HelpPageSectionDefinitionViewDTO();
					 newHelPageSectionMemoryDefinition.setId(sectionDBDef.getId());
					 newHelPageSectionMemoryDefinition.setDisplayName(sectionDBDef.getDisplayName());
					 newHelPageSectionMemoryDefinition.setDescription(sectionDBDef.getDescription());
					 newHelPageSectionMemoryDefinition.setLanguage(sectionDBDef.getLanguage());
					 
					 newMemorySection.getSectionDefinitions().add(newHelPageSectionMemoryDefinition);
					 newMemorySection.setSectionDetail(new ArrayList<HelpPageSectionDetailViewDTO>());
					 newPageDetail.setSection(newMemorySection);
					 
					 for(HelpPageSectionDetail sectionDBDet : dbSection.getSectionDetail()) {
						 if(!sectionDBDet.getIsDisplay()) continue;
						 for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet .getSectionDetailDefinitions()) {
							 String sectionDetDefDBLanguageCode = sectionDetDBDef .getLanguage();
							 if(!helpPagesMemoryMap.containsKey(sectionDetDefDBLanguageCode)) continue;
							 if(sectionDetDefDBLanguageCode.equals(sectionDeflanguageDBCode)) {
								 
								 HelpPageSectionDetailViewDTO newPageSectionDetailMemory = new HelpPageSectionDetailViewDTO();
								 newPageSectionDetailMemory.setId(sectionDBDet.getId());
								 newPageSectionDetailMemory.setFieldName(sectionDBDet.getFieldName());
								 newPageSectionDetailMemory.setIsDisplay(sectionDBDet.getIsDisplay());
								 newPageSectionDetailMemory.setPosition(sectionDBDet.getPosition());
								 newPageSectionDetailMemory.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinitionViewDTO>());
								 
								// add only page definitions specific to labguage,
								 HelpPageSectionDetailDefinitionViewDTO newHelPageSectionDetDefinitionMemory = new HelpPageSectionDetailDefinitionViewDTO();
								 newHelPageSectionDetDefinitionMemory.setId(sectionDetDBDef.getId());
								 newHelPageSectionDetDefinitionMemory.setDisplayName(sectionDetDBDef.getDisplayName());
								 newHelPageSectionDetDefinitionMemory.setDescription(sectionDetDBDef.getDescription());
								 newHelPageSectionDetDefinitionMemory.setLanguage(sectionDetDBDef.getLanguage());
								 newPageSectionDetailMemory.getSectionDetailDefinitions().add(newHelPageSectionDetDefinitionMemory);
								 newMemorySection.getSectionDetail().add(newPageSectionDetailMemory);
								 
							 }
							 
						 }
								
						 
					 }
					 
				// }
			// }
			 
		 }
		 
		 return isSection;
	 }
	 
	 public boolean checkAndPopulateSectionForDownload(HelpPageDetail pageDBDet,HelpPage helpDBPage,Map<String,Map<String,HelpPage>> helpPagesMemoryMap) {
		 boolean isSection=false;
		 if(pageDBDet.getSectionId() != null && !pageDBDet.getSectionId().isEmpty()) {
			 isSection=true;
		 }else {
			 return isSection;
		 }
		 Optional<HelpPageSection> optDBSection = helpPageSectionRepository.findById(pageDBDet.getSectionId());
		 
		 HelpPageSection dbSection = optDBSection.get();
		 for(HelpPageSectionDefinition sectionDBDef : dbSection.getSectionDefinitions()) {
			 String sectionDeflanguageDBCode = sectionDBDef.getLanguage();
			 if(!helpPagesMemoryMap.containsKey(sectionDeflanguageDBCode)) continue;
			 Map<String,HelpPage>  languageMemoryMap = (Map<String,HelpPage>) helpPagesMemoryMap.get(sectionDeflanguageDBCode);
			 HelpPage languageMemoryPage = (HelpPage)languageMemoryMap.get(helpDBPage.getId());
			 HelpPageDetail newPageDetail = new HelpPageDetail();
			 newPageDetail.setId(pageDBDet.getId());
			 newPageDetail.setFieldName(pageDBDet.getFieldName());
			 newPageDetail.setIsDisplay(pageDBDet.getIsDisplay());
			 newPageDetail.setPosition(pageDBDet.getPosition());
			 newPageDetail.setSectionId(pageDBDet.getSectionId());
			 newPageDetail.setSectionName(pageDBDet.getSectionName());
			 languageMemoryPage.getDetails().add(newPageDetail);
			 //for(HelpPageDetail pageDetMemory : languageMemoryPage.getDetails()) {
				 //if(pageDetMemory.getId().equals(pageDBDet.getId())) {
					 
					 HelpPageSectionViewDTO newMemorySection = new HelpPageSectionViewDTO();
					 newMemorySection.setId(dbSection.getId());
					 newMemorySection.setSectionName(dbSection.getSectionName());
					 newMemorySection.setSerialNumber(dbSection.getSerialNumber());
					 newMemorySection.setType(dbSection.getType());
					 newMemorySection.setSectionDefinitions(new ArrayList<HelpPageSectionDefinitionViewDTO>());
					 
					 HelpPageSectionDefinitionViewDTO newHelPageSectionMemoryDefinition = new HelpPageSectionDefinitionViewDTO();
					 newHelPageSectionMemoryDefinition.setId(sectionDBDef.getId());
					 newHelPageSectionMemoryDefinition.setDisplayName(sectionDBDef.getDisplayName());
					 newHelPageSectionMemoryDefinition.setDescription(sectionDBDef.getDescription());
					 newHelPageSectionMemoryDefinition.setLanguage(sectionDBDef.getLanguage());
					 
					 newMemorySection.getSectionDefinitions().add(newHelPageSectionMemoryDefinition);
					 newMemorySection.setSectionDetail(new ArrayList<HelpPageSectionDetailViewDTO>());
					 newPageDetail.setSection(newMemorySection);
					 
					 for(HelpPageSectionDetail sectionDBDet : dbSection.getSectionDetail()) {
						 for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet .getSectionDetailDefinitions()) {
							 String sectionDetDefDBLanguageCode = sectionDetDBDef .getLanguage();
							 if(!helpPagesMemoryMap.containsKey(sectionDetDefDBLanguageCode)) continue;
							 if(sectionDetDefDBLanguageCode.equals(sectionDeflanguageDBCode)) {
								 
								 HelpPageSectionDetailViewDTO newPageSectionDetailMemory = new HelpPageSectionDetailViewDTO();
								 newPageSectionDetailMemory.setId(sectionDBDet.getId());
								 newPageSectionDetailMemory.setFieldName(sectionDBDet.getFieldName());
								 newPageSectionDetailMemory.setIsDisplay(sectionDBDet.getIsDisplay());
								 newPageSectionDetailMemory.setPosition(sectionDBDet.getPosition());
								 newPageSectionDetailMemory.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinitionViewDTO>());
								 
								// add only page definitions specific to labguage,
								 HelpPageSectionDetailDefinitionViewDTO newHelPageSectionDetDefinitionMemory = new HelpPageSectionDetailDefinitionViewDTO();
								 newHelPageSectionDetDefinitionMemory.setId(sectionDetDBDef.getId());
								 newHelPageSectionDetDefinitionMemory.setDisplayName(sectionDetDBDef.getDisplayName());
								 newHelPageSectionDetDefinitionMemory.setDescription(sectionDetDBDef.getDescription());
								 newHelPageSectionDetDefinitionMemory.setLanguage(sectionDetDBDef.getLanguage());
								 newPageSectionDetailMemory.getSectionDetailDefinitions().add(newHelPageSectionDetDefinitionMemory);
								 newMemorySection.getSectionDetail().add(newPageSectionDetailMemory);
								 
							 }
							 
						 }
								
						 
					 }
					 
				// }
			// }
			 
		 }
		 
		 return isSection;
	 }
	
	   	   
	   @Transactional(propagation = Propagation.REQUIRES_NEW)
		public Map<String,Map<String,HelpPageSection>> getPersistedSectionsForDownload(Set<String> languageCodes, Set<String> helpPageSectionIds)
		{
			//Map<languageCode, Map<page id, pageObject>>
			Map<String,Map<String,HelpPageSection>> sectionMemoryMap = new HashMap<String,Map<String,HelpPageSection>>();
			for(String langCode : languageCodes) {
				sectionMemoryMap.put(langCode, new HashMap<String,HelpPageSection>());
			}
			List<HelpPageSection> persistedDBSections = (List<HelpPageSection>) helpPageSectionRepository.findAllByIdOrderBySerialNumberAsc(helpPageSectionIds);
			
			String languageCode = null;
			for(HelpPageSection helpDBSection : persistedDBSections) {
			//for(String pageId : helpPageIds) {
				//Optional<HelpPage> optHelpDBPage = helpPageRepository.findById(pageId);
				//HelpPage helpDBPage = optHelpDBPage.get();
				for(HelpPageSectionDefinition sectionDBDef : helpDBSection.getSectionDefinitions()) {
					
					// first getht elanguage of page def
					String sectionDefDBlanguageCode = sectionDBDef.getLanguage();
					if(!sectionMemoryMap.containsKey(sectionDefDBlanguageCode)) continue;
					 
					 // check if the page object of that language is alreday inmain map
					 Map<String,HelpPageSection> languageMemoryMap = (Map<String,HelpPageSection>) sectionMemoryMap.get(sectionDefDBlanguageCode);
					 HelpPageSection languageMemorySection = (HelpPageSection) languageMemoryMap.get(helpDBSection.getId());
					 if(languageMemorySection == null) { //  if the page specific to language does not exist, create it
						 languageMemorySection = new HelpPageSection();
						 languageMemorySection.setId(helpDBSection.getId());
						 languageMemorySection.setSectionName(helpDBSection.getSectionName());
						 languageMemorySection.setType(helpDBSection.getType());
						 languageMemorySection.setSerialNumber(helpDBSection.getSerialNumber());
						 languageMemorySection.setSectionDefinitions(new ArrayList<HelpPageSectionDefinition>());
						 languageMemoryMap.put(languageMemorySection.getId(),languageMemorySection);
					 }
					 // add only page definitions specific to labguage,
					 HelpPageSectionDefinition sectionMemoryDefinition = new HelpPageSectionDefinition();
					 sectionMemoryDefinition.setId(sectionDBDef.getId());
					 sectionMemoryDefinition.setDisplayName(sectionDBDef.getDisplayName());
					 sectionMemoryDefinition.setDescription(sectionDBDef.getDescription());
					 sectionMemoryDefinition.setLanguage(sectionDBDef.getLanguage());
					 languageMemorySection.getSectionDefinitions().add(sectionMemoryDefinition);
				} // end page definition	 
					 
				
				for(HelpPageSectionDetail sectionDBDet : helpDBSection.getSectionDetail()) {
					
					for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet.getSectionDetailDefinitions()) {
							String sectionDetDefDBLanguageCode = sectionDetDBDef.getLanguage();
							if(!sectionMemoryMap.containsKey(sectionDetDefDBLanguageCode)) continue;
							 // check if the page object of that language is alreday inmain map
							 Map<String,HelpPageSection> languageMemoryMap = (Map<String,HelpPageSection>) sectionMemoryMap.get(sectionDetDefDBLanguageCode);
							 HelpPageSection languageMemorySection = (HelpPageSection) languageMemoryMap.get(helpDBSection.getId());
							 
							 
							 List<HelpPageSectionDetail> sectionDetailMemoryList = languageMemorySection.getSectionDetail();
							 if(sectionDetailMemoryList == null) {
								 sectionDetailMemoryList = new ArrayList<HelpPageSectionDetail>();
								 languageMemorySection.setSectionDetail(sectionDetailMemoryList);
							 }
							 HelpPageSectionDetail newSectionDetail = new HelpPageSectionDetail();
							 newSectionDetail.setId(sectionDBDet.getId());
							 newSectionDetail.setFieldName(sectionDBDet.getFieldName());
							 newSectionDetail.setIsDisplay(sectionDBDet.getIsDisplay());
							 newSectionDetail.setPosition(sectionDBDet.getPosition());
							 newSectionDetail.setSelfReferrence(sectionDBDet.getSelfReferrence());
							 newSectionDetail.setSelfReferrenceId(sectionDBDet.getSelfReferrenceId());
							 newSectionDetail.setSectionName(sectionDBDet.getSectionName());
							
							 newSectionDetail.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinition>());
							// add only page definitions specific to labguage,
							 HelpPageSectionDetailDefinition newHelPageSectionDetDefinition = new HelpPageSectionDetailDefinition();
							 newHelPageSectionDetDefinition.setId(sectionDetDBDef.getId());
							 newHelPageSectionDetDefinition.setDisplayName(sectionDetDBDef.getDisplayName());
							 newHelPageSectionDetDefinition.setDescription(sectionDetDBDef.getDescription());
							 newHelPageSectionDetDefinition.setLanguage(sectionDetDBDef.getLanguage());
							 newSectionDetail.getSectionDetailDefinitions().add(newHelPageSectionDetDefinition);
							 languageMemorySection.getSectionDetail().add(newSectionDetail);
							 
							
					} //end section details definitions
				}	// end section details	
				
							 
							
						
				
			}
			return sectionMemoryMap;
		
		}
	   
	   @Transactional(propagation = Propagation.REQUIRES_NEW)
		public Map<String,String> getAllPageLanguages()
		{
			
		   List<Object[]> availablePageLanguages = (List<Object[]>) helpPageRepository.getAvailablePageLanguages();
		   HashMap<String,String> pageLanguageMap = null;
		   for(Object[] language : availablePageLanguages) {
			   if(pageLanguageMap == null) {
				   pageLanguageMap = new HashMap<String,String>();
				   
			   }
			   pageLanguageMap.put((String) language[0] , (String) language[1]);
		   }
		   return pageLanguageMap;
		   
		}
		
	   @Transactional(propagation = Propagation.REQUIRES_NEW)
		public Map<String,String> getAllSectionLanguages()
		{
			
		   List<Object[]> availableSectionLanguages = (List<Object[]>) helpPageSectionRepository.getAvailableSectionLanguages();
		   HashMap<String,String> sectionLanguageMap = null;
		   for(Object[] language : availableSectionLanguages) {
			   if(sectionLanguageMap == null) {
				   sectionLanguageMap = new HashMap<String,String>();
				   
			   }
			   sectionLanguageMap.put((String) language[0] , (String) language[1]);
		   }
		   return sectionLanguageMap;
		   
		}
	   
	   
	   @Transactional(propagation = Propagation.REQUIRES_NEW)
		public Map<String,String> getAllPageNames()
		{
			
		   List<Object[]> availablePageNames = (List<Object[]>) helpPageRepository.getAvailablePageNames();
		   HashMap<String,String> pageMapUnsorted = null;
		   for(Object[] page : availablePageNames) {
			   if(pageMapUnsorted == null) {
				   pageMapUnsorted = new HashMap<String,String>();
				   
			   }
			   pageMapUnsorted.put((String) page[0] , (String) page[1]);
		   }
		   Map<String, String> pageMap = new TreeMap<String, String>(pageMapUnsorted);
		   return pageMap;
		   
		}
	   
	   @Transactional(propagation = Propagation.REQUIRES_NEW)
		public Map<String,String> getAllSectionNames()
		{
			
		   List<Object[]> availableSectionNames = (List<Object[]>) helpPageSectionRepository.getAvailableSectionNames();
		   HashMap<String,String> sectionMapUnsorted = null;
		   for(Object[] section : availableSectionNames) {
			   if(sectionMapUnsorted == null) {
				   sectionMapUnsorted = new HashMap<String,String>();
				   
			   }
			   sectionMapUnsorted.put((String) section[0] , (String) section[1]);
		   }
		   
		   Map<String, String> sectionMap = new TreeMap<String, String>(sectionMapUnsorted);
		   return sectionMap;
		   
		}
		
	  	
}
	
	
