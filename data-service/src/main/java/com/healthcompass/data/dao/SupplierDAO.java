package com.healthcompass.data.dao;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.SupplierSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.querydsl.core.types.Predicate;


@Component
public class SupplierDAO {
	
	
	@Autowired
	private SupplierRepository supplierRepository;
	
	@Autowired
	private SupplierSearchPredicates preds;
	
	private Predicate toPredicate(SupplierSummaryViewSearchParams params) {

		// @formatter:off
		return preds.isOrganizationIdEq(params.getOrganizationId())
				.and(preds.isSupplierNameLike(params.getSupplierName())
				.and(preds.isIdsInList(params.getIdList())
				.and(preds.isActive(true))));
				
				
						
				
		// @formatter:on
	}
	
	public Optional<Supplier> findOne(UUID id) {
		Optional<Supplier> supplierDBObj = supplierRepository.findById(id);
		
		return supplierDBObj;
		
		
		
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Supplier save(Supplier supplier)
	{
		if(supplier.getId() == null){
			
			// generate new supplier number 
    		Integer supplierNumber = supplierRepository.generateNextSupplierNumber();
    		supplier.setSupplierNumber(supplierNumber);
		}
		return supplierRepository.save(supplier);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Supplier> saveAll(List<Supplier> suppliers)
	{
		for(Supplier supplier : suppliers){
			
			if(supplier.getId() == null){
				
				// generate new supplier number 
	    		Integer supplierNumber = supplierRepository.generateNextSupplierNumber();
	    		supplier.setSupplierNumber(supplierNumber);
			}
		}
		return (List<Supplier>) supplierRepository.saveAll(suppliers);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Supplier deleteById(UUID id) {
		Optional<Supplier> supplierDBObj = supplierRepository.findById(id);
		Supplier supplier = supplierDBObj.isPresent() ?  supplierDBObj.get() : null;
		if(supplier == null ) {
			
			throw new RecordNotFoundException("Supplier Not found with Id: "+id);
		}
		Integer count = supplierRepository.checkIfProductExistsInInventoryForthisSupplier(id);
		if(count > 0) {
						
			throw new HealthCompassCustomValidationException("Supplier cannot be deleted as Products are associated with Purchase Orders for  this Supplier:"+id,null);
		}
		supplier.setActive(false);
		supplierRepository.save(supplier);
		return null;
		
		
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Supplier update(Supplier supplier)
	{
		
	
		return supplierRepository.save(supplier);
		
			
	}
	
		@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the session for fetching lazy loading	 
		public List<Supplier>  findAll(SupplierSummaryViewSearchParams supplierSummaryViewSearchParams){
			return (List<Supplier>) supplierRepository.findAll(toPredicate(supplierSummaryViewSearchParams),Sort.by(Sort.Direction.ASC, "name"));
			
			
		}
	
}
