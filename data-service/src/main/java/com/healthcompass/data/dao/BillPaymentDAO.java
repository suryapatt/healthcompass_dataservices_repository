package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.LocationSequenceNumbers;
import com.healthcompass.data.model.LocationSequenceNumbersPK;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.LocationSequenceNumbersRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentHistoryViewResultsetExtractor;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewResultsetExtractor;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;



@Component

public class BillPaymentDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	//@Autowired
    //private AuthenticationFacade authenticationFacade;
	
	@Autowired 
	RateCardSearchPredicates preds;
	
	@Autowired
	private BillPaymentRepository billPaymentsRepo;
	
	@Autowired
	private LocationSequenceNumbersRepository locationSequenceNumbersRepository;
	
	
	
	@Autowired
	private RateCardModelAssembler rateCardModelAssembler;
	
	@Autowired
	private LocationEntityAssembler LocationEntityAssembler;
	
	@Autowired
	ReceiptViewResultsetExtractor receiptViewResultsetExtractor;
	
	@Autowired
	BillPaymentsMainViewResultsetExtractor billPaymentsMainViewResultsetExtractor;
	
	@Autowired
	BillPaymentHistoryViewResultsetExtractor billPaymentHistoryViewResultsetExtractor;
	
	
	@Autowired
	ValueSetDAO valueSetDAO;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<BillPayment>  save(List<BillPayment> billPayments, boolean updateParentServiceBill) {
		
		int INSERT_BATCH_SIZE = 2;
		String SQL_BILL_PAYMENT_INSERT="INSERT INTO billing.bill_payments(id,patient_id,bill_number,payment_amount,payment_date,payment_mode_id,active,created_on,created_by,updated_on,updated_by,receipt_number) values(?,?,?,?,?,?,?,?,?,?,?,?)";
		
		//final Integer receiptNumber = billPaymentsRepo.generateNextReceiptNumber();
		String locationCode=null;
		for (BillPayment billPayment : billPayments) {
			String billNumber = billPayment.getServiceBill().getBillNumber();
			int billNumberLength = billNumber.length();
			int locationCodeLength = billNumberLength-5; // we use five digits inclusing padding for numbers
			locationCode=billNumber.substring(0,locationCodeLength);
			break;
			
		}
		// generate new receipt
		Integer receiptNumber = null;
		
		LocationSequenceNumbers locationSequenceNumbers=null;
		Optional<LocationSequenceNumbers> optLocationLocationSequences = locationSequenceNumbersRepository.findById(new LocationSequenceNumbersPK(locationCode, "service_bill_receipt_number"));
		if(!optLocationLocationSequences.isPresent()) {
			receiptNumber=1;
			
			locationSequenceNumbers= new LocationSequenceNumbers(new LocationSequenceNumbersPK(locationCode, "service_bill_receipt_number"),receiptNumber,String.format("%05d" ,receiptNumber));
			
		}else {
			locationSequenceNumbers=(LocationSequenceNumbers) optLocationLocationSequences.get();
			receiptNumber=locationSequenceNumbers.getSequenceNumber()+1;
			locationSequenceNumbers.setSequenceNumber(receiptNumber);
			locationSequenceNumbers.setSequenceNumberPadded(String.format("%05d" ,receiptNumber));
		}
		
		final String newReceiptNumber = locationCode+""+String.format("%05d" ,receiptNumber);
		
		
		 
				
		
		locationSequenceNumbersRepository.save(locationSequenceNumbers);
		
						
		for (int i = 0; i < billPayments.size(); i += INSERT_BATCH_SIZE) {

			final List<BillPayment> inserBatchList = billPayments.subList(i,
					i + INSERT_BATCH_SIZE > billPayments.size() ? billPayments.size() : i + INSERT_BATCH_SIZE);


			
			jdbcTemplate.batchUpdate(SQL_BILL_PAYMENT_INSERT, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement pStmt, int j) throws SQLException {
					UUID billPaymentId=UUID.randomUUID();
					BillPayment billPayment = inserBatchList.get(j);
					billPayment.setId(billPaymentId);
					pStmt.setObject(1, billPaymentId,java.sql.Types.OTHER);
					pStmt.setObject(2, billPayment.getPatient().getId(),java.sql.Types.OTHER);
					pStmt.setString(3, billPayment.getServiceBill().getBillNumber());
					pStmt.setDouble(4, billPayment.getPaymentAmount());
					pStmt.setTimestamp(5,new java.sql.Timestamp(System.currentTimeMillis()));
					//pStmt.setDate(5,new Date(System.currentTimeMillis()));
					pStmt.setInt(6, billPayment.getPaymentMode().getId());
					pStmt.setBoolean(7,true);
					pStmt.setDate(8,new Date(System.currentTimeMillis()));
					pStmt.setString(9, billPayment.getCreatedBy());
					pStmt.setDate(10,new Date(System.currentTimeMillis()));
					pStmt.setString(11,billPayment.getUpdatedBy());
					billPayment.setReceiptNumber(newReceiptNumber);
					pStmt.setString(12, newReceiptNumber);
					
					
					
					
				}

				@Override
				public int getBatchSize() {
					return inserBatchList.size();
				}
				
				

			});
			
		}// end for batch insert
			
			
			if(updateParentServiceBill)		{
				String SQL_SERVICE_BILL_UPDATE = " UPDATE billing.service_bill set amount_paid=?, amount_due=?, payment_status_id=?,updated_on=?,updated_by=? where bill_number=?";
				final int NotPaidStatus = valueSetDAO.getBillPaymentNotPaidStatusCode().getId();
				final int fullyPaidStatus=valueSetDAO.getBillPaymentPaidStatusCode().getId();
				final int partiallyPaidStatus=valueSetDAO.getBillPaymentPartiallyPaidStatusCode().getId();
				int UPDATET_BATCH_SIZE = 2;
				
				for (int i = 0; i < billPayments.size(); i += UPDATET_BATCH_SIZE) {

					final List<BillPayment> updateBatchList = billPayments.subList(i,
							i + UPDATET_BATCH_SIZE > billPayments.size() ? billPayments.size() : i + UPDATET_BATCH_SIZE);


					
					jdbcTemplate.batchUpdate(SQL_SERVICE_BILL_UPDATE, new BatchPreparedStatementSetter() {
						@Override
						public void setValues(PreparedStatement pStmt, int j) throws SQLException {
							
							BillPayment billPayment = updateBatchList.get(j);
							
							Double amountBilled=billPayment.getServiceBill().getAmountBilled();
							Double originalDue = billPayment.getServiceBill().getAmountDue();
							Double originalPaid = amountBilled-originalDue;
							Double updatedAmountPaid = originalPaid +  billPayment.getPaymentAmount();
							Double updatedDueAmount = amountBilled-updatedAmountPaid;
							
							pStmt.setDouble(1,updatedAmountPaid);
							pStmt.setDouble(2,updatedDueAmount);
							
							//int PaymentstatusCode=19357;
							int PaymentstatusCode=0;
							if(amountBilled.compareTo(updatedAmountPaid) == 1)   PaymentstatusCode=partiallyPaidStatus;
							if(amountBilled.compareTo(updatedAmountPaid) == 0)   PaymentstatusCode=fullyPaidStatus;
							if(amountBilled.compareTo(updatedDueAmount) == 0)  PaymentstatusCode=NotPaidStatus;
							pStmt.setInt(3,PaymentstatusCode);
							pStmt.setDate(4,new Date(System.currentTimeMillis()));
							pStmt.setString(5,billPayment.getCreatedBy());
							pStmt.setString(6, billPayment.getServiceBill().getBillNumber());
						
							
													
							
						}

						@Override
						public int getBatchSize() {
							return updateBatchList.size();
						}
						
						

					});
				
			} // end for update batch
			
			
		} // end if update service bill	
			
			
	
		
		return billPayments; // List of bill payments with generated receipt numbers and primary key ids
		
		
	}
	
public ReceiptViewDTO getBillPaymentsForReceiptGeneration(List<BillPayment> billPaymentsWithNewlyCreatedKeys, boolean isPaymentForPreviouslyGeneratedBills){
		
	    // construct the in clauses for the subqueries that return the total paid amount and new total out standing amount
		int i=1;
		String idInClause="";
		String billNumberInClause="";
	    String totalPaymentReceivedSubQuery = "(select sum(payment_amount) as totalPaymentReceived from billing.bill_payments "+
	    		 " where id in (";
	    String totalNewOutstandingAmountSubQuery = "(select sum(amount_due) as totalOutstanding from billing.service_bill  " + 
				"	 where bill_number in ("; 
	    		for(BillPayment billPayment : billPaymentsWithNewlyCreatedKeys) {
	    			
	    			idInClause+="'"+billPayment.getId()+"'";
	    			billNumberInClause+="'"+billPayment.getServiceBill().getBillNumber()+"'";
	    			if(i<billPaymentsWithNewlyCreatedKeys.size()) {
	    				idInClause+=",";
	    				billNumberInClause+=",";
	    			}
	    			i++;
	    			
	    			
	    		}
	    		 totalPaymentReceivedSubQuery+=idInClause+")";
	    		 totalPaymentReceivedSubQuery+=" ) ";
	     		 totalNewOutstandingAmountSubQuery+=billNumberInClause+")";
	     		 totalNewOutstandingAmountSubQuery+=" ) ";
		
		
		String RECEIPT_GENERATION_QUERY = "select bp.id as billPaymentId,bp.bill_number as billNumber, " + 
				"bp.payment_amount as paymentAmount, bp.receipt_number as receiptNumber, val.display as paymentMode, totalBillPayment.totalPaymentReceived,totalOutstandingOnAllBills.totalOutStanding,sb.bill_date as billDate," + 
				"sb.amount_due as finalDue,pat.userid as patientId,pat.name as " + 
				"patientName, pat.telecom as phoneNumber, usertab.username as userName,loc.id as locationId, loc.name as locationName, org.id as " + 
				"organizationId, org.name as organizationName " + 
				"from " + totalPaymentReceivedSubQuery + " totalBillPayment," 
				 + totalNewOutstandingAmountSubQuery + " totalOutstandingOnAllBills, " +
				 " billing.bill_Payments bp "+
				 " inner join project_asclepius.value_set val on bp.payment_mode_id=val.id "+
				 " inner join billing.service_bill sb on bp.bill_number=sb.bill_number "+
				 " inner join project_asclepius.patient pat on bp.patient_id=pat.id "+
				 " inner join project_asclepius.auth_user usertab on pat.userid=usertab.id "+
				 " inner join project_asclepius.practitioner_role prRole on sb.practitioner_role_id = prRole.id "+
				 " inner join project_asclepius.location loc on prRole.location=loc.id "+
				 " inner join project_asclepius.organization org on loc.managing_organization=org.id "+
				 " where bp.id in (";
				
				RECEIPT_GENERATION_QUERY+=idInClause+")";
		        
				ReceiptViewDTO receiptViewDTO = (ReceiptViewDTO)  jdbcTemplate.query(RECEIPT_GENERATION_QUERY,receiptViewResultsetExtractor);
				
				// in the BillPayment table, we do not have info on actual due before payment. For final due we can get this value from Bill.
				//so we do a tweak little bit. let us take the original due amount from the object
				for(BillPaymentReceiptViewDTO billPaymentReceiptViewDTO : receiptViewDTO.getBillPayments()) {
		    		
					Optional<BillPayment> optTargetBillPayment = billPaymentsWithNewlyCreatedKeys.stream()
							.filter(billPayment -> billPayment.getId().equals(billPaymentReceiptViewDTO.getBillPaymentId())).findFirst();

							    if(!optTargetBillPayment.isPresent())  //check this logic where the chilkd
							    	continue;

							    BillPayment targetBillPayment = optTargetBillPayment.get();
							    if(isPaymentForPreviouslyGeneratedBills) {
							    	billPaymentReceiptViewDTO.setOriginalDue(targetBillPayment.getServiceBill().getAmountDue());
							    }else { // it it is during bill generation original due will be equal to amount billed
							    	billPaymentReceiptViewDTO.setOriginalDue(targetBillPayment.getServiceBill().getAmountBilled());
							    }
							    receiptViewDTO.setLoggedInUser(targetBillPayment.getCreatedBy());
		    		
				}
				    return receiptViewDTO;
		
	}
	
	
	
	
	
	
	
	

	
	public BillPaymentsMainViewDTO findAllPendingPayments(BillPaymentsSearchParams billPaymentsSerachParams){
		
		UUID organizationId = null;
		String billNumber = null;
		Integer patientId = null; //userid 
		String patientName= null;
		String phoneNumber =null;
		boolean includeOrgIdInSearch=false;
		boolean includeBillNumberInSearch=false;
		boolean includePatientIdInSearch=false;
		boolean includePatientNameSearch=false;
		boolean includePhoneNumberInSearch=false;
		
		try {
			organizationId = billPaymentsSerachParams.getOrganizationId();
			if(organizationId != null) includeOrgIdInSearch=true;
		}catch(Exception e) {
			organizationId=null;
		}

		
		try {
			billNumber = billPaymentsSerachParams.getBillNumber();
			if(billNumber != null && !billNumber.isEmpty()) includeBillNumberInSearch=true;
		}catch(Exception e) {
			billNumber=null;
		}
		try {
			patientId = billPaymentsSerachParams.getPatientId();
			if(patientId != null) includePatientIdInSearch=true;  // actually this will be userid value in the Patient table. So we put the user id column in where clause
			
		}catch(Exception e) {
			patientId=null;
		}
		try {
			patientName =  billPaymentsSerachParams.getPatientName();
			if(patientName != null && !patientName.isEmpty()) includePatientNameSearch=true;
			
		}catch(Exception e) {
			patientName=null;
		}
		try {
			phoneNumber = billPaymentsSerachParams.getPhoneNumber();
			if(phoneNumber != null) includePhoneNumberInSearch=true;
		}catch(Exception e) {
			phoneNumber=null;
		}
	
		 if(includePhoneNumberInSearch){
			 
		 }
		
		String PENDING_BILL_PAYMENTS_SELECT_QUERY= "select bill.bill_Number as billNumber,bill.bill_date as billDate, pat.id as patientId,pat.name as patientName,bill.amount_billed as amountBilled, bill.amount_paid as amountPaid, bill.amount_due as amountDue, "+
				" billPayments.lastPaidOn as lastPaidOn,val.code as paymentStatus "+
				" from billing.service_bill bill "+
				" inner join "+
				 " (select max(payment_date) as lastPaidOn, bill_number as billNumber from billing.bill_payments group by bill_number) "
				 + " billPayments "
				 + " on billPayments.billNumber=bill.bill_Number "+
				 " inner join project_asclepius.patient pat on pat.id=bill.patient_id " +
				 " inner join project_asclepius.auth_user user1 on pat.userid=user1.id ";
					if(includeOrgIdInSearch) {
						PENDING_BILL_PAYMENTS_SELECT_QUERY+=" inner join project_asclepius.practitioner_role prole on prole.id=bill.practitioner_role_id ";
					}
					
					PENDING_BILL_PAYMENTS_SELECT_QUERY+=" inner join project_asclepius.value_set val on val.id=bill.payment_status_id  "+
				 " where bill.bill_number is not null ";
		      //  boolean appendAnd=true;
		       // boolean appendWhere=true;
		        String andWhere="";
				 if(includeBillNumberInSearch){
					 andWhere = " and ";
					 PENDING_BILL_PAYMENTS_SELECT_QUERY = PENDING_BILL_PAYMENTS_SELECT_QUERY+""+ andWhere+" bill.bill_number='"+billNumber+"'";
					
					 
				 }
				 if(includePatientIdInSearch){
					 andWhere = " and ";
					 PENDING_BILL_PAYMENTS_SELECT_QUERY = PENDING_BILL_PAYMENTS_SELECT_QUERY+""+andWhere+" pat.userid = "+patientId+" ";
					
				 }
				 if(includePatientNameSearch){
					 andWhere = " and ";
					 PENDING_BILL_PAYMENTS_SELECT_QUERY = PENDING_BILL_PAYMENTS_SELECT_QUERY+""+ andWhere+"  pat.name like '%"+patientName+"%'";
					 
				 }
				 
				 if(includePhoneNumberInSearch){
					 andWhere = " and ";
					 PENDING_BILL_PAYMENTS_SELECT_QUERY = PENDING_BILL_PAYMENTS_SELECT_QUERY+""+ andWhere+"  user1.username='"+phoneNumber+"'";
				 }
				 if(includeOrgIdInSearch) {
					 andWhere = " and ";
					 PENDING_BILL_PAYMENTS_SELECT_QUERY = PENDING_BILL_PAYMENTS_SELECT_QUERY+""+ andWhere+"  prole.organization='"+organizationId+"'";
					 
				 }
				 
		
		
		
		
		
		return (BillPaymentsMainViewDTO) jdbcTemplate.query(PENDING_BILL_PAYMENTS_SELECT_QUERY,billPaymentsMainViewResultsetExtractor);
		
	}
	
	
public BillPaymentHistoryMainViewDTO findBillPaymentHistory(String billNumber){
		
		
			
		
		String BILL_PAYMENT_HISTORY_SELECT_QUERY= "select bp.id as billPaymentId,bp.bill_number as billNumber, bp.payment_amount as paymentAmount, bp.payment_date as paymentDate, bp.attachment_id as attachmentId, bp.receipt_number as receiptNumber, " + 
				" val.display as paymentMode, sb.id as billId,sb.bill_date as billDate, sb.amount_billed as amountBilled " + 
				" from billing.bill_payments  bp " + 
				" inner join project_asclepius.value_set val on bp.payment_mode_id=val.id " + 
				" inner join billing.service_bill sb on bp.bill_number=sb.bill_number " + 
				" where bp.bill_number= '"+billNumber.toUpperCase()+"' " +
				" order by bp.receipt_number asc,bp.payment_date asc "  ;
		
		
		
		
		
		return (BillPaymentHistoryMainViewDTO) jdbcTemplate.query(BILL_PAYMENT_HISTORY_SELECT_QUERY,billPaymentHistoryViewResultsetExtractor);
		
	}
	

	
}