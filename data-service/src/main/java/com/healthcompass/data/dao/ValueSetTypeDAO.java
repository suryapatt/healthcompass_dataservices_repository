package com.healthcompass.data.dao;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.repository.ValueSetRepository;
import com.healthcompass.data.repository.ValueSetTypeRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.querydsl.core.types.Predicate;

@Component

public class ValueSetTypeDAO {
	
	
	@Autowired ServiceBillItemSearchPredicates preds;
	
	@Autowired
	private ValueSetTypeRepository valueSetTypeRepository;
	
	

	@Transactional(propagation=Propagation.SUPPORTS)
	public ValueSetType getOptionAndOptionValues(String optionName) {   // actual values will be fetched by service
		return valueSetTypeRepository.getOptionAndOptionValues(optionName);
	}
	
	
	
	
	
	
}
