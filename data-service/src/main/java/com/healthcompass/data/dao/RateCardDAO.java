package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.querydsl.core.types.Predicate;



@Component
@Transactional
public class RateCardDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	//@Autowired
    //private AuthenticationFacade authenticationFacade;
	
	@Autowired 
	RateCardSearchPredicates preds;
	
	@Autowired
	private RateCardRepository rateCardRepo;
	
	@Autowired
	private LocationRepository locationRepo;
	
	@Autowired
	private RateCardModelAssembler rateCardModelAssembler;
	
	@Autowired
	private LocationEntityAssembler LocationEntityAssembler;
	
	@Autowired
	RateCardSummaryResultsetExtractor rateCardSummaryResultsetExtractor;
	
	@Autowired
	LocationRateCardsViewResultsetExtractor locationRateCardsViewResultsetExtractor;
	
	
	@Autowired
	RateCardCategoryRepository rateCardCategoryRepo;
	
	private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();
	
	private Predicate toPredicate(RateCardSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isLocationIdEq(params.getLocationId()))
				.and(preds.isActive(true))
				.and(preds.isIdsInList(params.getIdList()));
				
		// @formatter:on
	}
	
	
	
	
	
	

	public LocationRateCardsViewDTO findAllRateCards(RateCardSearchParams rateCardSearchParams){
		
		return findAllRateCards(rateCardSearchParams.getLocationId());
		
		
		
	}
	
	public LocationRateCardsViewDTO findAllRateCards(UUID locationId){
		
		
		String RATE_CARD_SELECT_QUERY = "select rc.id as id, rc.display as display,rc.description as description,rc.rate as rate, rc.active as active,"
				+ " rcg.id as rateCardCategoryId,rcg.code as rateCardCategoryCode, \r\n" + 
				" rcg.description as rateCardCategoryDescription,rcg.active as active, loc.id as locationId,loc.name as locationName \r\n" + 
				" from billing.rate_card rc\r\n" + 
				" inner join billing.rate_card_category rcg on rc.rate_card_category_id=rcg.id\r\n" + 
				" inner join project_asclepius.location loc on rc.location_id=loc.id\r\n" + 
				" where rc.active = true and rc.location_id='"+locationId+"'";
		
		
		return (LocationRateCardsViewDTO) jdbcTemplate.query(RATE_CARD_SELECT_QUERY,locationRateCardsViewResultsetExtractor);
		
	}
	public UUID  save(LocationRateCardsViewDTO refinedLocationObjectWithNewRateCardCat,Location locationFromClient) {
		
		try {
		 LoggedInUser user = new LoggedInUser();
		 user.setUserId(refinedLocationObjectWithNewRateCardCat.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : refinedLocationObjectWithNewRateCardCat.getLoggedInUser());
		 loggedInUser.set(user); // thread local for subsequent threads to provide loggged in user
		 
		UUID locationId = insertOrUpdateLocationRateCards(locationFromClient);
		
		return locationId;
		}finally {
			loggedInUser.remove();
		}
		
		
		
		
	
	}
	
	

	
	

	public UUID  insertOrUpdateLocationRateCards(Location locationFromClient) {
		int INSERT_BATCH_SIZE = 2;
		int UPDATE_BATCH_SIZE=2;
		
       
		
		
		
		// get the existing location object
		Location locationDBObj = locationRepo.findById(locationFromClient.getId()).get();
		
		
		
		
        // First identify the newly inserted RateCard, deletedRateCards and Modified Rate Cards
		ArrayList<RateCard> newRateCards = new ArrayList<RateCard>();
		ArrayList<RateCard> deletedRateCards = new ArrayList<RateCard>();
		ArrayList<RateCard> modifiedRateCards = new ArrayList<RateCard>();
						
		List<RateCard> rateCardsTobePersisted = locationFromClient.getRateCards();
		
		boolean isCloneOrNew=false;
		if(locationDBObj.getRateCards() == null || locationDBObj.getRateCards().isEmpty()) {
			isCloneOrNew = true;
		}
		for(RateCard rateCardToBePersisted : rateCardsTobePersisted) {
			
			rateCardToBePersisted.setLocation(locationDBObj);
			if(isCloneOrNew) { // If it is clone or new, treat this as a new rate card
				rateCardToBePersisted.setId(null); // just to make sure we are not overwriting existinbg rate card
				newRateCards.add(rateCardToBePersisted);
				continue;
			}
			// if here means edit location rate cards
			if(rateCardToBePersisted.getId() != null)
				
				modifiedRateCards.add(rateCardToBePersisted);
			else {
				rateCardToBePersisted.setId(null); // just to make sure we are not overwriting existinbg rate card
				newRateCards.add(rateCardToBePersisted);
			}
			
			
			
	    }
				
		
		

		String SQL_RATE_CARD_INSERT = "INSERT INTO billing.rate_card(display,description,rate_card_category_id,location_id,rate,active,created_by,created_on,updated_by,updated_on) values(?,?,?,?,?,?,?,?,?,?)";
		for (int i = 0; i < newRateCards.size(); i += INSERT_BATCH_SIZE) {

			final List<RateCard> inserBatchList = newRateCards.subList(i,
					i + INSERT_BATCH_SIZE > newRateCards.size() ? newRateCards.size() : i + INSERT_BATCH_SIZE);

			jdbcTemplate.batchUpdate(SQL_RATE_CARD_INSERT, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement pStmt, int j) throws SQLException {
					RateCard rateCard = inserBatchList.get(j);
					pStmt.setString(1, rateCard.getDisplay());
					pStmt.setString(2, rateCard.getDescription());
					pStmt.setInt(3, rateCard.getRateCardCategory().getId());
					pStmt.setObject(4, rateCard.getLocation().getId(),java.sql.Types.OTHER);
					pStmt.setDouble(5, rateCard.getRate());
					pStmt.setBoolean(6,true);
					pStmt.setString(7,loggedInUser.get().getUserId());
					pStmt.setDate(8,new Date(System.currentTimeMillis()));
					pStmt.setString(9,loggedInUser.get().getUserId());
					pStmt.setDate(10,new Date(System.currentTimeMillis()));
				}

				@Override
				public int getBatchSize() {
					return inserBatchList.size();
				}
				
				

			});
		}
		
		String SQL_RATE_CARD_UPDATE = "Update billing.rate_card set rate=?, active=?, description=?, display=?,rate_card_category_id=?, updated_by=?, updated_on=? where id=?";
		for (int i = 0; i < modifiedRateCards.size(); i += UPDATE_BATCH_SIZE) {

			final List<RateCard> updateBatchList = modifiedRateCards.subList(i,
					i + UPDATE_BATCH_SIZE > modifiedRateCards.size() ? modifiedRateCards.size() : i + UPDATE_BATCH_SIZE);

			jdbcTemplate.batchUpdate(SQL_RATE_CARD_UPDATE, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement pStmt, int j) throws SQLException {
					RateCard rateCard = updateBatchList.get(j);
					pStmt.setDouble(1, rateCard.getRate());
					pStmt.setBoolean(2,rateCard.getActive());
					pStmt.setString(3,rateCard.getDescription());
					pStmt.setString(4,rateCard.getDisplay());
					pStmt.setInt(5,rateCard.getRateCardCategory().getId());
					pStmt.setString(6,loggedInUser.get().getUserId());
					pStmt.setDate(7,new Date(System.currentTimeMillis()));
					pStmt.setInt(8,rateCard.getId());
				}

				@Override
				public int getBatchSize() {
					return updateBatchList.size();
				}
				
				

			});
		
		}
		return locationDBObj.getId();
}
	
	public RateCardSummaryOrganizationDTO getRateCardSummaryWithLocations(RateCardViewSummarySearchParams rateCardViewSummarySearchParams) {
		

		/*String SQL_RATE_CARD_SUMMARY_SELECT = "select l.id location_id,l.name location_name,org.id organization_id,org.name organization_name, count(rc.id) count_ratecard \r\n" + 
				"	  from billing.rate_card rc\r\n" + 
				"	 right outer join project_asclepius.location l on rc.location_id=l.id\r\n" + 
				"	 right outer join project_asclepius.organization org on l.managing_organization= org.id\r\n" + 
				//"	 where rc.active=true and org.id= '" + rateCardViewSummarySearchParams.getOrganizationId() + "'" +
				"	 where org.id= '" + rateCardViewSummarySearchParams.getOrganizationId() + "'" +
				"group by l.name ,org.name,l.id,org.id";*/
		
		
		String SQL_RATE_CARD_SUMMARY_SELECT = "select active_rc.rate_card_count as count_ratecard, loc.id as location_id, loc.name as location_name, org.id as organization_id, org.name organization_name " +
		" from 	(select count(id) as rate_card_count , location_id from billing.rate_card where active=true group by location_id ) as active_rc " +
		" right join project_asclepius.location loc on loc.id=active_rc.location_id " +
		" inner join project_asclepius.organization org on loc.managing_organization=org.id " +
		" where org.id='" + rateCardViewSummarySearchParams.getOrganizationId()+"'" + 
		" and loc.status= "+HealthCompassConstants.LOCATION_STATUS_ACTIVE_ID+" " +
		" group by loc.id,loc.name,org.id,org.name,active_rc.rate_card_count";
		
	
	return (RateCardSummaryOrganizationDTO) jdbcTemplate.query(SQL_RATE_CARD_SUMMARY_SELECT,rateCardSummaryResultsetExtractor);
	
	}
	
	public boolean deleteLocationRateCards(UUID locationId) {
		boolean isDeleted = false;
		// we will oo a soft delete
		String RATE_CARDS_DELETE = "update billing.rate_card set active=false where location_id='"+locationId+"'";
		try {
			jdbcTemplate.batchUpdate(RATE_CARDS_DELETE); 
			isDeleted = true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return isDeleted;
		
	}
	
}