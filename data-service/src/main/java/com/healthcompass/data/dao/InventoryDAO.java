package com.healthcompass.data.dao;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.InventoryRepository;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.InventoryDetailsSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.InventoryDetailsSearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.querydsl.core.types.Predicate;

@Component

public class InventoryDAO {
	
	
	
	
	@Autowired
	private InventoryRepository inventoryRepository;
	
	@Autowired 
	InventoryDetailsSearchPredicates preds;
	
	private Predicate toPredicate(InventoryDetailsSearchParams params) {

		// @formatter:off
		return preds.isActive(params.getActive())
				.and(preds.isOrganizationIdEq(params.getOrganizationId()))
				.and(preds.isLocationIdEq(params.getLocationId()))
				.and(preds.isProductIdEq(params.getProductId()))
				.and(preds.isProductNameLike(params.getProductName()))
				.and(preds.isAvailableUnitsOperator(params.getAvailableUnits(),params.getOperatorForAvbUnits()));
				
		// @formatter:on
	}
	
	
	public List<InventoryItem> findAllByIds(List<UUID> ids){
		
		return (List<InventoryItem>) inventoryRepository.findAllById(ids);
	}
	
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the session for fetching lazy loading
	public List<InventoryItem> findAllInventoryDetails(InventoryDetailsSearchParams inventoryDetailsSearchParams){
		
		return (List<InventoryItem>) inventoryRepository.findAll(toPredicate(inventoryDetailsSearchParams));
		
			
	}
	
	
	
	
}
