package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.PurchaseOrderMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.LocationSequenceNumbers;
import com.healthcompass.data.model.LocationSequenceNumbersPK;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.InventorySummaryRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.LocationSequenceNumbersRepository;
import com.healthcompass.data.repository.OrderRepository;
import com.healthcompass.data.repository.ProductRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.CompactInventoryItemsCollector;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentHistoryViewResultsetExtractor;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.CompactInventoryItemDetailsResultSetExtractor;
import com.healthcompass.data.view.dto.LocationDropDownListViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationViewResultSetExtrator;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderSummaryListViewResultextractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewResultsetExtractor;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;




@Component
public class PurchaseOrderDAO {
	
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private OrderRepository orderRepo;
	
	@Autowired
	private LocationRepository locationRepo;
	
	@Autowired
	private InventorySummaryRepository inventorySummaryRepo;
	
	@Autowired
	private LocationSequenceNumbersRepository locationSequenceNumbersRepository;
	
	
	@Autowired
	PurchaseOrderSummaryListViewResultextractor purchaseOrderSummaryListViewResultextractor;
	
	@Autowired 
	PurchaseOrderMainViewToEntityAssembler productListPurchaseOrderMainViewToEntityAssembler;
	
	@Autowired
	CompactInventoryItemDetailsResultSetExtractor compactInventoryItemDetailsResultSetExtractor;
	
	@Autowired 
	CompactInventoryItemsCollector compactInventoryItemsCollector;
	
	@Autowired 
	ProductSummaryOrganizationViewResultSetExtrator productSummaryOrganizationViewResultSetExtrator;
	
	
	
	 @Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
		public Optional<Order> findOne(UUID id) {
			
		
			return orderRepo.findById(id);
					
						
		}

	
	

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Order createOrUpdatePurchaseOrder(PurchaseOrderMainViewDTO purchaseOrderView) {
		
		 
	      
		 	boolean isNewOrder = false;
		 	Order order = null;
		   	if(purchaseOrderView.getOrder().getPurchaseOrderId() == null) {
			   isNewOrder=true;
		   	}
		   if(isNewOrder) {
				
				
				order = new Order();
				order.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
				order.setCreatedBy(purchaseOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : purchaseOrderView.getLoggedInUser());
				order.setOrderType("Purchase");
				order.setOrderDate(new java.sql.Timestamp(System.currentTimeMillis()));
				Organization org = new Organization();
				order.setOrganization(org);
				Location loc = new Location();
				order.setLocation(loc);
				Supplier supplier = new Supplier();
				order.setSupplier(supplier);
				List<OrderItem> orderItemsList = new ArrayList<OrderItem>();
				order.setOrderItems(orderItemsList);
				List<InventoryItem> inventoryItemsList = new ArrayList<InventoryItem>();
				order.setInventoryItems(inventoryItemsList);
				
		      }	else{
		    		Optional<Order> optOrder = orderRepo.findById(purchaseOrderView.getOrder().getPurchaseOrderId());
		    		order = optOrder.get();
		    		
		    		
		    	}


				
				Order orderUpdatedWithChanges = productListPurchaseOrderMainViewToEntityAssembler.toEntity(purchaseOrderView,order);
				
				// calculate order value, discount , net order value, order items rate, 
				// at the same time we also identify the orphan items that will be deleted in the following save prepare to delete the orphan order item
				Double orderValue=0.0;
				Double orderNetValue=0.0;
				Double orderDiscount=0.0;
				Double itemUnitRate = 0.0;
				Double itemDiscountedRate = 0.0;
				Double itemNetValue = 0.0;
				
				List<OrderItem> toBeDeletedOrderItems = new ArrayList<OrderItem>();
				
				for(OrderItem orderItem : orderUpdatedWithChanges.getOrderItems()) {
					if(orderItem.getOrder() == null) {
						
						toBeDeletedOrderItems.add(orderItem);
										
					}else {
						itemUnitRate = orderItem.getMrp()/orderItem.getQuantity();
						itemNetValue= orderItem.getMrp()-orderItem.getDiscount();
						if(orderItem.getDiscount() > 0) {
							itemDiscountedRate=itemNetValue/orderItem.getQuantity();
						}else {
							itemDiscountedRate=0.0;
						}
						
						orderItem.setRate(itemUnitRate);
						orderItem.setDiscountedRate(itemDiscountedRate);
						orderItem.setNetItemValue(itemNetValue);
						
						orderValue+=orderItem.getMrp();
						orderDiscount+=orderItem.getDiscount();
						
					}
					
				
				}
								
				order.getOrderItems().removeAll(toBeDeletedOrderItems);
				
				// set the calculated order values
				order.setOrderValue(orderValue);
				order.setDiscount(orderDiscount);
				order.setNetOrderValue(orderValue-orderDiscount);
				
				
				// before saving the inventory items prepare a list of Orphan Inventory items. reduce the InventorySummary counts for deleted/orphan inventory items
				// Aninventory item can become orphan if the PO is edited and an order item is assocuiated with a different product than a previous one that is saved
				Set<UUID> orphanInventoryIds = new HashSet<UUID>();
				Set<UUID> existingInventoryIds = new HashSet<UUID>();
				
				List<InventoryItem> toBeDeletedInventoryItems = new ArrayList<InventoryItem>();
				for(InventoryItem inventoryItem : orderUpdatedWithChanges.getInventoryItems()) {
					if(inventoryItem.getOrder() == null) {
						orphanInventoryIds.add(inventoryItem.getId());
						toBeDeletedInventoryItems.add(inventoryItem);
						
					}else {
						if(inventoryItem.getId() != null) {
							existingInventoryIds.add(inventoryItem.getId());
						}
						
						// set the inventory values mrp_per_unit, rate and max discount from the order item to be safe though these values are populated from orderitem view dto to inventory item view dto assuming ordr item dto values will be provided by client
						
						
						   Optional<OrderItem> optTargetOrderItem = order.getOrderItems().stream()
									.filter(orderItem -> (orderItem.getProduct().getId().compareTo(inventoryItem.getProduct().getId()) == 0) 
											&& orderItem.getBatchNumber().equals(inventoryItem.getBatchNumber())).findFirst();

									    if(optTargetOrderItem.isPresent()) {
									    	OrderItem orderItem = optTargetOrderItem.get();
									    	inventoryItem.setMrpPerUnit(orderItem.getMrp()/orderItem.getQuantity());
									    	inventoryItem.setUnitRate(orderItem.getRate());
									    	inventoryItem.setMaxDiscount(orderItem.getDiscountedRate());
									    } 
									    
					}
					
					
				
				}
				
				// prepare to remove the orphan inventory items
				order.getInventoryItems().removeAll(toBeDeletedInventoryItems);
				
				Map<Integer,List<CompactInventoryItemsCollector>> orphanInventoryItemDetails = new HashMap<Integer,List<CompactInventoryItemsCollector>>();
				if(orphanInventoryIds.size() > 0) {
					orphanInventoryItemDetails = getCompactInventoryItemsDetails(orphanInventoryIds);
				}
				
				Map<Integer,List<CompactInventoryItemsCollector>> existingInventoryItemDetails = new HashMap<Integer,List<CompactInventoryItemsCollector>>();
				if(existingInventoryIds.size() > 0) {
					existingInventoryItemDetails = getCompactInventoryItemsDetails(existingInventoryIds);
				}
				
				// SAVE THE ORDER (ORDER/ORDERITEMS/INVENTORY)
				if(isNewOrder){
					// generate new order number
		    		//Integer orderNumber = orderRepo.generateNextOrderNumber();
					// generate new order number
		    		Optional<Location> optLocation = locationRepo.findById( orderUpdatedWithChanges.getLocation().getId());
		    		Location location = optLocation.get();
		    		String locationCode = location.getLocationCode();
		    		
		    		Integer orderNumber = null;
		    		
		    		LocationSequenceNumbers locationSequenceNumbers=null;
		    		Optional<LocationSequenceNumbers> optLocationLocationSequences = locationSequenceNumbersRepository.findById(new LocationSequenceNumbersPK(locationCode, "purchase_order_number"));
		    		if(!optLocationLocationSequences.isPresent()) {
		    			orderNumber=1;
		    			
		    			locationSequenceNumbers= new LocationSequenceNumbers(new LocationSequenceNumbersPK(locationCode, "purchase_order_number"),orderNumber,String.format("%05d" ,orderNumber));
		    			
		    		}else {
		    			locationSequenceNumbers=(LocationSequenceNumbers) optLocationLocationSequences.get();
		    			orderNumber=locationSequenceNumbers.getSequenceNumber()+1;
		    			locationSequenceNumbers.setSequenceNumber(orderNumber);
		    			locationSequenceNumbers.setSequenceNumberPadded(String.format("%05d" ,orderNumber));
		    		}
		    		
		    		orderUpdatedWithChanges.setOrderNumber(locationCode+""+String.format("%05d" ,orderNumber));
		    		locationSequenceNumbersRepository.save(locationSequenceNumbers);
				}
				Order proxyPersistedOrder = (Order) orderRepo.save(orderUpdatedWithChanges);
				
				
				
				// create/update inventory summary
				// get the updated state of the Order
				Optional<Order> optFullyLoadedPersistedOrder = orderRepo.findById(proxyPersistedOrder.getId());
				Order fullyLoadedPersistedOrder = optFullyLoadedPersistedOrder.isPresent() ? optFullyLoadedPersistedOrder.get() : null;
				
				// Let us prepare for updating/inserting Inventory summary items based on updated state of the Order
				if(fullyLoadedPersistedOrder != null) {
					Set<Integer> productIdSet = new HashSet<Integer>(); // get all the distinct product ids from the order inventory items
					
					// build the  the latest state of the updated inventory in compact mode while iterating the inventory items
					Map<Integer,List<CompactInventoryItemsCollector>> latestUpdatedCompactInventoryDetails = new HashMap<Integer,List<CompactInventoryItemsCollector>>();
					for(InventoryItem inventoryItem : fullyLoadedPersistedOrder.getInventoryItems()) {
							
						     productIdSet.add(inventoryItem.getProduct().getId()); // we need this set to pull the InventorySummary list by product id
						
							//prepare a compact latest updated state of the inventory
						   	 CompactInventoryItemsCollector item = new CompactInventoryItemsCollector();
				        	 item.setInventoryId(inventoryItem.getId()); 
				        	 item.setBatchNumber(inventoryItem.getBatchNumber());
				        	 Integer prodId = inventoryItem.getProduct().getId();
				        	 item.setProductId(prodId);
				        	 
				        	 Integer availableUnits = inventoryItem.getAvailableUnits();
				        	 Double unitRate = inventoryItem.getUnitRate();
				        	 Double inventoryValue=availableUnits*unitRate;
				        	 
				        	 item.setProductAvailableUnits(availableUnits);
				        	 item.setProductUnitRate(unitRate);
				        	 item.setInventoryValue(inventoryValue);
				        	 
				        	 if(latestUpdatedCompactInventoryDetails.containsKey(prodId)) {
				        		 ((List<CompactInventoryItemsCollector>) latestUpdatedCompactInventoryDetails.get(prodId)).add(item);
				        		 
				        	 }else {
				        		 List<CompactInventoryItemsCollector> itemList = new ArrayList<CompactInventoryItemsCollector>();
				        		 itemList.add(item);
				        		 latestUpdatedCompactInventoryDetails.put(item.getProductId(),itemList);
				        		 
				        		 
				        	 }
				        	 
						
					}
					
					// if some inventory items were deleted because of becoming orphan from above save, we need to reduce the quantities in Inventory summery
					// so we will add the products related to orphaned inventory /deleted inventory items to get their summary
					if(!orphanInventoryItemDetails.isEmpty()) 	productIdSet.addAll(orphanInventoryItemDetails.keySet());
					
					// Now get the Inventory summary for each product in the set
					List<InventorySummary> invSummaryForSelectedProducts = inventorySummaryRepo.getInventorySummaryForProductSet(fullyLoadedPersistedOrder.getOrganization().getId(),
							
								fullyLoadedPersistedOrder.getLocation().getId(), productIdSet);
					
					// let us segregate new summary items and existing summary items
					List<InventorySummary> newInventorySummaryItems = new ArrayList<InventorySummary>();
					for(Integer prodId : productIdSet) {
						   
						    // check if the summary exists for a given product
							Optional<InventorySummary> optInventorySummaryItem = invSummaryForSelectedProducts.stream()
								.filter(inventorySummaryItem -> inventorySummaryItem.getProduct().getId().compareTo(prodId) == 0).findFirst();
									InventorySummary inventorySummary;
								    if(!optInventorySummaryItem.isPresent()) {  // if not present means we need to create a new summary item for the product
								    	
								    	inventorySummary = new InventorySummary();
								    	newInventorySummaryItems.add(inventorySummary);
								    	Organization org = new Organization();
								    	org.setId(fullyLoadedPersistedOrder.getOrganization().getId());
								    	inventorySummary.setOrganization(org);
								    	
								    	Location loc = new Location();
								    	loc.setId(fullyLoadedPersistedOrder.getLocation().getId());
								    	inventorySummary.setLocation(loc);
								    	
								    	Product prod = new Product();
								    	prod.setId(prodId);
								    	inventorySummary.setProduct(prod);
								    	inventorySummary.setAvailableUnits(0); // initialize
								    	inventorySummary.setUnitRate(0.0); //// initialize
								    	inventorySummary.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
								    	inventorySummary.setCreatedBy(purchaseOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : purchaseOrderView.getLoggedInUser());					    	
								    } 	else{
								    	inventorySummary = optInventorySummaryItem.get(); // if present means get the referrence to te summary item
								    }
								    
								    
								    // now lets do some calculations and derive net Available Units for each product.
								    Integer toBeNegatedAvailableUnits = 0;  
								    Double toBeNegatedInventoryValue=0.0;
								    // calculate net available units = latest available units -(orphan units + previous existing units)
								    // for this we need to iterate 3 maps that were constructed earlier orphan map, existing map and latest map
								    // while iterating each productid, we will derive at netAvailableUnits and netInventory Value
								    
								    // First iterate Orphan list
								    if(orphanInventoryItemDetails.containsKey(prodId)) {
								    	List<CompactInventoryItemsCollector> orphanProdList = orphanInventoryItemDetails.get(prodId);
								    	for(CompactInventoryItemsCollector compactInvItem : orphanProdList){
								    		toBeNegatedAvailableUnits+=compactInvItem.getProductAvailableUnits();
								    		toBeNegatedInventoryValue+=compactInvItem.getInventoryValue();
								    	}
								    }
								    
								    // next iterate the previous list before order update
								    if(existingInventoryItemDetails.containsKey(prodId)) {
								    	List<CompactInventoryItemsCollector> exisstingProdList = existingInventoryItemDetails.get(prodId);
								    	for(CompactInventoryItemsCollector compactInvItem : exisstingProdList){
								    		toBeNegatedAvailableUnits+=compactInvItem.getProductAvailableUnits();
								    		toBeNegatedInventoryValue+=compactInvItem.getInventoryValue();
								    	}
								    }
								    
								    // After above two loops netAvailableUnits and netInventory Value contains the amoutns that need to be subtracted from the latest list
								    
								    // now we iterate the latest list and subtact the values 
								    
								   
								    Integer netAvailableUnits = 0;  
								    Double netInventoryValue=0.0;
								    if(latestUpdatedCompactInventoryDetails.containsKey(prodId)) {
								    	Integer latestAvailableUnits = 0;
								    	Double latestAvailableInventoryValue=0.0;
								    	List<CompactInventoryItemsCollector> latestProdList = latestUpdatedCompactInventoryDetails.get(prodId);
								    	for(CompactInventoryItemsCollector compactInvItem : latestProdList){
								    		latestAvailableUnits += compactInvItem.getProductAvailableUnits() ;
								    		latestAvailableInventoryValue += compactInvItem.getInventoryValue();
								    	}
								    	netAvailableUnits = latestAvailableUnits-toBeNegatedAvailableUnits ;
							    		netInventoryValue = latestAvailableInventoryValue-toBeNegatedInventoryValue;
								    }
								    
								    
								    // now we setup the final value for summary item (availableUnits in summary = availableUnits in summary + netAvailable Units)
								    
								    Double currentInventorySummaryValue = inventorySummary.getUnitRate()*inventorySummary.getAvailableUnits();
								    inventorySummary.setAvailableUnits(inventorySummary.getAvailableUnits() + netAvailableUnits);
							    	Double newInventorySummaryValue = currentInventorySummaryValue+netInventoryValue;
							    	Double newSummaryUnitRate = newInventorySummaryValue/inventorySummary.getAvailableUnits();
							    	inventorySummary.setUnitRate(newSummaryUnitRate);
								    
								    
								    
								   
							    	/*Double inventorySummaryUnitRate = inventorySummary.getUnitRate();
							    	Double inventorySummaryValue = inventorySummary.getUnitRate()*inventorySummary.getAvailableUnits();
							    	
							    	Double totalNewUnitRate = 0.0;
							    	Integer totalAvailableUnits = 0;
							    	if(orphanInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
							    		CompactInventoryItemsCollector orphanInvItem = orphanInventoryItemDetails.get(inventoryItem.getProduct().getId());
							    		if(orphanInvItem.getInventoryId().equals(inventoryItem.getId())) {
							    			Double orphanInventoryValue = orphanInvItem.getProductUnitRate()*orphanInvItem.getProductAvailableUnits();
								    		if(inventorySummaryValue.compareTo(orphanInventoryValue) > 0 && inventorySummary.getAvailableUnits().compareTo(orphanInvItem.getProductAvailableUnits()) > 0) {
								    			totalNewUnitRate = (inventorySummaryValue-orphanInventoryValue)/(inventorySummary.getAvailableUnits()-orphanInvItem.getProductAvailableUnits());
								    		} 
								    		
									    	
									    	totalAvailableUnits = inventorySummary.getAvailableUnits();
									    	totalAvailableUnits -= orphanInvItem.getProductAvailableUnits();
							    		}else{
							    			totalAvailableUnits = inventorySummary.getAvailableUnits();
									    	totalAvailableUnits += inventoryItem.getAvailableUnits();
								    		if(existingInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
								    			CompactInventoryItemsCollector existingInvItem = existingInventoryItemDetails.get(inventoryItem.getProduct().getId());
								    			totalAvailableUnits-=existingInvItem.getProductAvailableUnits();
								    			if(totalAvailableUnits <0) totalAvailableUnits=0;
								    		}
								    		totalNewUnitRate = (inventoryValue+inventorySummaryValue)/(inventoryItem.getAvailableUnits()+inventorySummary.getAvailableUnits());
							    		}
							    		
							    	}else {
							    		totalAvailableUnits = inventorySummary.getAvailableUnits();
								    	totalAvailableUnits += inventoryItem.getAvailableUnits();
							    		if(existingInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
							    			CompactInventoryItemsCollector existingInvItem = existingInventoryItemDetails.get(inventoryItem.getProduct().getId());
							    			totalAvailableUnits-=existingInvItem.getProductAvailableUnits();
							    			if(totalAvailableUnits <0) totalAvailableUnits=0;
							    		}
							    		totalNewUnitRate = (inventoryValue+inventorySummaryValue)/(inventoryItem.getAvailableUnits()+inventorySummary.getAvailableUnits());
								    	
								    	
							    	}
							    	// calculate the new unit rate by adding currentinventory and current inventory summary avaiable untis and rates
							    	
							    	inventorySummary.setAvailableUnits(totalAvailableUnits);
							    	inventorySummary.setUnitRate(totalNewUnitRate);*/
							    	
								    	
							    	inventorySummary.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
							    	inventorySummary.setUpdatedBy(purchaseOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : purchaseOrderView.getLoggedInUser());
							    	
								    			
								    								   
								    	
								    
								   
					
					}
					invSummaryForSelectedProducts.addAll(newInventorySummaryItems);
					inventorySummaryRepo.saveAll(invSummaryForSelectedProducts);
					
					
				}
				
	            return fullyLoadedPersistedOrder; 
	        		 
	}
	
public Map<Integer, List<CompactInventoryItemsCollector>> getCompactInventoryItemsDetails(Set<UUID> inventoryIds){
		
	
			
		
		
		if(inventoryIds.size() > 0) {
			String idInclause = "";
			int i=1;
			for(UUID invId : inventoryIds) {
				if(i==1) idInclause = " where inv.id in (";
				idInclause+="'"+invId+"'";
				i++;
				if(i<=inventoryIds.size()) idInclause+=", ";
				if(i>inventoryIds.size()) idInclause+=")";
			}
			String ORPHAN_INVENTORY_ITEMS_SELECT_QUERY= "select inv.id as inventoryId,inv.batch_number as batchNumber,inv.product_id as productId,inv.available_units as availableUnits,inv.unit_rate as unitRate \r\n" + 
					"	from inventory.inventory inv ";
			
			ORPHAN_INVENTORY_ITEMS_SELECT_QUERY+=idInclause;
			return (Map<Integer, List<CompactInventoryItemsCollector>>) jdbcTemplate.query(ORPHAN_INVENTORY_ITEMS_SELECT_QUERY,compactInventoryItemDetailsResultSetExtractor);
		}
		
		
		return null;
		
		
		
		
	}

public PurchaseOrderSummaryListViewDTO getPurchaseOrderSummaryListByOrganization(UUID organizationId)  {
	
	String PURCHASE_ORDER_SUMMARY_LIST_QUERY= "select org.id as organizationId,org.name as organizationName,loc.id as locationId,loc.name as locationName, " + 
			"	sup.id as supplierId, sup.name as supplierName,  po.id as purchaseOrderId, po.order_date as purchaseOrderDate, po.order_number as purchaseOrderNumber " +
			"  from inventory.order po " +
			"  inner join inventory.supplier sup on po.supplier_id=sup.id " +
			"  inner join project_asclepius.location loc on po.location_id=loc.id" +
			"  inner join project_asclepius.organization org on po.organization_id=org.id " +
			"     where org.id ='" + organizationId+"' and po.order_type='Purchase'" +
			"   order by purchaseOrderNumber desc ";
	
	
	
	
	return (PurchaseOrderSummaryListViewDTO) jdbcTemplate.query(PURCHASE_ORDER_SUMMARY_LIST_QUERY,purchaseOrderSummaryListViewResultextractor);
}





}