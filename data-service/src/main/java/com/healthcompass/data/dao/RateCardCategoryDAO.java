package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardCategoryModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardCategoryModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.querydsl.core.types.Predicate;

@Component
public class RateCardCategoryDAO {
	
	
	@Autowired 
	LocationSearchPredicates preds;
	
	@Autowired
	private RateCardCategoryRepository rateCardCategoryRepo;
	
	
	@Autowired
	private RateCardCategoryModelAssembler  rateCardCategoryModelAssembler;
	
	@Autowired
	private ServiceBillEntityAssembler serviceBillEntityAssembler;
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	RateCrdCatDistinctDescResultsetExtractor rateCrdCatDistinctDescResultsetExtractor;
	
	private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();
	
	public Optional<RateCardCategoryModel> findOne(Integer id) {
		
		
		return rateCardCategoryRepo.findById(id)
		.map(rateCardCategoryModelAssembler::toModel) ;
		//return serviceBill;
		
		//Optional<User> user = userRepository.findById(id);
		//Optional
		 //Resource<User> resource =  new Resource<User>(user);
		// return resource;
		
	}
	
public Location checkAndPersistNewRateCardCategories(LocationRateCardsViewDTO locationRateCardsViewFromClient) {
		try {
		LoggedInUser user = new LoggedInUser();
		user.setUserId(locationRateCardsViewFromClient.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : locationRateCardsViewFromClient.getLoggedInUser());
	 
		loggedInUser.set(user); // thread local for subsequent threads to provide loggged in user
		Location LocationObj = new Location();
		//get the Location Obj from Client
		//LocationEntityAssembler.toEntity(locationModel,LocationObjFromClient);
		// 
		
		LocationObj.setId(locationRateCardsViewFromClient.getLocationId());
		LocationObj.setName(locationRateCardsViewFromClient.getLocationName());
		for (LocationRateCardViewDTO rateCard : locationRateCardsViewFromClient.getRateCards()) {
				 
			RateCard rateCardToPersist = new RateCard();
			rateCardToPersist.setId(rateCard.getId());
			rateCardToPersist.setDisplay(rateCard.getDisplay());
			rateCardToPersist.setDescription(rateCard.getDescription());
			rateCardToPersist.setActive(rateCard.getActive());
			rateCardToPersist.setRate(rateCard.getRate());
			RateCardCategory rateCat = new RateCardCategory();
			rateCat.setId(rateCard.getRateCardCategory().getRateCardCategoryId());
			rateCat.setCode(rateCard.getRateCardCategory().getRateCardCategoryCode());
			rateCat.setDescription(rateCard.getRateCardCategory().getRateCardCatgeoryDescription());
			rateCardToPersist.setRateCardCategory(rateCat);	
			
			if(LocationObj.getRateCards() == null) {
				List<RateCard> rateCards = new ArrayList<RateCard>();
				LocationObj.setRateCards(rateCards);
			}
			LocationObj.getRateCards().add(rateCardToPersist);
		}
		return persistNewRateCardCategories(LocationObj);
	}finally {
		loggedInUser.remove();
	}
	
}
		public Location persistNewRateCardCategories(Location locationFromClient) {
			
			
	       
			
					
			// get distinct set of rate card category desciptions
			String SQL_RATE_CARD_CAT_DESC = "select distinct id,description from billing.rate_card_category";
			Map rateCardCatDistinctDescFromDB = (HashMap) jdbcTemplate.query(SQL_RATE_CARD_CAT_DESC,rateCrdCatDistinctDescResultsetExtractor);			
			List<RateCard> rateCardsTobePersisted = locationFromClient.getRateCards();
			
			Map<String, List<RateCard>> rateCardsWithNewCat = new HashMap<String,List<RateCard>>();
			Map<String, RateCardCategory> newRateCardCategoriesMap = new HashMap<String,RateCardCategory>();
			List<RateCardCategory> newRateCardCategoriesListForPersist = new ArrayList<RateCardCategory>();
			for(RateCard rateCardToBePersisted : rateCardsTobePersisted) {
				String rateCardCategoryDesc =  rateCardToBePersisted.getRateCardCategory().getDescription();
				
					
				    if(rateCardCatDistinctDescFromDB.containsKey((rateCardCategoryDesc))) {
				    	RateCardCategory existingFromDB = (RateCardCategory) rateCardCatDistinctDescFromDB.get(rateCardCategoryDesc);
				    	rateCardToBePersisted.getRateCardCategory().setId(existingFromDB.getId());
				    	rateCardToBePersisted.getRateCardCategory().setDescription(existingFromDB.getDescription());
				    }else {
				    	
				    	RateCardCategory newRateCardCategory = rateCardToBePersisted.getRateCardCategory();
				    	newRateCardCategory.setId(null);
				    	String code=null;
				    	if(rateCardCategoryDesc.length() >=3) {
				    		code = rateCardCategoryDesc.substring(0, 3);
				    	}else{
				    		code = rateCardCategoryDesc;
				    	}
				    	newRateCardCategory.setCode(code);
				    	newRateCardCategory.setCreatedBy(loggedInUser.get().getUserId());
				    	newRateCardCategory.setActive(true); //always
				    	newRateCardCategory.setCreatedBy(loggedInUser.get().getUserId());
				    	newRateCardCategory.setCreatedOn(new Timestamp(System.currentTimeMillis()));
				    	newRateCardCategory.setUpdatedBy(loggedInUser.get().getUserId());
				    	newRateCardCategory.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				    	
				    	newRateCardCategoriesListForPersist.add(newRateCardCategory);
				    	newRateCardCategoriesMap.put(rateCardCategoryDesc,newRateCardCategory);
				    	
				    	if(rateCardsWithNewCat.get(rateCardCategoryDesc) == null) {
				    		List<RateCard> categoryRateCards = new ArrayList<RateCard>();
				    		rateCardsWithNewCat.put(rateCardCategoryDesc, categoryRateCards);
				    	}
				    	rateCardsWithNewCat.get(rateCardCategoryDesc).add(rateCardToBePersisted);
				    	
				    	
				    	
				    }
			} // end for persisted
				    
				  

            List<RateCardCategory> persistedRateCardCategories = persistRateCategoriesFirstAndCommit(newRateCardCategoriesListForPersist);
	    
	    // now set the orphan rateCards with persisted Rate Card Cetgories
	    for(RateCardCategory persistedCat : persistedRateCardCategories) {
	    	String keyDesc = persistedCat.getDescription();
	    	for(RateCard orphanRateCard : rateCardsWithNewCat.get(keyDesc)){
	    		orphanRateCard.setRateCardCategory(persistedCat);
	    	}
	    	
	    } // all the orphan rate cards are ste with thei new parent rate card categorie

	    return locationFromClient;
	}
		
	private List<RateCardCategory> persistRateCategoriesFirstAndCommit(List<RateCardCategory> newRateCardCategoriesListForPersist){
		
		 List<RateCardCategory> persistedRateCardCategories = (List<RateCardCategory>) rateCardCategoryRepo.saveAll(newRateCardCategoriesListForPersist);
		 return persistedRateCardCategories;
	}
	
}
