package com.healthcompass.data.dao;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.querydsl.core.types.Predicate;

@Component

public class ServiceBillItemDAO {
	
	
	@Autowired ServiceBillItemSearchPredicates preds;
	
	@Autowired
	private ServiceBillItemRepository serviceBillItemRepository;
	
	
	@Autowired
	private ServiceBillItemModelAssembler serviceBillItemModelAssembler;
	
	
	private Predicate toPredicate(ServiceBillItemSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isServiceBillIdEq(params.getServiceBillId())
		        .and(preds.isIdsInList(params.getIdList())));
				
		// @formatter:on
	}
	
	public List<ServiceBillItem> findAll(){
		//return serviceRepository.findAll();
		return null;
	}
	
	
	public Optional<ServiceBillItemViewDTO> findOne(UUID id) {
		
		
		return serviceBillItemRepository.findById(id)
				.map(serviceBillItemModelAssembler::toModel) ;
		
		
		
	}
	
	public CollectionModel<ServiceBillItemViewDTO> findAll(ServiceBillItemSearchParams serviceBillItemParams){
		List<ServiceBillItem> serviceBillItems = (List<ServiceBillItem>) serviceBillItemRepository.findAll(toPredicate(serviceBillItemParams));
		return serviceBillItemModelAssembler.toCollectionModel(serviceBillItems);
	}
	
	
	
	
	
}
