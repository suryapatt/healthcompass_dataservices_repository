package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.PurchaseOrderMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.SalesOrderMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.LocationSequenceNumbers;
import com.healthcompass.data.model.LocationSequenceNumbersPK;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.InventoryRepository;
import com.healthcompass.data.repository.InventorySummaryRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.LocationSequenceNumbersRepository;
import com.healthcompass.data.repository.OrderRepository;
import com.healthcompass.data.repository.ProductRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.SalesOrderInventoryProductSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.CompactInventoryItemsAndOrderItemsCollector;
import com.healthcompass.data.util.CompactInventoryItemsCollector;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentHistoryViewResultsetExtractor;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.CompactInventoryAndOitemsDetailsResultSetExtractor;
import com.healthcompass.data.view.dto.CompactInventoryItemDetailsResultSetExtractor;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationViewResultSetExtrator;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewResultsetExtractor;
import com.healthcompass.data.view.dto.SalesOrderItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProdSearchListMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.SalesOrderProdSearchSummaryViewResultsetExtractor;
import com.healthcompass.data.view.dto.SalesOrderProductSearchListMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProductSearchSummaryListViewDTO;
import com.healthcompass.data.view.dto.SalesOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.SalesOrderSummaryListViewResultextractor;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;



@Component
public class SalesOrderDAO {
	
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private OrderRepository orderRepo;
	
	@Autowired
	private InventoryDAO inventoryDAO;
	
	@Autowired
	private LocationRepository locationRepo;
	
	@Autowired
	private InventorySummaryRepository inventorySummaryRepo;
	
	@Autowired
	private InventoryRepository inventoryRepo;
	
	@Autowired
	private LocationSequenceNumbersRepository locationSequenceNumbersRepository;
	
	@Autowired 
	SalesOrderMainViewToEntityAssembler salesOrderMainViewToEntityAssembler;
	
	@Autowired
	CompactInventoryAndOitemsDetailsResultSetExtractor compactInventoryAndOitemsDetailsResultSetExtractor;
	
	@Autowired 
	CompactInventoryItemsCollector compactInventoryItemsCollector;
	
	@Autowired 
	ProductSummaryOrganizationViewResultSetExtrator productSummaryOrganizationViewResultSetExtrator;
	
	@Autowired
	SalesOrderProdSearchListMainViewResultsetExtractor salesOrderProdSearchListMainViewResultsetExtractor;
	
	@Autowired
	SalesOrderProdSearchSummaryViewResultsetExtractor salesOrderProdSearchSummaryViewResultsetExtractor;
	
	@Autowired
	SalesOrderSummaryListViewResultextractor salesOrderSummaryListViewResultextractor;
	
	 @Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
		public Optional<Order> findOne(UUID id) {
			
		
			return orderRepo.findById(id);
					
						
		}

	
	

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Order createOrUpdateSalesOrder(SalesOrderMainViewDTO salesOrderView) {
		
		 	boolean isNewOrder = false;
		 	Order order = null;
		   	if(salesOrderView.getOrder().getSalesOrderId() == null) {
			   isNewOrder=true;
		   	}
		   if(isNewOrder) {
				
				
				order = new Order();
				order.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
				order.setCreatedBy(salesOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : salesOrderView.getLoggedInUser());
				order.setOrderDate(new java.sql.Timestamp(System.currentTimeMillis()));
				order.setOrderType("Sales");
				Organization org = new Organization();
				order.setOrganization(org);
				Location loc = new Location();
				order.setLocation(loc);
				List<OrderItem> orderItemsList = new ArrayList<OrderItem>();
				order.setOrderItems(orderItemsList);
				
				
				//order.setInventoryItems(inventoryItemsList);
				order.setInventoryItems(null);
				
		      }	else{
		    		Optional<Order> optOrder = orderRepo.findById(salesOrderView.getOrder().getSalesOrderId());
		    		order = optOrder.get();
		    		
		    		
		    	}
		   
		       // let us get all the inventory ids items corresponding to the inventory ids received from client 
		        List<UUID> newInventoryIdList = new ArrayList<UUID>();
                for(SalesOrderItemViewDTO orderItem : salesOrderView.getOrder().getOrderItems() ) {
                	newInventoryIdList.add(orderItem.getInventory().getInventoryId());
                }
		        List<InventoryItem> newInventoryItemList = inventoryDAO.findAllByIds(newInventoryIdList);
				
		        
		        // let us also get the List of inventory items that are previously associated with Order items but may be candidates
		        // for orphanism in the following save as some of the orderitem/inventory association may have changed from cleint
		        
		        Set<UUID> orphanInventoryIds = new HashSet<UUID>();
				Set<UUID> existingInventoryIds = new HashSet<UUID>();
				
				 for(OrderItem orderItem : order.getOrderItems()) {
					
					 Optional<SalesOrderItemViewDTO> optTargetOrderItemDTO = salesOrderView.getOrder().getOrderItems().stream()
								.filter(orderItemDTO -> (orderItemDTO.getInventory().getInventoryId().equals(orderItem.getInventory().getId()))).findFirst();
								if(optTargetOrderItemDTO.isPresent()) {
									
									existingInventoryIds.add(orderItem.getInventory().getId());
								}else{
									orphanInventoryIds.add(orderItem.getInventory().getId());
								}
									
				 } 
							    
				
				
	               // get the compact list for previous state for Orphan and Existing Inventory items including rhe order item quantities
			        
				Map<Integer,List<CompactInventoryItemsAndOrderItemsCollector>> orphanInventoryItemDetails = 
						new HashMap<Integer,List<CompactInventoryItemsAndOrderItemsCollector>>();
				
				if(orphanInventoryIds.size() > 0) {
					orphanInventoryItemDetails = getCompactInventoryItemsDetails(orphanInventoryIds,order.getId());
				}
				
				Map<Integer,List<CompactInventoryItemsAndOrderItemsCollector>> existingInventoryItemDetails = new HashMap<Integer,List<CompactInventoryItemsAndOrderItemsCollector>>();
				if(existingInventoryIds.size() > 0) {
					existingInventoryItemDetails = getCompactInventoryItemsDetails(existingInventoryIds,order.getId());
				}
		              
		        
		        // Now we will construct the order/orderitems/inventory entity structure by passing the view model and also above retrieved inventory items
				Order orderUpdatedWithChanges = salesOrderMainViewToEntityAssembler.toEntity(salesOrderView,order,newInventoryItemList);
				
				// calculate order value, discount , net order value, order items rate, 
				// at the same time we also identify the orphan items that will be deleted in the following save. prepare to delete the orphan order item
				Double orderValue=0.0;
				Double orderNetValue=0.0;
				Double orderDiscount=0.0;
				Double itemUnitRate = 0.0;
				Double itemDiscountedRate = 0.0;
				Double itemNetValue = 0.0;
				
				List<OrderItem> toBeDeletedOrderItems = new ArrayList<OrderItem>();
				
				for(OrderItem orderItem : orderUpdatedWithChanges.getOrderItems()) {
					if(orderItem.getOrder() == null) {
						
						toBeDeletedOrderItems.add(orderItem);
						//orderItem.setInventory(null);				
					}else {
						orderItem.setMrp(orderItem.getRate()*orderItem.getQuantity());
						//itemUnitRate = orderItem.getMrp()/orderItem.getQuantity();
						orderItem.setDiscountedRate(0.0);
						orderItem.setDiscount(orderItem.getDiscountedRate()*orderItem.getQuantity());
						
						itemNetValue= orderItem.getMrp()-orderItem.getDiscount();
						
						
						
						orderItem.setNetItemValue(itemNetValue);
						
						orderValue+=orderItem.getMrp();
						orderDiscount+=orderItem.getDiscount();
						
					}
					
				
				}
								
							
				
				// before saving the inventory items prepare a list of  Inventory items that were added earlier to 
				// the order during creation but changed to a different inventory item during edit. We call these as Orphan Inventory IDs. There can also be a different case where an order item
				//is deleted from order during edit but created during creation. The inventory items associated with such order items are also called as orphan inventory items
				//is we need to add back the availableUnits for these type of inventory Items and also add these available Units to Inventory Summary available units for each product
				
			     // get the compactlists with  qtys abd available units for existing and orphan items.
				// First just pcikup the ids of orphans and exisiting
				
				
				
				
				// Adjust the available units of inventory items that are updated with quantities that are different from previous save
				// Finally we set up the  Available Units for the existing Inventory items which are previously saved
				// before we save. We need to add up the available units that are reduced in the previous save
				for(OrderItem orderItem : orderUpdatedWithChanges.getOrderItems()) {
					
					if(existingInventoryItemDetails.containsKey(orderItem.getInventory().getProduct().getId())) {
						List<CompactInventoryItemsAndOrderItemsCollector> previousItemStateList = 
								(List<CompactInventoryItemsAndOrderItemsCollector>) existingInventoryItemDetails.get((orderItem.getInventory().getProduct().getId()));
						for(CompactInventoryItemsAndOrderItemsCollector previousItemState : previousItemStateList) {
							if(previousItemState.getInventoryId().equals(orderItem.getInventory().getId())) {
								Integer previousItemQty = previousItemState.getOrderItemQuantity();
								//bumpup the inventory available units
								orderItem.getInventory().setAvailableUnits(orderItem.getInventory().getAvailableUnits()+previousItemQty);
								 if(orderItem.getInventory().getAvailableUnits().compareTo(0) == -1)  orderItem.getInventory().setAvailableUnits(0);
							}
						}
						
						
					}
					
					
				}	
					
				// Now remove the orphanOrder Items
				order.getOrderItems().removeAll(toBeDeletedOrderItems);
				
				// set the calculated order values
				order.setOrderValue(orderValue);
				order.setDiscount(orderDiscount);
				order.setNetOrderValue(orderValue-orderDiscount);
				
				// SAVE THE ORDER (ORDER/ORDERITEMS/INVENTORY)
				if (isNewOrder) {
					// generate new order number
					//Integer orderNumber = orderRepo.generateNextOrderNumber();
					Optional<Location> optLocation = locationRepo.findById( orderUpdatedWithChanges.getLocation().getId());
		    		Location location = optLocation.get();
		    		String locationCode = location.getLocationCode();
		    		
		    		Integer orderNumber = null;
		    		
		    		LocationSequenceNumbers locationSequenceNumbers=null;
		    		Optional<LocationSequenceNumbers> optLocationLocationSequences = locationSequenceNumbersRepository.findById(new LocationSequenceNumbersPK(locationCode, "sales_order_number"));
		    		if(!optLocationLocationSequences.isPresent()) {
		    			orderNumber=1;
		    			
		    			locationSequenceNumbers= new LocationSequenceNumbers(new LocationSequenceNumbersPK(locationCode, "sales_order_number"),orderNumber,String.format("%05d" ,orderNumber));
		    			
		    		}else {
		    			locationSequenceNumbers=(LocationSequenceNumbers) optLocationLocationSequences.get();
		    			orderNumber=locationSequenceNumbers.getSequenceNumber()+1;
		    			locationSequenceNumbers.setSequenceNumber(orderNumber);
		    			locationSequenceNumbers.setSequenceNumberPadded(String.format("%05d" ,orderNumber));
		    		}
		    		
		    		
		    		locationSequenceNumbersRepository.save(locationSequenceNumbers);
		    		
		    		orderUpdatedWithChanges.setOrderNumber(locationCode+""+String.format("%05d" ,orderNumber));
				
		    		
		    		
		    		
				}
							
				// SAVE THE ORDER (ORDER/ORDERITEMS/INVENTORY)
				Order proxyPersistedOrder = (Order) orderRepo.save(orderUpdatedWithChanges);
				
				// update the state of Orphaned Inventory Items because of above save.
				// we need to bump up the available units for these items. First get the list of orphan items
				// by passing orphan inventory ids
				List<UUID> tempList = new ArrayList<UUID>();
				tempList.addAll(orphanInventoryIds);
				 List<InventoryItem> orphanInventoryItemListFromDB = inventoryDAO.findAllByIds(tempList);
				 
				
				 for(InventoryItem invItem : orphanInventoryItemListFromDB) {
					 if(orphanInventoryItemDetails.containsKey(invItem.getProduct().getId())) {
						 List<CompactInventoryItemsAndOrderItemsCollector> previousItemStateOrphanList = 
									(List<CompactInventoryItemsAndOrderItemsCollector>) orphanInventoryItemDetails.get(invItem.getProduct().getId());
						 for(CompactInventoryItemsAndOrderItemsCollector previousItemStateOfOrphan : previousItemStateOrphanList) {
								if(previousItemStateOfOrphan.getInventoryId().equals(invItem.getId())) {
									Integer previousItemQty = previousItemStateOfOrphan.getOrderItemQuantity();
									//bumpup the inventory available units
									invItem.setAvailableUnits(invItem.getAvailableUnits()+previousItemQty);
									if(invItem.getAvailableUnits().compareTo(0) == -1)  invItem.setAvailableUnits(0);
									invItem.setUpdatedBy(salesOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : salesOrderView.getLoggedInUser());
								}
						 }	
					}
				 }
				 // persist orphan inventory items with previous state
				 if(!orphanInventoryItemDetails.isEmpty() && orphanInventoryItemListFromDB.size() >0) {
					 inventoryRepo.saveAll(orphanInventoryItemListFromDB);
				 }
				
				
				// create/update inventory summary
				// get the updated state of the Order
				Optional<Order> optFullyLoadedPersistedOrder = orderRepo.findById(proxyPersistedOrder.getId());
				Order fullyLoadedPersistedOrder = optFullyLoadedPersistedOrder.isPresent() ? optFullyLoadedPersistedOrder.get() : null;
				
				// Let us prepare for updating/inserting Inventory summary items based on updated state of the Order
				if(fullyLoadedPersistedOrder != null) {
					Set<Integer> productIdSet = new HashSet<Integer>(); // get all the distinct product ids from the order inventory items
					
					// build the  the latest state of the updated inventory in compact mode while iterating the inventory items
					Map<Integer,List<CompactInventoryItemsAndOrderItemsCollector>> latestUpdatedCompactInventoryDetails = new HashMap<Integer,List<CompactInventoryItemsAndOrderItemsCollector>>();
					for(OrderItem orderItem : fullyLoadedPersistedOrder.getOrderItems()) {
						
							 InventoryItem inventoryItem = orderItem.getInventory();
						     productIdSet.add(inventoryItem.getProduct().getId()); // we need this set to pull the InventorySummary list by product id
						
							//prepare a compact latest updated state of the inventory
						     CompactInventoryItemsAndOrderItemsCollector item = new CompactInventoryItemsAndOrderItemsCollector();
				        	 item.setInventoryId(inventoryItem.getId()); 
				        	 item.setBatchNumber(inventoryItem.getBatchNumber());
				        	 Integer prodId = inventoryItem.getProduct().getId();
				        	 item.setProductId(prodId);
				        	 
				        	 Integer availableUnits = inventoryItem.getAvailableUnits();
				        	 Double unitRate = inventoryItem.getUnitRate();
				        	 //Double inventoryValue=availableUnits*unitRate;
				        	 Double inventoryValue=orderItem.getQuantity()*unitRate; // we have to consider the quantity of the current order item here not the available units from inventory
				        	 
				        	 item.setProductAvailableUnits(availableUnits);
				        	 item.setProductUnitRate(unitRate);
				        	 item.setInventoryValue(inventoryValue);
				        	 item.setOrderItemQuantity(orderItem.getQuantity());
				        	 
				        	 if(latestUpdatedCompactInventoryDetails.containsKey(prodId)) {
				        		 ((List<CompactInventoryItemsAndOrderItemsCollector>) latestUpdatedCompactInventoryDetails.get(prodId)).add(item);
				        		 
				        	 }else {
				        		 List<CompactInventoryItemsAndOrderItemsCollector> itemList = new ArrayList<CompactInventoryItemsAndOrderItemsCollector>();
				        		 itemList.add(item);
				        		 latestUpdatedCompactInventoryDetails.put(item.getProductId(),itemList);
				        		 
				        		 
				        	 }
				        	 
						
					}
					
					// if some inventory items were deleted because of becoming orphan from above save, we need to reduce the quantities in Inventory summery
					// so we will add the products related to orphaned inventory /deleted inventory items to get their summary
					if(!orphanInventoryItemDetails.isEmpty()) 	productIdSet.addAll(orphanInventoryItemDetails.keySet());
					
					// Now get the Inventory summary for each product in the set
					List<InventorySummary> invSummaryForSelectedProducts = inventorySummaryRepo.getInventorySummaryForProductSet(fullyLoadedPersistedOrder.getOrganization().getId(),
							
								fullyLoadedPersistedOrder.getLocation().getId(), productIdSet);
					
					// let us segregate new summary items and existing summary items
					List<InventorySummary> newInventorySummaryItems = new ArrayList<InventorySummary>();
					
					for(Integer prodId : productIdSet) {
						   
						    // check if the summary exists for a given product
							Optional<InventorySummary> optInventorySummaryItem = invSummaryForSelectedProducts.stream()
								.filter(inventorySummaryItem -> inventorySummaryItem.getProduct().getId().compareTo(prodId) == 0).findFirst();
									InventorySummary inventorySummary = null;
								    if(!optInventorySummaryItem.isPresent()) { 					    	
								    }else{
								    	inventorySummary = optInventorySummaryItem.get(); // if present means get the referrence to te summary item
								    }
								    
								    
								    // now lets do some calculations and derive net Available Units for each product.
								    Integer toBeBumpedUpAvailableUnits = 0;  
								    Double toBeBumpedInventoryValue=0.0;
								    // calculate net available units = latest available units -(orphan units + previous existing units)
								    // for this we need to iterate 3 maps that were constructed earlier orphan map, existing map and latest map
								    // while iterating each productid, we will derive at netAvailableUnits and netInventory Value
								    
								    // First iterate Orphan list
								    if(orphanInventoryItemDetails.containsKey(prodId)) {
								    	List<CompactInventoryItemsAndOrderItemsCollector> orphanProdList = orphanInventoryItemDetails.get(prodId);
								    	for(CompactInventoryItemsAndOrderItemsCollector compactInvItem : orphanProdList){
								    		//toBeBumpedUpAvailableUnits+=compactInvItem.getProductAvailableUnits();
								    		toBeBumpedUpAvailableUnits+=compactInvItem.getOrderItemQuantity();
								    		//toBeBumpedInventoryValue+=compactInvItem.getInventoryValue();
								    		toBeBumpedInventoryValue+=compactInvItem.getProductUnitRate()*compactInvItem.getOrderItemQuantity(); // consider item quantity from order item nit the inventory available units
								    	}
								    }
								    
								    // next iterate the previous list before order update
								    if(existingInventoryItemDetails.containsKey(prodId)) {
								    	List<CompactInventoryItemsAndOrderItemsCollector> exisstingProdList = existingInventoryItemDetails.get(prodId);
								    	for(CompactInventoryItemsAndOrderItemsCollector compactInvItem : exisstingProdList){
								    		//toBeBumpedUpAvailableUnits+=compactInvItem.getProductAvailableUnits();
								    		toBeBumpedUpAvailableUnits+=compactInvItem.getOrderItemQuantity(); // consider item quantity from order item nit the inventory available units
								    		//toBeBumpedInventoryValue+=compactInvItem.getInventoryValue();
								    		toBeBumpedInventoryValue+=compactInvItem.getProductUnitRate()*compactInvItem.getOrderItemQuantity(); // consider item quantity from order item nit the inventory available units
								    	}
								    }
								    
								    
								    // After above two loops netAvailableUnits and netInventory Value contains the units that need to be bumped up from the latest list
								    // now we iterate the latest list and add the values 
								    
								   
								    Integer netAvailableUnits = 0;  
								    Double netInventoryValue=0.0;
								    if(latestUpdatedCompactInventoryDetails.containsKey(prodId)) {
								    	Integer latestAvailableUnits = 0;
								    	Double latestAvailableInventoryValue=0.0;
								    	List<CompactInventoryItemsAndOrderItemsCollector> latestProdList = latestUpdatedCompactInventoryDetails.get(prodId);
								    	for(CompactInventoryItemsAndOrderItemsCollector compactInvItem : latestProdList){
								    		//latestAvailableUnits -= compactInvItem.getProductAvailableUnits() ;
								    		latestAvailableUnits -= compactInvItem.getOrderItemQuantity(); // consider item quantity from order item nit the inventory available units
								    		//latestAvailableInventoryValue -= compactInvItem.getInventoryValue();
								    		latestAvailableInventoryValue -= compactInvItem.getProductUnitRate()*compactInvItem.getOrderItemQuantity(); // consider item quantity from order item nit the inventory available units
								    	}
								    	netAvailableUnits = latestAvailableUnits;
							    		netInventoryValue = latestAvailableInventoryValue;
								    }
								    
								    // ad the amounts to be added from orphan and existing inventory items that were updated to a different qty in this save
								    netAvailableUnits += toBeBumpedUpAvailableUnits;
								    netInventoryValue += toBeBumpedInventoryValue;
								    // now we setup the final value for summary item (availableUnits in summary = availableUnits in summary + netAvailable Units)
								    
								    Double currentInventorySummaryValue = inventorySummary.getUnitRate()*inventorySummary.getAvailableUnits();
								    inventorySummary.setAvailableUnits(inventorySummary.getAvailableUnits() + netAvailableUnits);
								    if(inventorySummary.getAvailableUnits().compareTo(0) == -1)  inventorySummary.setAvailableUnits(0);
							    	Double newInventorySummaryValue = currentInventorySummaryValue+netInventoryValue;
							    	Double newSummaryUnitRate = newInventorySummaryValue/inventorySummary.getAvailableUnits();
							    	inventorySummary.setUnitRate(newSummaryUnitRate);
								    
								    
								    
								   
							    	/*Double inventorySummaryUnitRate = inventorySummary.getUnitRate();
							    	Double inventorySummaryValue = inventorySummary.getUnitRate()*inventorySummary.getAvailableUnits();
							    	
							    	Double totalNewUnitRate = 0.0;
							    	Integer totalAvailableUnits = 0;
							    	if(orphanInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
							    		CompactInventoryItemsCollector orphanInvItem = orphanInventoryItemDetails.get(inventoryItem.getProduct().getId());
							    		if(orphanInvItem.getInventoryId().equals(inventoryItem.getId())) {
							    			Double orphanInventoryValue = orphanInvItem.getProductUnitRate()*orphanInvItem.getProductAvailableUnits();
								    		if(inventorySummaryValue.compareTo(orphanInventoryValue) > 0 && inventorySummary.getAvailableUnits().compareTo(orphanInvItem.getProductAvailableUnits()) > 0) {
								    			totalNewUnitRate = (inventorySummaryValue-orphanInventoryValue)/(inventorySummary.getAvailableUnits()-orphanInvItem.getProductAvailableUnits());
								    		} 
								    		
									    	
									    	totalAvailableUnits = inventorySummary.getAvailableUnits();
									    	totalAvailableUnits -= orphanInvItem.getProductAvailableUnits();
							    		}else{
							    			totalAvailableUnits = inventorySummary.getAvailableUnits();
									    	totalAvailableUnits += inventoryItem.getAvailableUnits();
								    		if(existingInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
								    			CompactInventoryItemsCollector existingInvItem = existingInventoryItemDetails.get(inventoryItem.getProduct().getId());
								    			totalAvailableUnits-=existingInvItem.getProductAvailableUnits();
								    			if(totalAvailableUnits <0) totalAvailableUnits=0;
								    		}
								    		totalNewUnitRate = (inventoryValue+inventorySummaryValue)/(inventoryItem.getAvailableUnits()+inventorySummary.getAvailableUnits());
							    		}
							    		
							    	}else {
							    		totalAvailableUnits = inventorySummary.getAvailableUnits();
								    	totalAvailableUnits += inventoryItem.getAvailableUnits();
							    		if(existingInventoryItemDetails.containsKey(inventoryItem.getProduct().getId())) {
							    			CompactInventoryItemsCollector existingInvItem = existingInventoryItemDetails.get(inventoryItem.getProduct().getId());
							    			totalAvailableUnits-=existingInvItem.getProductAvailableUnits();
							    			if(totalAvailableUnits <0) totalAvailableUnits=0;
							    		}
							    		totalNewUnitRate = (inventoryValue+inventorySummaryValue)/(inventoryItem.getAvailableUnits()+inventorySummary.getAvailableUnits());
								    	
								    	
							    	}
							    	// calculate the new unit rate by adding currentinventory and current inventory summary avaiable untis and rates
							    	
							    	inventorySummary.setAvailableUnits(totalAvailableUnits);
							    	inventorySummary.setUnitRate(totalNewUnitRate);*/
							    	
								    	
							    	inventorySummary.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
							    	inventorySummary.setUpdatedBy(salesOrderView.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : salesOrderView.getLoggedInUser());
							    	
							    	
								       
								    	
								    
								   
					
					}
					invSummaryForSelectedProducts.addAll(newInventorySummaryItems);
					inventorySummaryRepo.saveAll(invSummaryForSelectedProducts);
					
				}
				
	            return fullyLoadedPersistedOrder; 
	        		 
	}
	

public Map<Integer, List<CompactInventoryItemsAndOrderItemsCollector>> getCompactInventoryItemsDetails(Set<UUID> inventoryIds, UUID orderId){
	
	
	
	if(orderId == null) {
		return new HashMap<Integer, List<CompactInventoryItemsAndOrderItemsCollector>>();
	}
	
	if(inventoryIds.size() > 0) {
		String idInclause = "";
		int i=1;
		for(UUID invId : inventoryIds) {
			if(i==1) idInclause = " and inv.id in (";
			idInclause+="'"+invId+"'";
			i++;
			if(i<=inventoryIds.size()) idInclause+=", ";
			if(i>inventoryIds.size()) idInclause+=")";
		}
		String INVENTORY_ITEMS_SELECT_QUERY= "select inv.id as inventoryId,inv.batch_number as batchNumber,inv.product_id as productId,inv.available_units as availableUnits,"
				+ "inv.unit_rate as unitRate, oItem.qty as itemQuantity from inventory.inventory inv \r\n" + 
				"	inner join inventory.order_items oItem on inv.id=oItem.inventory_id " +
				" where oItem.order_id = '"+orderId+"'";
				 
				
		
		INVENTORY_ITEMS_SELECT_QUERY+=idInclause;
		return (Map<Integer, List<CompactInventoryItemsAndOrderItemsCollector>>) jdbcTemplate.query(INVENTORY_ITEMS_SELECT_QUERY,compactInventoryAndOitemsDetailsResultSetExtractor);
	}
	
	
	return null;
	
	
	
	
}

public SalesOrderProductSearchListMainViewDTO findAllInventoryProducts(SalesOrderInventoryProductSearchParams salesOrderInventoryProductSearchParams){
	
	UUID organizationId = null;
	UUID locationId = null;
	Integer marketerId = null;
	String marketerName=null;
	List<UUID> inventoryIds=null;
	String productName=null;
	Boolean activeFlag=null;
	Map<String,Object> advancedSearchParams = new HashMap<String,Object>();
	
	boolean includeOrgIdInSearch=false;
	boolean includeLocIdInSearch=false;
	boolean includeMarketerIdInSearch=false;
	boolean includeMarketerNameInSearch=false;
	boolean includeInventoryIdInSearch=false;
	boolean includeProductNameInSearch=false;
	boolean includeAdvancedParamsInSearch=false;
	boolean includeActiveFlagInSearch=false;
	
	try {
		organizationId = salesOrderInventoryProductSearchParams.getOrganizationId();
		if(organizationId != null) includeOrgIdInSearch=true;
	}catch(Exception e) {
		organizationId=null;
	}
	
	try {
		locationId = salesOrderInventoryProductSearchParams.getLocationId();
		if(locationId != null) includeLocIdInSearch=true;
	}catch(Exception e) {
		locationId=null;
	}
	
	try {
		marketerId = salesOrderInventoryProductSearchParams.getMarketerId();
		if(marketerId != null) includeMarketerIdInSearch=true;
	}catch(Exception e) {
		marketerId=null;
	}
	
	try {
		marketerName = salesOrderInventoryProductSearchParams.getMarketerName();
		if(marketerName != null && marketerName.trim().length() != 0) includeMarketerNameInSearch=true;
	}catch(Exception e) {
		marketerName=null;
	}
	
	try {
	    inventoryIds = salesOrderInventoryProductSearchParams.getInventoryIds();
		if(inventoryIds != null && !inventoryIds.isEmpty()) includeInventoryIdInSearch=true;
	}catch(Exception e) {
		inventoryIds=null;
	}
	
	
	try {
		productName = salesOrderInventoryProductSearchParams.getProductName();
		if(productName != null && productName.trim().length() != 0) includeProductNameInSearch=true;
	}catch(Exception e) {
		productName=null;
	}
	
	try {
		advancedSearchParams = salesOrderInventoryProductSearchParams.getAdvancedSearchParams();
		if(advancedSearchParams != null && !advancedSearchParams.isEmpty()) includeAdvancedParamsInSearch=true;
	}catch(Exception e) {
		advancedSearchParams=null;
	}
	
	try {
		activeFlag = salesOrderInventoryProductSearchParams.getActive();
		if(activeFlag != null) {
			includeActiveFlagInSearch=true;
		}else {
			activeFlag=true;
			includeActiveFlagInSearch=true;
		}
	}catch(Exception e) {
		activeFlag=null;
	}
	String META_TAG_ATTRIBUTES_SUBQUERY=" ";
	
	
		
	
	String PRODUCT_SEARCH_SELECT_QUERY= "select org.id as organizationId,org.name as organizationName,loc.id as locationId,loc.name as locationName,mark.id as marketerId,"
			+ "mark.name as marketerName,inv.id as inventoryId, inv.batch_number as batchNumber, inv.expiry_date as expiryDate, inv.unit_rate as unitRate, \r\n" + 
			" inv.available_units as availableUnits,prod.id as productId,prod.name as productName,\r\n" + 
			" prod.description as productDescription, \r\n" + 
			" prod.gst_rate as prodGstRate,prod.sales_tax_rate as\r\n" + 
			" prodSalesTaxRate,prod.central_tax_rate as prodCentralTaxRate, prod.vat_rate as prodVatRate,\r\n" + 
			" mtag.id as metaTagId,mtag.name as metaTagName,mtagAttr.value as metaTagAttributeValue,mTagAttr.id as metaTagAttributeId,"
			+ "mTagAttr.active as metaTagAttributeActive \r\n";
			
	
			
			    
	      
			PRODUCT_SEARCH_SELECT_QUERY+= " from inventory.inventory inv "
					+ "inner join project_asclepius.location loc on inv.location_id=loc.id\r\n" + 
					"   inner join project_asclepius.Organization org on inv.organization_id=org.id\r\n" + 
					"    inner join inventory.product prod  on inv.product_id=prod.id\r\n" + 
					"   inner join inventory.marketer mark on prod.marketer_id=mark.id \r\n" + 
					"   inner join inventory.meta_tag_attributes mtagAttr on prod.id = mtagAttr.product_id\r\n" + 
					" inner join inventory.meta_tag mTag on mtagAttr.meta_tag_id=mTag.id\r\n" ; 
			         
			
			PRODUCT_SEARCH_SELECT_QUERY += " where inv.available_Units > 0 and inv.expiry_date > current_date ";
	         
			String andWhere = " and ";
	         
	         if(includeActiveFlagInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.active  = " + activeFlag +" ";
				 
				 
			 }
	         
	         if(includeInventoryIdInSearch){
				 String inventoryIdInClause="( ";
				 int j = 1;
				 for(UUID invId : inventoryIds) {
		    			
					 inventoryIdInClause+="'"+invId+"' ";
		    			
	    			if(j<inventoryIds.size()) {
	    				inventoryIdInClause+=",";
	    				j++;
	    			}
	    			
		    			
		    			
		    	}
				 inventoryIdInClause+=")";
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" inv.id in " + inventoryIdInClause+" ";
				// andWhere = " and ";
				 
			 }
	         
			 if(includeProductNameInSearch){
				
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.name like '%" + productName+"%'";
				// andWhere = " and ";
				 
			 }
			 if(includeMarketerNameInSearch){
				
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.name like '%" + marketerName+"%'";
				 //andWhere = " and ";
			 }
			 if(includeMarketerIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.id =" + marketerId+" ";
				// andWhere = " and ";
				 
			 }
			 if(includeLocIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" loc.id ='" + locationId+"'";
				 //andWhere = " and ";
			 }
			 if(includeOrgIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" org.id ='" + organizationId+"'";
				 //andWhere = " and ";
			 }
			 
			 
			 if(includeAdvancedParamsInSearch){
				    
				     PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.id in  ";
				     
					META_TAG_ATTRIBUTES_SUBQUERY += "   (select distinct(product_id) from inventory.meta_tag_attributes attributes " +
							 " inner join inventory.meta_tag meta on attributes.meta_tag_id=meta.id " +
							 " where attributes.active=true " ;
					
					META_TAG_ATTRIBUTES_SUBQUERY += " and ( ";
					
					int numberOfAdditionalParams = 0;
					for(String attributeName : advancedSearchParams.keySet()) {
						numberOfAdditionalParams++;
						String value = (String) advancedSearchParams.get(attributeName);
						META_TAG_ATTRIBUTES_SUBQUERY += "( meta.name='"+attributeName+"' and attributes.value='"+value+"') ";
						if(numberOfAdditionalParams < advancedSearchParams.keySet().size()) META_TAG_ATTRIBUTES_SUBQUERY += " OR ";
					}
					META_TAG_ATTRIBUTES_SUBQUERY += " ) ) ";
					
					//META_TAG_ATTRIBUTES_SUBQUERY += " )  additional_attributes on prod.id= additional_attributes.product_id ";
					
					 PRODUCT_SEARCH_SELECT_QUERY += META_TAG_ATTRIBUTES_SUBQUERY ;
					 
				}
			 
			
			 
			 PRODUCT_SEARCH_SELECT_QUERY += " order by org.id asc, loc.id asc,prod.id asc,inv.expiry_date asc, inv.id asc,inv.batch_number asc ";
	
	
	
	
	return (SalesOrderProductSearchListMainViewDTO) jdbcTemplate.query(PRODUCT_SEARCH_SELECT_QUERY,salesOrderProdSearchListMainViewResultsetExtractor);
	
}

public SalesOrderProductSearchSummaryListViewDTO getProductSummaryForSalesOrder(SalesOrderInventoryProductSearchParams salesOrderInventoryProductSearchParams){
	
	UUID organizationId = null;
	UUID locationId = null;
	Integer marketerId = null;
	String marketerName=null;
	List<UUID> inventoryIds=null;
	String productName=null;
	Boolean activeFlag=null;
	Map<String,Object> advancedSearchParams = new HashMap<String,Object>();
	
	boolean includeOrgIdInSearch=false;
	boolean includeLocIdInSearch=false;
	boolean includeMarketerIdInSearch=false;
	boolean includeMarketerNameInSearch=false;
	boolean includeInventoryIdInSearch=false;
	boolean includeProductNameInSearch=false;
	boolean includeAdvancedParamsInSearch=false;
	boolean includeActiveFlagInSearch=false;
	
	try {
		organizationId = salesOrderInventoryProductSearchParams.getOrganizationId();
		if(organizationId != null) includeOrgIdInSearch=true;
	}catch(Exception e) {
		organizationId=null;
	}
	
	try {
		locationId = salesOrderInventoryProductSearchParams.getLocationId();
		if(locationId != null) includeLocIdInSearch=true;
	}catch(Exception e) {
		locationId=null;
	}
	
	try {
		marketerId = salesOrderInventoryProductSearchParams.getMarketerId();
		if(marketerId != null) includeMarketerIdInSearch=true;
	}catch(Exception e) {
		marketerId=null;
	}
	
	try {
		marketerName = salesOrderInventoryProductSearchParams.getMarketerName();
		if(marketerName != null && marketerName.trim().length() != 0) includeMarketerNameInSearch=true;
	}catch(Exception e) {
		marketerName=null;
	}
	
	try {
	    inventoryIds = salesOrderInventoryProductSearchParams.getInventoryIds();
		if(inventoryIds != null && !inventoryIds.isEmpty()) includeInventoryIdInSearch=true;
	}catch(Exception e) {
		inventoryIds=null;
	}
	
	
	try {
		productName = salesOrderInventoryProductSearchParams.getProductName();
		if(productName != null && productName.trim().length() != 0) includeProductNameInSearch=true;
	}catch(Exception e) {
		productName=null;
	}
	
	try {
		advancedSearchParams = salesOrderInventoryProductSearchParams.getAdvancedSearchParams();
		if(advancedSearchParams != null && !advancedSearchParams.isEmpty()) includeAdvancedParamsInSearch=true;
	}catch(Exception e) {
		advancedSearchParams=null;
	}
	
	try {
		activeFlag = salesOrderInventoryProductSearchParams.getActive();
		if(activeFlag != null) includeActiveFlagInSearch=true;
	}catch(Exception e) {
		activeFlag=null;
	}
	String META_TAG_ATTRIBUTES_SUBQUERY=" ";
	
	
		
	
	String PRODUCT_SEARCH_SELECT_QUERY= "select org.id as organizationId,org.name as organizationName,loc.id as locationId,loc.name as locationName,"
			+ " inv.id as inventoryId, inv.batch_number as batchNumber, inv.expiry_date as expiryDate, inv.unit_rate as unitPrice,  \r\n" + 
			"  inv.available_units as availableUnits,prod.id as productId,prod.name as productName,\r\n" + 
			" prod.description as productDescription \r\n" ;
			
			
	      

			PRODUCT_SEARCH_SELECT_QUERY+= " from inventory.inventory inv "
					+ "inner join project_asclepius.location loc on inv.location_id=loc.id\r\n" + 
					"   inner join project_asclepius.Organization org on inv.organization_id=org.id\r\n" + 
					"    inner join inventory.product prod  on inv.product_id=prod.id\r\n" + 
					"   inner join inventory.marketer mark on prod.marketer_id=mark.id \r\n" + 
					"   inner join inventory.meta_tag_attributes mtagAttr on prod.id = mtagAttr.product_id\r\n" + 
					" inner join inventory.meta_tag mTag on mtagAttr.meta_tag_id=mTag.id\r\n" ; 
			         
			

			PRODUCT_SEARCH_SELECT_QUERY += " where inv.available_Units > 0  and inv.expiry_date > current_date ";

	         
			String andWhere = " and ";
	         
	         if(includeActiveFlagInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.active  = " + activeFlag +" ";
				 
				 
			 }
	         
	         if(includeInventoryIdInSearch){
				 String inventoryIdInClause="( ";
				 int j = 1;
				 for(UUID invId : inventoryIds) {
		    			
					 inventoryIdInClause+="'"+invId+"' ";
		    			
	    			if(j<inventoryIds.size()) {
	    				inventoryIdInClause+=",";
	    				j++;
	    			}
	    			
		    			
		    			
		    	}
				 inventoryIdInClause+=")";
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" inv.id in " + inventoryIdInClause+" ";
				// andWhere = " and ";
				 
			 }
	         
			 if(includeProductNameInSearch){
				
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.name like '%" + productName+"%'";
				// andWhere = " and ";
				 
			 }
			 if(includeMarketerNameInSearch){
				
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.name like '%" + marketerName+"%'";
				 //andWhere = " and ";
			 }
			 if(includeMarketerIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.id =" + marketerId+" ";
				// andWhere = " and ";
				 
			 }
			 if(includeLocIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" loc.id ='" + locationId+"'";
				 //andWhere = " and ";
			 }
			 if(includeOrgIdInSearch){
				 
				 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" org.id ='" + organizationId+"'";
				 //andWhere = " and ";
			 }
			 
			 
			 if(includeAdvancedParamsInSearch){
				    
				     PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.id in  ";
				     
					META_TAG_ATTRIBUTES_SUBQUERY += "   (select distinct(product_id) from inventory.meta_tag_attributes attributes " +
							 " inner join inventory.meta_tag meta on attributes.meta_tag_id=meta.id " +
							 " where attributes.active=true " ;
					
					META_TAG_ATTRIBUTES_SUBQUERY += " and ( ";
					
					int numberOfAdditionalParams = 0;
					for(String attributeName : advancedSearchParams.keySet()) {
						numberOfAdditionalParams++;
						String value = (String) advancedSearchParams.get(attributeName);
						META_TAG_ATTRIBUTES_SUBQUERY += "( meta.name='"+attributeName+"' and attributes.value='"+value+"') ";
						if(numberOfAdditionalParams < advancedSearchParams.keySet().size()) META_TAG_ATTRIBUTES_SUBQUERY += " OR ";
					}
					META_TAG_ATTRIBUTES_SUBQUERY += " ) ) ";
					
					//META_TAG_ATTRIBUTES_SUBQUERY += " )  additional_attributes on prod.id= additional_attributes.product_id ";
					
					 PRODUCT_SEARCH_SELECT_QUERY += META_TAG_ATTRIBUTES_SUBQUERY ;
					 
				}
			 
			
			 
			 PRODUCT_SEARCH_SELECT_QUERY += " order by org.id asc, loc.id asc,prod.id asc,inv.expiry_date asc, inv.id asc,inv.batch_number asc ";
	
	
	
	
	return (SalesOrderProductSearchSummaryListViewDTO) jdbcTemplate.query(PRODUCT_SEARCH_SELECT_QUERY,salesOrderProdSearchSummaryViewResultsetExtractor);
	
}

public SalesOrderSummaryListViewDTO getSalesOrderSummaryListByOrganization(UUID organizationId)  {
	
	String SALES_ORDER_SUMMARY_LIST_QUERY= "select org.id as organizationId,org.name as organizationName,loc.id as locationId,loc.name as locationName, " + 
			"	so.id as salesOrderId, so.order_date as salesOrderDate, so.order_number as salesOrderNumber, so.net_order_value as salesOrderValue " +
			"  from inventory.order so " +
			"   inner join project_asclepius.location loc on so.location_id=loc.id" +
			"  inner join project_asclepius.organization org on so.organization_id=org.id " +
			"     where org.id ='" + organizationId+"' and so.order_type='Sales'" +
			"   order by salesOrderNumber desc ";
	
	
	
	
	return (SalesOrderSummaryListViewDTO) jdbcTemplate.query(SALES_ORDER_SUMMARY_LIST_QUERY,salesOrderSummaryListViewResultextractor);
}


}