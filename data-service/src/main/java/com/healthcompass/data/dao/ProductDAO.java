package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.MetaTagAttributesRepository;
import com.healthcompass.data.repository.MetaTagRepository;
import com.healthcompass.data.repository.ProductRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentHistoryViewResultsetExtractor;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationViewResultSetExtrator;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewResultsetExtractor;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;



@Component
public class ProductDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private MetaTagRepository metaTagRepo;
	
	@Autowired
	private MetaTagAttributesRepository metaTagAttribRepo;
	
	
	@Autowired
	ProductListMainViewResultsetExtractor productListMainViewResultsetExtractor;
	
	@Autowired 
	ProductListMainViewToEntityAssembler productListMainViewToEntityAssembler;
	
	@Autowired
	ProductSummaryOrganizationViewResultSetExtrator productSummaryOrganizationViewResultSetExtrator;
	
	 @Transactional(propagation = Propagation.SUPPORTS) 
	public List<Product> findAllProductsByIds(List<Integer> productIdList){
		 return (List<Product>) productRepo.findAllById(productIdList);
		
	}
	
	
	
	

	
	public ProductListMainViewDTO findAllProducts(ProductSearchParams productSearchParams){
		
		UUID organizationId = null;
		Integer marketerId = null;
		String marketerName=null;
		List<Integer> prodIds=null;
		String productName=null;
		Boolean activeFlag=null;
		Map<String,Object> advancedSearchParams = new HashMap<String,Object>();
		
		boolean includeOrgIdInSearch=false;
		boolean includeMarketerIdInSearch=false;
		boolean includeMarketerNameInSearch=false;
		boolean includeProductIdInSearch=false;
		boolean includeProductNameInSearch=false;
		boolean includeAdvancedParamsInSearch=false;
		boolean includeActiveFlagInSearch=false;
		
		try {
			organizationId = productSearchParams.getOrganizationId();
			if(organizationId != null) includeOrgIdInSearch=true;
		}catch(Exception e) {
			organizationId=null;
		}
		
		try {
			marketerId = productSearchParams.getMarketerId();
			if(marketerId != null) includeMarketerIdInSearch=true;
		}catch(Exception e) {
			marketerId=null;
		}
		
		try {
			marketerName = productSearchParams.getMarketerName();
			if(marketerName != null && marketerName.trim().length() != 0) includeMarketerNameInSearch=true;
		}catch(Exception e) {
			marketerName=null;
		}
		
		try {
		    prodIds = productSearchParams.getProductIds();
			if(prodIds != null && !prodIds.isEmpty()) includeProductIdInSearch=true;
		}catch(Exception e) {
			prodIds=null;
		}
		
		
		try {
			productName = productSearchParams.getProductName();
			if(productName != null && productName.trim().length() != 0) includeProductNameInSearch=true;
		}catch(Exception e) {
			productName=null;
		}
		
		try {
			advancedSearchParams = productSearchParams.getAdvancedSearchParams();
			if(advancedSearchParams != null && !advancedSearchParams.isEmpty()) includeAdvancedParamsInSearch=true;
		}catch(Exception e) {
			advancedSearchParams=null;
		}
		
		try {
			activeFlag = productSearchParams.getActive();
			if(activeFlag != null) {
				
				includeActiveFlagInSearch=true;
			}else {
				activeFlag=true;
				includeActiveFlagInSearch=true;
			}
		}catch(Exception e) {
			activeFlag=null;
		}
		String META_TAG_ATTRIBUTES_SUBQUERY=" ";
		
		
			
		
		String PRODUCT_SEARCH_SELECT_QUERY= "select org.id as organizationId,org.name as organizationName,mark.id as marketerId,mark.name as marketerName,prod.id as productId,prod.name as productName,\r\n" + 
				" prod.description as productDescription, prod.molecule_id as moleculeId,\r\n" + 
				" prod.hsn_code_id as hsnCode,prod.units as productUnits,prod.pack as productPack,\r\n" + 
				" prod.schedule_drug_code as scheduleDrugCode,prod.gst_rate as prodGstRate,prod.sales_tax_rate as\r\n" + 
				" prodSalesTaxRate,prod.central_tax_rate as prodCentralTaxRate, prod.vat_rate as prodVatRate,\r\n" + 
				" prod.reorder_level as prodReOrderLevel, prod.lead_time as prodLeadTime, prod.active as productActive, \r\n" + 
				" mtag.id as metaTagId,mtag.name as metaTagName,mtagAttr.value as metaTagAttributeValue,mTagAttr.id as metaTagAttributeId,mTagAttr.active as metaTagAttributeActive from \r\n"+
				" inventory.product prod  \r\n ";
		
				
				PRODUCT_SEARCH_SELECT_QUERY+= " inner join inventory.marketer mark on prod.marketer_id=mark.id \r\n" + 
						"  inner join project_asclepius.Organization org on mark.organization_id=org.id \r\n" +
						"  inner join inventory.meta_tag_attributes mtagAttr on prod.id = mtagAttr.product_id\r\n" + 
				        " inner join inventory.meta_tag mTag on mtagAttr.meta_tag_id=mTag.id\r\n" ; 
				         
		      
				
		         String andWhere = " ";
		         
		         if(includeActiveFlagInSearch){
					 andWhere = " where ";
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.active  = " + activeFlag +" ";
					 andWhere = " and ";
					 
				 }else {
					 andWhere = " where ";
				 }
		         
		         if(includeProductIdInSearch){
					 String prodIdInClause="( ";
					 int j = 1;
					 for(Integer prodId : prodIds) {
			    			
						 prodIdInClause+=prodId+" ";
			    			
		    			if(j<prodIds.size()) {
		    				prodIdInClause+=",";
		    				j++;
		    			}
		    			
			    			
			    			
			    	}
					 prodIdInClause+=")";
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.id in " + prodIdInClause+" ";
					 andWhere = " and ";
					 
				 }
		         
				 if(includeProductNameInSearch){
					
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.name like '%" + productName+"%'";
					 andWhere = " and ";
					 
				 }
				 if(includeMarketerNameInSearch){
					
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.name like '%" + marketerName+"%'";
					 andWhere = " and ";
				 }
				 if(includeMarketerIdInSearch){
					 
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" mark.id =" + marketerId+" ";
					 andWhere = " and ";
					 
				 }
				 if(includeOrgIdInSearch){
					 
					 PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" org.id ='" + organizationId+"'";
					 andWhere = " and ";
				 }
				 
				 
				 if(includeAdvancedParamsInSearch){
					    
					     PRODUCT_SEARCH_SELECT_QUERY += " " + andWhere +" prod.id in  ";
					     
						META_TAG_ATTRIBUTES_SUBQUERY += "   (select distinct(product_id) from inventory.meta_tag_attributes attributes " +
								 " inner join inventory.meta_tag meta on attributes.meta_tag_id=meta.id " +
								 " where attributes.active=true " ;
						
						META_TAG_ATTRIBUTES_SUBQUERY += " and ( ";
						
						int numberOfAdditionalParams = 0;
						for(String attributeName : advancedSearchParams.keySet()) {
							numberOfAdditionalParams++;
							String value = (String) advancedSearchParams.get(attributeName);
							META_TAG_ATTRIBUTES_SUBQUERY += "( meta.name='"+attributeName+"' and attributes.value='"+value+"') ";
							if(numberOfAdditionalParams < advancedSearchParams.keySet().size()) META_TAG_ATTRIBUTES_SUBQUERY += " OR ";
						}
						META_TAG_ATTRIBUTES_SUBQUERY += " ) ) ";
						
						//META_TAG_ATTRIBUTES_SUBQUERY += " )  additional_attributes on prod.id= additional_attributes.product_id ";
						
						 PRODUCT_SEARCH_SELECT_QUERY += META_TAG_ATTRIBUTES_SUBQUERY ;
						 
					}
				 
				
				 
				 //PRODUCT_SEARCH_SELECT_QUERY += " order by organizationId asc,marketerId asc,productId asc ";
				 PRODUCT_SEARCH_SELECT_QUERY += " order by organizationId asc,productName asc "; // This sorting will not work if we use meta tag attributes in search, use the above sorting
		
				 
		
		
		return (ProductListMainViewDTO) jdbcTemplate.query(PRODUCT_SEARCH_SELECT_QUERY,productListMainViewResultsetExtractor);
		
	}
	

	
	
	//@Transactional(propagation = Propagation.REQUIRES_NEW)
	
	/*public List<Product> createProducts(List<Product> products) {
	         return (List<Product> ) productRepo.saveAll(products);
	}*/

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Product> createOrUpdateProducts(ProductListMainViewDTO productsView) {
		
				// get  the product ids to be updated
				List<Integer> productIdList = new ArrayList<Integer>();
				for (ProductViewDTO product : productsView.getProducts()) {
					if(product.getProductId() != null ) productIdList.add(product.getProductId());
				}
				List<Product> existingProducts = new ArrayList<Product>();
				if(!productIdList.isEmpty()) existingProducts = (List<Product>) productRepo.findAllById(productIdList);
				List<Product> existingProductsUpdatedWithChanges = productListMainViewToEntityAssembler.toEntityList(productsView,existingProducts);
	         return (List<Product> ) productRepo.saveAll(existingProductsUpdatedWithChanges);
	}
	
	public ProductSummaryOrganizationDTO findAllProductsByOrganization(UUID organizationId, boolean isInludeOnlyActive){
		
		
		
		
		
		String PRODUCT_SEARCH_SELECT_QUERY= "select org.id as organizationId,org.name as organizationName,prod.id as productId,prod.name as productName, \r\n" + 
				"	  prod.description as productDescription, mark.id as marketerId, mark.name as marketerName from inventory.product prod \r\n" + 
				"	  inner join inventory.marketer mark on prod.marketer_id=mark.id \r\n" + 
				"	  inner join project_asclepius.organization org on mark.organization_id=org.Id where org.id ='" + organizationId+"'";
				if(isInludeOnlyActive) {
					PRODUCT_SEARCH_SELECT_QUERY += " and prod.active=true ";
				}
					PRODUCT_SEARCH_SELECT_QUERY += " order by productName asc ";
		
		
		
		
		return (ProductSummaryOrganizationDTO) jdbcTemplate.query(PRODUCT_SEARCH_SELECT_QUERY,productSummaryOrganizationViewResultSetExtrator);
		
	}

	public ProductSummaryOrganizationDTO findAllProductsByOrganization(UUID organizationId){
		
		return findAllProductsByOrganization(organizationId,true); //for a general product summaary search we icnclude only active
		//for purchase order creation we icnlude all products
		
			
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteProduct(Integer id) {
		
		Integer iventoryCount = productRepo.checkIfProductExistsInInventory(id);
		if(iventoryCount > 0) {
						
			throw new HealthCompassCustomValidationException("Product cannot be deleted as Inventory Items are associated with this Product:"+id,null);
		}
		
		Integer posoCount = productRepo.checkIfProductExistsInPOSOOrders(id);
		
		if(posoCount > 0) {
			
			throw new HealthCompassCustomValidationException("Product cannot be deleted as there are Purchase/Sales order associated with this Product:"+id,null);
		}
		
		Optional<Product> optProduct = productRepo.findById(id);
		Product existingProduct = optProduct.get();
		if(existingProduct != null) {
			productRepo.deleteProduct(id);
			metaTagAttribRepo.deleteMetaTagAttributes(id);
			
		}
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<MetaTagViewDTO> getMetaTags() {
		List<MetaTag> metaTags =  
				(ArrayList<MetaTag>) metaTagRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
		List<MetaTagViewDTO> metaTagViews = new ArrayList<MetaTagViewDTO>();
		for (MetaTag metaTag : metaTags) {
			MetaTagViewDTO metaTagView = new MetaTagViewDTO();
			metaTagView.setMetaTagId(metaTag.getId());
			metaTagView.setMetaTagName(metaTag.getName());
			metaTagViews.add(metaTagView);
		}
		return metaTagViews;
	}
	
	


	
}