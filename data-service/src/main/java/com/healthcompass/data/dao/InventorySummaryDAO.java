package com.healthcompass.data.dao;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.InventoryRepository;
import com.healthcompass.data.repository.InventorySummaryRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.InventorySummarySearchPredicates;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchProductParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.ProductDropDownListViewDTO;
import com.healthcompass.data.view.dto.ProductDropDownListViewResultSetExtrator;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.SalesOrderSummaryListViewDTO;
import com.querydsl.core.types.Predicate;



@Component

public class InventorySummaryDAO {
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired 
	InventorySummarySearchPredicates preds;
	
	@Autowired
	private InventorySummaryRepository inventorySummaryRepo;
	
	@Autowired
	private ProductDropDownListViewResultSetExtrator productDropDownListViewResultSetExtrator;
	
		
	private Predicate toPredicate(InventorySummarySearchParams params) {

		// @formatter:off
		return preds.isActive(params.getActive())
				.and(preds.isOrganizationIdEq(params.getOrganizationId()))
				.and(preds.isLocationIdEq(params.getLocationId()))
				.and(preds.isProductIdEq(params.getProductId()))
				.and(preds.isProductNameLike(params.getProductName()))
				.and(preds.isAvailableUnitsOperator(params.getAvailableUnits(),params.getOperatorForAvbUnits()));
				
		// @formatter:on
	}
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the session for fetching lazy loading
	public List<InventorySummary> findAllInventorySummary(InventorySummarySearchParams inventorySummarySearchParams){
		
		return (List<InventorySummary>) inventorySummaryRepo.findAll(toPredicate(inventorySummarySearchParams));
		
			
	}
	
	public ProductDropDownListViewDTO getDistinctProductsForInventorySummarySearch(InventorySummarySearchProductParams inventorySummarySearchProductParams)  {
		
		UUID organizationId = null;
		UUID locationId = null;
		
		
		boolean includeOrgIdInSearch=false;
		boolean includeLocIdInSearch=false;
		
		
		try {
			organizationId = inventorySummarySearchProductParams.getOrganizationId();
			if(organizationId != null) includeOrgIdInSearch=true;
		}catch(Exception e) {
			organizationId=null;
		}
		
		try {
			locationId = inventorySummarySearchProductParams.getLocationId();
			if(locationId != null) includeLocIdInSearch=true;
		}catch(Exception e) {
			locationId=null;
		}
		
		
		
		
			
		
		String DISTINCT_PRODUCT_QUERY= "select distinct sum.product_id as id,prod.name as name, mark.name as marketerName from inventory.inventory_summary sum\r\n" + 
				"inner join inventory.product prod on sum.product_id=prod.id "
				+ " inner join inventory.marketer mark on prod.marketer_id=mark.id where prod.active=true ";
		
			
				String andWhere = " and ";
		         
				if(includeOrgIdInSearch){
					 
					DISTINCT_PRODUCT_QUERY += " " + andWhere +" sum.organization_id ='" + organizationId+"'";
					 //andWhere = " and ";
				 }
				 if(includeLocIdInSearch){
					 
					 DISTINCT_PRODUCT_QUERY += " " + andWhere +" sum.location_id ='" + locationId+"'";
					 //andWhere = " and ";
				 }
				
				 
				 DISTINCT_PRODUCT_QUERY += " order by name asc ";
		

		
		
		
		
		return (ProductDropDownListViewDTO) jdbcTemplate.query(DISTINCT_PRODUCT_QUERY,productDropDownListViewResultSetExtrator);
	}
	
}