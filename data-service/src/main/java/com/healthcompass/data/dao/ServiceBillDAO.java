package com.healthcompass.data.dao;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.LocationSequenceNumbers;
import com.healthcompass.data.model.LocationSequenceNumbersPK;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.BillPaymentRepository;
import com.healthcompass.data.repository.LocationSequenceNumbersRepository;
import com.healthcompass.data.repository.PractitionerRoleRepository;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillHistorySearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillHistorySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.PatientOutStandingBillsAmountRestltSetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;

import org.springframework.transaction.annotation.Propagation;

@Component

public class ServiceBillDAO {
	
	
	@Autowired 
	ServiceBillSearchPredicates preds;
	
	@Autowired 
	ServiceBillHistorySearchPredicates historyPreds;
	
	
	@Autowired
	private ServiceBillRepository serviceBillRepository;
	
	@Autowired
	private LocationSequenceNumbersRepository locationSequenceNumbersRepository;
	
	@Autowired
	private PractitionerRoleRepository practitionerRoleRepository;
	
	
	
	@Autowired
	private ValueSetDAO valueSetDAO;
	
	@Autowired
	ServiceBillMainViewModelAsembler serviceBillMainViewModelAsembler;
	@Autowired
	private ServiceBillModelAssembler serviceBillModelAssembler;
	
	@Autowired
	ServiceBillMainViewToEntityAssembler serviceBillMainViewToEntityAssembler;
	
	@Autowired
	private ServiceBillEntityAssembler serviceBillEntityAssembler;
	
	@Autowired
	private ServiceBillItemRepository serviceBillItemRepository;
		
	@Autowired
	private BillPaymentRepository billPaymentRepository;
		
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	PatientOutStandingBillsAmountRestltSetExtractor patientOutStandingBillsAmountRestltSetExtractor;
	
	private Predicate toPredicate(ServiceBillSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isActive(true))
				.and(preds.isIdsInList(params.getIdList())
				.and(preds.isEncounterIdEq(params.getEncounterId()))
				.and(preds.isPatientIdEq(params.getPatientId()))
				.and(preds.isPractitionerRoleIdEq(params.getPractitionarRoleId())));
						
				
		// @formatter:on
	}
	
	private Predicate toBillingHistoryPredicate(ServiceBillHistorySearchParams params) {

		// @formatter:off
		return historyPreds.isBillGenerated(true)
				.and(historyPreds.isActive(true))
				.and(historyPreds.isPatientIdEq(params.getPatientId()))
				.and(historyPreds.isPractitionerRoleIdsInList(params.getPractitionarRoleIds()));
						
				
		// @formatter:on
	}
	
	public List<ServiceBill> findAll(){
		//return serviceRepository.findAll();
		return null;
	}
	
	// @Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
	public Optional<ServiceBill> findOne(UUID id) {
		
		
		return serviceBillRepository.findById(id);
				
		
		
	}
	
 public Optional<ServiceBill> getById(UUID id) {
		
		
		return serviceBillRepository.findById(id);
				
		
		
		
	}

    //@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
	public List<ServiceBill>  findAll(ServiceBillSearchParams serviceBillSearchParams){
		return (List<ServiceBill>) serviceBillRepository.findAll(toPredicate(serviceBillSearchParams));
		
		
	}
    
    public Double getOutStandingPendingBillsAmount(UUID patientId) {
		
    	
		String SQL_OUTSTANDING_BILLS_AMOUNT_SELECT = "SELECT sum(bill.amount_due)  as outStandingPendingBillsAmount from billing.service_bill bill where bill.amount_due != 0 and bill.active=true and bill.bill_number is not null and bill.patient_id='"+patientId+"'";
		return (Double) jdbcTemplate.query(SQL_OUTSTANDING_BILLS_AMOUNT_SELECT,patientOutStandingBillsAmountRestltSetExtractor);
	
	}
	
	/*public ServiceBill save(ServiceBillModel serviceBillModel) {
		
		ServiceBill serviceBill = null;
		if(serviceBillModel.getId() == null) {
			// create new Service Bill
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

			//BeanUtils.copyProperties(serviceBillModel, );//copy properties from dto
			serviceBill = mapper.convertValue(serviceBillModel, ServiceBill.class);
			//serviceBill.setId(UUID.randomUUID());
			for(ServiceBillItem serviceBillItem : serviceBill.getServiceBillItems()) {
				//serviceBillItem.setId(UUID.randomUUID());
				serviceBillItem.setServiceBill(serviceBill);
			}
			serviceBill.setServiceBillItems(serviceBill.getServiceBillItems());
			serviceBill = serviceBillRepository.save(serviceBill);
			
			
		}else {
			// Update existing Service Bill
			Optional<ServiceBill> serviceBillDBOBJ = serviceBillRepository.findById(serviceBillModel.getId());
			ServiceBill serviceBillFromDB = serviceBillDBOBJ.get();
			serviceBillFromDB.getServiceBillItems().clear();
			serviceBillEntityAssembler.toEntity(serviceBillModel,serviceBillFromDB);
			for(ServiceBillItem serviceBillItem : serviceBillFromDB.getServiceBillItems()) {
				serviceBillItem.setServiceBill(serviceBillFromDB);
			}
			serviceBillFromDB.setServiceBillItems(serviceBillFromDB.getServiceBillItems());
			
			
			
			
			serviceBill = serviceBillRepository.save(serviceBillFromDB);
			
			
		}
		return serviceBill;
		
	}*/
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	//public ServiceBillViewMainDTO save(ServiceBillViewMainDTO serviceBillMainViewDTO) {
	public ServiceBill save(ServiceBillViewMainDTO serviceBillMainViewDTO, boolean isGenerateBill) {
		
		if(serviceBillMainViewDTO == null) return null;
    	boolean isNewServiceBillIntance = false;
    	ServiceBill serviceBill = null;
    	
    	if(serviceBillMainViewDTO.getServiceBill().getServiceBillId() == null) {
    		isNewServiceBillIntance=true;
    		 
    	} 
    	if(isNewServiceBillIntance) {
    		 serviceBill = new ServiceBill();
    		 Encounter enc = new Encounter();
    		 
		     Patient pat = new Patient();
		   
		     PractitionerRole prac = new PractitionerRole();
		    
		     //enc.setPatient(pat);
		     serviceBill.setEncounter(enc);
		     
		     serviceBill.setPatient(pat);
		    serviceBill.setPractitionerRole(prac);
		    
		   /* ValueSet paymentMode = new ValueSet();
	    	serviceBill.setPaymentMode(paymentMode);*/
	    	
	    	ValueSet draftBillStatus = new ValueSet();
	    	
	    	draftBillStatus.setId(HealthCompassConstants.BILL_STATUS_PENDING_ID); // later change this to get them from db, uncomment below line when there is unique code in db
	    	//draftBillStatus = valueSetDAO.getDraftBillStatusCode();
    		serviceBill.setStatus(draftBillStatus);
    		
    		/*ValueSet billPaymentStatus = new ValueSet();
	    	serviceBill.setPaymentStatus(draftBillStatus);*/
	    	
	    	serviceBill.setActive(true);
	    	serviceBill.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	serviceBill.setCreatedBy(serviceBillMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : serviceBillMainViewDTO.getLoggedInUser());
	    	serviceBill.setBillDate(new java.sql.Timestamp(System.currentTimeMillis()));
	    	List<ServiceBillItem> serviceBillItems = new ArrayList<ServiceBillItem>(); 
	    	serviceBill.setServiceBillItems(serviceBillItems);
	    	
    	}else{
    		Optional<ServiceBill> serviceBillDBOBJ = serviceBillRepository.findById(serviceBillMainViewDTO.getServiceBill().getServiceBillId());
    		serviceBill = serviceBillDBOBJ.get();
    		
    		
    	}
    	serviceBill = serviceBillMainViewToEntityAssembler.toEntity(serviceBillMainViewDTO,serviceBill) ;
    	if(isGenerateBill) {
    		// set Bill Status
    		ValueSet generateBillStatus = new ValueSet();
    		generateBillStatus.setId(HealthCompassConstants.BILL_STATUS_GENERATED_ID); // later change this to get them from db, uncomment below line when there is unique code in db
    		//generateBillStatus = valueSetDAO.getGenerateBillStatusCode();
    		serviceBill.setStatus(generateBillStatus);
    		
    		// set Bill Payment Status
    		ValueSet billPaymentStatus = new ValueSet();
    		int billPaymentType = serviceBill.getAmountBilled().compareTo(serviceBill.getAmountPaid());
    		if(billPaymentType == 1) {
    			if(serviceBill.getAmountBilled().compareTo(serviceBill.getAmountDue()) == 0) { // complete bill amount is due
    				billPaymentStatus = valueSetDAO.getBillPaymentNotPaidStatusCode();
    			}else { // partially paid
    				billPaymentStatus = valueSetDAO.getBillPaymentPartiallyPaidStatusCode();
    			}
    			
    		}else { // completely paid
    			billPaymentStatus = valueSetDAO.getBillPaymentPaidStatusCode();
    		}
    		
    		serviceBill.setPaymentStatus(billPaymentStatus);
    		
    		
    		// generate new bill number
    		Optional<PractitionerRole> optPractitionerRole = practitionerRoleRepository.findById(serviceBill.getPractitionerRole().getId());
    		PractitionerRole practitionerRole = optPractitionerRole.get();
    		String locationCode = practitionerRole.getLocation().getLocationCode();
    		
    		Integer billNumber = null;
    		
    		LocationSequenceNumbers locationSequenceNumbers=null;
    		Optional<LocationSequenceNumbers> optLocationLocationSequences = locationSequenceNumbersRepository.findById(new LocationSequenceNumbersPK(locationCode, "service_bill_number"));
    		if(!optLocationLocationSequences.isPresent()) {
    			billNumber=1;
    			
    			locationSequenceNumbers= new LocationSequenceNumbers(new LocationSequenceNumbersPK(locationCode, "service_bill_number"),billNumber,String.format("%05d" ,billNumber));
    			
    		}else {
    			locationSequenceNumbers=(LocationSequenceNumbers) optLocationLocationSequences.get();
    			billNumber=locationSequenceNumbers.getSequenceNumber()+1;
    			locationSequenceNumbers.setSequenceNumber(billNumber);
    			locationSequenceNumbers.setSequenceNumberPadded(String.format("%05d" ,billNumber));
    		}
    		
    		 
    		
    		    		
    		
    		
    		serviceBill.setBillDate(new java.sql.Timestamp(System.currentTimeMillis()));
    		
    		locationSequenceNumbersRepository.save(locationSequenceNumbers);
    		serviceBill.setBillNumber(locationCode+""+String.format("%05d" ,billNumber));
    	}
    	
    	serviceBill = serviceBillRepository.save(serviceBill);
    	Optional<ServiceBill> persistedServiceBill = serviceBillRepository.findById(serviceBill.getId());
    	
    	
    	ServiceBill newOrUpdatedServiceBill = persistedServiceBill.get();
    	
    	try {   // we will insert BillPayment after updating the bill with bill number. If we n=mix both update and insert in same save block, bill payments insert is done first befoire updating the bill with bill number adn causing issue
    		    // sort of hack for hibernate ordering insert/updates in batch
    		
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return newOrUpdatedServiceBill;
    	//return serviceBillMainViewModelAsembler.toModel(newOrUpdatedServiceBill);	
			
	
		
	}
	

@Transactional(propagation = Propagation.REQUIRES_NEW)
public void deleteServiceBill(UUID id) {
	
		
		Optional<ServiceBill> optServiceBill = serviceBillRepository.findById(id);
		ServiceBill existingServiceBill = optServiceBill.get();
		if(existingServiceBill != null) {
			serviceBillRepository.deleteBill(id);
			serviceBillItemRepository.deleteBillItem(id);
			if(existingServiceBill.getBillNumber() != null) {
					billPaymentRepository.deleteBillPayments(existingServiceBill.getBillNumber());
			}
		}
		
	
}
	
//@Transactional(propagation = Propagation.SUPPORTS)
public boolean checkIfBillExistsForPatientEncounterAndPractitioner(UUID patientId,UUID encounterId,UUID practitionerId) {
	boolean billExists = false;
	Integer billCount = serviceBillRepository.checkIfBillExists(patientId, encounterId, practitionerId);
	if(billCount.compareTo(0) != 0) {
		billExists = true;
		
	}
	return billExists;
}

//@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
	public List<ServiceBill>  findBillingHistory(ServiceBillHistorySearchParams serviceBillHistorySearchParams){
	
		return (List<ServiceBill>) serviceBillRepository.findAll(toBillingHistoryPredicate(serviceBillHistorySearchParams));
		
		
	}
	
	private  Date getCurrentUtcTime() throws ParseException {
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
	    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	    SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
	    return localDateFormat.parse( simpleDateFormat.format(new Date()) );
	}
	
	
	
}
