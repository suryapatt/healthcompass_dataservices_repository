package com.healthcompass.data.dao;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.binder.BinderException;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.MarketerRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.MarketerSearchPredicates;
import com.healthcompass.data.search.predicates.SupplierSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.querydsl.core.types.Predicate;


@Component
public class MarketerDAO {
	
	
	@Autowired
	private MarketerRepository marketerRepository;
	
	
	
	@Autowired
	private MarketerSearchPredicates preds;
	
	private Predicate toPredicate(MarketerSummaryViewSearchParams params) {

		// @formatter:off
		return preds.isOrganizationIdEq(params.getOrganizationId())
				.and(preds.isMarketerNameLike(params.getMarketerName())
				.and(preds.isIdsInList(params.getIdList())
				.and(preds.isActive(true))));
				
				
						
				
		// @formatter:on
	}
	
	public Optional<Marketer> findOne(Integer id) {
		Optional<Marketer> marketerDBObj = marketerRepository.findById(id);
		
		return marketerDBObj;
		
		
		
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Marketer save(Marketer marketer)
	{
		return marketerRepository.save(marketer);
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Marketer> saveAll(List<Marketer> marketers)
	{
		return (List<Marketer>) marketerRepository.saveAll(marketers);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Supplier deleteById(Integer id) {
		
		Optional<Marketer> marketerDBObj = marketerRepository.findById(id);
		Marketer marketer = marketerDBObj.isPresent() ?  marketerDBObj.get() : null;
		if(marketer == null ) {
			
			throw new RecordNotFoundException("Marketer Not found with Id: "+id);
		}
		Integer count = marketerRepository.checkIfProductExists(id);
		if(count > 0) {
						
			throw new HealthCompassCustomValidationException("Marketer cannot be deleted as Products are associated wiht this Marketer:"+id,null);
		}
		marketer.setActive(false);
		marketerRepository.save(marketer);
		return null;
		
		
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Marketer update(Marketer marketer)
	{
		
	
		return marketerRepository.save(marketer);
		
			
	}
	
		@Transactional(propagation = Propagation.SUPPORTS) //give chance to callers to maintain the seeion for fetching lazy loading	 
		public List<Marketer>  findAll(MarketerSummaryViewSearchParams marketerSummaryViewSearchParams){
			return (List<Marketer>) marketerRepository.findAll(toPredicate(marketerSummaryViewSearchParams),Sort.by(Sort.Direction.ASC, "name"));
			
			
		}
		
		public boolean checkIfMarketerExistsWithSameName(String name) {
			boolean marketerExists = false;
			Integer marketerCount = marketerRepository.checkIfMarketerExists(name);
			if(marketerCount.compareTo(0) != 0) {
				marketerExists = true;
				
			}
			return marketerExists;
        }
}
