package com.healthcompass.data.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

public class CSVHelper {

	 public static String TYPE = "text/csv";
	  static String[] HEADERs = { "OrganizationId", "Name", "Description"};

	  public static boolean hasCSVFormat(MultipartFile file) {

	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }

	    return true;
}
	  public static MarketerUploadMainViewDTO csvToMarketers(InputStream is) {
		    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		        CSVParser csvParser = new CSVParser(fileReader,
		            CSVFormat.DEFAULT.withEscape(Character.valueOf('\\')).withFirstRecordAsHeader()
		            .withIgnoreHeaderCase().withTrim().withNullString("\"\""));) {

		      List<Marketer> marketers = new ArrayList<Marketer>();

		      Iterable<CSVRecord> csvRecords = csvParser.getRecords();
		      Stream<CSVRecord> csvRecordStream = StreamSupport.stream(csvRecords.spliterator(),false);
		      
		      MarketerUploadMainViewDTO marketerUploadMainViewDTO = new MarketerUploadMainViewDTO();
		  	  boolean isAggregateInfoSet=false;
		  	  List<MarketerViewDTO> marketerViewDTO = new ArrayList<MarketerViewDTO>();		      
		      for (CSVRecord csvRecord : csvRecords) {
		    	 
		    		  MarketerViewDTO marketer = new MarketerViewDTO();
				        if(!isAggregateInfoSet) {
				        	
				        	if(csvRecord.get("OrganizationId") != null && !csvRecord.get("OrganizationId").trim().isBlank()) {
				        	 marketerUploadMainViewDTO.setOrganizationId(UUID.fromString(csvRecord.get("OrganizationId")));
				        	}
				        	isAggregateInfoSet=true; 
				        }
				        marketer.setName(csvRecord.get("Name"));
					    marketer.setDescription(csvRecord.get("Description"));
					    marketer.setActive(true);
					    marketerViewDTO.add(marketer);
				       
				     
		    	 
		      }
		       
		      marketerUploadMainViewDTO.setMarketers(marketerViewDTO);
		      return marketerUploadMainViewDTO;
		    } catch (IOException e) {
		    	e.printStackTrace();
		      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		    }
		    
		    
		    
		  }
	  
	  public static SupplierUploadMainViewDTO csvToSuppliers(InputStream is) {
		    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		        CSVParser csvParser = new CSVParser(fileReader,
		            CSVFormat.DEFAULT.withEscape(Character.valueOf('\\')).withFirstRecordAsHeader()
		            .withIgnoreHeaderCase().withTrim().withNullString("\"\""));) {

		      List<Marketer> marketers = new ArrayList<Marketer>();

		      Iterable<CSVRecord> csvRecords = csvParser.getRecords();
		      Stream<CSVRecord> csvRecordStream = StreamSupport.stream(csvRecords.spliterator(),false);
		      
		      SupplierUploadMainViewDTO supplierUploadMainViewDTO = new SupplierUploadMainViewDTO();
		  	  boolean isAggregateInfoSet=false;
		  	  List<SupplierViewDTO> supplierViewDTO = new ArrayList<SupplierViewDTO>();		      
		      for (CSVRecord csvRecord : csvRecords) {
		    	  SupplierViewDTO supplier = new SupplierViewDTO();
		        if(!isAggregateInfoSet) {
		        	if(csvRecord.get("OrganizationId") != null && !csvRecord.get("OrganizationId").trim().isBlank()) {
		        		supplierUploadMainViewDTO.setOrganizationId(UUID.fromString(csvRecord.get("OrganizationId")));
			        }
			        	isAggregateInfoSet=true; 
		        	
		        	 
		        }
		        supplier.setName(csvRecord.get("Name"));
		        supplier.setDescription(csvRecord.get("Description"));
		        supplier.setActive(true);
			    supplierViewDTO.add(supplier);
		       
		      }
		      supplierUploadMainViewDTO.setSuppliers(supplierViewDTO);
		      return supplierUploadMainViewDTO;
		    } catch (IOException e) {
		    	e.printStackTrace();
		      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		    }
		    
		    
		    
		  }

}
