package com.healthcompass.data.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.healthcompass.data.CustomFilter;
import com.healthcompass.data.model.TimeZoneInformation;
import com.healthcompass.data.service.TimeZoneService;
import com.macasaet.fernet.Key;
import com.macasaet.fernet.StringValidator;
import com.macasaet.fernet.Token;
import com.macasaet.fernet.Validator;

@Component
public class HealthCompassConstants {
	

	static TimeZoneService timeZoneService;
	
	@Autowired
    public void setTimeZoneService(TimeZoneService timeZoneService){
		HealthCompassConstants.timeZoneService = timeZoneService;
    }
	
	
	
	public static int BILL_STATUS_PENDING_ID=19355;
	public static int BILL_STATUS_GENERATED_ID=19357;
	public static int LOCATION_STATUS_ACTIVE_ID=185;
	public static int PRACTITIONER_ROLE_DOCTOR_ID=1264;
	public static String loggedInUserName = "HC Admin";
	private static String encodeDecodeKey = "CSMMNtPnLo9aE64V6XLuOZO-bYNslmFO3oFXF6RA-K8=";
	public static String defaultLocalDateFormat = "dd-MM-yyyy";
	public static String defaultLocalTimeStampDateFormat = "dd-MM-yyyy HH:mm:ss";
	public static String defaultSQLDateFormat = "yyyy-MM-dd";
	public static String defaultSQLTimesStampDateFormat = "yyyy-MM-dd HH:mm:ss";
	//public static Map<String,String> timeZoneDateFormatMap = new HashMap<String,String>();
	//public static Map<String,String> timeZoneDateFormatMapOnlyDate = new HashMap<String,String>();
	
	
	public static String getDecodedValue(String attributeName,String encodedValue) {
		
	
		try {
			final Key key = new Key(encodeDecodeKey);
			//String encodedPatientName = getName("{\"(154,gAAAAABgacGWoJVG18JfA7lkQzUXzRbgMog1y1T2VgJg01S8Kfxv9ZnldSg4ADlMjI_RcKLrm0NOS4txNjsH3kxvpano5r4mBQ==,gAAAAABgacGWM4DRCeHKqO0Udc_az_ZSXOBagAPhLYTq7tAwTaT9bmHtcS2fwy-emnnelMgWSCY9OYctJAXclHwdynloORG3HQ==,gAAAAABgacGWRy3xbBtmh0uzGKTs7qUtxF7vYZZ6bActI3XL3Q2VeOhmWmTgForZyjL3BBPZAPk4Ujw-mr1mOBCU0ZWgOP8Oaw==,,,\\\"(\\\"\\\"2021-04-04 19:09:30\\\"\\\",\\\"\\\"9999-12-31 05:30:00\\\"\\\")\\\")\"}");
			final Token token = Token.fromString(encodedValue);
			
	       
	        final Validator<String> validator = new StringValidator() {
	    		public TemporalAmount getTimeToLive() {
	                return Duration.ofSeconds(Instant.MAX.getEpochSecond());
	            }
	    		};
	        final String payload = token.validateAndDecrypt(key, validator);
	        System.out.println("Attribute Name:"+attributeName+"");
	        
	        System.out.println("Payload is " + payload);
	        return payload;
		}catch(Exception e) {
			System.out.println("Error while decoding Attribute:"+attributeName+" : Message:"+e.getMessage());
			System.out.println("Encoded Value:"+encodedValue+"");
			
			e.printStackTrace();
			return encodedValue;
		}
		
		
	}
	
	public static String getEncodeValue(String attributeName,String decodeValue) {
		
		try {
			final Key key = new Key(encodeDecodeKey);
			//String encodedPatientName = getName("{\"(154,gAAAAABgacGWoJVG18JfA7lkQzUXzRbgMog1y1T2VgJg01S8Kfxv9ZnldSg4ADlMjI_RcKLrm0NOS4txNjsH3kxvpano5r4mBQ==,gAAAAABgacGWM4DRCeHKqO0Udc_az_ZSXOBagAPhLYTq7tAwTaT9bmHtcS2fwy-emnnelMgWSCY9OYctJAXclHwdynloORG3HQ==,gAAAAABgacGWRy3xbBtmh0uzGKTs7qUtxF7vYZZ6bActI3XL3Q2VeOhmWmTgForZyjL3BBPZAPk4Ujw-mr1mOBCU0ZWgOP8Oaw==,,,\\\"(\\\"\\\"2021-04-04 19:09:30\\\"\\\",\\\"\\\"9999-12-31 05:30:00\\\"\\\")\\\")\"}");
			Token encodedValue = Token.generate(key, decodeValue);
			
			
	       
	      
	        
	        System.out.println("Attribute Name:"+attributeName+"");
	        System.out.println("Encode Value is " + encodedValue.serialise());
	        return encodedValue.serialise();
		}catch(Exception e) {
			System.out.println("Error while emcoding Attribute:"+attributeName+" : Message:"+e.getMessage());
			System.out.println("Decode Value:"+decodeValue+"");
			
			e.printStackTrace();
			return decodeValue;
		}
		
		
	}
	
	public static java.sql.Date convertLocalDateToSQLFormat(String dateAsString,String destPatten) {
		java.sql.Date convertedDate=null;
         String userTimeZone = CustomFilter.getUserTimeZone().get();
         TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
	     String userLocalDateFormatOnlyDate = timeZoneDateInfo.getTimeZoneDateFormat();
		 
		try {
		DateFormat sourceDateFormat = new SimpleDateFormat(userLocalDateFormatOnlyDate);
		java.util.Date originalDate = sourceDateFormat.parse(dateAsString);
		
		convertedDate = new java.sql.Date(originalDate.getTime());
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return convertedDate;
	}
	
	public static String convertSQLDateToLocalDateString(java.sql.Date date) {
		String convertedDate="";
		if(date == null) return convertedDate;
		 String userTimeZone = CustomFilter.getUserTimeZone().get();
		 TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
	     String userLocalDateFormatOnlyDate = timeZoneDateInfo.getTimeZoneDateFormat();
    	 
		try {
		DateFormat dateFormat = new SimpleDateFormat(userLocalDateFormatOnlyDate);
		convertedDate = dateFormat.format(date.getTime());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return convertedDate;
	}
	
	public static String convertSQLDateTimeStampToLocalDateString(java.sql.Timestamp date) {
		String convertedDate=null;
		 String userTimeZone = CustomFilter.getUserTimeZone().get();
		 TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
	     String userLocalDateFormat = timeZoneDateInfo.getTimeZoneDateTimeFormat();
	     String userLocalDateFormatOnlyDate = timeZoneDateInfo.getTimeZoneDateFormat();
    	 
    	 DateFormat sdf = new SimpleDateFormat(userLocalDateFormat);
    	 DateTimeFormatter zoneConvertDateformatter = DateTimeFormatter.ofPattern(userLocalDateFormat+" z");
		 ZonedDateTime dbZonedDateTime = ZonedDateTime.parse(sdf.format(date.getTime())+" UTC", zoneConvertDateformatter); // In db, this will be in UTC zone
		 ZonedDateTime  clientZonedDateTime = dbZonedDateTime.withZoneSameInstant(ZoneId.of(userTimeZone));
		 
		 
         DateTimeFormatter clientDateFormatter = DateTimeFormatter.ofPattern(userLocalDateFormatOnlyDate); // change this as per client timezone
	   
	    convertedDate = clientZonedDateTime.format(clientDateFormatter);
	    
		
		return convertedDate;
	}
	
	public static String convertSQLDateTimeStampToLocalTimeStampDateString(java.sql.Timestamp date) {
		String convertedDate="";
		if(date == null) return convertedDate;
		
		 String userTimeZone = CustomFilter.getUserTimeZone().get();
		 TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
	     String userLocalDateFormat = timeZoneDateInfo.getTimeZoneDateTimeFormat();
    	 DateFormat sdf = new SimpleDateFormat(userLocalDateFormat); // change this to actual client date format. for now IST
		 DateTimeFormatter zoneConvertDateformatter = DateTimeFormatter.ofPattern(userLocalDateFormat+" z"); // change this to actual client date format. for now IST
		 ZonedDateTime dbZonedDateTime = ZonedDateTime.parse(sdf.format(date.getTime())+" UTC", zoneConvertDateformatter); // In db, this will be in UTC zone
        ZonedDateTime  clientZonedDateTime = dbZonedDateTime.withZoneSameInstant(ZoneId.of(userTimeZone)); //  replace the zone with actual client zone. For nopw we use IST
        DateTimeFormatter clientDateFormatter = DateTimeFormatter.ofPattern(userLocalDateFormat); // change this as per client timezone
	   
	    convertedDate = clientZonedDateTime.format(clientDateFormatter);
       
		
		return convertedDate;
		
		
	   
	   
	}
	
	

}
