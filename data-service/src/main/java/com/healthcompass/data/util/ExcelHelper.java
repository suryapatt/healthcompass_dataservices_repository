package com.healthcompass.data.util;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
//
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.HelpPageDefinition;
import com.healthcompass.data.model.HelpPageDetail;
import com.healthcompass.data.model.HelpPageDetailDefinition;
import com.healthcompass.data.model.HelpPageSection;
import com.healthcompass.data.model.HelpPageSectionDefinition;
import com.healthcompass.data.model.HelpPageSectionDetail;
import com.healthcompass.data.model.HelpPageSectionDetailDefinition;
import com.healthcompass.data.model.Language;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceField;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.LocalizationResourceSection;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.LanguageRepository;

@Component
public class ExcelHelper {
	//public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static String TYPE = "application/vnd.ms-excel";
	  
	  //static String[] HEADERs = { "Id", "Title", "Description", "Published" };
	  static String PAGE_SHEET = "Page_Fields"; 
	  static String SECTION_SHEET = "Page_Sections";

	   
	  
	  public static boolean hasExcelFormat(MultipartFile file) {

	    if (!TYPE.equals(file.getContentType())) {
	      return false;
	    }

	    return true;
	  }

	 
	  public static List<HelpPage> excelToHelpPages(InputStream is) {
	    try {
	      Workbook workbook = new HSSFWorkbook(is);

	      Sheet sheet = workbook.getSheet(PAGE_SHEET);
	      Iterator<Row> rows = sheet.iterator();

	      List<HelpPage> helpPages = new ArrayList<HelpPage>();
	 
         
          
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();

	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        
	        Iterator<Cell> cellsInRow = currentRow.iterator();
           
	       
	        HelpPage page = null;
	        HelpPageDefinition pageDefinition = null;
	        HelpPageDetail pageDetail = null;
	        HelpPageDetailDefinition pageDetailDefinition = null;
	        HelpPageSection section = null;
	       
	    	String pageId=null;
	    	String pageDefinitionId=null;
	    	String pageDetailId=null;
	    	String pageDetailDefinitionId=null;
	    	String helpSectionId=null;
	    	String languageCode=null;
	    	Integer serialNumber=null;
	    	String type=null;
	    	Integer fieldPosition=null;
	    	String sectionName=null;
	    	String sectionId=null;
	    	String padded=null;
	    	String paddedFieldPos=null;
	    	
	        
	    	// 0 - Language, 1 S. No.2- Type 3-Page ID	4- Page Name, 5 - Page defiinition id
	    	// 6 -Page Display Name 7- page descrpition	8-field position, 9 page detail id, 11-field name
	    	// 12 dsta type 13-section 14-section id 15-display 16-page detail def id
	    	// 17 field display name 18-field description
	    	
	        
	    	int lastColumn = currentRow.getLastCellNum();
	        for (int cellIdx = 0; cellIdx < lastColumn; cellIdx++) {
	       
	        	Cell currentCell  = currentRow.getCell(cellIdx);
	        	 if (currentCell == null) continue;

	          switch (cellIdx) {
	          
	         
              
	          case 0:
	                {   
	 	               //  Language,
	                	languageCode = (String) currentCell.getStringCellValue();
	                	
	      	            
	                } 
	                break;
	                
	          case 1:
              { 
            	  // S. No.
            	  double sno = currentCell.getNumericCellValue();
            	 
            	  serialNumber =  Integer.valueOf((int) sno);
            
    	            
              } 
              break;
              
	          case 2:
        		{
        			//3- Type	
        			type = (String) currentCell.getStringCellValue();
              	
        		}
      	 
      	  
          break;

	          case 3:
	        	 // 3-Page ID	
	        	pageId = (String) currentCell.getStringCellValue();
	        	boolean isExistingInDB = pageId != null && !pageId.isEmpty() && !pageId.isBlank() ; //  update case
	        	padded = String.format("%03d" , serialNumber);
	        	
              	page = checkAndGetPage(type+padded,helpPages);
              	
              	if(page.getId() == null) {
              		
              		page.setId(type+""+padded);
              		page.setSerialNumber(serialNumber);
              		page.setPadded(padded);
              		page.setType(type);
              		page.setPageDefinitions(new ArrayList<HelpPageDefinition>());
              		page.setDetails(new ArrayList<HelpPageDetail>());
              		helpPages.add(page);
              	}
	            break;
	            
	         
	           
	          
	            
	          case 4:
	        	 // 4- Page Name
	        	  String pageName =  (String) currentCell.getStringCellValue();
	        	  padded = String.format("%03d" , serialNumber);
	        	  page = checkAndGetPage(type+padded,helpPages);
	              	
	              	if(page.getId() == null) {
	              		
	              		page.setId(type+""+padded);
	              		page.setSerialNumber(serialNumber);
	              		page.setPadded(padded);
	              		page.setType(type);
	              		page.setPageDefinitions(new ArrayList<HelpPageDefinition>());
	              		page.setDetails(new ArrayList<HelpPageDetail>());
	              		helpPages.add(page);
	              	}
            	  page.setPageName(pageName);
              	
              	            	  
	        	  
	            break;
	           
		    
	          case 5:
	        	  // page definitio id
	        	  pageDefinitionId = (String) currentCell.getStringCellValue();
	        	  padded = String.format("%03d" , serialNumber);
	        	  pageDefinition = checkAndGetPageDefinition(type+padded+languageCode, page.getPageDefinitions());
	              	if(pageDefinition.getId() == null) {
	              		pageDefinition.setId(page.getId()+languageCode);
	              		pageDefinition.setPage(page.getId());
	              		pageDefinition.setLanguage(languageCode);
	              		page.getPageDefinitions().add(pageDefinition);
	              		
	              	}
	            break;
	            
	          case 6:
	        	  // page display name
	        	  String pageDisplayName = (String) currentCell.getStringCellValue();
	        	 
	        	  
	        	    pageDefinition = checkAndGetPageDefinition(type+padded+languageCode, page.getPageDefinitions());
	              	if(pageDefinition.getId() == null) {
	              		pageDefinition.setId(page.getId()+languageCode);
	              		pageDefinition.setPage(page.getId());
	              		pageDefinition.setLanguage(languageCode);
	              		page.getPageDefinitions().add(pageDefinition);
	              		
	              	}
	              	
	              	 pageDefinition.setDisplayName(pageDisplayName);
	              	 
	            break;
	            
	          case 7:
	        	  // page decsription
	        	  pageDefinition.setDescription((String) currentCell.getStringCellValue());
	        	
	            break;
	            
	        
	            
	          case 8:
	        	  // field position
	        	  
	        	  double fpos = currentCell.getNumericCellValue();
	            	 
	        	  fieldPosition =  Integer.valueOf((int) fpos);
	        	 
	        	
	        	
	        	  
	            break;
	            
	          case 9:
	        	    // page detail id
	        	    pageDetailId =  (String) currentCell.getStringCellValue();
	        	    paddedFieldPos = String.format("%03d" , fieldPosition);
            		pageDetail = checkAndGetHelpPageDetail(page.getId()+paddedFieldPos, page.getDetails());
            		if(pageDetail.getId() == null) {
                  		
                  		pageDetail.setId(page.getId()+paddedFieldPos);
                  		pageDetail.setPaddedPosition(paddedFieldPos);
                  		pageDetail.setPosition(fieldPosition);
                  		pageDetail.setPageName(page.getPageName());
                  		pageDetail.setParentPage(page.getId());
                  		page.getDetails().add(pageDetail);
                  		pageDetail.setDetailDefinitions(new ArrayList<HelpPageDetailDefinition>());
                  		
                  	}
	        	  
	        	 
            	  
	        	
	            break;
	          case 10:
	        	  //10-field name,
	        	  String fieldName = (String) currentCell.getStringCellValue();
	        	 
	        	  paddedFieldPos = String.format("%03d" , fieldPosition);
	        	  pageDetail = checkAndGetHelpPageDetail(page.getId()+paddedFieldPos, page.getDetails());
          		  if(pageDetail.getId() == null) {
                		
                		pageDetail.setId(page.getId()+paddedFieldPos);
                		pageDetail.setPaddedPosition(paddedFieldPos);
                		pageDetail.setPosition(fieldPosition);
                		pageDetail.setPageName(page.getPageName());
                		pageDetail.setParentPage(page.getId());
                		page.getDetails().add(pageDetail);
                		pageDetail.setDetailDefinitions(new ArrayList<HelpPageDetailDefinition>());
                		
                	}
          		 pageDetail.setFieldName(fieldName);
          		
	            break;
	          case 11:
	        	  //11-data type
	        	  pageDetail.setDataType((String) currentCell.getStringCellValue());
	            break;
	          case 12:
	        	  // section name
	        	 
	        	  sectionName = (String) currentCell.getStringCellValue();
	        	  if(sectionName != null && !sectionName.isEmpty()) {
	        		  pageDetail.setSectionName(sectionName);
	        	  }
	        	 
	        	  
	            break;
	            
	          case 13:  
	        	  //13 -Section Id	
		  	    	
	        	 
	        	
	        	 sectionId = (String) currentCell.getStringCellValue();
	        	 if(sectionId != null && !sectionId.isEmpty()) {
	        		
	        			 pageDetail.setSectionId(sectionId);
		        		  
	        		
	        		
	        		
	        		
	        	 }
	        	 
            		
            	 
	        	 
            	  
	        	  break; 
	        	  
	          case 14:   
	        	//14 -display	
	        	  Boolean isDisplay =  (Boolean) currentCell.getBooleanCellValue();
	        	  
	        	  pageDetail.setIsDisplay(isDisplay);
            	  
	        	  break; 
	        	  
	        	     	  
	        	  
	          case 15:
	        	
	        	  // page detail definition id
	        	    pageDetailDefinitionId =  (String) currentCell.getStringCellValue();
	        	    if(pageDetail.getSectionId() == null) {
	        	    	
	        	    	  pageDetailDefinition = checkAndGetHelpPageDetailDefinition(pageDetail.getId()+""+languageCode, pageDetail.getDetailDefinitions());
	                		if(pageDetailDefinition.getId() == null) {
	                      		
	                			pageDetailDefinition.setId(pageDetail.getId()+""+languageCode);
	                			pageDetailDefinition.setPageDetail(pageDetail.getId());
	                			pageDetail.getDetailDefinitions().add(pageDetailDefinition);
	                			pageDetailDefinition.setLanguage(languageCode);
	                      		
	                      		
	                      	}
	        	    }
	        	  
	        	  
	        	  break; 
	          case 16: 
	        		//16-Field Display name
	        	    String pageDetDefDisplayName = (String) currentCell.getStringCellValue();
	        	   if(pageDetail.getSectionId() == null) {
	        		  
	        		   pageDetailDefinition = checkAndGetHelpPageDetailDefinition(pageDetail.getId()+""+languageCode, pageDetail.getDetailDefinitions());
               		if(pageDetailDefinition.getId() == null) {
                     		
               			pageDetailDefinition.setId(pageDetail.getId()+""+languageCode);
               			pageDetailDefinition.setPageDetail(pageDetail.getId());
               			pageDetail.getDetailDefinitions().add(pageDetailDefinition);
               			pageDetailDefinition.setLanguage(languageCode);
                     		
                     		
                     	}
	        		   pageDetailDefinition.setDisplayName(pageDetDefDisplayName);
	        		
	        	   }
	        	  
	        	  
	        	  break; 
	         
	          case 17: 
	        		//16-Field description
	        	  if(pageDetail.getSectionId() == null) {
	        	   pageDetailDefinition.setDescription((String) currentCell.getStringCellValue());
	        	  }
	        	  break; 

	          default:
	            break;
	          }

	          //cellIdx++;
	         
	        }

	       
	      }

	      workbook.close();

	      return helpPages;
	    } catch (IOException e) {
	      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
	    }
	  }
	  
	  public static List<HelpPageSection> excelToHelpPageSections(InputStream is) {
		    try {
		      Workbook workbook = new HSSFWorkbook(is);

		      Sheet sheet = workbook.getSheet(SECTION_SHEET);
		      Iterator<Row> rows = sheet.iterator();

		      List<HelpPageSection> helpPageSections = new ArrayList<HelpPageSection>();
		 
	         
	          
		      int rowNumber = 0;
		      while (rows.hasNext()) {
		        Row currentRow = rows.next();

		        // skip header
		        if (rowNumber == 0) {
		          rowNumber++;
		          continue;
		        }
		        
		        Iterator<Cell> cellsInRow = currentRow.iterator();
	           
		       
		        HelpPageSection section = null;
		        HelpPageSectionDefinition sectionDefinition = null;
		        HelpPageSectionDetail sectionDetail = null;
		        HelpPageSectionDetailDefinition sectionDetailDefinition = null;
		       
		       
		    	String sectionId=null;
		    	String sectionDefinitionId=null;
		    	String sectionDetailId=null;
		    	String sectionDetailDefinitionId=null;
		    	
		    	String languageCode=null;
		    	Integer serialNumber=null;
		    	String type=null;
		    	Integer fieldPosition=null;
		    	String selfReferrence=null;
		    	String selfReferrenceId=null;
		    	String padded=null;
		    	String paddedFieldPos=null;
		        
		    	// 0 - Language, 1 S. No.2- Type 3-Section ID	4- Section Name, 5 - Section defiinition id
		    	// 6 -Section Display Name 7- Section descrpition	8-field position, 9 section detail id, 
		    	// 10-field name , 11-self referrence 12-self referrence ID, 13-display 
		    	// 14-section detail def id  15 field display name 16-field description
		    	
		        
		    	int lastColumn = currentRow.getLastCellNum();
		        for (int cellIdx = 0; cellIdx < lastColumn; cellIdx++) {
		       
		        	Cell currentCell  = currentRow.getCell(cellIdx);
		        	 if (currentCell == null) continue;

		          switch (cellIdx) {
		          
		         
	              
		          case 0:
		                {   
		 	               //  Language,
		                	languageCode = (String) currentCell.getStringCellValue();
		                	
		      	            
		                } 
		                break;
		                
		          case 1:
	              { 
	            	  // S. No.
	            	  double sno = currentCell.getNumericCellValue();
	            	 
	            	  serialNumber =  Integer.valueOf((int) sno);
	            
	    	            
	              } 
	              break;
	              
		          case 2:
	        		{
	        			//3- Type	
	        			type = (String) currentCell.getStringCellValue();
	              	
	        		}
	      	 
	      	  
	          break;

		          case 3:
		        	 // 3-Page ID	
		        	sectionId = (String) currentCell.getStringCellValue();
		        	boolean isExistingInDB = sectionId != null && !sectionId.isEmpty() && !sectionId.isBlank() ; //  update case
		        	padded = String.format("%03d" , serialNumber);
		        	
	              	section = checkAndGetSection(type+padded,helpPageSections);
	              	
	              	if(section.getId() == null) {
	              		
	              		section.setId(type+""+padded);
	              		section.setSerialNumber(serialNumber);
	              		section.setPadded(padded);
	              		section.setType(type);
	              		section.setSectionDefinitions(new ArrayList<HelpPageSectionDefinition>());
	              		section.setSectionDetail(new ArrayList<HelpPageSectionDetail>());
	              		helpPageSections.add(section);
	              	}
		            break;
		            
		         
		           
		          
		            
		          case 4:
		        	 // 4- Page Name
		        	  String sectionName =  (String) currentCell.getStringCellValue();
		        	  padded = String.format("%03d" , serialNumber);
			          section = checkAndGetSection(type+padded,helpPageSections);
		              	if(section.getId() == null) {
		        	 
		        		  
		        		  section.setId(type+""+padded);
		              		section.setSerialNumber(serialNumber);
		              		section.setPadded(padded);
		              		section.setType(type);
		              		section.setSectionDefinitions(new ArrayList<HelpPageSectionDefinition>());
		              		section.setSectionDetail(new ArrayList<HelpPageSectionDetail>());
		              		helpPageSections.add(section);
		        	  }
		        	  section.setSectionName(sectionName);
	              	
	              	            	  
		        	  
		            break;
		           
			    
		          case 5:
		        	  // page definitio id
		        	  sectionDefinitionId = (String) currentCell.getStringCellValue();
		        	  padded = String.format("%03d" , serialNumber);
		        	  sectionDefinition = checkAndGetSectionDefinition(type+padded+languageCode, section.getSectionDefinitions());
		              	if(sectionDefinition.getId() == null) {
		              		sectionDefinition.setId(section.getId()+languageCode);
		              		sectionDefinition.setSection(section.getId());
		              		sectionDefinition.setLanguage(languageCode);
		              		section.getSectionDefinitions().add(sectionDefinition);
		              		
		              	}
		            break;
		            
		          case 6:
		        	  // page display name
		        	  String sectionDefDisplayName = (String) currentCell.getStringCellValue();
		        	  padded = String.format("%03d" , serialNumber);
		        	  sectionDefinition = checkAndGetSectionDefinition(type+padded+languageCode, section.getSectionDefinitions());
		              	if(sectionDefinition.getId() == null) {
		              		sectionDefinition.setId(section.getId()+languageCode);
		              		sectionDefinition.setSection(section.getId());
		              		sectionDefinition.setLanguage(languageCode);
		              		section.getSectionDefinitions().add(sectionDefinition);
		              		
		              	}
		              	 sectionDefinition.setDisplayName(sectionDefDisplayName);
		            break;
		            
		          case 7:
		        	  // page decsription
		        	  sectionDefinition.setDescription((String) currentCell.getStringCellValue());
		        	
		            break;
		            
		        
		            
		          case 8:
		        	  // field position
		        	  
		        	  double fpos = currentCell.getNumericCellValue();
		            	 
		        	  fieldPosition =  Integer.valueOf((int) fpos);
		        	 
		        	
		        	
		        	  
		            break;
		            
		          case 9:
		        	    // page detail id
		        	    sectionDetailId =  (String) currentCell.getStringCellValue();
		        	    paddedFieldPos = String.format("%03d" , fieldPosition);
	            		sectionDetail = checkAndGetHelpSectionDetail(section.getId()+paddedFieldPos, section.getSectionDetail());
	            		if(sectionDetail.getId() == null) {
	                  		
	            			sectionDetail.setId(section.getId()+paddedFieldPos);
	            			sectionDetail.setPaddedPosition(paddedFieldPos);
	            			sectionDetail.setPosition(fieldPosition);
	            			sectionDetail.setSectionName(section.getSectionName());
	            			sectionDetail.setParentSection(section.getId());
	            			section.getSectionDetail().add(sectionDetail);
	            			sectionDetail.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinition>());
	                  		
	                  	}
		        	  
		        	 
	            	  
		        	
		            break;
		          case 10:
		        	  //10-field name,
		        	  String fieldName = (String) currentCell.getStringCellValue();
		        	 
		        	  paddedFieldPos = String.format("%03d" , fieldPosition);
	            		sectionDetail = checkAndGetHelpSectionDetail(section.getId()+paddedFieldPos, section.getSectionDetail());
	            		if(sectionDetail.getId() == null) {
	                  		
	            			sectionDetail.setId(section.getId()+paddedFieldPos);
	            			sectionDetail.setPaddedPosition(paddedFieldPos);
	            			sectionDetail.setPosition(fieldPosition);
	            			sectionDetail.setSectionName(section.getSectionName());
	            			sectionDetail.setParentSection(section.getId());
	            			section.getSectionDetail().add(sectionDetail);
	            			sectionDetail.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinition>());
	                  		
	                  	}
	            		 sectionDetail.setFieldName(fieldName);
		            break;
		          case 11:
		        	  //11-data type
		        	  sectionDetail.setSelfReferrence((String) currentCell.getStringCellValue());
		            break;
		          case 12:
		        	  
		        	  sectionDetail.setSelfReferrenceId((String) currentCell.getStringCellValue());
		        	  
		            break;
		            
		         
		        	  
		          case 13:   
		        	//14 -display	
		        	  Boolean isDisplay =  (Boolean) currentCell.getBooleanCellValue();
		        	  
		        	  sectionDetail.setIsDisplay(isDisplay);
	            	  
		        	  break; 
		        	  
		        	     	  
		        	  
		          case 14:
		        	
		        	  // page detail definition id
		        	    sectionDetailDefinitionId =  (String) currentCell.getStringCellValue();
		        	  
		        	    	
		        	    	  sectionDetailDefinition = checkAndGetHelpSectionDetailDefinition(sectionDetail.getId()+""+languageCode, sectionDetail.getSectionDetailDefinitions());
		                		if(sectionDetailDefinition.getId() == null) {
		                      		
		                			sectionDetailDefinition.setId(sectionDetail.getId()+""+languageCode);
		                			sectionDetailDefinition.setSectionDetail(sectionDetail.getId());
		                			sectionDetail.getSectionDetailDefinitions().add(sectionDetailDefinition);
		                			sectionDetailDefinition.setLanguage(languageCode);
		                      		
		                      		
		                      	}
		        	   
		        	  
		        	  
		        	  break; 
		          case 15: 
		        		//16-Field Display name
		        	  String sectionDetDefDisplayName = (String) currentCell.getStringCellValue();
		        	  sectionDetailDefinition = checkAndGetHelpSectionDetailDefinition(sectionDetail.getId()+""+languageCode, sectionDetail.getSectionDetailDefinitions());
              		  if(sectionDetailDefinition.getId() == null) {
                    		
              			sectionDetailDefinition.setId(sectionDetail.getId()+""+languageCode);
              			sectionDetailDefinition.setSectionDetail(sectionDetail.getId());
              			sectionDetail.getSectionDetailDefinitions().add(sectionDetailDefinition);
              			sectionDetailDefinition.setLanguage(languageCode);
                    		
                    		
                    	}  
		        	  sectionDetailDefinition.setDisplayName(sectionDetDefDisplayName);
		        		
		        	  
		        	  
		        	  
		        	  break; 
		         
		          case 16: 
		        		//16-Field description
		        	 
		        		  sectionDetailDefinition.setDescription((String) currentCell.getStringCellValue());
		        	  
		        	  break; 

		          default:
		            break;
		          }

		          //cellIdx++;
		         
		        }

		       
		      }

		      workbook.close();

		      return helpPageSections;
		      
		    } catch (IOException e) {
		      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		    }
		  }

	  
	 /* public static List<LocalizationResourceGroup> excelToResourcesOld(InputStream is,Map<String, BigInteger> languageMap) {
		    try {
		      Workbook workbook = new HSSFWorkbook(is);

		      Sheet sheet = workbook.getSheet(SHEET);
		      Iterator<Row> rows = sheet.iterator();

		      List<LocalizationResourceGroup> resourceGroups = new ArrayList<LocalizationResourceGroup>();
		 
	         
	          
		      int rowNumber = 0;
		      while (rows.hasNext()) {
		        Row currentRow = rows.next();

		        // skip header
		        if (rowNumber == 0) {
		          rowNumber++;
		          continue;
		        }
		        
		        Iterator<Cell> cellsInRow = currentRow.iterator();
	           
		       
		        LocalizationResourceGroup resourceGroup = null;
		        LocalizationResourceCategory resourceCategory = null;
		        LocalizationResource resource = null;
		        LocalizationResourceSection section = null;
		        LocalizationResourceField field = null;
		    	Integer resourceGroupSystemId=null;
		    	Integer resourceCategorystemId=null;
		    	Integer resourceSystemId=null;
		    	Integer resourceSectionSystemId=null;
		    	Integer resourceFieldSystemId=null;
		        
		        //int cellIdx = 0;
		        int lastColumn = currentRow.getLastCellNum();
		        for (int cellIdx = 0; cellIdx < lastColumn; cellIdx++) {
		        //while (cellsInRow.hasNext()) {
		          //Cell currentCell = cellsInRow.next();
		        	Cell currentCell  = currentRow.getCell(cellIdx);
		        	 if (currentCell == null) continue;

		          switch (cellIdx) {
		          
		         
	              
		          case 0:
		                {   
		 	               // The spreadsheet is empty in this cell
			           
		                	String resourceGroupCode = (String) currentCell.getStringCellValue();
		                	resourceGroup = checkAndGetResourceGroup(resourceGroupCode,resourceGroups);
		                	if(resourceGroup.getResourceGroupCode() == null) {
		                		resourceGroup.setResourceGroupCode(resourceGroupCode);
		                		resourceGroup.setResourceCategories(new ArrayList<LocalizationResourceCategory>());
		                		resourceGroups.add(resourceGroup);
		                	}
		      	            
		                } 
		                break;
		                
		          case 1:
	              { 
	            	  String groupId =  (String) currentCell.getStringCellValue();
	            	  if(groupId == null || groupId.isEmpty()) {
	            		  resourceGroup.setId(null);
	            	  }else {
	            		  resourceGroupSystemId = Integer.valueOf(groupId);
	            		  resourceGroup.setId(resourceGroupSystemId);
	            	  }
	              	
	              	
	            	
	    	            
	              } 
	              break;

		          case 2:
		        	  resourceGroup.setResourceGroupName((String) currentCell.getStringCellValue());
		            break;
		            
		         

		          case 3:
		          		{
		          			String resourceCategoryId = (String) currentCell.getStringCellValue();
		                	resourceCategory = checkAndGetResourceCategory(resourceCategoryId,resourceGroup.getResourceCategories());
		                	if(resourceCategory.getResourceCategoryId() == null) {
		                		resourceCategory.setResourceCategoryId(resourceCategoryId);
		                		resourceCategory.setResourceGroup(resourceGroup.getResourceGroupCode());
		                		resourceCategory.setResources(new ArrayList<LocalizationResource>());
		                		resourceGroup.getResourceCategories().add(resourceCategory);
		                		
		                	}
		          			 
		          		}
		        	 
		        	  
		            break;
		            
		          case 4:
		        	  
		        	  String catId =  (String) currentCell.getStringCellValue();
	            	  if(catId == null || catId.isEmpty()) {
	            		  resourceCategory.setId(null);
	            	  }else {
	            		  resourceCategorystemId = Integer.valueOf(catId);
	            		  resourceCategory.setId(resourceCategorystemId);
	            	  }
	              	
	              	
	            	
	    	            
	            	  
		        	  
		            break;

		          case 5:
		        	  resourceCategory.setResourceCategoryCode((String) currentCell.getStringCellValue());
		            break;
		            
		          case 6:
		        	  resourceCategory.setResourceCategoryName((String) currentCell.getStringCellValue());
		            break;
		            
		          case 7:
		        	  resourceCategory.setResourceCategoryLevel((int) currentCell.getNumericCellValue());
		            break;
		            
		        
		            
		          case 8:
		        	  
		        	  String resourceId = (String) currentCell.getStringCellValue();
	              		resource = checkAndGetResource(resourceId,resourceCategory.getResources());
	              	if(resource.getResourceId() == null) {
	              		resource.setResourceId(resourceId);
	              		resource.setResourceCategory(resourceCategory.getResourceCategoryId());
	              		resource.setResourceGroupCode(resourceGroup.getResourceGroupCode());
	              		resource.setResourceSections(new ArrayList<LocalizationResourceSection>());
	              		resourceCategory.getResources().add(resource);
	              		
	              		
	              	}
		        	  
		            break;
		            
		          case 9:
		        	  
		        	  String resId =  (String) currentCell.getStringCellValue();
	            	  if(resId == null || resId.isEmpty()) {
	            		  resource.setId(null);
	            	  }else {
	            		  resourceSystemId = Integer.valueOf(resId);
	            		  resource.setId(resourceSystemId);
	            	  }
	            	  
		        	
		            break;
		          case 10:
		        	  resource.setResourceCode((String) currentCell.getStringCellValue());
		            break;
		          case 11:
		        	  resource.setResourceSubCategory((String) currentCell.getStringCellValue());
		            break;
		          case 12:
		        	  resource.setResourceName((String) currentCell.getStringCellValue());
		            break;
		            
		          case 13:  //Resource Section ID
		        	  
		        	  String resourceSectionId = (String) currentCell.getStringCellValue();
	            		section = checkAndGetResourceSection(resourceSectionId,resource.getResourceSections());
	            	if(section.getResourceSectionId() == null) {
	            		section.setResourceSectionId(resourceSectionId);
	            		section.setResource(resource.getResourceId());
	            		section.setResourceSectionFields(new ArrayList<LocalizationResourceField>());
	            		resource.getResourceSections().add(section);
	            		
	            		
	            	}
		        	 
	            	  
		        	  break; 
		        	  
		          case 14:   // Resource Section System ID
		        	  
		        	  String resSedId =  (String) currentCell.getStringCellValue();
	            	  if(resSedId == null || resSedId.isEmpty()) {
	            		  section.setId(null);
	            	  }else {
	            		  resourceSectionSystemId = Integer.valueOf(resSedId);
	            		  section.setId(resourceSectionSystemId);
	            	  }
	            	  
		        	  break; 
		        	  
		        	     	  
		        	  
		          case 15: // Resource Section Code
		        	  
		        	  section.setResourceSectionCode((String) currentCell.getStringCellValue());
		        	  
		        	  break; 
		          case 16: // Resource Section Name
		        	  
		        	  section.setResourceSectionName((String) currentCell.getStringCellValue());
		        	  
		        	  break; 
		          case 17: // Resource Section Position
		        	  
		        	  String position =  (String) currentCell.getStringCellValue();
	            	  Integer resourceSectionPosition = Integer.valueOf(position);
	            	  section.setPosition(resourceSectionPosition);
	            	        	  
		        	  break; 
		        	  
		          case 18: // Field ID
		        	  
		        	  String resourceSectionFieldId = (String) currentCell.getStringCellValue();
	          		  field = checkAndGetResourceField(resourceSectionFieldId, section.getResourceSectionFields());
	          	if(field.getResourceSectionFieldId() == null) {
	          		field.setResourceSectionFieldId(resourceSectionFieldId);
	          		field.setResourceSection(section.getResourceSectionId());
	          		section.getResourceSectionFields().add(field);
	          		
	          		
	          	}
		        	  
		        	  break; 
		          case 19:  // Field System ID
		        	  
		        	  String fieldId =  (String) currentCell.getStringCellValue();
	            	  if(fieldId == null || fieldId.isEmpty()) {
	            		  field.setId(null);
	            	  }else {
	            		  resourceFieldSystemId = Integer.valueOf(fieldId);
	            		  field.setId(resourceFieldSystemId);
	            	  }
		        	  
		        	  
		        	  break; 
		          case 20: // Field Code
		        	  
		        	  field.setResourceSectionFieldCode((String) currentCell.getStringCellValue());
		        	  
		        	  break;
		          case 21: // Field Position
		        	  
		        	  String fieldPosition =  (String) currentCell.getStringCellValue();
	            	  Integer resourceSectionFieldPosition = Integer.valueOf(fieldPosition);
	            	  field.setPosition(resourceSectionFieldPosition);
		        	  
		        	  
		        	  break;
		          case 22: // Field Name
		        	  
		        	  field.setResourceSectionFieldName((String) currentCell.getStringCellValue());
		        	  
		        	  break;
		          case 23:
		        	  
		        	  String languageCode = (String) currentCell.getStringCellValue();
		        	  BigInteger languageId = (BigInteger) languageMap.get(languageCode);
		        	  
		        	  field.setLanguageId(languageId);
		            break;
		         	         
		          case 24:
		        	  field.setText((String) currentCell.getStringCellValue());
		            break;

		          default:
		            break;
		          }

		          //cellIdx++;
		         
		        }

		       
		      }

		      workbook.close();

		      return resourceGroups;
		    } catch (IOException e) {
		      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		    }
		  }*/
		  
		  
	  private static HelpPage checkAndGetPage(String pageId, List<HelpPage> helpPages) {
		  if(helpPages == null || helpPages.isEmpty()) {
			  return new HelpPage();
		  }
		  Optional<HelpPage> optTargetPage = helpPages.stream()
					.filter(page -> page.getId().equals(pageId)).findFirst();

					    if(optTargetPage.isPresent())  //check this logic where the chilkd
					    	
					    {
					    	return optTargetPage.get();
					    }else{
					    	
					    	
					    	return new HelpPage();
					    }
	  }
	  
	  private static HelpPageDefinition  checkAndGetPageDefinition(String pageDefId, List<HelpPageDefinition> pageDefinitions) {
		  
		  if(pageDefinitions == null || pageDefinitions.isEmpty()) {
			  return new HelpPageDefinition();
		  }
		  
		  Optional<HelpPageDefinition> optTargetHelpPageDefinition = pageDefinitions.stream()
					.filter(pageDef -> pageDef.getId().equals(pageDefId)).findFirst();

					    if(optTargetHelpPageDefinition.isPresent())  //check this logic where the chilkd
					    	
					    {
					    	return optTargetHelpPageDefinition.get();
					    }else{
					    	
					    	
					    	return new HelpPageDefinition();
					    }
	  }
	  
 private static HelpPageDetail  checkAndGetHelpPageDetail(String pageDetailId, List<HelpPageDetail> pageDetailList) {
		  
		  if(pageDetailList == null || pageDetailList.isEmpty()) {
			  return new HelpPageDetail();
		  }
		  
		  Optional<HelpPageDetail> optTargetHelpPageDetail = pageDetailList.stream()
					.filter(pageDetail -> pageDetail.getId().equals(pageDetailId)).findFirst();

					    if(optTargetHelpPageDetail.isPresent())  //check this logic where the chilkd
					    	
					    {
					    	return optTargetHelpPageDetail.get();
					    }else{
					    	
					    	
					    	return new HelpPageDetail();
					    }
	  }
 
 private static HelpPageDetailDefinition  checkAndGetHelpPageDetailDefinition(String pageDetailDefId, List<HelpPageDetailDefinition> pageDetailDefinitions) {
	  
	  if(pageDetailDefinitions == null || pageDetailDefinitions.isEmpty()) {
		  return new HelpPageDetailDefinition();
	  }
	  
	  Optional<HelpPageDetailDefinition> optTargetHelpPageDetailDefinition = pageDetailDefinitions.stream()
				.filter(pageDetailDef -> pageDetailDef.getId().equals(pageDetailDefId)).findFirst();

				    if(optTargetHelpPageDetailDefinition.isPresent())  //check this logic where the chilkd
				    	
				    {
				    	return optTargetHelpPageDetailDefinition.get();
				    }else{
				    	
				    	
				    	return new HelpPageDetailDefinition();
				    }
 }
	  
	
 private static HelpPageSection  checkAndGetHelpPageSection(String sectionId, HelpPageSection section) {
	  
	  if(section == null || sectionId == null) {
		  return new HelpPageSection();
	  }
	  if(sectionId.equals(section.getId())) {
		  return section;
	  }else {
		  return new HelpPageSection();
	  }
	  
	
}
 
 private static HelpPageSection checkAndGetSection(String sectionId, List<HelpPageSection> helpPageSections) {
	  if(helpPageSections == null || helpPageSections.isEmpty()) {
		  return new HelpPageSection();
	  }
	  Optional<HelpPageSection> optTargetSection = helpPageSections.stream()
				.filter(section -> section.getId().equals(sectionId)).findFirst();

				    if(optTargetSection.isPresent())  //check this logic where the chilkd
				    	
				    {
				    	return optTargetSection.get();
				    }else{
				    	
				    	
				    	return new HelpPageSection();
				    }
 }
 
 private static HelpPageSectionDefinition  checkAndGetSectionDefinition(String sectionDefId, List<HelpPageSectionDefinition> sectionDefinitions) {
	  
	  if(sectionDefinitions == null || sectionDefinitions.isEmpty()) {
		  return new HelpPageSectionDefinition();
	  }
	  
	  Optional<HelpPageSectionDefinition> optTargetHelpPageSectionDefinition = sectionDefinitions.stream()
				.filter(sectionDef -> sectionDef.getId().equals(sectionDefId)).findFirst();

				    if(optTargetHelpPageSectionDefinition.isPresent())  //check this logic where the chilkd
				    	
				    {
				    	return optTargetHelpPageSectionDefinition.get();
				    }else{
				    	
				    	
				    	return new HelpPageSectionDefinition();
				    }
 }
 
private static HelpPageSectionDetail  checkAndGetHelpSectionDetail(String sectionDetailId, List<HelpPageSectionDetail> sectionDetailList) {
	  
	  if(sectionDetailList == null || sectionDetailList.isEmpty()) {
		  return new HelpPageSectionDetail();
	  }
	  
	  Optional<HelpPageSectionDetail> optTargetHelpSectionDetail = sectionDetailList.stream()
				.filter(sectionDetail -> sectionDetail.getId().equals(sectionDetailId)).findFirst();

				    if(optTargetHelpSectionDetail.isPresent())  //check this logic where the chilkd
				    	
				    {
				    	return optTargetHelpSectionDetail.get();
				    }else{
				    	
				    	
				    	return new HelpPageSectionDetail();
				    }
 }

private static HelpPageSectionDetailDefinition  checkAndGetHelpSectionDetailDefinition(String sectionDetailDefId, List<HelpPageSectionDetailDefinition> sectionDetailDefinitions) {
 
 if(sectionDetailDefinitions == null || sectionDetailDefinitions.isEmpty()) {
	  return new HelpPageSectionDetailDefinition();
 }
 
 Optional<HelpPageSectionDetailDefinition> optTargetHelpSectionDetailDefinition = sectionDetailDefinitions.stream()
			.filter(sectionDetailDef -> sectionDetailDef.getId().equals(sectionDetailDefId)).findFirst();

			    if(optTargetHelpSectionDetailDefinition.isPresent())  //check this logic where the chilkd
			    	
			    {
			    	return optTargetHelpSectionDetailDefinition.get();
			    }else{
			    	
			    	
			    	return new HelpPageSectionDetailDefinition();
			    }
}
 

	

}
