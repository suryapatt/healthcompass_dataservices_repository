package com.healthcompass.data.util;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.healthcompass.data.CustomFilter;
import com.healthcompass.data.model.TimeZoneInformation;
import com.healthcompass.data.service.TimeZoneService;

@Component
public class CustomDateDeSerializer extends  JsonDeserializer<Timestamp> {
	
	
    
	private  static TimeZoneService timeZoneService;
	
	

    public CustomDateDeSerializer() {
        
    }

    @Autowired
    public CustomDateDeSerializer(TimeZoneService timeZoneService) {
      
        this.timeZoneService= timeZoneService;
    }
	
    // use joda library for thread safe issue
    private static final DateTimeFormatter newDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public java.sql.Timestamp deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    	 
	        Timestamp returnTime = null;
	        String userTimeZone = CustomFilter.getUserTimeZone().get();
	        TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
	        String userLocalDateFormat = timeZoneDateInfo.getTimeZoneDateTimeFormat();
	        try {
	        	if (jp.getCurrentToken().equals(JsonToken.VALUE_STRING)) {
	        		//SimpleDateFormat sdfClient = new SimpleDateFormat(userLocalDateFormat);
	        		//Date clientDate = sdfClient.parse(jp.getText().toString());
	        	 DateTimeFormatter clientDateformatter = DateTimeFormatter.ofPattern(userLocalDateFormat+" z"); // change here to actual date format from client.
	           	 ZonedDateTime clientZonedDateTime = ZonedDateTime.parse((jp.getText().toString()+" "+userTimeZone), clientDateformatter);  // change to actual timezone from client
	           	 ZonedDateTime  utcZonedDateTime = clientZonedDateTime.withZoneSameInstant(ZoneId.of("UTC"));// we will store UTC date in Database
	        	 DateTimeFormatter databaseDateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	        	 utcZonedDateTime.format(databaseDateformatter);
	        	 
	        	 // we have to usee Timestamp(log millis) cosntructir. But the ZonedDateTime does not have a method to get the milliseconds from its current date
	        	 // The Timestamp constructor always adds up 1900 to the year that is passed into below constructor. Not sure why. we need to completely avaoid using below constructor
	        	 // for now we tweak the year by subtracting 1900.
	        	 
	        	 returnTime = new Timestamp(utcZonedDateTime.getYear()-1900,utcZonedDateTime.getMonthValue()-1,utcZonedDateTime.getDayOfMonth(),
	        			 utcZonedDateTime.getHour(),utcZonedDateTime.getMinute(),utcZonedDateTime.getSecond(),utcZonedDateTime.getNano());
	        	
	        		//returnTime=new Timestamp(clientDate.getTime());
	           }
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	        
	       
        
            
        return returnTime;
    }

   
}
