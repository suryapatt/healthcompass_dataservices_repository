package com.healthcompass.data.util;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.healthcompass.data.CustomFilter;
import com.healthcompass.data.model.TimeZoneInformation;
import com.healthcompass.data.service.TimeZoneService;

@Component
public class CustomDateSerializer extends JsonSerializer<Timestamp> {


private  static TimeZoneService timeZoneService;
	


    

	
    public CustomDateSerializer() {
        
    }
    
    @Autowired
    public CustomDateSerializer(TimeZoneService timeZoneService) {
      
        this.timeZoneService= timeZoneService;
    }

   

    @Override
    public void serialize(Timestamp date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    	 String convertedDate="";
    	 if(date==null) {
    		 jsonGenerator.writeString(convertedDate);
    		 return;
    	 }
    	 try {
    		 String userTimeZone = CustomFilter.getUserTimeZone().get();
        	 TimeZoneInformation timeZoneDateInfo = timeZoneService.get(userTimeZone);
    	     String userLocalDateFormat = timeZoneDateInfo.getTimeZoneDateTimeFormat();
        	 DateFormat sdf = new SimpleDateFormat(userLocalDateFormat); // change this to actual client date format. for now IST
    		 DateTimeFormatter zoneConvertDateformatter = DateTimeFormatter.ofPattern(userLocalDateFormat+" z"); // change this to actual client date format. for now IST
    		 ZonedDateTime dbZonedDateTime = ZonedDateTime.parse(sdf.format(date.getTime())+" UTC", zoneConvertDateformatter); // In db, this will be in UTC zone
            ZonedDateTime  clientZonedDateTime = dbZonedDateTime.withZoneSameInstant(ZoneId.of(userTimeZone)); //  replace the zone with actual client zone. For nopw we use IST
            DateTimeFormatter clientDateFormatter = DateTimeFormatter.ofPattern(userLocalDateFormat); // change this as per client timezone
    	   
    	    convertedDate = clientZonedDateTime.format(clientDateFormatter);
           
            
           // dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
           
            jsonGenerator.writeString(convertedDate);
    	 }catch(Exception e) {
    		 System.out.println("Error during date serialization ");
    		 e.printStackTrace();
    	 }
    	 
    }
}
