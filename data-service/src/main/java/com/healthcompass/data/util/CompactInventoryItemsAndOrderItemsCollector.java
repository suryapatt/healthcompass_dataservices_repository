package com.healthcompass.data.util;

import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class CompactInventoryItemsAndOrderItemsCollector {
	
	UUID inventoryId;
	String batchNumber;
	Integer productId;
	Integer productAvailableUnits;
	Double productUnitRate;
	Integer orderItemQuantity;
	
	
	public Integer getOrderItemQuantity() {
		return orderItemQuantity;
	}
	public void setOrderItemQuantity(Integer orderItemQuantity) {
		this.orderItemQuantity = orderItemQuantity;
	}
	Double inventoryValue; //calculated productAvailableUnits*unitRate
		
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getProductAvailableUnits() {
		return productAvailableUnits;
	}
	public void setProductAvailableUnits(Integer productAvailableUnits) {
		this.productAvailableUnits = productAvailableUnits;
	}
	public Double getProductUnitRate() {
		return productUnitRate;
	}
	public void setProductUnitRate(Double productUnitRate) {
		this.productUnitRate = productUnitRate;
	}
		
		
		public UUID getInventoryId() {
			return inventoryId;
		}
		public void setInventoryId(UUID inventoryId) {
			this.inventoryId = inventoryId;
		}
		public String getBatchNumber() {
			return batchNumber;
		}
		public void setBatchNumber(String batchNumber) {
			this.batchNumber = batchNumber;
		}
		public Double getInventoryValue() {
			return inventoryValue;
		}
		public void setInventoryValue(Double inventoryValue) {
			this.inventoryValue = inventoryValue;
		}
		
		
		


}
