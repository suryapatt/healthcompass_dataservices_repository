package com.healthcompass.data.util;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

public class HealthCompassValidationUtils {
	public static void rejectIfEmptyOrWhitespace(
			Errors errors, Map<String,Object> errorsMap,String field, String errorCode, String defaultMessage) {
		
		Object value = errors.getFieldValue(field);
		if (value == null || !StringUtils.hasText(value.toString())) {
			errorsMap.put(field,defaultMessage);
			markError(errorsMap);
		}
		
	}
	
	public static void markError(
			Map<String,Object> errorsMap) {
		errorsMap.put("errors", "true");
		
		
	}
	
	public static boolean checkAndclearErrorMarker(
			Map<String,Object> errorsMap, boolean hasErrorsInParent) {
		boolean hasErrors=false;
		boolean hasErrorsHere=false;
		if(errorsMap.containsKey("errors")) {
			errorsMap.remove("errors");
			hasErrorsHere=true;
			
			
		}
		hasErrors = hasErrorsInParent ? hasErrorsInParent : hasErrorsHere;
		for(String key : errorsMap.keySet()) {
			if(errorsMap.get(key) instanceof String) continue;
			if(errorsMap.get(key) instanceof Map) {
				hasErrors=checkAndclearErrorMarker((Map) errorsMap.get(key),hasErrors);
			}
			if(errorsMap.get(key) instanceof ArrayList) {
				ArrayList<Map<String,Object>> list = (ArrayList<Map<String,Object>>) errorsMap.get(key);
					
				for(Map<String,Object> map : list) {
					hasErrors=checkAndclearErrorMarker(map,hasErrors);
				}
				
			}
		}
		
		return hasErrors;
		
		
	}
}
