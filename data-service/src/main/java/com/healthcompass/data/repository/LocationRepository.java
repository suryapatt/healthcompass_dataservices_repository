package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface LocationRepository extends PagingAndSortingRepository<Location, UUID>, QuerydslPredicateExecutor<Location> {
 
}
