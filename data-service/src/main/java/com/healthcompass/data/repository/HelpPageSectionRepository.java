package com.healthcompass.data.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.HelpPageSection;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface HelpPageSectionRepository extends PagingAndSortingRepository<HelpPageSection, String>, QuerydslPredicateExecutor<HelpPageSection> {
 
	 String getSectionNamesQuery = "select distinct id,section_name,sno from project_asclepius.section order by sno asc";
	 @Query(value=getSectionNamesQuery,nativeQuery = true)
	 List<Object[]> getAvailableSectionNames() ;
	 
	
	 String getAvailableSectionLanguages = "select distinct secdef.language as languageCode, lang.language as language from project_asclepius.section_definition secdef \r\n" + 
		 		"	 inner join project_asclepius.country_language lang on lower(secdef.language)=lower(lang.language_code)";
		 @Query(value=getAvailableSectionLanguages,nativeQuery = true)
		 List<Object[]> getAvailableSectionLanguages() ;
		 
		 @Query("select section from HelpPageSection section where section.id in ?1 Order by section.serialNumber Asc")
		 List<HelpPageSection> findAllByIdOrderBySerialNumberAsc(Set<String> helpPageSectionIds);
		
}
