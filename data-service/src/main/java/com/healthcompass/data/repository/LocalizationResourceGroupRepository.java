package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface LocalizationResourceGroupRepository extends PagingAndSortingRepository<LocalizationResourceGroup, String>, QuerydslPredicateExecutor<LocalizationResourceGroup> {
 
	 
	 String checkResourceGroupExistsQuery = "select count(*) from project_asclepius.resource_group where  resource_group_code=:resourceGroupCode";
	 @Query(value=checkResourceGroupExistsQuery,nativeQuery = true)
	 Integer checkResourceGroupExistsQuery(@Param("resourceGroupCode") String resourceGroupCode) ;
	 
}
