package com.healthcompass.data.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;


public interface InventorySummaryRepository extends PagingAndSortingRepository<InventorySummary, UUID>, QuerydslPredicateExecutor<InventorySummary>{
 
	@Query("SELECT invSummary FROM InventorySummary invSummary WHERE  invSummary.organization.id = :organizationId and invSummary.location.id = :locationId  and invSummary.product.id in :productIdSet")
		 List<InventorySummary> getInventorySummaryForProductSet(@Param("organizationId") UUID organizationId,@Param("locationId") UUID locationId,@Param("productIdSet") Set<Integer> productIdSet);
	 
	 
}
	
