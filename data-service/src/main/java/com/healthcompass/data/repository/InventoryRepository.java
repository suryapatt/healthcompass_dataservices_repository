package com.healthcompass.data.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface InventoryRepository extends PagingAndSortingRepository<InventoryItem, UUID>, QuerydslPredicateExecutor<InventoryItem> {
     // we need to retireve UUID as Cast(id as varchar),  other wise we get "No Dialect mapping for JDBC type: 1111" error
	 String getLatestAvailableUnitsQuery = "select Cast(id as varchar) inventoryId,available_units from inventory.inventory where id in (:inventoryIdList) ";
	 @Query(value=getLatestAvailableUnitsQuery,nativeQuery = true)
	 List<Object[]> getLatestAvailableUnits(@Param("inventoryIdList") List<UUID> inventoryIdList) ;
	 
	
}
