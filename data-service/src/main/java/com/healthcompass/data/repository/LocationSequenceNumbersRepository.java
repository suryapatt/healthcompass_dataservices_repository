package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.LocationSequenceNumbers;
import com.healthcompass.data.model.LocationSequenceNumbersPK;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface LocationSequenceNumbersRepository extends PagingAndSortingRepository<LocationSequenceNumbers, LocationSequenceNumbersPK>, QuerydslPredicateExecutor<LocationSequenceNumbers> {
 
	 
}
