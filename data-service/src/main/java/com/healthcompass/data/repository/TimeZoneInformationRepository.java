package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.healthcompass.data.model.TimeZoneInformation;




public interface TimeZoneInformationRepository extends PagingAndSortingRepository<TimeZoneInformation, String>, QuerydslPredicateExecutor<TimeZoneInformation> {
 
}
