package com.healthcompass.data.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface HelpPageRepository extends PagingAndSortingRepository<HelpPage, String>, QuerydslPredicateExecutor<HelpPage> {
 
	 String getPageNamesQuery = "select distinct id,page_name,sno from project_asclepius.page order by sno asc";
	 @Query(value=getPageNamesQuery,nativeQuery = true)
	 List<Object[]> getAvailablePageNames() ;
	 
	 
	
	 String getAvailablePageLanguages = "select distinct pd.language as languageCode, lang.language as language from project_asclepius.page_definition pd\r\n" + 
	 		"	 inner join project_asclepius.country_language lang on lower(pd.language)=lower(lang.language_code)";
	 @Query(value=getAvailablePageLanguages,nativeQuery = true)
	 List<Object[]> getAvailablePageLanguages() ;
	
	 
}
