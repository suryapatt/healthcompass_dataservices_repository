package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface ServiceBillItemRepository extends PagingAndSortingRepository<ServiceBillItem, UUID>, QuerydslPredicateExecutor<ServiceBillItem> {

	 @Modifying
	 @Query("update ServiceBillItem sbi set sbi.active=false where serviceBill.id=:serviceBillId")
	 void deleteBillItem(@Param("serviceBillId") UUID serviceBillId);
	
}
