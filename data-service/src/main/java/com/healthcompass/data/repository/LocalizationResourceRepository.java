package com.healthcompass.data.repository;

import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface LocalizationResourceRepository extends PagingAndSortingRepository<LocalizationResource, String>, QuerydslPredicateExecutor<LocalizationResource> {
 
	 String getAllExistingResourceIdsForALanguage = "select resource_id from project_asclepius.resource where  resource_id=:resourceId and resource_category_id=:resourceCategoryId and language_id=:languageId";
	 @Query(value=getAllExistingResourceIdsForALanguage,nativeQuery = true)
	Set<String> getAllExistingResourceIdsForALanguage(@Param("resourceId") String resourceId,@Param("resourceCategoryId") String resourceCategoryId, @Param("languageId") Integer languageId) ;
}
