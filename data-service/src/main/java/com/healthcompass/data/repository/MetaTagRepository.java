package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.MetaTagAttributes;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface MetaTagRepository extends PagingAndSortingRepository<MetaTag, Integer>, QuerydslPredicateExecutor<MetaTag> {
 
	 
	 
	 
}
