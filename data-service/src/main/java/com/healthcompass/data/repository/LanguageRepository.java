package com.healthcompass.data.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.Language;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface LanguageRepository extends PagingAndSortingRepository<Language, BigInteger>, QuerydslPredicateExecutor<Language> {
     // we need to retireve UUID as Cast(id as varchar),  other wise we get "No Dialect mapping for JDBC type: 1111" error
	 String getLangaueCodeIdMap = "select language_code,id from project_asclepius.country_language";
	 @Query(value=getLangaueCodeIdMap,nativeQuery = true)
	 public List<Object[]> getLangaueCodeIdMap();
	 
	
}
