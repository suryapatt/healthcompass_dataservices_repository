package com.healthcompass.data.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface RateCardCategoryRepository extends PagingAndSortingRepository<RateCardCategory, Integer>, QuerydslPredicateExecutor<RateCardCategory> {
 
}
