package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;


public interface ValueSetTypeRepository extends PagingAndSortingRepository<ValueSetType, Integer>, QuerydslPredicateExecutor<ValueSetType>{
 
	
	
	
	 @Query("Select options from ValueSetType options where options.name=:optionName")
	 ValueSetType getOptionAndOptionValues(@Param("optionName") String optionName);
	
}

	

