package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Supplier;

public interface SupplierRepository extends PagingAndSortingRepository<Supplier, UUID>, QuerydslPredicateExecutor<Supplier> {
 
	 String nextSequenceValueQuery = "select nextval('inventory.supplier_number_seq')";
	 @Query(value=nextSequenceValueQuery,nativeQuery = true)
	 Integer generateNextSupplierNumber(); 
	 
	 String checkIfProductExistsInPOForthisSupplier = "select count(*) from inventory.order where supplier_id=:supplierId ";
	 @Query(value=checkIfProductExistsInPOForthisSupplier,nativeQuery = true)
	 Integer checkIfProductExistsInInventoryForthisSupplier(@Param("supplierId") UUID supplierId) ;
	 
}
