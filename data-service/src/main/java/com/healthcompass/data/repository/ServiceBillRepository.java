package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;


public interface ServiceBillRepository extends PagingAndSortingRepository<ServiceBill, UUID>, QuerydslPredicateExecutor<ServiceBill> {
 
	 @Modifying
	 @Query("update ServiceBill sb set sb.active=false where id=:serviceBillId")
	 void deleteBill(@Param("serviceBillId") UUID serviceBillId);
	 	 
	 String checkUniqueBillConstQuery = "select count(*) from billing.service_bill where  patient_id=:patientId and encounter_id=:encounterId and practitioner_role_id=:practitionerId and active=true and bill_number is not null ";
	 @Query(value=checkUniqueBillConstQuery,nativeQuery = true)
	 Integer checkIfBillExists(@Param("patientId") UUID patientId,@Param("encounterId") UUID encounterId,@Param("practitionerId") UUID practitionerId) ;
	 
			
	 
}
