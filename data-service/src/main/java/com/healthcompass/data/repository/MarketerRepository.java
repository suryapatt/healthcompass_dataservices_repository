package com.healthcompass.data.repository;


import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Marketer;


public interface MarketerRepository extends PagingAndSortingRepository<Marketer, Integer>, QuerydslPredicateExecutor<Marketer> {
	
	 String checkIfProductExistsAndActiveForthisMarketer = "select count(*) from inventory.product where marketer_id=:marketerId and active=true";
	 @Query(value=checkIfProductExistsAndActiveForthisMarketer,nativeQuery = true)
	 Integer checkIfProductExists(@Param("marketerId") Integer marketerId) ;
	 
	 String checkIfMarketerExistQuery = "select count(*) from inventory.marketer where  name=:name";
	 @Query(value=checkIfMarketerExistQuery,nativeQuery = true)
	 Integer checkIfMarketerExists(@Param("name") String name) ;
 
}
