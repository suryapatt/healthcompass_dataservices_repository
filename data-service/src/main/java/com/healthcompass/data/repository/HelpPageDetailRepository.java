package com.healthcompass.data.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;


public interface HelpPageDetailRepository extends PagingAndSortingRepository<HelpPage, String>, QuerydslPredicateExecutor<HelpPage> {
 
	 String getPageIdsQuery = "select distinct page_id from project_asclepius.page_detail where section_id in (:sectionIdList) ";
	 @Query(value=getPageIdsQuery,nativeQuery = true)
	 Set<String> getDistinctPageIds(@Param("sectionIdList") Set<String> sectionIdList) ;
	
	 
	 
	 
}
