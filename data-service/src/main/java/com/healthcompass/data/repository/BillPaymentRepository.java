package com.healthcompass.data.repository;


import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


import org.springframework.jdbc.core.JdbcTemplate;

public interface BillPaymentRepository extends PagingAndSortingRepository<BillPayment, Integer>, QuerydslPredicateExecutor<BillPayment> {
	
	
	 @Modifying
	 @Query("update BillPayment bp set bp.active=false where serviceBill.billNumber=:serviceBillNumber")
	 void deleteBillPayments(@Param("serviceBillNumber") String serviceBillNumber);

	
}
