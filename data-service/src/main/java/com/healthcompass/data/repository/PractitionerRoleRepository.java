package com.healthcompass.data.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface PractitionerRoleRepository extends PagingAndSortingRepository<PractitionerRole, UUID>, QuerydslPredicateExecutor<PractitionerRole> {
	
	 String getAllDoctorRoleIdsForALocation = "select Cast(id as varchar) from project_asclepius.practitioner_role where code=:doctorCode and location=:locationId and active=true";
	 @Query(value=getAllDoctorRoleIdsForALocation,nativeQuery = true)
	 List<Object> getAllDoctorRoleIdsForALocation(@Param("doctorCode") Integer doctorCode,@Param("locationId") UUID locationId);
	 
	 
	 String getAllDoctorRoleIdsForOrganization = "select Cast(id as varchar) from project_asclepius.practitioner_role where code=:doctorCode and organization=:organizationId and active=true";
	 @Query(value=getAllDoctorRoleIdsForOrganization,nativeQuery = true)
	 List<Object> getAllDoctorRoleIdsForOrganization(@Param("doctorCode") Integer doctorCode,@Param("organizationId") UUID organizationId);
	 
	 
}
