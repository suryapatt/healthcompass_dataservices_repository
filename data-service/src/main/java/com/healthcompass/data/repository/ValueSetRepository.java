package com.healthcompass.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;


public interface ValueSetRepository extends PagingAndSortingRepository<ValueSet, Integer>, QuerydslPredicateExecutor<ValueSet>{
 
	 @Query("Select val from ValueSet val where val.code like " + "'issued%'")
	 ValueSet getGenerateBillStatusCode();
	 
	 @Query("Select val from ValueSet val where val.code = '" + "draft" +"'")
	 ValueSet getDraftBillStatusCode();
	 
	 @Query("Select val from ValueSet val where val.code like " + "'partially paid%'")
	 ValueSet getBillPaymentPartiallyPaidStatusCode();
	 
	 @Query("Select val from ValueSet val where val.code like " + "'paid%'")
	 ValueSet getBillPaymentPaidStatusCode();
	 
	 @Query("Select val from ValueSet val where val.code like " + "'not paid%'")
	 ValueSet getBillPaymentNotPaidStatusCode();
	 

	 @Query("Select val from ValueSet val where val.code like 'not paid%' OR  val.code like 'paid%'  OR val.code like 'partially paid%'")
	 List<ValueSet> getAllBillPaymentNotPaidStatusCode();
}
