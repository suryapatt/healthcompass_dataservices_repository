package com.healthcompass.data.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.Supplier;


public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>, QuerydslPredicateExecutor<Product> {
 
	 String checkIfProductExistsInInventory = "select count(*) from inventory.inventory where product_id=:productId";
	 @Query(value=checkIfProductExistsInInventory,nativeQuery = true)
	 Integer checkIfProductExistsInInventory(@Param("productId") Integer productId) ;
	 
	 String checkIfProductExistsPOSOOrders = "select count(*) from inventory.order_items where product_id=:productId";
	 @Query(value=checkIfProductExistsPOSOOrders,nativeQuery = true)
	 Integer checkIfProductExistsInPOSOOrders(@Param("productId") Integer productId) ;
	 
	 @Modifying
	 @Query("update Product prod set prod.active=false where prod.id=:productId")
	 void deleteProduct(@Param("productId") Integer productId);
	 
}
