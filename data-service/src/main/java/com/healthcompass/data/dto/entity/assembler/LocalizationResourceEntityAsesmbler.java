package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.HelpPageDefinition;
import com.healthcompass.data.model.HelpPageDetail;
import com.healthcompass.data.model.HelpPageDetailDefinition;
import com.healthcompass.data.model.HelpPageSection;
import com.healthcompass.data.model.HelpPageSectionDefinition;
import com.healthcompass.data.model.HelpPageSectionDetail;
import com.healthcompass.data.model.HelpPageSectionDetailDefinition;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceField;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.LocalizationResourceSection;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class LocalizationResourceEntityAsesmbler {
	
	
	
	private static class ItemAction{
		
		
		
		enum ACTION {REMOVE_FROM_PARENT,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		HelpPageDefinition updatedPageDefinitionItem;
		HelpPageDetail updatedPageDetailItem;
		HelpPageDetailDefinition updatedPageDetailDefinitionItem;
		
		HelpPageSectionDefinition updatedSectionDefinitionItem;
		HelpPageSectionDetail updatedSectionDetailItem;
		HelpPageSectionDetailDefinition updatedSectionDetailDefinitionItem;
		
		
		ACTION action;
		
		ItemAction(){
			
		}
		
		
	} // end inner class
	

	 public HelpPage toHelpPageEntity(HelpPage source, HelpPage target) 
	    {
	  
		
		    target.setId(source.getId());
		    target.setPadded(source.getPadded());
		    target.setPageName(source.getPageName());
		    target.setSerialNumber(source.getSerialNumber());
		    target.setPadded(source.getPadded());
		   
	    	ArrayList<HelpPageDefinition> newPageDefinitionList = new ArrayList<HelpPageDefinition>();
	    	ArrayList<HelpPageDetail> newPageDetailList = new ArrayList<HelpPageDetail>();
	    	
	    	for(HelpPageDefinition pageDefinition : source.getPageDefinitions()) {
	    		
	    		ItemAction actionOnPageDefinition = updateExistingPageDefinition(target, pageDefinition.getId(), pageDefinition);
	    	
	    		if(actionOnPageDefinition.action == actionOnPageDefinition.action.DO_NOTHING) {
	    			continue;
	    		}
	    		
	    		if(actionOnPageDefinition.action == actionOnPageDefinition.action.UPDATE_EXISTING) {
	    			HelpPageDefinition updatedItem = actionOnPageDefinition.updatedPageDefinitionItem;
	    			//createResources(category,newCategory);
	    			
	    			continue;
	    			
	    		}
	    		if(actionOnPageDefinition.action == actionOnPageDefinition.action.CREATE_NEW) {
	    			
	    			HelpPageDefinition updatedItem = actionOnPageDefinition.updatedPageDefinitionItem;
	    			
	    			HelpPageDefinition newPageDefinition = new HelpPageDefinition();
	    			
	    			boolean isNewCreated = updatePageDefinition(newPageDefinition, pageDefinition);
	    			if(isNewCreated) {
	    				
	    				newPageDefinitionList.add(newPageDefinition);
	    			}
	    			//createResources(category,newCategory);
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	target.getPageDefinitions().addAll(newPageDefinitionList);
    		
	    	for(HelpPageDetail pageDetail : source.getDetails()) {
	    		
	    		ItemAction actionOnPageDetail = updateExistingPageDetail(target, pageDetail.getId(), pageDetail);
	    	
	    		if(actionOnPageDetail.action == actionOnPageDetail.action.DO_NOTHING) {
	    			continue;
	    		}
	    		
	    		if(actionOnPageDetail.action == actionOnPageDetail.action.UPDATE_EXISTING) {
	    			HelpPageDetail updatedItem = actionOnPageDetail.updatedPageDetailItem;
	    			createPageDetailDefinitions(pageDetail,updatedItem);
	    			
	    			continue;
	    			
	    		}
	    		if(actionOnPageDetail.action == actionOnPageDetail.action.CREATE_NEW) {
	    			
	    			HelpPageDetail updatedItem = actionOnPageDetail.updatedPageDetailItem;
	    			
	    			HelpPageDetail newPageDetail = new HelpPageDetail();
	    			newPageDetail.setDetailDefinitions(new ArrayList<HelpPageDetailDefinition>());
	    			boolean isNewCreated = updatePageDetail(newPageDetail, pageDetail);
	    			if(isNewCreated) {
	    				
	    				newPageDetailList.add(newPageDetail);
	    			}
	    			createPageDetailDefinitions(pageDetail,newPageDetail);
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	target.getDetails().addAll(newPageDetailList);
    		
	        return target;
	   
	    }
	 
	 public HelpPageSection toHelpSectionEntity(HelpPageSection source, HelpPageSection target) 
	    {
	  
		
		    target.setId(source.getId());
		    target.setPadded(source.getPadded());
		    target.setSectionName(source.getSectionName());
		    target.setSerialNumber(source.getSerialNumber());
		    target.setPadded(source.getPadded());
		   
	    	ArrayList<HelpPageSectionDefinition> newSectionDefinitionList = new ArrayList<HelpPageSectionDefinition>();
	    	ArrayList<HelpPageSectionDetail> newSectionDetailList = new ArrayList<HelpPageSectionDetail>();
	    	
	    	for(HelpPageSectionDefinition sectionDefinition : source.getSectionDefinitions()) {
	    		
	    		ItemAction actionOnSectionDefinition = updateExistingSectionDefinition(target, sectionDefinition.getId(), sectionDefinition);
	    	
	    		if(actionOnSectionDefinition.action == actionOnSectionDefinition.action.DO_NOTHING) {
	    			continue;
	    		}
	    		
	    		if(actionOnSectionDefinition.action == actionOnSectionDefinition.action.UPDATE_EXISTING) {
	    			HelpPageSectionDefinition updatedItem = actionOnSectionDefinition.updatedSectionDefinitionItem;
	    			//createResources(category,newCategory);
	    			
	    			continue;
	    			
	    		}
	    		if(actionOnSectionDefinition.action == actionOnSectionDefinition.action.CREATE_NEW) {
	    			
	    			HelpPageSectionDefinition updatedItem = actionOnSectionDefinition.updatedSectionDefinitionItem;
	    			
	    			HelpPageSectionDefinition newSectionDefinition = new HelpPageSectionDefinition();
	    			
	    			boolean isNewCreated = updateSectionDefinition(newSectionDefinition, sectionDefinition);
	    			if(isNewCreated) {
	    				
	    				newSectionDefinitionList.add(newSectionDefinition);
	    			}
	    			//createResources(category,newCategory);
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	target.getSectionDefinitions().addAll(newSectionDefinitionList);
 		
	    	for(HelpPageSectionDetail sectionDetail : source.getSectionDetail()) {
	    		
	    		ItemAction actionOnSectionDetail = updateExistingSectionDetail(target, sectionDetail.getId(), sectionDetail);
	    	
	    		if(actionOnSectionDetail.action == actionOnSectionDetail.action.DO_NOTHING) {
	    			continue;
	    		}
	    		
	    		if(actionOnSectionDetail.action == actionOnSectionDetail.action.UPDATE_EXISTING) {
	    			HelpPageSectionDetail updatedItem = actionOnSectionDetail.updatedSectionDetailItem;
	    			createSectionDetailDefinitions(sectionDetail,updatedItem);
	    			
	    			continue;
	    			
	    		}
	    		if(actionOnSectionDetail.action == actionOnSectionDetail.action.CREATE_NEW) {
	    			
	    			HelpPageSectionDetail updatedItem = actionOnSectionDetail.updatedSectionDetailItem;
	    			
	    			HelpPageSectionDetail newSectionDetail = new HelpPageSectionDetail();
	    			newSectionDetail.setSectionDetailDefinitions(new ArrayList<HelpPageSectionDetailDefinition>());
	    			boolean isNewCreated = updateSectionDetail(newSectionDetail, sectionDetail);
	    			if(isNewCreated) {
	    				
	    				newSectionDetailList.add(newSectionDetail);
	    			}
	    			createSectionDetailDefinitions(sectionDetail,newSectionDetail);
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	target.getSectionDetail().addAll(newSectionDetailList);
 		
	        return target;
	   
	    }
	 
	 
	 public ItemAction updateExistingPageDefinition(HelpPage target, String pageDefinitionId, HelpPageDefinition sourcePageDefinition) {
		 
		 ItemAction pageDefinitionItemAction = new ItemAction();
		 
		    if(target == null || sourcePageDefinition == null || target.getPageDefinitions() == null) {
		    	pageDefinitionItemAction.action = pageDefinitionItemAction.action.DO_NOTHING;
		    	pageDefinitionItemAction.updatedPageDefinitionItem = null;
		    	return pageDefinitionItemAction;
		    }
		        
		  
		        
		   
		    
		    if(pageDefinitionId== null) {
		    	
		    	pageDefinitionItemAction.action = pageDefinitionItemAction.action.CREATE_NEW;
		    	pageDefinitionItemAction.updatedPageDefinitionItem = null;
		    	return pageDefinitionItemAction;
		    		
		    		
		    }

		    Optional<HelpPageDefinition> optPageDefinition = target.getPageDefinitions().stream()
		.filter(pageDefinition -> pageDefinition.getId().equals(pageDefinitionId)).findFirst();

		    if(!optPageDefinition.isPresent())  {
		    	pageDefinitionItemAction.action = pageDefinitionItemAction.action.CREATE_NEW;
		    	pageDefinitionItemAction.updatedPageDefinitionItem = null;
		    	return pageDefinitionItemAction;
		    		
		    }
		    	

		    HelpPageDefinition targetPageDefinition = optPageDefinition.get();
		    		   

		    // now implement the update function
		    boolean status = updatePageDefinition(targetPageDefinition, sourcePageDefinition);
		    if(status) {
		    	
		    	pageDefinitionItemAction.action = pageDefinitionItemAction.action.UPDATE_EXISTING;
		    	pageDefinitionItemAction.updatedPageDefinitionItem = targetPageDefinition;
		    	return pageDefinitionItemAction;
		    	
		    	
		    	
		    }

		    pageDefinitionItemAction.action = pageDefinitionItemAction.action.DO_NOTHING;
		    pageDefinitionItemAction.updatedPageDefinitionItem = null;
	    	return pageDefinitionItemAction;
		}
	 
	 private boolean updatePageDefinition(HelpPageDefinition target, HelpPageDefinition source) {
		 
		target.setId(source.getId());
		target.setDisplayName(source.getDisplayName());
		target.setLanguage(source.getLanguage());
		target.setPage(source.getPage());
		target.setDescription(source.getDescription());
		
		
		return true;
		
 		
	 }
	 
	 
 public ItemAction updateExistingPageDetail(HelpPage target, String pageDetailId, HelpPageDetail sourcePageDetail) {
		 
		 ItemAction pageDetailItemAction = new ItemAction();
		 
		    if(target == null || sourcePageDetail == null || target.getDetails() == null) {
		    	pageDetailItemAction.action = pageDetailItemAction.action.DO_NOTHING;
		    	pageDetailItemAction.updatedPageDetailItem = null;
		    	return pageDetailItemAction;
		    }
		        
		 
		   
		    
		    if(pageDetailId== null) {
		    	
		    	pageDetailItemAction.action = pageDetailItemAction.action.CREATE_NEW;
		    	pageDetailItemAction.updatedPageDetailItem = null;
		    	return pageDetailItemAction;
		    		
		    		
		    }

		    Optional<HelpPageDetail> optPageDetail = target.getDetails().stream()
		.filter(pageDetail -> pageDetail.getId().equals(pageDetailId)).findFirst();

		    if(!optPageDetail.isPresent())  {
		    	pageDetailItemAction.action = pageDetailItemAction.action.CREATE_NEW;
		    	pageDetailItemAction.updatedPageDetailItem = null;
		    	return pageDetailItemAction;
		    		
		    }
		    	

		    HelpPageDetail targetPageDetail = optPageDetail.get();
		    		   

		    // now implement the update function
		    boolean status = updatePageDetail(targetPageDetail, sourcePageDetail);
		    if(status) {
		    	
		    	pageDetailItemAction.action = pageDetailItemAction.action.UPDATE_EXISTING;
		    	pageDetailItemAction.updatedPageDetailItem = targetPageDetail;
		    	return pageDetailItemAction;
		    	
		    	
		    	
		    }

		    pageDetailItemAction.action = pageDetailItemAction.action.DO_NOTHING;
	    	pageDetailItemAction.updatedPageDetailItem = null;
	    	return pageDetailItemAction;
		}
	 
	 private boolean updatePageDetail(HelpPageDetail target, HelpPageDetail source) {
		 
		target.setId(source.getId());
		target.setDataType(source.getDataType());
		target.setFieldName(source.getFieldName());
		target.setIsDisplay(source.getIsDisplay());
		target.setPaddedPosition(source.getPaddedPosition());
		target.setParentPage(source.getParentPage());
		target.setPageName(source.getPageName());
		target.setPosition(source.getPosition());
		target.setSectionId(source.getSectionId());
		target.setSectionName(source.getSectionName());
		
		
		
		return true;
		
 		
	 }
	 
public ItemAction updateExistingPageDetailDefinition(HelpPageDetail target, String pageDetailDefinitionId, HelpPageDetailDefinition sourcePageDetailDefinition) {
		 
		 ItemAction pageDetailDefinitionItemAction = new ItemAction();
		 
		    if(target == null || sourcePageDetailDefinition == null || target.getDetailDefinitions() == null) {
		    	pageDetailDefinitionItemAction.action = pageDetailDefinitionItemAction.action.DO_NOTHING;
		    	pageDetailDefinitionItemAction.updatedPageDetailDefinitionItem = null;
		    	return pageDetailDefinitionItemAction;
		    }
		        
		   
		        
		   
		    
		    if(pageDetailDefinitionId== null) {
		    	
		    	pageDetailDefinitionItemAction.action = pageDetailDefinitionItemAction.action.CREATE_NEW;
		    	pageDetailDefinitionItemAction.updatedPageDetailDefinitionItem = null;
		    	return pageDetailDefinitionItemAction;
		    		
		    		
		    }

		    Optional<HelpPageDetailDefinition> optPageDetailDefinition = target.getDetailDefinitions().stream()
		.filter(pageDetailDefinition -> pageDetailDefinition.getId().equals(pageDetailDefinitionId)).findFirst();

		    if(!optPageDetailDefinition.isPresent())  {
		    	pageDetailDefinitionItemAction.action = pageDetailDefinitionItemAction.action.CREATE_NEW;
		    	pageDetailDefinitionItemAction.updatedPageDetailDefinitionItem = null;
		    	return pageDetailDefinitionItemAction;
		    		
		    }
		    	

		    HelpPageDetailDefinition targetPageDetailDefinition = optPageDetailDefinition.get();
		    		   

		    // now implement the update function
		    boolean status = updatePageDetailDefinition(targetPageDetailDefinition, sourcePageDetailDefinition);
		    if(status) {
		    	
		    	pageDetailDefinitionItemAction.action = pageDetailDefinitionItemAction.action.UPDATE_EXISTING;
		    	pageDetailDefinitionItemAction.updatedPageDetailDefinitionItem = targetPageDetailDefinition;
		    	return pageDetailDefinitionItemAction;
		    		
		    	
		    	
		    	
		    }

		    pageDetailDefinitionItemAction.action = pageDetailDefinitionItemAction.action.DO_NOTHING;
	    	pageDetailDefinitionItemAction.updatedPageDetailDefinitionItem = null;
	    	return pageDetailDefinitionItemAction;
		}
	 
	 private boolean updatePageDetailDefinition(HelpPageDetailDefinition target, HelpPageDetailDefinition source) {
		 
		target.setId(source.getId());
		target.setDisplayName(source.getDisplayName());
		target.setLanguage(source.getLanguage());
		target.setPageDetail(source.getPageDetail());
		target.setDescription(source.getDescription());
		
		
		return true;
		
 		
	 }
	 
	 public HelpPageDetail createPageDetailDefinitions (HelpPageDetail sourcePageDetail, HelpPageDetail targetPageDetail) {
		 
		 ArrayList<HelpPageDetailDefinition> newPageDetailDefinitionList = new ArrayList<HelpPageDetailDefinition>();
	    	
	    	for(HelpPageDetailDefinition sourcePageDetailDefinition : sourcePageDetail.getDetailDefinitions()) {
	    		
	    		ItemAction actionOnPageDetailDefinition = updateExistingPageDetailDefinition(targetPageDetail, sourcePageDetailDefinition.getId(), sourcePageDetailDefinition);
	    	
	    		if(actionOnPageDetailDefinition.action == actionOnPageDetailDefinition.action.DO_NOTHING) {
	    			continue;
	    		}
	    		
	    		if(actionOnPageDetailDefinition.action == actionOnPageDetailDefinition.action.UPDATE_EXISTING) {
	    			HelpPageDetailDefinition updatedItem = actionOnPageDetailDefinition.updatedPageDetailDefinitionItem;
	    			
	    			
	    			continue;
	    			
	    		}
	    		if(actionOnPageDetailDefinition.action == actionOnPageDetailDefinition.action.CREATE_NEW) {
	    			HelpPageDetailDefinition updatedItem = actionOnPageDetailDefinition.updatedPageDetailDefinitionItem;
	    			
	    			HelpPageDetailDefinition newPageDetailDefinition = new HelpPageDetailDefinition();
	    			
	    			boolean isNewCreated = updatePageDetailDefinition(newPageDetailDefinition, sourcePageDetailDefinition);
	    			if(isNewCreated) {
	    				
	    				newPageDetailDefinitionList.add(newPageDetailDefinition);
	    			}
	    			
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	targetPageDetail.getDetailDefinitions().addAll(newPageDetailDefinitionList);
 		
 		
	        return targetPageDetail;
	 }


	 public ItemAction updateExistingSectionDefinition(HelpPageSection target, String sectionDefinitionId, HelpPageSectionDefinition sourceSectionDefinition) {
	 
	 ItemAction sectionDefinitionItemAction = new ItemAction();
	 
	    if(target == null || sourceSectionDefinition == null || target.getSectionDefinitions() == null) {
	    	sectionDefinitionItemAction.action = sectionDefinitionItemAction.action.DO_NOTHING;
	    	sectionDefinitionItemAction.updatedSectionDefinitionItem = null;
	    	return sectionDefinitionItemAction;
	    }
	        
	  
	        
	   
	    
	    if(sectionDefinitionId== null) {
	    	
	    	sectionDefinitionItemAction.action = sectionDefinitionItemAction.action.CREATE_NEW;
	    	sectionDefinitionItemAction.updatedSectionDefinitionItem = null;
	    	return sectionDefinitionItemAction;
	    		
	    		
	    }

	    Optional<HelpPageSectionDefinition> optSectionDefinition = target.getSectionDefinitions().stream()
	.filter(sectionDefinition -> sectionDefinition.getId().equals(sectionDefinitionId)).findFirst();

	    if(!optSectionDefinition.isPresent())  {
	    	sectionDefinitionItemAction.action = sectionDefinitionItemAction.action.CREATE_NEW;
	    	sectionDefinitionItemAction.updatedSectionDefinitionItem = null;
	    	return sectionDefinitionItemAction;
	    		
	    }
	    	

	    HelpPageSectionDefinition targetSectionDefinition = optSectionDefinition.get();
	    		   

	    // now implement the update function
	    boolean status = updateSectionDefinition(targetSectionDefinition, sourceSectionDefinition);
	    if(status) {
	    	
	    	sectionDefinitionItemAction.action = sectionDefinitionItemAction.action.UPDATE_EXISTING;
	    	sectionDefinitionItemAction.updatedSectionDefinitionItem = targetSectionDefinition;
	    	return sectionDefinitionItemAction;
	    	
	    	
	    	
	    }

	    sectionDefinitionItemAction.action = sectionDefinitionItemAction.action.DO_NOTHING;
	    sectionDefinitionItemAction.updatedSectionDefinitionItem = null;
   	    return sectionDefinitionItemAction;
	}

	 private boolean updateSectionDefinition(HelpPageSectionDefinition target, HelpPageSectionDefinition source) {
	 
		target.setId(source.getId());
		target.setDisplayName(source.getDisplayName());
		target.setLanguage(source.getLanguage());
		target.setSection(source.getSection());
		target.setDescription(source.getDescription());
		
		
		return true;
	
	
	 }


public ItemAction updateExistingSectionDetail(HelpPageSection target, String sectionDetailId, HelpPageSectionDetail sourceSectionDetail) {
	 
	 ItemAction sectionDetailItemAction = new ItemAction();
	 
	    if(target == null || sourceSectionDetail == null || target.getSectionDetail() == null) {
	    	sectionDetailItemAction.action = sectionDetailItemAction.action.DO_NOTHING;
	    	sectionDetailItemAction.updatedSectionDetailItem = null;
	    	return sectionDetailItemAction;
	    }
	        
	 
	   
	    
	    if(sectionDetailId== null) {
	    	
	    	sectionDetailItemAction.action = sectionDetailItemAction.action.CREATE_NEW;
	    	sectionDetailItemAction.updatedSectionDetailItem = null;
	    	return sectionDetailItemAction;
	    		
	    		
	    }

	    Optional<HelpPageSectionDetail> optSectionDetail = target.getSectionDetail().stream()
	.filter(sectionDetail -> sectionDetail.getId().equals(sectionDetailId)).findFirst();

	    if(!optSectionDetail.isPresent())  {
	    	sectionDetailItemAction.action = sectionDetailItemAction.action.CREATE_NEW;
	    	sectionDetailItemAction.updatedSectionDetailItem = null;
	    	return sectionDetailItemAction;
	    		
	    }
	    	

	    HelpPageSectionDetail targetSectionDetail = optSectionDetail.get();
	    		   

	    // now implement the update function
	    boolean status = updateSectionDetail(targetSectionDetail, sourceSectionDetail);
	    if(status) {
	    	
	    	sectionDetailItemAction.action = sectionDetailItemAction.action.UPDATE_EXISTING;
	    	sectionDetailItemAction.updatedSectionDetailItem = targetSectionDetail;
	    	return sectionDetailItemAction;
	    	
	    	
	    	
	    }

	    sectionDetailItemAction.action = sectionDetailItemAction.action.DO_NOTHING;
    	sectionDetailItemAction.updatedSectionDetailItem = null;
    	return sectionDetailItemAction;
	}

private boolean updateSectionDetail(HelpPageSectionDetail target, HelpPageSectionDetail source) {
	 
	target.setId(source.getId());
	target.setSelfReferrence(source.getSelfReferrence());
	target.setSelfReferrenceId(source.getSelfReferrenceId());
	target.setFieldName(source.getFieldName());
	target.setIsDisplay(source.getIsDisplay());
	target.setPaddedPosition(source.getPaddedPosition());
	target.setParentSection(source.getParentSection());
	target.setSectionName(source.getSectionName());
	target.setPosition(source.getPosition());
	
	
	
	
	return true;
	
	
}

public ItemAction updateExistingSectionDetailDefinition(HelpPageSectionDetail target, String sectioDetailDefinitionId, HelpPageSectionDetailDefinition sourceSectionDetailDefinition) {
	 
	 ItemAction sectionDetailDefinitionItemAction = new ItemAction();
	 
	    if(target == null || sourceSectionDetailDefinition == null || target.getSectionDetailDefinitions() == null) {
	    	sectionDetailDefinitionItemAction.action = sectionDetailDefinitionItemAction.action.DO_NOTHING;
	    	sectionDetailDefinitionItemAction.updatedSectionDetailDefinitionItem = null;
	    	return sectionDetailDefinitionItemAction;
	    }
	        
	   
	        
	   
	    
	    if(sectioDetailDefinitionId== null) {
	    	
	    	sectionDetailDefinitionItemAction.action = sectionDetailDefinitionItemAction.action.CREATE_NEW;
	    	sectionDetailDefinitionItemAction.updatedSectionDetailDefinitionItem = null;
	    	return sectionDetailDefinitionItemAction;
	    		
	    		
	    }

	    Optional<HelpPageSectionDetailDefinition> optSectionDetailDefinition = target.getSectionDetailDefinitions().stream()
	.filter(sectionDetailDefinition -> sectionDetailDefinition.getId().equals(sectioDetailDefinitionId)).findFirst();

	    if(!optSectionDetailDefinition.isPresent())  {
	    	
	    	sectionDetailDefinitionItemAction.action = sectionDetailDefinitionItemAction.action.CREATE_NEW;
	    	sectionDetailDefinitionItemAction.updatedSectionDetailDefinitionItem = null;
	    	return sectionDetailDefinitionItemAction;
	    		
	    }
	    	

	    HelpPageSectionDetailDefinition targetSectionDetailDefinition = optSectionDetailDefinition.get();
	    		   

	    // now implement the update function
	    boolean status = updateSectionDetailDefinition(targetSectionDetailDefinition, sourceSectionDetailDefinition);
	    if(status) {
	    	
	    	sectionDetailDefinitionItemAction.action = sectionDetailDefinitionItemAction.action.UPDATE_EXISTING;
	    	sectionDetailDefinitionItemAction.updatedSectionDetailDefinitionItem = targetSectionDetailDefinition;
	    	return sectionDetailDefinitionItemAction;
	    		
	    	
	    	
	    	
	    }

	    sectionDetailDefinitionItemAction.action = sectionDetailDefinitionItemAction.action.DO_NOTHING;
    	sectionDetailDefinitionItemAction.updatedSectionDetailDefinitionItem = null;
    	return sectionDetailDefinitionItemAction;
	}

	private boolean updateSectionDetailDefinition(HelpPageSectionDetailDefinition target, HelpPageSectionDetailDefinition source) {
	 
		target.setId(source.getId());
		target.setDisplayName(source.getDisplayName());
		target.setLanguage(source.getLanguage());
		target.setSectionDetail(source.getSectionDetail());
		target.setDescription(source.getDescription());
		
	
		return true;
	
	
}

public HelpPageSectionDetail createSectionDetailDefinitions (HelpPageSectionDetail sourceSectionDetail, HelpPageSectionDetail targetSectionDetail) {
	 
	 ArrayList<HelpPageSectionDetailDefinition> newSectionDetailDefinitionList = new ArrayList<HelpPageSectionDetailDefinition>();
   	
   	for(HelpPageSectionDetailDefinition sourceSectionDetailDefinition : sourceSectionDetail.getSectionDetailDefinitions()) {
   		
   		ItemAction actionOnSectionDetailDefinition = updateExistingSectionDetailDefinition(targetSectionDetail, sourceSectionDetailDefinition.getId(), sourceSectionDetailDefinition);
   	
   		if(actionOnSectionDetailDefinition.action == actionOnSectionDetailDefinition.action.DO_NOTHING) {
   			continue;
   		}
   		
   		if(actionOnSectionDetailDefinition.action == actionOnSectionDetailDefinition.action.UPDATE_EXISTING) {
   			HelpPageSectionDetailDefinition updatedItem = actionOnSectionDetailDefinition.updatedSectionDetailDefinitionItem;
   			
   			
   			continue;
   			
   		}
   		if(actionOnSectionDetailDefinition.action == actionOnSectionDetailDefinition.action.CREATE_NEW) {
   			HelpPageSectionDetailDefinition updatedItem = actionOnSectionDetailDefinition.updatedSectionDetailDefinitionItem;
   			
   			HelpPageSectionDetailDefinition newSectionDetailDefinition = new HelpPageSectionDetailDefinition();
   			
   			boolean isNewCreated = updateSectionDetailDefinition(newSectionDetailDefinition, sourceSectionDetailDefinition);
   			if(isNewCreated) {
   				
   				newSectionDetailDefinitionList.add(newSectionDetailDefinition);
   			}
   			
   			continue;
   		}
   		
   			
   		
   	}
   	
   	//serviceBill.getServiceBillItems().clear();
   	targetSectionDetail.getSectionDetailDefinitions().addAll(newSectionDetailDefinitionList);
	
	
       return targetSectionDetail;
}
	        
}
