package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;


import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.MetaTagAttributes;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class ProductListMainViewToEntityAssembler {

private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();	
private static class ProductItemAction {
		
		enum ACTION {REMOVE,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		Product updatedProduct;
		ACTION action;
		
		ProductItemAction(){
			
		}
		ProductItemAction(ACTION action, Product updatedProduct){
			this.action=action;
			this.updatedProduct = updatedProduct;
		}
		public Product getUpdatedProduct() {
			return updatedProduct;
		}
		public void setUpdatedProduct(Product updatedProduct) {
			this.updatedProduct = updatedProduct;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		} 
		
	} // end inner class

private static class MetaTagAttributesAction {
	
	enum ACTION {REMOVE,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
	MetaTagAttributes updatedMetaTagAttribute;
	ACTION action;
	
	MetaTagAttributesAction(){
		
	}
	MetaTagAttributesAction(ACTION action, MetaTagAttributes updatedMetaTagAttribute){
		this.action=action;
		this.updatedMetaTagAttribute = updatedMetaTagAttribute;
	}
	public MetaTagAttributes getUpdatedMetaTagAttribute() {
		return updatedMetaTagAttribute;
	}
	public void setUpdatedMetaTagAttribute(MetaTagAttributes updatedMetaTagAttribute) {
		this.updatedMetaTagAttribute = updatedMetaTagAttribute;
	}
	public ACTION getAction() {
		return action;
	}
	public void setAction(ACTION action) {
		this.action = action;
	}
	
	
} // end inner class
	
	public List<Product> toEntityList(ProductListMainViewDTO productListMainViewDTO){
		
		 
	     List<Product> newProducts = new ArrayList<Product>();
	     return toEntityList(productListMainViewDTO,newProducts);
		
    }
	
	public List<Product> toEntityList(ProductListMainViewDTO productListMainViewDTO, List<Product> products) 
    {
	try {
	LoggedInUser user = new LoggedInUser();
	user.setUserId(productListMainViewDTO.getLoggedInUser());
	loggedInUser.set(user); // thread local for subsequent threads to provide loggged
	
	ArrayList<Product> newProductList = new ArrayList<Product>();
    for(ProductViewDTO clientProduct : productListMainViewDTO.getProducts()){
    	
    	ProductItemAction actionOnProductItem = updateExisting(products, clientProduct.getProductId(), clientProduct);;
		//ServiceBillItem updatedItem = 
		if(actionOnProductItem.action == actionOnProductItem.action.DO_NOTHING) {
			continue;
		}
		if(actionOnProductItem.action == actionOnProductItem.action.REMOVE) {
			/*Product tobeDeletedItem = actionOnProductItem.getUpdatedItem();
			tobeDeletedItem.setServiceBill(null);
			serviceBill.getServiceBillItems().remove(tobeDeletedItem);
			continue;*/
			
		}
		if(actionOnProductItem.action == actionOnProductItem.action.UPDATE_EXISTING) {
			Product updatedItem = actionOnProductItem.getUpdatedProduct();
			
			//updatedItem.setServiceBill(serviceBill);
			
			continue;
			
		}
		if(actionOnProductItem.action == actionOnProductItem.action.CREATE_NEW) {
			Product updatedItem = actionOnProductItem.getUpdatedProduct();
			
			Product prod = new Product();
			prod.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
			prod.setCreatedBy(productListMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : productListMainViewDTO.getLoggedInUser());
			prod.setActive(true);
			Marketer mark = new Marketer();
			prod.setMarketer(mark);
			boolean isDefaultMetsTagExist=false;
			if(clientProduct.getMetaTagAttributes() != null)
			{
				for(MetaTagAttributesViewDTO cleintMetaTagAttribute : clientProduct.getMetaTagAttributes()){
					if(cleintMetaTagAttribute.getMetaTag().getMetaTagId() == 10){ // meta tag type
						isDefaultMetsTagExist=true;
					}
				}
			}
			if(!isDefaultMetsTagExist){
				if(prod.getMetaTagAttributes() == null) {
					prod.setMetaTagAttributes(new ArrayList<MetaTagAttributes>());
				 }
				MetaTagAttributes mtagattr = new MetaTagAttributes();
				mtagattr.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
				mtagattr.setCreatedBy(loggedInUser.get().getUserId());
				mtagattr.setActive(true);
				mtagattr.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
				mtagattr.setUpdatedBy(loggedInUser.get().getUserId());
				MetaTag metaTag = new MetaTag();
				metaTag.setId(10);
				mtagattr.setMetaTag(metaTag);
				mtagattr.setValue("Medicine");
				mtagattr.setProduct(prod);
				prod.getMetaTagAttributes().add(mtagattr);
				
			}
			boolean isNewCreated = updateProduct(prod, clientProduct);
			if(isNewCreated) {
				//sbi.setServiceBill(serviceBill);
				newProductList.add(prod);
			}
			
		}
		
			
		
	}
   
    products.addAll(newProductList);
	return products;
    }finally {
		loggedInUser.remove();
	}
	
	//serviceBill.setServiceBillItems(serviceBill.getServiceBillItems());
   // return serviceBill;
    	
    }	
    
 

	 
	 
	 public ProductItemAction updateExisting(List<Product> products, Integer productId, ProductViewDTO updatedProduct) {
		 ProductItemAction productItemAction = new ProductItemAction();
		    if(products == null  || updatedProduct == null)
		        return new ProductItemAction(productItemAction.action.DO_NOTHING, null);
		    
		    if(products == null)
		    	 return new ProductItemAction(productItemAction.action.DO_NOTHING, null);
		    
		    if(productId== null) {
		    	if(updatedProduct.getActive() == false)
		    		 return new ProductItemAction(productItemAction.action.DO_NOTHING, null);
		    	else
		    		
		    		return new ProductItemAction(productItemAction.action.CREATE_NEW, null);
		    }

		 Optional<Product> optTargetProduct = products.stream()
		.filter(productItem -> productItem.getId().equals(productId)).findFirst();

		    if(!optTargetProduct.isPresent())  //check this logic where the chilkd
		    	return new ProductItemAction(productItemAction.action.CREATE_NEW, null);
		    
		    Product targetProduct = optTargetProduct.get();
		    
		   

		    // now implement the update function
		    boolean status = updateProduct(targetProduct, updatedProduct);
		    if(status) {
		    	return new ProductItemAction(productItemAction.action.UPDATE_EXISTING, targetProduct);
		    	//return targetServiceBillItem;
		    	
		    }

		    // if status is true, you also have to call save() on your OrderRepository
		    return new ProductItemAction(productItemAction.action.DO_NOTHING, null);
		}
	 
	 private boolean updateProduct(Product target, ProductViewDTO source) {
		 
		 target.setId(source.getProductId());
		 Marketer marketer = new Marketer();
		 marketer.setId(source.getMarketerId()); // just set the parent link
		 target.setMarketer(marketer);
		// target.setManufacturer(marketer);
		 target.setName(source.getName());
		 target.setDescription(source.getDescription());
		 //target.setMolecule(source.getMoleculeId());
		 //target.setHsnCode(source.getHsnCode());
		 //target.setUnits(source.getUnits());
		 //target.setPack(source.getPack());
		 //target.setScheduleDrugCode(source.getScheduleDrugCode());
		 //target.setGstRate(source.getGstRate());
		 //target.setCentralTaxRate(source.getCentralTaxRate());
		 //target.setSalesTaxRate(source.getSalesTaxRate());
		// target.setVatRate(source.getVatRate());
		 target.setReOrderLevel(source.getReOrderLevel());
		 target.setLeadTime(source.getLeadTime());
		 target.setActive(source.getActive());
		 if(source.getMetaTagAttributes() != null && !source.getMetaTagAttributes().isEmpty()) {
			 if(target.getMetaTagAttributes() == null) {
				 target.setMetaTagAttributes(new ArrayList<MetaTagAttributes>());
			 }
			 ArrayList<MetaTagAttributes> newMetaTagAttributesList = new ArrayList<MetaTagAttributes>();
			 for(MetaTagAttributesViewDTO metaTagAttribute : source.getMetaTagAttributes()) {
				 MetaTagAttributesAction actionOnItem = updateExistingMetaTagAttributes(target.getMetaTagAttributes(), metaTagAttribute.getAttributeId(), metaTagAttribute);
				 if(actionOnItem.action == actionOnItem.action.DO_NOTHING) {
		    			continue;
		    		}
		    		if(actionOnItem.action == actionOnItem.action.REMOVE) {
		    			MetaTagAttributes tobeDeletedItem = actionOnItem.getUpdatedMetaTagAttribute();
		    			tobeDeletedItem.setProduct(null);
		    			target.getMetaTagAttributes().remove(tobeDeletedItem);
		    			continue;
		    			//serviceBill.setServiceBillItems(serviceBill.getServiceBillItems());
		    		}
		    		if(actionOnItem.action == actionOnItem.action.UPDATE_EXISTING) {
		    			MetaTagAttributes updatedItem = actionOnItem.getUpdatedMetaTagAttribute();
		    			updatedItem.setProduct(target);
		    			//existingBillItemsList.add(updatedItem);
		    			continue;
		    			
		    		}
		    		if(actionOnItem.action == actionOnItem.action.CREATE_NEW) {
		    			MetaTagAttributes updatedItem = actionOnItem.getUpdatedMetaTagAttribute();
		    			
		    			MetaTagAttributes mtagattr = new MetaTagAttributes();
		    			mtagattr.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		    			mtagattr.setCreatedBy(loggedInUser.get().getUserId());
		    			mtagattr.setActive(true);
		    			MetaTag metaTag = new MetaTag();
		    			mtagattr.setMetaTag(metaTag);
		    			boolean isNewCreated = updateMetaTagAttribute(mtagattr, metaTagAttribute);
		    			if(isNewCreated) {
		    				mtagattr.setProduct(target);
		    				
		    				newMetaTagAttributesList.add(mtagattr);
		    			}
		    			continue;
		    		}
		    		
		    			
		    		
		    	}
		    	
		    	//serviceBill.getServiceBillItems().clear();
	    		target.getMetaTagAttributes().addAll(newMetaTagAttributesList);
	    		target.setMetaTagAttributes(target.getMetaTagAttributes());
		        //return true;
			 }
			
		
		target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		target.setUpdatedBy(loggedInUser.get().getUserId());
		return true;
		
 		
	 }
	 
	 public MetaTagAttributesAction updateExistingMetaTagAttributes(List<MetaTagAttributes> metaTagAttributes, Integer metaTagAttributeId, MetaTagAttributesViewDTO updatedMetaTagAttribute) {
		 MetaTagAttributesAction metaTagAttributeAction = new MetaTagAttributesAction();
		    if(metaTagAttributes == null  || updatedMetaTagAttribute == null)
		        return new MetaTagAttributesAction(metaTagAttributeAction.action.DO_NOTHING, null);
		    
		    if(metaTagAttributes == null)
		    	return new MetaTagAttributesAction(metaTagAttributeAction.action.DO_NOTHING, null);
		    
		    if(metaTagAttributeId== null) {
		    	if(updatedMetaTagAttribute.getActive() == false)
		    		return new MetaTagAttributesAction(metaTagAttributeAction.action.DO_NOTHING, null);
		    	else
		    		return new MetaTagAttributesAction(metaTagAttributeAction.action.CREATE_NEW, null);
		    		
		    }

		    Optional<MetaTagAttributes> optTargetMetaTagAttribute = metaTagAttributes.stream()
		.filter(metaTagAttrib -> metaTagAttrib.getId().equals(metaTagAttributeId)).findFirst();

		    if(!optTargetMetaTagAttribute.isPresent())  //check this logic where the chilkd
		    	return new MetaTagAttributesAction(metaTagAttributeAction.action.CREATE_NEW, null);
		    
		    MetaTagAttributes targetMetaTagAttribute = optTargetMetaTagAttribute.get();
		    
		   

		    // now implement the update function
		    boolean status = updateMetaTagAttribute(targetMetaTagAttribute, updatedMetaTagAttribute);
		    if(status) {
		    	return new MetaTagAttributesAction(metaTagAttributeAction.action.UPDATE_EXISTING, targetMetaTagAttribute);
		    	//return targetServiceBillItem;
		    	
		    }

		    // if status is true, you also have to call save() on your OrderRepository
		    return new MetaTagAttributesAction(metaTagAttributeAction.action.DO_NOTHING, null);
		}
        
	 private boolean updateMetaTagAttribute(MetaTagAttributes target, MetaTagAttributesViewDTO updatedMetaTagAttribute) {
		 
		 target.setId(updatedMetaTagAttribute.getAttributeId());
		 target.setActive(updatedMetaTagAttribute.getActive());
		 Product prod = new Product();
		 target.setProduct(prod);
		 target.setValue(updatedMetaTagAttribute.getAttrubuteValue());
		 MetaTag metaTag = new MetaTag();
		 metaTag.setId(updatedMetaTagAttribute.getMetaTag().getMetaTagId());
		 target.setMetaTag(metaTag);
		  		 		
		 target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		 target.setUpdatedBy(loggedInUser.get().getUserId());
		 return true;
		
 		
	 }
}
