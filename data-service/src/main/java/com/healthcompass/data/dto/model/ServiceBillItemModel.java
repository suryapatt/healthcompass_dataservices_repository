package com.healthcompass.data.dto.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.healthcompass.data.model.ServiceBill;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "serviceBillItem")
@Relation(collectionRelation = "serviceBillItems")
//@JsonInclude(Include.NON_NULL) -- enable this if attributes with null values not to be sent to the client
@Component
public class ServiceBillItemModel extends RepresentationModel<ServiceBillItemModel>{
  
	
	
	private UUID id;
	private ServiceBillModel serviceBill;
	//private ServiceModel service;
	private RateCardModel rateCard;
	private Double rate;
	private Double amount;
	private Double discountRate;
	private Double discount;
	private Double gstRate;
	private Double salesTaxRate;
	private Double centralTaxRate;
	private Double vatRate;
	private Double netAmount;
	private java.sql.Timestamp createdOn;
	private String createdBy;
	private java.sql.Timestamp updatedOn;
	private String updatedBy;


	
	
	
}
