package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.RateCardCategoryModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.RateCardCategoryResource;


@Component
public class RateCardCategoryEntityAssembler  {
 
    public RateCardCategoryEntityAssembler() {
        
    }
    
    public RateCardCategory  toEntity(RateCardCategoryModel rateCardCategoryModel,RateCardCategory rateCardCategory) 
    {
    	if( rateCardCategoryModel == null) return null;
        BeanUtils.copyProperties(rateCardCategoryModel, rateCardCategory, "_links"); //ignore valueSetType
        return rateCardCategory;
    }
     

}
