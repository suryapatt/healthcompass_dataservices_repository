package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.MarketerResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.SupplierResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class MarketerMainViewModelAsembler extends RepresentationModelAssemblerSupport<Marketer, MarketerMainViewDTO> {
 
	 @Autowired
	 MarketerViewModelAssembler marketerViewModelAssembler;
	 
	 
	 
	 public MarketerMainViewModelAsembler() {
	        super(MarketerResource.class, MarketerMainViewDTO.class);
	    }   
   
  
  
    @Override
    public MarketerMainViewDTO toModel(Marketer entity) 
     {
    	
    	MarketerMainViewDTO marketerMainVieww = new MarketerMainViewDTO();
    	
    	 	
         
    
    	marketerMainVieww.setOrganizationId(entity.getOrganization().getId());
    	marketerMainVieww.setOrganizationName(entity.getOrganization().getName());
    	marketerMainVieww.setMarketer(marketerViewModelAssembler.toModel(entity));
    	    	    	
        return marketerMainVieww;
    }
    
        
    @Override
    public CollectionModel<MarketerMainViewDTO> toCollectionModel(Iterable<? extends Marketer> entities) 
    {
        CollectionModel<MarketerMainViewDTO> marketers = super.toCollectionModel(entities);
               
         
        return marketers;
    }
 
    public List<MarketerViewDTO> toMarketersViewModel(List<Marketer> marketers) {
		if (marketers.isEmpty())
			return Collections.emptyList();
		List<MarketerViewDTO> marketersList = new ArrayList<MarketerViewDTO>();
         for(Marketer marketer : marketers) {
        	 MarketerViewDTO marketerViewDTO = new MarketerViewDTO();
        	 marketerViewDTO.setId(marketer.getId());
        	 marketerViewDTO.setName(marketer.getName());
        	 marketerViewDTO.setDescription(marketer.getDescription());
        	 marketerViewDTO.setActive(marketer.getActive());
        	 marketersList.add(marketerViewDTO);
         }
		
		return marketersList;
	
		
				
	}

}
