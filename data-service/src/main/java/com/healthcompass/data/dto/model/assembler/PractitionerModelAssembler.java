package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PractitionerModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Practitioner;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PractitionerResource;

@Component
public class PractitionerModelAssembler extends RepresentationModelAssemblerSupport<Practitioner, PractitionerModel> {
 
    public PractitionerModelAssembler() {
        super(PractitionerResource.class, PractitionerModel.class);
    }
    
    @Autowired
    OrganizationModelAssembler organizationModelAssembler;
 
    @Override
    public PractitionerModel toModel(Practitioner entity) 
    {
    	PractitionerModel practitionerModel = instantiateModel(entity);
         
    	practitionerModel.add(linkTo(
                methodOn(PractitionerResource.class)
                .getPractitioner(entity.getId()))
                .withSelfRel());
         
    	practitionerModel.setId(entity.getId());
    	practitionerModel.setName(entity.getDecodedName());
    	practitionerModel.setAddress(entity.getAddress());
    	//practitionerModel.setOrganization(organizationModelAssembler.toModel(entity.getOrganization()));
    	practitionerModel.setCreatedBy(entity.getCreatedBy());
    	practitionerModel.setUpdatedBy(entity.getUpdatedBy());
    	practitionerModel.setCreatedOn(entity.getCreatedOn());
    	practitionerModel.setUpdatedOn(entity.getUpdatedOn());
       
        return practitionerModel;
    }
     
    @Override
    public CollectionModel<PractitionerModel> toCollectionModel(Iterable<? extends Practitioner> entities) 
    {
        CollectionModel<PractitionerModel> practitionerModel = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return practitionerModel;
    }
 
    
}
