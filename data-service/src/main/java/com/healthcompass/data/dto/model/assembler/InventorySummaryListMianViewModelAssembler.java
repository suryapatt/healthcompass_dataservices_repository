package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.InventorySummaryResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.SupplierResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.InventorySummaryListMainViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class InventorySummaryListMianViewModelAssembler extends RepresentationModelAssemblerSupport<InventorySummary, InventorySummaryListMainViewDTO> {
 
	 @Autowired
	 InventorySummaryViewModelAssembler inventorySummaryViewModelAssembler;
	 
	 
	 
	 public InventorySummaryListMianViewModelAssembler() {
	        super(InventorySummaryResource.class, InventorySummaryListMainViewDTO.class);
	    }   
   
  
  
    @Override
    public InventorySummaryListMainViewDTO toModel(InventorySummary entity) 
     {
    	
    	InventorySummaryListMainViewDTO inventorySummaryListMainViewDTO = new InventorySummaryListMainViewDTO();
    	
    	 	
         
    
    	inventorySummaryListMainViewDTO.setOrganizationId(entity.getOrganization().getId());
    	inventorySummaryListMainViewDTO.setOrganizationName(entity.getOrganization().getName());
    	inventorySummaryListMainViewDTO.setLocationId(entity.getLocation().getId());
    	inventorySummaryListMainViewDTO.setLocationName(entity.getLocation().getName());
    	
    	
    	    	    	
        return inventorySummaryListMainViewDTO;
    }
    
        
    @Override
    public CollectionModel<InventorySummaryListMainViewDTO> toCollectionModel(Iterable<? extends InventorySummary> entities) 
    {
        CollectionModel<InventorySummaryListMainViewDTO> inventorySummaries = super.toCollectionModel(entities);
               
         
        return inventorySummaries;
    }
 
    public List<InventorySummaryViewDTO> toInventorySummaryViewModel(List<InventorySummary> inventorySummaries) {
		if (inventorySummaries.isEmpty())
			return Collections.emptyList();
		List<InventorySummaryViewDTO> inventorySummariesList = new ArrayList<InventorySummaryViewDTO>();
         for(InventorySummary inventorySummary : inventorySummaries) {
        	 InventorySummaryViewDTO inventorySummaryViewDTO = new InventorySummaryViewDTO();
        	 inventorySummaryViewDTO.setOrganizationId(inventorySummary.getOrganization().getId());
        	 inventorySummaryViewDTO.setOrganizationName(inventorySummary.getOrganization().getName());
        	 inventorySummaryViewDTO.setLocationId(inventorySummary.getLocation().getId());
        	 inventorySummaryViewDTO.setLocationName(inventorySummary.getLocation().getName());
        	 inventorySummaryViewDTO.setInventorySummaryId(inventorySummary.getId());
        	 inventorySummaryViewDTO.setProductId(inventorySummary.getProduct().getId());
        	 inventorySummaryViewDTO.setProductName(inventorySummary.getProduct().getName());
        	 inventorySummaryViewDTO.setAvailableUnits(inventorySummary.getAvailableUnits());
        	 inventorySummaryViewDTO.setUnitRate(inventorySummary.getUnitRate());
        	 inventorySummaryViewDTO.setReOrderLevel(inventorySummary.getProduct().getReOrderLevel());
        	 inventorySummaryViewDTO.setLeadTime(inventorySummary.getProduct().getLeadTime());
        	
        	 inventorySummariesList.add(inventorySummaryViewDTO);
         }
		
		return inventorySummariesList;
	
		
				
	}

}
