package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;


@Component
public class ServiceEntityAssembler  {
 
    public ServiceEntityAssembler() {
        
    }
    
    @Autowired
    ValueSetEntityAssembler valueSetEntityAssembler;
 
   
    public Service toEntity(ServiceModel serviceModel) 
    {
    	if( serviceModel == null) return null;
        Service service = new Service();
        BeanUtils.copyProperties(serviceModel, service, "valueSet","_links"); //ignore valueSetType
         service.setServiceType(valueSetEntityAssembler.toEntity(serviceModel.getValueSet()));
        
         
        
        return service;
    }
   
 
    
}
