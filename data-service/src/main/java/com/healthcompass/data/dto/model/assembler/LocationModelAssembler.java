package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.LocationResource;


@Component
public class LocationModelAssembler extends RepresentationModelAssemblerSupport<Location, LocationModel> {
 
    public LocationModelAssembler() {
        super(LocationResource.class, LocationModel.class);
    }
    
    @Autowired
    OrganizationModelAssembler organizationModelAssembler;
    
    @Autowired
    RateCardModelAssembler rateCardModelAssembler;
 
    @Override
    public LocationModel toModel(Location entity) 
    {
    	LocationModel locationModel = instantiateModel(entity);
         
    	locationModel.add(linkTo(
                methodOn(LocationResource.class)
                .getLocation(entity.getId()))
                .withSelfRel());
         
    	locationModel.setId(entity.getId());
    	locationModel.setName(entity.getName());
    	locationModel.setAddress(entity.getAddress());
    	//locationModel.setRateCards(rateCardModelAssembler.toRateCardCollectionModel(entity.getRateCards()));
    	//locationModel.setOrganization(organizationModelAssembler.toModel(entity.getOrganization()));
    	locationModel.setCreatedBy(entity.getCreatedBy());
    	locationModel.setUpdatedBy(entity.getUpdatedBy());
    	locationModel.setCreatedOn(entity.getCreatedOn());
    	locationModel.setUpdatedOn(entity.getUpdatedOn());
       
        return locationModel;
    }
     
    @Override
    public CollectionModel<LocationModel> toCollectionModel(Iterable<? extends Location> entities) 
    {
        CollectionModel<LocationModel> locationModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return locationModels;
    }
 
    

}
