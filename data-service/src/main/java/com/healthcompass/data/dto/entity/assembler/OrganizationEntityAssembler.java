package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.PractitionerRoleModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.LocationResource;
import com.healthcompass.data.resource.OrganizationResource;
import com.healthcompass.data.resource.PatientResource;
import com.healthcompass.data.resource.PractitionerRoleResource;
import com.healthcompass.data.resource.ServiceBillItemResource;


@Component
public class OrganizationEntityAssembler  {
 
    public OrganizationEntityAssembler() {
       
    }
    
    @Autowired
    PractitionerRoleEntityAssmebler practitionerRoleEntityAssembler;
 
    
    public Organization  toEntity(OrganizationModel organizationModel,Organization organization) 
    {
    	if( organizationModel == null) return null;
        BeanUtils.copyProperties(organizationModel, organization, "organization","_links"); //ignore valueSetType
        return organization;
    }
     
    
 
   

}
