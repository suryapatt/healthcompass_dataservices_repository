package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;

@Component
public class ValueSetTypeModelAssembler extends RepresentationModelAssemblerSupport<ValueSetType, ValueSetTypeModel> {
 
    public ValueSetTypeModelAssembler() {
        super(ValueSetTypeResource.class, ValueSetTypeModel.class);
    }
 
    @Override
    public ValueSetTypeModel toModel(ValueSetType entity) 
    {
        ValueSetTypeModel valueSetTypeModel = instantiateModel(entity);
         
        valueSetTypeModel.add(linkTo(
                methodOn(ValueSetTypeResource.class)
                .getValueSetTypeId(entity.getId()))
                .withSelfRel());
         
        valueSetTypeModel.setName(entity.getName());
        valueSetTypeModel.setId(entity.getId());
        valueSetTypeModel.setValueSets(toValueSetsModel(entity.getValueSets()));
        
        /*valueSetTypeModel.setDefinition(entity.getDefinition());
       // valueSetTypeModel.setReferredFrom(entity.getReferredFrom());
       // valueSetTypeModel.setSortOrder(entity.getSortOrder());
        valueSetTypeModel.setStandardLevel(entity.getStandardLevel());
        valueSetTypeModel.setSource(entity.getSource());
        
        valueSetTypeModel.setActive(entity.isActive());
        valueSetTypeModel.setCreatedOn(entity.getCreatedOn());
        valueSetTypeModel.setCreatedBy(entity.getCreatedBy());
        valueSetTypeModel.setUpdatedOn(entity.getUpdatedOn());
        valueSetTypeModel.setUpdatedBy(entity.getUpdatedBy());*/
        return valueSetTypeModel;
    }
     
    @Override
    public CollectionModel<ValueSetTypeModel> toCollectionModel(Iterable<? extends ValueSetType> entities) 
    {
        CollectionModel<ValueSetTypeModel> valueSetTypeModels = super.toCollectionModel(entities);
         
        valueSetTypeModels.add(linkTo(methodOn(ValueSetTypeResource.class).getAllValueSetTypes()).withSelfRel());
         
        return valueSetTypeModels;
    }
 
    private List<ValueSetModel> toValueSetsModel(List<ValueSet> valueSets) {
		if (valueSets.isEmpty())
			return Collections.emptyList();

		return valueSets.stream()
				.map(valueSet -> ValueSetModel.builder()
						.id(valueSet.getId())
						.display(valueSet.getDisplay())
						.code(valueSet.getCode())
						//.commonName(valueSet.getCommonName())
						//.definition(valueSet.getDefinition())
						//.userGroup(valueSet.getUserGroup())
						//.system(valueSet.getSystem())
						.build()
						.add(linkTo(
								methodOn(ValueSetResource.class)
								.getValueSetId(valueSet.getId()))
								.withSelfRel()))
						.collect(Collectors.toList());
				
	}
   

}
