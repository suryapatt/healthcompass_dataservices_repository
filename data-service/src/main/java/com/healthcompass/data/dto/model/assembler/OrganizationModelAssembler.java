package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.PractitionerRoleModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.LocationResource;
import com.healthcompass.data.resource.OrganizationResource;
import com.healthcompass.data.resource.PatientResource;
import com.healthcompass.data.resource.PractitionerRoleResource;
import com.healthcompass.data.resource.ServiceBillItemResource;


@Component
public class OrganizationModelAssembler extends RepresentationModelAssemblerSupport<Organization, OrganizationModel> {
 
    public OrganizationModelAssembler() {
        super(OrganizationResource.class, OrganizationModel.class);
    }
    
    @Autowired
    PractitionerRoleModelAssmebler practitionerRoleModelAssembler;
 
    @Override
    public OrganizationModel toModel(Organization entity) 
    {
    	OrganizationModel organizationModel = instantiateModel(entity);
         
    	organizationModel.add(linkTo(
                methodOn(OrganizationResource.class)
                .getOrganization(entity.getId()))
                .withSelfRel());
         
    	organizationModel.setId(entity.getId());
    	organizationModel.setName(entity.getName());
    	organizationModel.setCreatedBy(entity.getCreatedBy());
    	organizationModel.setUpdatedBy(entity.getUpdatedBy());
    	organizationModel.setCreatedOn(entity.getCreatedOn());
    	organizationModel.setUpdatedOn(entity.getUpdatedOn());
    	organizationModel.setLocations(toLocationsModel(entity.getLocations()));
    	organizationModel.setPatients(toPatientsModel(entity.getPatients()));
    	organizationModel.setPractitionerRoles(toPractitionerRolesModel(entity.getPractitionerRoles()));
       
        return organizationModel;
    }
     
    
 
    private List<LocationModel> toLocationsModel(List<Location> locations) {
		if (locations.isEmpty())
			return Collections.emptyList();

		return locations.stream()
				.map(location -> LocationModel.builder()
						.id(location.getId())
						.name(location.getName())
						.address(location.getAddress())
						//.organization(toModel(patient.getOrganization()))
						.createdOn(location.getCreatedOn())
						.createdBy(location.getCreatedBy())
						.updatedOn(location.getUpdatedOn())
						.updatedBy(location.getUpdatedBy())
						.build()
						.add(linkTo(
								methodOn(LocationResource.class)
								.getLocation(location.getId()))
								.withSelfRel()))
						.collect(Collectors.toList());
				
	}
    
    private List<PatientModel> toPatientsModel(List<Patient> patients) {
		if (patients.isEmpty())
			return Collections.emptyList();

		return patients.stream()
				.map(patient -> PatientModel.builder()
						.id(patient.getId())
						.name(patient.getDecodedName())
						.address(patient.getDecodedAddress())
						//.organization(toModel(patient.getOrganization()))
						.createdOn(patient.getCreatedOn())
						.createdBy(patient.getCreatedBy())
						.updatedOn(patient.getUpdatedOn())
						.updatedBy(patient.getUpdatedBy())
						.build()
						.add(linkTo(
								methodOn(PatientResource.class)
								.getPatient(patient.getId()))
								.withSelfRel()))
						.collect(Collectors.toList());
				
	}
    
    private List<PractitionerRoleModel> toPractitionerRolesModel(List<PractitionerRole> practitionerRoles) {
		if (practitionerRoles.isEmpty())
			return Collections.emptyList();

		return practitionerRoles.stream()
				.map(practitionerRole -> PractitionerRoleModel.builder()
						.id(practitionerRole.getId())
						.code(practitionerRole.getCode())
						//.organization(toModel(practitionerRole.getOrganization())
						//.practitioners(practitionerRoleModelAssembler.toPractitionersCollection(practitionerRole.getPractitioners())))
						.createdOn(practitionerRole.getCreatedOn())
						.createdBy(practitionerRole.getCreatedBy())
						.updatedOn(practitionerRole.getUpdatedOn())
						.updatedBy(practitionerRole.getUpdatedBy())
						.build()
						.add(linkTo(
								methodOn(PractitionerRoleResource.class)
								.getPractitionerRole(practitionerRole.getId()))
								.withSelfRel()))
						.collect(Collectors.toList());
				
	}

}
