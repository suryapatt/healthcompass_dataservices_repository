package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PractitionerModel;
import com.healthcompass.data.dto.model.PractitionerRoleModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Practitioner;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PractitionerRoleResource;

@Component
public class PractitionerRoleEntityAssmebler {
 
    public PractitionerRoleEntityAssmebler() {
        
    }
    
    @Autowired
    OrganizationEntityAssembler organizationEntityAssembler;
    
    @Autowired
    PractitionerEntityAssmebler practitionerEntityAssembler;
 
    @Autowired
	 ValueSetEntityAssembler valueSetEntityAssembler;
    
    public PractitionerRole  toEntity(PractitionerRoleModel practitionerRoleModel,PractitionerRole practitionerRole) 
    {
    if( practitionerRoleModel == null) return null;
    BeanUtils.copyProperties(practitionerRoleModel, practitionerRole, "practitioner","organization","speciality","_links"); //ignore valueSetType
   
    
    
    practitionerRole.setPractitioner(practitionerEntityAssembler.toEntity(practitionerRoleModel.getPractitioner(),practitionerRole.getPractitioner()));
    //location.setOrganization
    practitionerRole.setOrganization(organizationEntityAssembler.toEntity(practitionerRoleModel.getOrganization(), practitionerRole.getOrganization()));
    
   return practitionerRole;
    }
    
 public List<PractitionerRole> toEntityCollection(List<PractitionerRoleModel> practitionerRoleModels,List<PractitionerRole> practitionerRoles){
    	
    	
    	for(PractitionerRoleModel practitionerRoleModel : practitionerRoleModels ) {
    		practitionerRoles.add(toEntity(practitionerRoleModel, new PractitionerRole()));
    	}
    	
    	return practitionerRoles;
    }

}
