package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;

@Component

public class ValueSetEntityAssembler  {
 
    public ValueSetEntityAssembler() {
        
    }
 
    @Autowired
    private ValueSetTypeEntityAssembler valueSetTypeEntityAssembler;
   
    
    public ValueSet toEntity(ValueSetModel valueSetModel) 
    {
    	if( valueSetModel == null) return null;
        ValueSet valueSet = new ValueSet();
        BeanUtils.copyProperties(valueSetModel, valueSet, "valueSetType","_links"); //ignore valueSetType
        valueSet.setValueSetType(valueSetTypeEntityAssembler.toEntity(valueSetModel.getValueSetType()));
                 
       /* valueSet.setId(entity.getId());
        valueSet.setCode(entity.getCode());
        valueSet.setCommonName(entity.getCommonName());
        valueSet.setDefinition(entity.getDefinition());
        valueSet.setDisplay(entity.getDisplay());
        valueSet.setLevel(entity.getLevel());
        valueSet.setSortOrder(entity.getSortOrder());
        valueSet.setUserGroup(entity.getUserGroup());
        valueSetModel.setSystem(entity.getSystem());
        //valueSetModel.setValueSetType(valueSetTypeModelAssembler.toModel(entity.getValueSetType()));
        valueSetModel.setIsActive(entity.getIsActive());
        valueSetModel.setCreatedOn(entity.getCreatedOn());
        valueSetModel.setCreatedBy(entity.getCreatedBy());
        valueSetModel.setUpdatedOn(entity.getUpdatedOn());
        valueSetModel.setUpdatedBy(entity.getUpdatedBy());*/
        return valueSet;
    }
     
    

}
