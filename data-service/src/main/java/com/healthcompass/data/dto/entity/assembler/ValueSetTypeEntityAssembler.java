package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;

@Component
public class ValueSetTypeEntityAssembler  {
 
    public ValueSetTypeEntityAssembler() {
        
    }
 
    
    public ValueSetType toEntity(ValueSetTypeModel valueSetTypeModel) 
    {
    	 if( valueSetTypeModel == null) return null;
    	 ValueSetType valueSetType = new ValueSetType();
         BeanUtils.copyProperties(valueSetTypeModel, valueSetType, "valueSets","_links"); //ignore valueSetType
         valueSetType.setValueSets(null);
         
       
        return valueSetType;
    }
     
   

}
