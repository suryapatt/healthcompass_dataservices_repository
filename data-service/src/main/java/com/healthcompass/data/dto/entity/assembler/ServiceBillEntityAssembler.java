package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;

@Component
public class ServiceBillEntityAssembler  {
 
	 @Autowired
	 ValueSetEntityAssembler valueSetEntityAssembler;
	 @Autowired
	 ServiceBillItemEntityAssembler serviceBillItemEntityAssembler;
	 
    public ServiceBillEntityAssembler() {
        
    }
   
 
    
    public ServiceBill toEntity(ServiceBillModel serviceBillModel, ServiceBill serviceBill) 
    {
    	if(serviceBillModel == null) return null;
        BeanUtils.copyProperties(serviceBillModel, serviceBill, "serviceBillItems","status","paymentMode","_links"); //ignore valueSetType
        
        serviceBill.setStatus(valueSetEntityAssembler.toEntity(serviceBillModel.getStatus()));
        serviceBill.setPaymentMode(valueSetEntityAssembler.toEntity(serviceBillModel.getPaymentMode()));
        serviceBill.setServiceBillItems(serviceBillItemEntityAssembler.toEntityCollection(serviceBillModel.getServiceBillItems(),serviceBill.getServiceBillItems()));
         
    	
        return serviceBill;
    }
     
    
   

}
