package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;


@Component
public class ServiceBillItemModelAssembler extends RepresentationModelAssemblerSupport<ServiceBillItem, ServiceBillItemViewDTO> {
 
    public ServiceBillItemModelAssembler() {
        super(ServiceBillItemResource.class, ServiceBillItemViewDTO.class);
    }
    
    
    
    @Autowired
    RateCardModelAssembler rateCardModelAssembler;
 
    @Override
    public ServiceBillItemViewDTO toModel(ServiceBillItem entity) 
    {
    	ServiceBillItemViewDTO serviceBillItemViewDTO = instantiateModel(entity);
         
    	serviceBillItemViewDTO.add(linkTo(
                methodOn(ServiceBillItemResource.class)
                .getServiceBillItem(entity.getId()))
                .withSelfRel());
         
    	serviceBillItemViewDTO.setServiceBillItemId(entity.getId());
    	serviceBillItemViewDTO.setQuantity(entity.getQuantity()); // change this an get from entity
    	serviceBillItemViewDTO.setAmount(entity.getAmount());
    	serviceBillItemViewDTO.setRate(entity.getRate());
    	
    	
    	
    	/*serviceBillItemViewDTO.setCentralTaxRate(entity.getCentralTaxRate());
    	serviceBillItemViewDTO.setGstRate(entity.getGstRate());
    	serviceBillItemViewDTO.setSalesTaxRate(entity.getSalesTaxRate());
    	serviceBillItemViewDTO.setVatRate(entity.getVatRate());
    	serviceBillItemViewDTO.setDiscountRate(entity.getDiscountRate());
    	serviceBillItemViewDTO.setDiscount(entity.getDiscount());
    	serviceBillItemViewDTO.setNetAmount(entity.getNetAmount());*/
    	
    	serviceBillItemViewDTO.setRateCard(rateCardModelAssembler.toModel(entity.getRateCard()));
    	
        return serviceBillItemViewDTO;
    }
     
    @Override
    public CollectionModel<ServiceBillItemViewDTO> toCollectionModel(Iterable<? extends ServiceBillItem> entities) 
    {
        CollectionModel<ServiceBillItemViewDTO> serviceBillItemsModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return serviceBillItemsModels;
    }
 
    

}
