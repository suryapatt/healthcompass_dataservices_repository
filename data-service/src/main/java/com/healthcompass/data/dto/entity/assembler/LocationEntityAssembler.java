package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.LocationResource;


@Component
public class LocationEntityAssembler  {
 
    public LocationEntityAssembler() {
        
    }
    
    @Autowired
    OrganizationEntityAssembler organizationEntityAssembler;
    
    @Autowired
    RateCardEntityAssembler rateCardEntiyAssembler;
 

    @Autowired
    OrganizationEntityAssembler organizationdEntiyAssembler;
 
     	
   
    public Location toEntity(LocationModel locationModel, Location location) 
    {
    	if( locationModel == null) return null;
        BeanUtils.copyProperties(locationModel, location, "rateCards","organization","_links"); //ignore valueSetType
       
        
        
        location.setRateCards(rateCardEntiyAssembler.toEntityCollection(locationModel.getRateCards(),new ArrayList<RateCard>()));
        //location.setOrganization
       location.setOrganization(organizationdEntiyAssembler.toEntity(locationModel.getOrganization(), location.getOrganization()));
         
    	
        return location;
    }
     
    
    

}
