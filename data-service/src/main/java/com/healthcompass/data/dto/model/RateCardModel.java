package com.healthcompass.data.dto.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "rateCard")
@Relation(itemRelation = "rateCard" , collectionRelation = "rateCards")
@JsonInclude(Include.NON_NULL)
@Component
public class RateCardModel extends RepresentationModel<RateCardModel>{
  
	
	private Integer id;
	private String display;
	private String description;
	private UUID locationId;
	private RateCardCategoryModel rateCardCategory;
	private Double rate;
	private Boolean active;
	private java.sql.Timestamp createdOn;
	private String createdBy;
	private java.sql.Timestamp updatedOn;
	private String updatedBy;
	
	
	
}
