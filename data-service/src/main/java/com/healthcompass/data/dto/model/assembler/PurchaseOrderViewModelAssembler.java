package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PatientResource;
import com.healthcompass.data.resource.PurchaseOrderResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;

@Component
public class PurchaseOrderViewModelAssembler extends RepresentationModelAssemblerSupport<Order, PurchaseOrderViewDTO> {
 
	
	 
    public PurchaseOrderViewModelAssembler() {
        super(PurchaseOrderResource.class, PurchaseOrderViewDTO.class);
    }
   
  
   @Override
    public PurchaseOrderViewDTO toModel(Order entity) 
    {
	   PurchaseOrderViewDTO purchaseOrderViewDTO = instantiateModel(entity);
         
	   purchaseOrderViewDTO.add(linkTo(
                methodOn(PurchaseOrderResource.class)
                .getPurchaseOrder(entity.getId()))
                .withSelfRel());
         
	   purchaseOrderViewDTO.setPurchaseOrderId(entity.getId());
	   purchaseOrderViewDTO.setOrganizationId(entity.getOrganization().getId());
	   purchaseOrderViewDTO.setLocationId(entity.getLocation().getId());
	   purchaseOrderViewDTO.setSupplierId(entity.getSupplier().getId());
	   
	   purchaseOrderViewDTO.setOrderNumber(entity.getOrderNumber());
	   purchaseOrderViewDTO.setOrderDate(entity.getOrderDate());
	   
	   purchaseOrderViewDTO.setOrderValue(entity.getOrderValue());
	   purchaseOrderViewDTO.setDiscount(entity.getDiscount());
	   purchaseOrderViewDTO.setNetOrderValue(entity.getNetOrderValue());
	 
	   purchaseOrderViewDTO.setOrderComment(entity.getOrderComment());
	   purchaseOrderViewDTO.setAttachementId(entity.getAttachmentId());
	  
	   purchaseOrderViewDTO.setOrderItems(toPurchaseOrderItemsModel(entity.getOrderItems()));
      
       
       
        return purchaseOrderViewDTO;
    }
     
    @Override
    public CollectionModel<PurchaseOrderViewDTO> toCollectionModel(Iterable<? extends Order> entities) 
    {
        CollectionModel<PurchaseOrderViewDTO> purchaseOrderModels = super.toCollectionModel(entities);
         
        //serviceBillModels.add(linkTo(methodOn(ServiceBillResource.class).getAllServiceBills()).withSelfRel());
         
        return purchaseOrderModels;
    }
 
    private List<PurchaseOrderItemViewDTO> toPurchaseOrderItemsModel(List<OrderItem> orderItems) {
    	
		if (orderItems.isEmpty())
			return Collections.emptyList();
		/* Optional<ServiceBillItem> optTargetServiceBillItem = serviceBill.getServiceBillItems().stream()
					.filter(billItem -> billItem.getId().equals(billItemId)).findFirst();*/

		return orderItems.stream().filter(orderItem -> orderItem != null)
				.map(orderItem -> PurchaseOrderItemViewDTO.builder()
						.orderItemId(orderItem.getId())
						.batchNumber(orderItem.getBatchNumber())
						//.expiryDate(orderItem.getExpiryDate())
						.expiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(orderItem.getExpiryDate()))
						.rate(orderItem.getRate())
						.quantity(orderItem.getQuantity())
						.mrp(orderItem.getMrp())
						.netItemValue(orderItem.getNetItemValue())
						.discount(orderItem.getDiscount())
						.discountedRate(orderItem.getDiscountedRate())
						.productName(orderItem.getProductName())
						//.gstRate(orderItem.getGstRate())
						//.centralTaxRate(orderItem.getCentralTaxRate())
						//.salesTaxRate(orderItem.getSalesTaxRate())
						//.vatRate(orderItem.getVatRate())
						.product(toPurchaseOrderProductViewModel(orderItem.getProduct()))
						.build())
						.collect(Collectors.toList());
		
		
		
		
				
	}
   
    public PurchaseOrderProductViewDTO toPurchaseOrderProductViewModel(Product purchaseOrderProduct) 
    {
    	PurchaseOrderProductViewDTO purchaseOrderProductView = new PurchaseOrderProductViewDTO();
    	purchaseOrderProductView.setProductId(purchaseOrderProduct.getId());
    	purchaseOrderProductView.setProductName(purchaseOrderProduct.getName());
    	purchaseOrderProductView.setProductDescription(purchaseOrderProduct.getDescription());
	  
	        
       
        return purchaseOrderProductView;
    }

}
