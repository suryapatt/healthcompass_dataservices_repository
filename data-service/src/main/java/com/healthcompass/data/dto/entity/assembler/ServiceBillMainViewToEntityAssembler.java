package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class ServiceBillMainViewToEntityAssembler {
	
	private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();
	
	private static class BillItemAction{
		
		
		
		enum ACTION {REMOVE_FROM_BILL,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		ServiceBillItem updatedItem;
		ACTION action;
		
		BillItemAction(){
			
		}
		BillItemAction(ACTION action, ServiceBillItem updatedItem){
			this.action=action;
			this.updatedItem = updatedItem;
		}
		public ServiceBillItem getUpdatedItem() {
			return updatedItem;
		}
		public void setUpdatedItem(ServiceBillItem updatedItem) {
			this.updatedItem = updatedItem;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		} 
		
	} // end inner class
	

	 public ServiceBill toEntity(ServiceBillViewMainDTO serviceBillMainViewDTO, ServiceBill serviceBill) 
	    {
	    try {	
		 LoggedInUser user = new LoggedInUser();
		 user.setUserId(serviceBillMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : serviceBillMainViewDTO.getLoggedInUser());
		 loggedInUser.set(user); // thread local for subsequent threads to provide loggged in user
		 
	    serviceBill.getEncounter().setId(serviceBillMainViewDTO.getEncounterId());
		    
		    serviceBill.getPatient().setId(serviceBillMainViewDTO.getPatientId());
		    serviceBill.getPractitionerRole().setId(serviceBillMainViewDTO.getPractitionerId());
		    
		    if(serviceBillMainViewDTO.getServiceBill().getPaymentMode() != null) {
		    	ValueSet updatedPaymentMode = new ValueSet();
			    updatedPaymentMode.setId(serviceBillMainViewDTO.getServiceBill().getPaymentMode().getId());
			    serviceBill.setPaymentMode(updatedPaymentMode);
		    }
		    if(serviceBillMainViewDTO.getServiceBill().getStatus() != null) {
		    	 ValueSet updatedStatus = new ValueSet();
		    	 updatedStatus.setId(serviceBillMainViewDTO.getServiceBill().getStatus().getId());
				  serviceBill.setStatus(updatedStatus);
		    }
		    
		    if(serviceBillMainViewDTO.getServiceBill().getPaymentStatus() != null) {
		    	 ValueSet updatedPaymentStatus = new ValueSet();
				  updatedPaymentStatus.setId(serviceBillMainViewDTO.getServiceBill().getPaymentStatus().getId());
				  serviceBill.setStatus(updatedPaymentStatus);
		    }
		    
		    if(serviceBillMainViewDTO.getServiceBill().getAttachmentId() != null) {
		    	
				  serviceBill.setAttachmentId(serviceBillMainViewDTO.getServiceBill().getAttachmentId());
		    }
		    
		    serviceBill.setActive(serviceBillMainViewDTO.getServiceBill().getActive());
		    serviceBill.setSubTotal(serviceBillMainViewDTO.getServiceBill().getSubTotal());
		    serviceBill.setDiscountRate(serviceBillMainViewDTO.getServiceBill().getDiscountRate());
		    serviceBill.setDiscount(serviceBillMainViewDTO.getServiceBill().getDiscount()); //calculate here
		    serviceBill.setAmountPaid(serviceBillMainViewDTO.getServiceBill().getAmountPaid());
		    if(serviceBill.getAmountPaid() == null) serviceBill.setAmountPaid(0.0);
	    	serviceBill.setAmountBilled(serviceBillMainViewDTO.getServiceBill().getAmountBilled());
	    	//update pending amount into pending payments table
	    	serviceBill.setAmountDue(serviceBillMainViewDTO.getServiceBill().getAmountDue()); //calculate here
	    	//serviceBill.setAmountPending(serviceBillMainViewDTO.getServiceBill().getAmountPending());
	    	//serviceBill.setBillDate(serviceBillMainViewDTO.getServiceBill().getBillDate());
	    	serviceBill.setBillingDoneBy(serviceBillMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : serviceBillMainViewDTO.getLoggedInUser());
	    	serviceBill.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	serviceBill.setUpdatedBy(serviceBillMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : serviceBillMainViewDTO.getLoggedInUser());
	    	ArrayList<ServiceBillItem> newBillItemsList = new ArrayList<ServiceBillItem>();
	    	//ArrayList<ServiceBillItem> existingBillItemsList = new ArrayList<ServiceBillItem>();
	    	for(ServiceBillItemViewDTO serviceBillItemViewDTOItem : serviceBillMainViewDTO.getServiceBill().getServiceBillItems()) {
	    		
	    		BillItemAction actionOnBillItem = updateExisting(serviceBill, serviceBillItemViewDTOItem.getServiceBillItemId(), serviceBillItemViewDTOItem);;
	    		//ServiceBillItem updatedItem = 
	    		if(actionOnBillItem.action == actionOnBillItem.action.DO_NOTHING) {
	    			continue;
	    		}
	    		if(actionOnBillItem.action == actionOnBillItem.action.REMOVE_FROM_BILL) {
	    			ServiceBillItem tobeDeletedItem = actionOnBillItem.getUpdatedItem();
	    			tobeDeletedItem.setServiceBill(null);
	    			serviceBill.getServiceBillItems().remove(tobeDeletedItem);
	    			continue;
	    			//serviceBill.setServiceBillItems(serviceBill.getServiceBillItems());
	    		}
	    		if(actionOnBillItem.action == actionOnBillItem.action.UPDATE_EXISTING) {
	    			ServiceBillItem updatedItem = actionOnBillItem.getUpdatedItem();
	    			updatedItem.setServiceBill(serviceBill);
	    			//existingBillItemsList.add(updatedItem);
	    			continue;
	    			
	    		}
	    		if(actionOnBillItem.action == actionOnBillItem.action.CREATE_NEW) {
	    			ServiceBillItem updatedItem = actionOnBillItem.getUpdatedItem();
	    			
	    			ServiceBillItem sbi = new ServiceBillItem();
	    			sbi.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    			sbi.setCreatedBy(serviceBillMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : serviceBillMainViewDTO.getLoggedInUser());
	    			sbi.setActive(true);
	    			RateCard rateCard = new RateCard();
	    			sbi.setRateCard(rateCard);
	    			boolean isNewCreated = updateServiceBillItem(sbi, serviceBillItemViewDTOItem);
	    			if(isNewCreated) {
	    				sbi.setServiceBill(serviceBill);
	    				newBillItemsList.add(sbi);
	    			}
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	//serviceBill.getServiceBillItems().clear();
    		serviceBill.getServiceBillItems().addAll(newBillItemsList);
    		serviceBill.setServiceBillItems(serviceBill.getServiceBillItems());
    		
	        return serviceBill;
	    }finally {
			loggedInUser.remove();
		}
	    }
	 
	 public BillItemAction updateExisting(ServiceBill serviceBill, UUID billItemId, ServiceBillItemViewDTO updatedBillItem) {
		 BillItemAction billItemAction = new BillItemAction();
		    if(serviceBill == null || updatedBillItem == null)
		        return new BillItemAction(billItemAction.action.DO_NOTHING, null);
		    
		    if(serviceBill.getServiceBillItems() == null)
		    	 return new BillItemAction(billItemAction.action.DO_NOTHING, null);
		    
		    if(billItemId== null) {
		    	if(updatedBillItem.getActive() == false)
		    		return new BillItemAction(billItemAction.action.DO_NOTHING, null);
		    	else
		    		
		    		return new BillItemAction(billItemAction.action.CREATE_NEW, null);
		    }

		    Optional<ServiceBillItem> optTargetServiceBillItem = serviceBill.getServiceBillItems().stream()
		.filter(billItem -> billItem.getId().equals(billItemId)).findFirst();

		    if(!optTargetServiceBillItem.isPresent())  //check this logic where the chilkd
		    	return new BillItemAction(billItemAction.action.CREATE_NEW, null);

		    ServiceBillItem targetServiceBillItem = optTargetServiceBillItem.get();
		    if(targetServiceBillItem.getActive()==true && updatedBillItem.getActive()==false && serviceBill.getStatus().getId()==HealthCompassConstants.BILL_STATUS_PENDING_ID) { // pending, bill not generatoed
		    	
		    	return new BillItemAction(billItemAction.action.REMOVE_FROM_BILL, targetServiceBillItem);
		    	
		    }
		   

		    // now implement the update function
		    boolean status = updateServiceBillItem(targetServiceBillItem, updatedBillItem);
		    if(status) {
		    	return new BillItemAction(billItemAction.action.UPDATE_EXISTING, targetServiceBillItem);
		    	//return targetServiceBillItem;
		    	
		    }

		    // if status is true, you also have to call save() on your OrderRepository
		    return new BillItemAction(billItemAction.action.DO_NOTHING, null);
		}
	 
	 private boolean updateServiceBillItem(ServiceBillItem target, ServiceBillItemViewDTO source) {
		 
		target.setAmount(source.getAmount());
		target.setRate(source.getRate());
		target.setQuantity(source.getQuantity());  
		
		
		
		/*target.setCentralTaxRate(source.getCentralTaxRate());
		target.setGstRate(source.getGstRate());
		target.setSalesTaxRate(source.getSalesTaxRate());
		target.setVatRate(source.getVatRate());
		target.setDiscount(source.getDiscount());
		target.setDiscountRate(source.getDiscountRate());
		target.setNetAmount(source.getNetAmount()); // Calculate Net amount*/
		
		
		target.setActive(source.getActive());
		RateCard updatedRateCard = new RateCard();
		updatedRateCard.setId(source.getRateCard().getId());
		target.setRateCard(updatedRateCard);
 		 		
		target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		target.setUpdatedBy(loggedInUser.get().getUserId());
		return true;
		
 		
	 }
        
}
