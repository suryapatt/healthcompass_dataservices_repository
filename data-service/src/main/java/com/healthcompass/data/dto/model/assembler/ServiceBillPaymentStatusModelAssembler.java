package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;
import com.healthcompass.data.view.dto.ServiceBillPaymentStatusViewDTO;
import com.healthcompass.data.view.dto.ServiceBillStatusViewDTO;

@Component
public class ServiceBillPaymentStatusModelAssembler extends RepresentationModelAssemblerSupport<ValueSet, ServiceBillPaymentStatusViewDTO> {
 
    public ServiceBillPaymentStatusModelAssembler() {
        super(ValueSetResource.class, ServiceBillPaymentStatusViewDTO.class);
    }
 
    
    @Override
    public ServiceBillPaymentStatusViewDTO toModel(ValueSet entity) 
    {
    	ServiceBillPaymentStatusViewDTO paymentStatus = instantiateModel(entity);
         
    	paymentStatus.add(linkTo(
                methodOn(ValueSetResource.class)
                .getValueSetId(entity.getId()))
                .withSelfRel());
         
    	paymentStatus.setId(entity.getId());
    	paymentStatus.setCode(entity.getCode());
        paymentStatus.setDisplay(entity.getDisplay());
        
        return paymentStatus;
    }
    
   /* private List<ServiceModel> toValueSetTypeModel(List<Service> services) {
        if (services.isEmpty())
            return Collections.emptyList();
 
        return services.stream()
                .map(service -> ServiceModel.builder()
                        .id(service.getId())
                        .localtionId(service.getLocaltionId())
                        .organizationId(service.getOrganizationId())
                        .build()
                        .add(linkTo(
                                methodOn(ServiceResource.class)
                                .getServiceId(service.getId())
                                .withSelfRel()))
                .collect(Collectors.toList()));
    }*/

}
