package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;


import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.MetaTagAttributes;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.SalesOrderItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class SalesOrderMainViewToEntityAssembler {
private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();
private static class OrderAction {
		
		enum ACTION {REMOVE,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		Order updatedOrder;
		ACTION action;
		
		OrderAction(){
			
		}
		OrderAction(ACTION action, Order updatedOrder){
			this.action=action;
			this.updatedOrder = updatedOrder;
		}
		public Order getUpdatedOrder() {
			return updatedOrder;
		}
		public void setUpdatedOrder(Order updatedOrder) {
			this.updatedOrder = updatedOrder;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		} 
		
	} // end inner class

private static class OrderItemAction {
	
	enum ACTION {REMOVE_FROM_ORDER,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
	OrderItem updatedOrderItem;
	ACTION action;
	
	OrderItemAction(){
		
	}
	OrderItemAction(ACTION action, OrderItem updatedOrderItem){
		this.action=action;
		this.updatedOrderItem = updatedOrderItem;
	}
	public OrderItem getUpdatedOrderItem() {
		return updatedOrderItem;
	}
	public void setUpdatedOrderItem(OrderItem updatedOrderItem) {
		this.updatedOrderItem = updatedOrderItem;
	}
	public ACTION getAction() {
		return action;
	}
	public void setAction(ACTION action) {
		this.action = action;
	}
}	
	private static class InventoryItemAction {
		
		enum ACTION {REMOVE_FROM_ORDER,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		InventoryItem updatedInventoryItem;
		ACTION action;
		
		InventoryItemAction(){
			
		}
		InventoryItemAction(ACTION action, InventoryItem updatedInventoryItem){
			this.action=action;
			this.updatedInventoryItem = updatedInventoryItem;
		}
		public InventoryItem getUpdatedInventoryItem() {
			return this.updatedInventoryItem;
		}
		public void setUpdatedupdatedInventoryItem(InventoryItem updatedupdatedInventoryItem) {
			this.updatedInventoryItem = updatedInventoryItem;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		}
	
} // end inner class
	
	public Order toEntity(SalesOrderMainViewDTO salesOrderMainViewDTO,Order salesOrder, List<InventoryItem> inventoryItems){
	
		try {
		 LoggedInUser user = new LoggedInUser();
		 user.setUserId(salesOrderMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : salesOrderMainViewDTO.getLoggedInUser());
		 
		 loggedInUser.set(user); // thread local for subsequent threads to provide loggged in user
		 
		 ArrayList<InventoryItem> newInventoryItemList = new ArrayList<InventoryItem>();
    
	      // update the Parent Order
		  updateOrder(salesOrder, salesOrderMainViewDTO.getOrder()); //same for new and edit
		  
		  // update/create order items
		  ArrayList<OrderItem> newOrderItemList = new ArrayList<OrderItem>();
	    	
	    	for(SalesOrderItemViewDTO orderItemViewDTO : salesOrderMainViewDTO.getOrder().getOrderItems()) {
	    		
	    		OrderItemAction actionOnOrderItem = updateExistingOrderItem(salesOrder, orderItemViewDTO.getOrderItemId(),orderItemViewDTO);;
	    		
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.DO_NOTHING) {
	    			continue;
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.REMOVE_FROM_ORDER) {
	    			OrderItem tobeDeletedItem = actionOnOrderItem.getUpdatedOrderItem();
	    			tobeDeletedItem.setOrder(null);
	    			salesOrder.getOrderItems().remove(tobeDeletedItem);
	    			continue;
	    			
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.UPDATE_EXISTING) {
	    			OrderItem targetOrderItem = actionOnOrderItem.getUpdatedOrderItem();
	    			associateInventoryItemToOrderItem(inventoryItems, orderItemViewDTO.getInventory().getInventoryId(), targetOrderItem);
	    			 
	    			boolean status = updateOrderItem(targetOrderItem, orderItemViewDTO);
	    			targetOrderItem.setOrder(salesOrder);
	    			
	    			// update the corresponding Inventory Item hereInventory Item here
	    			continue;
	    			
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.CREATE_NEW) {
	    			OrderItem updatedItem = actionOnOrderItem.getUpdatedOrderItem();
	    			
	    			OrderItem oItem = new OrderItem();
	    			oItem.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    			oItem.setCreatedBy(salesOrderMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : salesOrderMainViewDTO.getLoggedInUser());
	    			Product product = new Product();
	    			oItem.setProduct(product);
	    			associateInventoryItemToOrderItem(inventoryItems, orderItemViewDTO.getInventory().getInventoryId(), oItem);
	    			boolean isNewCreated = updateOrderItem(oItem, orderItemViewDTO);
	    			if(isNewCreated) {
	    				oItem.setOrder(salesOrder);
	    				newOrderItemList.add(oItem);
	    			}
	    			
	    			//create new Inventory Item here
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	
	    	
	    	//serviceBill.getServiceBillItems().clear();
	    	salesOrder.getOrderItems().addAll(newOrderItemList);
	    	salesOrder.setOrderItems(salesOrder.getOrderItems());
  		
  		
  		// remove orphan OrderItems
  		for (OrderItem orderItem : salesOrder.getOrderItems()) {
  			
  			if(isOrderItemOrphan(orderItem,salesOrderMainViewDTO.getOrder().getOrderItems())){
  				orderItem.setOrder(null);
  				//purchaseOrder.getOrderItems().remove(orderItem);
  				
  			}
  					    
  					   
  		}
  		
  		  		
	   
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			loggedInUser.remove();
		}
		 return salesOrder;	
		}	 
	  
    	 
	private boolean isOrderItemOrphan(OrderItem orderItem, List<SalesOrderItemViewDTO> sourceItems){
			boolean isOrphan = false;
			if(orderItem == null  || sourceItems == null ) {
			    isOrphan=false;
				return isOrphan;
			}
		   
		    
		    if(orderItem.getId() == null) {
		    	isOrphan=false;
		    	return isOrphan;
		    }
			Optional<SalesOrderItemViewDTO> optSalesOrderItemViewDTO =  sourceItems.stream()
					.filter(salesOrderItemView -> (salesOrderItemView.getOrderItemId() != null) && (salesOrderItemView.getOrderItemId().equals(orderItem.getId()))).findFirst();

					    if(!optSalesOrderItemViewDTO.isPresent())  //check this logic where the chilkd
					    	isOrphan=true;
					    	return isOrphan;
			
		}
	
	private boolean isInventoryItemOrphan(InventoryItem inventoryItem, UUID inventoryId, UUID purchaseOrderId,List<PurchaseOrderItemViewDTO> sourceItems){
		boolean isOrphan = false;
		if(inventoryId == null) {
			isOrphan=false;
			return isOrphan;
		}
		if(purchaseOrderId == null) {
			isOrphan=false;
			return isOrphan;
		}
		if(inventoryItem.getOrder() == null) {
			isOrphan=true;
			return isOrphan;
		}
		UUID existingInventoryOrderId = inventoryItem.getOrder().getId();
		Optional<PurchaseOrderItemViewDTO> optPurchaseOrderItemViewDTO =  sourceItems.stream()
				.filter(purchaseOrderItemView -> ((purchaseOrderItemView.getProduct().getProductId().compareTo(inventoryItem.getProduct().getId())==0)) && (inventoryItem.getBatchNumber().equals(purchaseOrderItemView.getBatchNumber()) && purchaseOrderId.equals(existingInventoryOrderId))).findFirst();

				    if(!optPurchaseOrderItemViewDTO.isPresent())  //check this logic where the chilkd
				    	isOrphan=true;
		return isOrphan;
		
	}
 
	
	 
	 private boolean updateOrder(Order target, SalesOrderViewDTO source) {
		 
		target.setId(source.getSalesOrderId());
	    target.getOrganization().setId(source.getOrganizationId()); 
		target.getLocation().setId(source.getLocationId());
		target.setOrderValue(source.getOrderValue());
		target.setNetOrderValue(source.getNetOrderValue());
		target.setDiscount(source.getDiscount());
		target.setOrderComment(source.getOrderComment());
		target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		target.setUpdatedBy(loggedInUser.get().getUserId());
		return true;
		
 		
	 }
	 
	 public OrderItemAction updateExistingOrderItem(Order salesOrder, UUID orderItemId , SalesOrderItemViewDTO updatedSalesItemViewDTO) {
		 	OrderItemAction orderItemAction = new OrderItemAction();
		    if(salesOrder == null  || updatedSalesItemViewDTO == null)
		        return new OrderItemAction(orderItemAction.action.DO_NOTHING, null);
		    
		    if(salesOrder == null)
		    	return new OrderItemAction(orderItemAction.action.DO_NOTHING, null);
		    
		    if(orderItemId== null) {
		    	
		    		return new OrderItemAction(orderItemAction.action.CREATE_NEW, null);
		    		
		    }

		    Optional<OrderItem> optTargetOrderItem = salesOrder.getOrderItems().stream()
		.filter(orderItem -> orderItem.getId().equals(orderItemId)).findFirst();

		    if(!optTargetOrderItem.isPresent()) {
		    	
		    	return new OrderItemAction(orderItemAction.action.CREATE_NEW, null);
		    } else{
		    	OrderItem targetOrderItem = optTargetOrderItem.get();
		    	return new OrderItemAction(orderItemAction.action.UPDATE_EXISTING, targetOrderItem);
		    }
		    
		    
		  
		   

		    
		   
		}
        
	 private boolean updateOrderItem(OrderItem target, SalesOrderItemViewDTO sourceSalesOrderViewItem) {
		 
		 target.setId(sourceSalesOrderViewItem.getOrderItemId());
		 target.setBatchNumber(target.getInventory().getBatchNumber());
		 target.setExpiryDate((target.getInventory().getExpiryDate()));
		// target.setCentralTaxRate(sourceSalesOrderViewItem.getCentralTaxRate());
		// target.setSalesTaxRate(sourceSalesOrderViewItem.getSalesTaxRate());
		// target.setVatRate(sourceSalesOrderViewItem.getVatRate());
		// target.setGstRate(sourceSalesOrderViewItem.getGstRate());
		 target.setRate(sourceSalesOrderViewItem.getRate()); 
		 target.setQuantity(sourceSalesOrderViewItem.getQuantity());
		 target.setMrp(sourceSalesOrderViewItem.getItemValue());  // will redo calculations later
		 target.setDiscountedRate(sourceSalesOrderViewItem.getDiscountedRate()); // will redo calculations later
		 target.setDiscount(sourceSalesOrderViewItem.getDiscount()); // will redo calculations later
		 target.setNetItemValue(sourceSalesOrderViewItem.getNetItemValue()); // will redo calculations later
		 Integer availableUnits = target.getInventory().getAvailableUnits();
		 availableUnits-=sourceSalesOrderViewItem.getQuantity(); // will redo calculations after checking orphan order items and orphan inventory items which were earlier associated during creation but removed in edit
		 target.getInventory().setAvailableUnits(availableUnits);
		 target.getInventory().setUpdatedBy(loggedInUser.get().getUserId());
		 target.getInventory().setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		 Product prod = new Product();
		 prod.setId(sourceSalesOrderViewItem.getProduct().getProductId());
		 target.setProduct(prod);
		 target.setProductName(sourceSalesOrderViewItem.getProductName());
		 target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		 target.setUpdatedBy(loggedInUser.get().getUserId());
		 return true;
		
 		
	 }
	 
	 public OrderItem  associateInventoryItemToOrderItem(List<InventoryItem> inventoryItemList, UUID inventoryId ,OrderItem updatedOrderItem) {
		 

		    Optional<InventoryItem> optTargetInventoryItem = inventoryItemList.stream()
		.filter(inventoryItem -> (inventoryItem.getId().equals(inventoryId))).findFirst();

		    if(optTargetInventoryItem.isPresent())  {
		    	InventoryItem targetInventoryItem = optTargetInventoryItem.get();
		    	updatedOrderItem.setInventory(targetInventoryItem);
		    }//check this logic where the chilkd
		    	
		    return updatedOrderItem;
		  		    
		    
		   

		   
		}
	 
 private boolean updateInventoryItem(InventoryItem target, OrderItem sourceOrderItem) {
		 
		
	    // target.getOrder().setId(sourceOrderItem.getOrder().getId());
	 	 target.getProduct().setId(sourceOrderItem.getProduct().getId());
	 	 target.getOrganization().setId(sourceOrderItem.getOrder().getOrganization().getId());
	 	 target.getLocation().setId(sourceOrderItem.getOrder().getLocation().getId());
	 	 target.setBatchNumber(sourceOrderItem.getBatchNumber());
		 target.setExpiryDate(sourceOrderItem.getExpiryDate());
		 target.setAvailableUnits(sourceOrderItem.getQuantity());
		 // check with Arvind on below calulations
		// Double mrpPerUnit = sourceOrderItem.getMrp()/sourceOrderItem.getQuantity(); calculated during save
		// target.setMrpPerUnit(mrpPerUnit);
		// target.setUnitRate(sourceOrderItem.getRate()); // calculated during save
		// calculated during save target.setMaxDiscount(sourceOrderItem.getDiscountedRate());  // Check if it is order item discount or discount rate
		 target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		 target.setUpdatedBy(loggedInUser.get().getUserId());
		 return true;
		 
		
		
 		
	 }
     
}
