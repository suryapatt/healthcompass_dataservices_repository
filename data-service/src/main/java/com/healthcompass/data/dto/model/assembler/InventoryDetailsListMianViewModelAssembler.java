package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.InventorySummaryResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.SupplierResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.InventoryDetailsListMainViewDTO;
import com.healthcompass.data.view.dto.InventoryDetailsViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryListMainViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class InventoryDetailsListMianViewModelAssembler extends RepresentationModelAssemblerSupport<InventoryItem, InventoryDetailsListMainViewDTO> {
 
	 @Autowired
	 InventoryDetailsViewModelAssembler InventoryDetailsViewModelAssembler;
	 
	 
	 
	 public InventoryDetailsListMianViewModelAssembler() {
	        super(InventorySummaryResource.class, InventoryDetailsListMainViewDTO.class);
	    }   
   
  
  
    @Override
    public InventoryDetailsListMainViewDTO toModel(InventoryItem entity) 
     {
    	
    	InventoryDetailsListMainViewDTO inventoryDetailsListMainViewDTO = new InventoryDetailsListMainViewDTO();
    	
    	 	
         
    
    	inventoryDetailsListMainViewDTO.setOrganizationId(entity.getOrganization().getId());
    	inventoryDetailsListMainViewDTO.setOrganizationName(entity.getOrganization().getName());
    	inventoryDetailsListMainViewDTO.setLocationId(entity.getLocation().getId());
    	inventoryDetailsListMainViewDTO.setLocationName(entity.getLocation().getName());
    	
    	
    	    	    	
        return inventoryDetailsListMainViewDTO;
    }
    
        
    @Override
    public CollectionModel<InventoryDetailsListMainViewDTO> toCollectionModel(Iterable<? extends InventoryItem> entities) 
    {
        CollectionModel<InventoryDetailsListMainViewDTO> inventoryDetails = super.toCollectionModel(entities);
               
         
        return inventoryDetails;
    }
 
    public List<InventoryDetailsViewDTO> toInventoryDetailsViewModel(List<InventoryItem> inventoryItems) {
		if (inventoryItems.isEmpty())
			return Collections.emptyList();
		List<InventoryDetailsViewDTO> inventoryItemsList = new ArrayList<InventoryDetailsViewDTO>();
         for(InventoryItem inventoryItem : inventoryItems) {
        	 InventoryDetailsViewDTO inventoryDetailsViewDTO = new InventoryDetailsViewDTO();
        	 inventoryDetailsViewDTO.setOrganizationId(inventoryItem.getOrganization().getId());
        	 inventoryDetailsViewDTO.setOrganizationName(inventoryItem.getOrganization().getName());
        	 inventoryDetailsViewDTO.setLocationId(inventoryItem.getLocation().getId());
        	 inventoryDetailsViewDTO.setLocationName(inventoryItem.getLocation().getName());
        	 inventoryDetailsViewDTO.setInventoryId(inventoryItem.getId());
        	 inventoryDetailsViewDTO.setProductId(inventoryItem.getProduct().getId());
        	 inventoryDetailsViewDTO.setProductName(inventoryItem.getProduct().getName());
        	 inventoryDetailsViewDTO.setAvailableUnits(inventoryItem.getAvailableUnits());
        	 inventoryDetailsViewDTO.setUnitRate(inventoryItem.getUnitRate());
        	 inventoryDetailsViewDTO.setReOrderLevel(inventoryItem.getProduct().getReOrderLevel());
        	 inventoryDetailsViewDTO.setLeadTime(inventoryItem.getProduct().getLeadTime());
        	 inventoryDetailsViewDTO.setExpiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(inventoryItem.getExpiryDate()));
        	 inventoryDetailsViewDTO.setBatchNumber(inventoryItem.getBatchNumber());
        	
        	 inventoryItemsList.add(inventoryDetailsViewDTO);
         }
		
		return inventoryItemsList;
	
		
				
	}

}
