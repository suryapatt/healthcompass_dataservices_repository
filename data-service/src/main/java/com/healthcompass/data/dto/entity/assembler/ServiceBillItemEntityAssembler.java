package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ServiceBillItemResource;


@Component
public class ServiceBillItemEntityAssembler  {
 
	 @Autowired
	 RateCardEntityAssembler rateCardEntityAssembler;
	 
    public ServiceBillItemEntityAssembler() {
        
    }
    
    
    
   
 

    public ServiceBillItem toEntity(ServiceBillItemModel serviceBillItemModel,ServiceBillItem serviceBillItem) 
    {
    	if( serviceBillItemModel == null) return null;
        BeanUtils.copyProperties(serviceBillItemModel, serviceBillItem, "rateCard","serviceBill","_links"); //ignore valueSetType
        // REMOVE SERVICE AND SET RATE
        serviceBillItem.setRateCard(rateCardEntityAssembler.toEntity(serviceBillItemModel.getRateCard(), new RateCard()));
        serviceBillItem.setServiceBill(null);
         	
        return serviceBillItem;
    }
    
    public List<ServiceBillItem> toEntityCollection(List<ServiceBillItemModel> serviceBillItemModels,List<ServiceBillItem> serviceBillItems){
    	
    	
    	for(ServiceBillItemModel serviceBillItemModel : serviceBillItemModels ) {
    		serviceBillItems.add(toEntity(serviceBillItemModel, new ServiceBillItem()));
    	}
    	
    	return serviceBillItems;
    }
     
   

}
