package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PatientResource;
import com.healthcompass.data.resource.PurchaseOrderResource;
import com.healthcompass.data.resource.SalesOrderResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.SalesOrderInventoryItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderItemViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProductViewDTO;
import com.healthcompass.data.view.dto.SalesOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;

@Component
public class SalesOrderViewModelAssembler extends RepresentationModelAssemblerSupport<Order, SalesOrderViewDTO> {
 
	
	 
    public SalesOrderViewModelAssembler() {
        super(SalesOrderResource.class, SalesOrderViewDTO.class);
    }
   
  
   @Override
    public SalesOrderViewDTO toModel(Order entity) 
    {
	   SalesOrderViewDTO salesOrderViewDTO = instantiateModel(entity);
         
	   salesOrderViewDTO.add(linkTo(
                methodOn(SalesOrderResource.class)
                .getSalesOrder(entity.getId()))
                .withSelfRel());
         
	   salesOrderViewDTO.setSalesOrderId(entity.getId());
	   salesOrderViewDTO.setOrganizationId(entity.getOrganization().getId());
	   salesOrderViewDTO.setLocationId(entity.getLocation().getId());
	   
	   
	   salesOrderViewDTO.setOrderNumber(entity.getOrderNumber());
	   salesOrderViewDTO.setOrderDate(entity.getOrderDate());
	   
	   salesOrderViewDTO.setOrderValue(entity.getOrderValue());
	   salesOrderViewDTO.setDiscount(entity.getDiscount());
	   salesOrderViewDTO.setNetOrderValue(entity.getNetOrderValue());
	 
	   salesOrderViewDTO.setOrderComment(entity.getOrderComment());
	   salesOrderViewDTO.setAttachementId(entity.getAttachmentId());
	  
	   salesOrderViewDTO.setOrderItems(toSalesOrderItemsModel(entity.getOrderItems()));
      
       
       
        return salesOrderViewDTO;
    }
     
    @Override
    public CollectionModel<SalesOrderViewDTO> toCollectionModel(Iterable<? extends Order> entities) 
    {
        CollectionModel<SalesOrderViewDTO> salesOrderModels = super.toCollectionModel(entities);
         
        //serviceBillModels.add(linkTo(methodOn(ServiceBillResource.class).getAllServiceBills()).withSelfRel());
         
        return salesOrderModels;
    }
 
    private List<SalesOrderItemViewDTO> toSalesOrderItemsModel(List<OrderItem> orderItems) {
    	
		if (orderItems.isEmpty())
			return Collections.emptyList();
		/* Optional<ServiceBillItem> optTargetServiceBillItem = serviceBill.getServiceBillItems().stream()
					.filter(billItem -> billItem.getId().equals(billItemId)).findFirst();*/
		SalesOrderInventoryItemViewDTO  inventoryItem = new SalesOrderInventoryItemViewDTO();
		
		return orderItems.stream().filter(orderItem -> orderItem != null)
				.map(orderItem -> SalesOrderItemViewDTO.builder()
						.orderItemId(orderItem.getId())
						.inventory(toSalesOrderInventoryViewModel(orderItem.getInventory()))
						.batchNumber(orderItem.getBatchNumber())
						//.expiryDate(orderItem.getExpiryDate())
						.expiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(orderItem.getExpiryDate()))
						.rate(orderItem.getRate())
						.quantity(orderItem.getQuantity())
						.itemValue(orderItem.getMrp())
						.netItemValue(orderItem.getNetItemValue())
						.discount(orderItem.getDiscount())
						.discountedRate(orderItem.getDiscountedRate())
						.productName(orderItem.getProductName())
						//.gstRate(orderItem.getGstRate())
						//.centralTaxRate(orderItem.getCentralTaxRate())
						//.salesTaxRate(orderItem.getSalesTaxRate())
						//.vatRate(orderItem.getVatRate())
						.product(toSalesOrderProductViewModel(orderItem.getProduct()))
						.build())
						.collect(Collectors.toList());
		
		
		
		
				
	}
   
    public SalesOrderProductViewDTO toSalesOrderProductViewModel(Product salesOrderProduct) 
    {
    	SalesOrderProductViewDTO salesOrderProductView = new SalesOrderProductViewDTO();
    	salesOrderProductView.setProductId(salesOrderProduct.getId());
    	salesOrderProductView.setProductName(salesOrderProduct.getName());
    	salesOrderProductView.setProductDescription(salesOrderProduct.getDescription());
	  
	        
       
        return salesOrderProductView;
    }
    
    public SalesOrderInventoryItemViewDTO toSalesOrderInventoryViewModel(InventoryItem salesOrderInventory) 
    {
    	SalesOrderInventoryItemViewDTO salesOrderInventoryView = new SalesOrderInventoryItemViewDTO();
    	salesOrderInventoryView.setInventoryId(salesOrderInventory.getId());
    	salesOrderInventoryView.setBatchNumber(salesOrderInventory.getBatchNumber());
    	salesOrderInventoryView.setExpiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(salesOrderInventory.getExpiryDate()));
    	salesOrderInventoryView.setAvailableUnits(salesOrderInventory.getAvailableUnits());
    	
        return salesOrderInventoryView;
    }

}
