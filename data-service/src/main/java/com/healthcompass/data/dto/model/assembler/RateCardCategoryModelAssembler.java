package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.RateCardCategoryModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.RateCardCategoryResource;

@Component
public class RateCardCategoryModelAssembler extends RepresentationModelAssemblerSupport<RateCardCategory, RateCardCategoryModel> {
 
    public RateCardCategoryModelAssembler() {
        super(RateCardCategoryResource.class, RateCardCategoryModel.class);
    }
    
    @Autowired
    ValueSetModelAssembler valueSetModelAssembler;
 
    @Override
    public RateCardCategoryModel toModel(RateCardCategory entity) 
    {
    	RateCardCategoryModel rateCardCategoryModel = instantiateModel(entity);
         
    	rateCardCategoryModel.add(linkTo(
                methodOn(RateCardCategoryResource.class)
                .getRateCardCategory(entity.getId()))
                .withSelfRel());
         
    	rateCardCategoryModel.setId(entity.getId());
    	rateCardCategoryModel.setCode(entity.getCode());
    	rateCardCategoryModel.setDescription(entity.getDescription());
    	rateCardCategoryModel.setCreatedOn(entity.getCreatedOn());
        rateCardCategoryModel.setCreatedBy(entity.getCreatedBy());
        rateCardCategoryModel.setUpdatedOn(entity.getUpdatedOn());
        rateCardCategoryModel.setUpdatedBy(entity.getUpdatedBy());
        return rateCardCategoryModel;
    }
     
    @Override
    public CollectionModel<RateCardCategoryModel> toCollectionModel(Iterable<? extends RateCardCategory> entities) 
    {
        CollectionModel<RateCardCategoryModel> rateCardCategoryModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return rateCardCategoryModels;
    }
 
   /* private List<ServiceModel> toServiceModel(List<Service> services) {
        if (services.isEmpty())
            return Collections.emptyList();
 
        return services.stream()
                .map(service -> ServiceModel.builder()
                        .id(service.getId())
                        .locationId(service.getLocationId())
                        .organizationId(service.getOrganizationId())
                        .build()
                        .add(linkTo(
                                methodOn(ServiceResource.class)
                                .getService(service.getId()))
                                .withSelfRel()))
                .collect(Collectors.toList());
    }*/

}
