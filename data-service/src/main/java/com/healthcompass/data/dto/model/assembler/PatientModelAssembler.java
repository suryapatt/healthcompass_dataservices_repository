package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PatientResource;


@Component
public class PatientModelAssembler extends RepresentationModelAssemblerSupport<Patient, PatientModel> {
 
    public PatientModelAssembler() {
        super(PatientResource.class,PatientModel.class);
    }
    
    @Autowired
    OrganizationModelAssembler organizationModelAssmbler;
 
    @Override
    public PatientModel toModel(Patient entity) 
    {
    	PatientModel patientModel = instantiateModel(entity);
         
    	patientModel.add(linkTo(
                methodOn(PatientResource.class)
                .getPatient(entity.getId()))
                .withSelfRel());
         
    	patientModel.setId(entity.getId());
    	patientModel.setName(entity.getDecodedName());
    	//patientModel.setOrganization(organizationModelAssmbler.toModel(entity.getOrganization()));
    	patientModel.setCreatedBy(entity.getCreatedBy());
    	patientModel.setUpdatedBy(entity.getUpdatedBy());
    	patientModel.setCreatedOn(entity.getCreatedOn());
    	patientModel.setUpdatedOn(entity.getUpdatedOn());
       
        return patientModel;
    }
     
    @Override
    public CollectionModel<PatientModel> toCollectionModel(Iterable<? extends Patient> entities) 
    {
        CollectionModel<PatientModel> patientModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return patientModels;
    }
 
    
}
