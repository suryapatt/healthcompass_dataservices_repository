package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PractitionerRoleModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PractitionerRoleResource;


@Component
public class PractitionerRoleModelAssmebler extends RepresentationModelAssemblerSupport<PractitionerRole, PractitionerRoleModel> {
 
    public PractitionerRoleModelAssmebler() {
        super(PractitionerRoleResource.class, PractitionerRoleModel.class);
    }
    
    @Autowired
    OrganizationModelAssembler organizationModelAssembler;
    
    @Autowired
    PractitionerModelAssembler practitionerModelAssembler;
 
    @Override
    public PractitionerRoleModel toModel(PractitionerRole entity) 
    {
    	PractitionerRoleModel practitionerRoleModel = instantiateModel(entity);
         
    	practitionerRoleModel.add(linkTo(
                methodOn(PractitionerRoleResource.class)
                .getPractitionerRole(entity.getId()))
                .withSelfRel());
         
    	practitionerRoleModel.setId(entity.getId());
    	practitionerRoleModel.setCode(entity.getCode());
    	//practitionerRoleModel.setPractitioners(practitionerModelAssembler.toCollectionModel(entity.getPractitioners()));
    	//practitionerRoleModel.setOrganization(organizationModelAssembler.toModel(entity.getOrganization()));
    	practitionerRoleModel.setCreatedBy(entity.getCreatedBy());
    	practitionerRoleModel.setUpdatedBy(entity.getUpdatedBy());
    	practitionerRoleModel.setCreatedOn(entity.getCreatedOn());
    	practitionerRoleModel.setUpdatedOn(entity.getUpdatedOn());
       
        return practitionerRoleModel;
    }
     
    @Override
    public CollectionModel<PractitionerRoleModel> toCollectionModel(Iterable<? extends PractitionerRole> entities) 
    {
        CollectionModel<PractitionerRoleModel> practitionerRoleModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return practitionerRoleModels;
    }
 
   

}
