package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.RateCardCategoryModel;
import com.healthcompass.data.dto.model.RateCardModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.RateCardResource;




@Component
public class RateCardEntityAssembler {
 
	@Autowired
	LocationEntityAssembler locationEntityAssembler;
	
	@Autowired
	RateCardCategoryEntityAssembler rateCardCategoryEntityAssembler;
	
	public RateCard  toEntity(RateCardModel rateCardModel,RateCard rateCard) 
    {
    	if( rateCardModel == null) return null;
        BeanUtils.copyProperties(rateCardModel, rateCard,"rateCardCategory","location", "_links"); //ignore valueSetType
       // rateCard.setLocation(locationEntityAssembler.toEntity(rateCardModel.getLocation(),rateCard.getLocation()));
        rateCard.setRateCardCategory(rateCardCategoryEntityAssembler.toEntity(rateCardModel.getRateCardCategory(),new RateCardCategory()));
        return rateCard;
    }
	
	 public List<RateCard> toEntityCollection(List<RateCardModel> rateCardModels,List<RateCard> rateCards){
	    	
	    	
	    	for(RateCardModel rateCardModel : rateCardModels ) {
	    		rateCards.add(toEntity(rateCardModel, new RateCard()));
	    	}
	    	
	    	return rateCards;
	    }

}
