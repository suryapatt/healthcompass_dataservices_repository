package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.InventorySummaryResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.SupplierResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.InventoryDetailsViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class InventoryDetailsViewModelAssembler extends RepresentationModelAssemblerSupport<InventoryItem, InventoryDetailsViewDTO> {
 
	 
	 public InventoryDetailsViewModelAssembler() {
	        super(InventorySummaryResource.class, InventoryDetailsViewDTO.class);
	    }   
   
  
    @Override
    public InventoryDetailsViewDTO toModel(InventoryItem entity) 
     {
    	
    	InventoryDetailsViewDTO inventoryDetails = new InventoryDetailsViewDTO();
    	
    	inventoryDetails.setInventoryId(entity.getId());
    	inventoryDetails.setProductId(entity.getProduct().getId());
    	inventoryDetails.setProductName(entity.getProduct().getName());
    	inventoryDetails.setAvailableUnits(entity.getAvailableUnits());
    	inventoryDetails.setUnitRate(entity.getUnitRate());
    	inventoryDetails.setBatchNumber(entity.getBatchNumber());
    	inventoryDetails.setExpiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(entity.getExpiryDate()));
    	
    	return inventoryDetails;
     
    }
    
     
    @Override
    public CollectionModel<InventoryDetailsViewDTO> toCollectionModel(Iterable<? extends InventoryItem> entities) 
    {
        CollectionModel<InventoryDetailsViewDTO> inventoryList = super.toCollectionModel(entities);
         
        return inventoryList;
    }
 
   
   
   

}
