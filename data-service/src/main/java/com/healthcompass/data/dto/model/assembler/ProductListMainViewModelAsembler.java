package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.MetaTagAttributes;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ProductResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class ProductListMainViewModelAsembler extends RepresentationModelAssemblerSupport<Product, ProductListMainViewDTO> {
 
	 @Autowired
	 ServiceBillModelAssembler serviceBillModelAssembler;
	 
	 @Autowired
	 SpecialityViewAssembler specialityViewAssembler;
	 
	 public ProductListMainViewModelAsembler() {
	        super(ProductResource.class, ProductListMainViewDTO.class);
	    }   
   
  
  
    @Override
    public ProductListMainViewDTO toModel(Product entity) 
     {
    	
      	return null;
    }
    
    
    
     
    @Override
    public CollectionModel<ProductListMainViewDTO> toCollectionModel(Iterable<? extends Product> entities) 
    {
       /* CollectionModel<ProductListMainViewDTO> productModels = super.toCollectionModel(entities);
        return productModels;*/
    	return null;
    }
 
   
    public ProductListMainViewDTO toProductsModel(List<Product> entities) 
    {
    	ProductListMainViewDTO productListMainViewDTO = null;
    	ArrayList<ProductViewDTO> productsView = new ArrayList<ProductViewDTO>();
    	List<MetaTagAttributesViewDTO> metaTagAttributesView =  new ArrayList<MetaTagAttributesViewDTO>();
         boolean isOrganizationInformationSet = false;
         boolean isProductInfoSet=false;
         boolean isMetaTagAttributeInfoSet=false;
              
         ProductViewDTO newProduct = new ProductViewDTO();
         for(Product product : entities) {
        	 if(!isOrganizationInformationSet) {
        		 productListMainViewDTO = new  ProductListMainViewDTO();
        		 productListMainViewDTO.setOrganizationId(product.getMarketer().getOrganization().getId());
        		 productListMainViewDTO.setOrganizationName(product.getMarketer().getOrganization().getName());
        		 
        		 isOrganizationInformationSet=true;		 
        	 }
        	 
          	
        	 newProduct = new ProductViewDTO();
    		 newProduct.setProductId(product.getId());
    		 newProduct.setMarketerId(product.getMarketer().getId());
    		 newProduct.setMarketerName(product.getMarketer().getName());
    		 newProduct.setName(product.getName());
    		 newProduct.setDescription(product.getDescription());
    		// newProduct.setMoleculeId(product.getMolecule());
    		// newProduct.setHsnCode(product.getHsnCode());
    		 //newProduct.setUnits(product.getUnits());
    		// newProduct.setPack(product.getPack());
    		 //newProduct.setScheduleDrugCode(product.getScheduleDrugCode());
    		 //newProduct.setGstRate(product.getGstRate());
    		// newProduct.setCentralTaxRate(product.getCentralTaxRate());
    		 //newProduct.setSalesTaxRate(product.getSalesTaxRate());
    		 //newProduct.setVatRate(product.getVatRate());
    		 newProduct.setReOrderLevel(product.getReOrderLevel());
    		 newProduct.setLeadTime(product.getLeadTime());
    		 newProduct.setActive(product.getActive());
        	 metaTagAttributesView = new ArrayList<MetaTagAttributesViewDTO>();
        	 for(MetaTagAttributes metaTagAttribute : product.getMetaTagAttributes()) {
        		 if(metaTagAttribute.getActive()){

             		MetaTagAttributesViewDTO metaTagAttributeView = new MetaTagAttributesViewDTO();
             		metaTagAttributeView.setActive(metaTagAttribute.getActive());
             		metaTagAttributeView.setAttributeId(metaTagAttribute.getId());
             		metaTagAttributeView.setAttrubuteValue(metaTagAttribute.getValue());
     				MetaTagViewDTO metaTag = new MetaTagViewDTO();
     				metaTag.setMetaTagId(metaTagAttribute.getMetaTag().getId());
     				metaTag.setMetaTagName(metaTagAttribute.getMetaTag().getName());
     				metaTagAttributeView.setMetaTag(metaTag);
     				metaTagAttributesView.add(metaTagAttributeView);
     					        		
             	}
        	 }
        	 newProduct.setMetaTagAttributes(metaTagAttributesView);
        	 productsView.add(newProduct);
        	 
         productListMainViewDTO.setProducts(productsView);
        
    }
         return  productListMainViewDTO;
    }
   
   

}
