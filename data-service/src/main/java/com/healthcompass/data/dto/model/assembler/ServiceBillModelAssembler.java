package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PatientResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;

@Component
public class ServiceBillModelAssembler extends RepresentationModelAssemblerSupport<ServiceBill, ServiceBillViewDTO> {
 
	@Autowired
	ServiceBillPaymentModeViewAssembler serviceBillPaymentModeViewAssembler;
	
	@Autowired
	ServiceBillStatusViewAssembler serviceBillStatusViewAssembler;
	 
	@Autowired
	ServiceBillItemModelAssembler serviceBillItemModelAssembler;
	
	 @Autowired
	 RateCardModelAssembler rateCardModelAssembler;
	 
	 @Autowired
	 ServiceBillPaymentStatusModelAssembler serviceBillPaymentStatusModelAssembler;
	 
    public ServiceBillModelAssembler() {
        super(ServiceBillResource.class, ServiceBillViewDTO.class);
    }
   
  
   @Override
    public ServiceBillViewDTO toModel(ServiceBill entity) 
    {
	   ServiceBillViewDTO serviceBillViewDTO = instantiateModel(entity);
         
	   serviceBillViewDTO.add(linkTo(
                methodOn(ServiceBillResource.class)
                .getServiceBill(entity.getId()))
                .withSelfRel());
         
	   serviceBillViewDTO.setServiceBillId(entity.getId());
	   serviceBillViewDTO.setSubTotal(entity.getSubTotal());
	   serviceBillViewDTO.setDiscountRate(entity.getDiscountRate());
	   serviceBillViewDTO.setDiscount(entity.getDiscount());
	   serviceBillViewDTO.setAmountPaid(entity.getAmountPaid());
	   serviceBillViewDTO.setAmountBilled(entity.getAmountBilled());
	   serviceBillViewDTO.setAmountDue(entity.getAmountDue());
	   serviceBillViewDTO.setBillDate(entity.getBillDate());
	   serviceBillViewDTO.setBillingDoneBy(entity.getBillingDoneBy());
	   serviceBillViewDTO.setEncounterId(entity.getEncounter().getId());
       serviceBillViewDTO.setPatientId(entity.getPatient().getId());
       serviceBillViewDTO.setPractitionerRoleId(entity.getPractitionerRole().getId());
    	if(entity.getStatus() != null) {
    		serviceBillViewDTO.setStatus(serviceBillStatusViewAssembler.toModel(entity.getStatus()));
    	}
    	if(entity.getPaymentMode() != null) {
          serviceBillViewDTO.setPaymentMode(serviceBillPaymentModeViewAssembler.toModel(entity.getPaymentMode()));
    	}
    	if(entity.getPaymentStatus() != null) {
            serviceBillViewDTO.setPaymentStatus(serviceBillPaymentStatusModelAssembler.toModel(entity.getPaymentStatus()));
      	}
    	
    	if(entity.getAttachmentId() != null) {
            serviceBillViewDTO.setAttachmentId(entity.getAttachmentId());
      	}
       serviceBillViewDTO.setActive(entity.getActive()); 
      // serviceBillViewDTO.setServiceBillItems((List<ServiceBillItemViewDTO>) serviceBillItemModelAssembler.toCollectionModel(entity.getServiceBillItems()));
       serviceBillViewDTO.setServiceBillItems(toServiceBillItemsModel(entity.getServiceBillItems()));
      
       
       
        return serviceBillViewDTO;
    }
     
    @Override
    public CollectionModel<ServiceBillViewDTO> toCollectionModel(Iterable<? extends ServiceBill> entities) 
    {
        CollectionModel<ServiceBillViewDTO> serviceBillModels = super.toCollectionModel(entities);
         
        //serviceBillModels.add(linkTo(methodOn(ServiceBillResource.class).getAllServiceBills()).withSelfRel());
         
        return serviceBillModels;
    }
 
    private List<ServiceBillItemViewDTO> toServiceBillItemsModel(List<ServiceBillItem> serviceBillItems) {
		if (serviceBillItems.isEmpty())
			return Collections.emptyList();
		/* Optional<ServiceBillItem> optTargetServiceBillItem = serviceBill.getServiceBillItems().stream()
					.filter(billItem -> billItem.getId().equals(billItemId)).findFirst();*/

		return serviceBillItems.stream().filter(serviceBillItem -> serviceBillItem.getActive() == true)
				.map(serviceBillItem -> ServiceBillItemViewDTO.builder()
						.serviceBillItemId(serviceBillItem.getId())
						.rate(serviceBillItem.getRate())
						.quantity(serviceBillItem.getQuantity())
						.amount(serviceBillItem.getAmount())
						//.discount(serviceBillItem.getDiscount())
						//.discountRate(serviceBillItem.getDiscountRate())
						//.gstRate(serviceBillItem.getGstRate())
						//.netAmount(serviceBillItem.getNetAmount())
						//.centralTaxRate(serviceBillItem.getCentralTaxRate())
						//.salesTaxRate(serviceBillItem.getSalesTaxRate())
						//.vatRate(serviceBillItem.getVatRate())
						.active(serviceBillItem.getActive())
						.quantity(serviceBillItem.getQuantity())
						.rateCard(rateCardModelAssembler.toModel(serviceBillItem.getRateCard()))
						.build()
						.add(linkTo(
								methodOn(ServiceBillItemResource.class)
								.getServiceBillItem(serviceBillItem.getId()))
								.withSelfRel()))
						.collect(Collectors.toList());
		
		
		
		
				
	}
   

}
