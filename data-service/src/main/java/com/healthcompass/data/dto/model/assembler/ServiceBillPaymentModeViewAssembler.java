package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.view.dto.ServiceBillPaymentModeViewDTO;

@Component
public class ServiceBillPaymentModeViewAssembler extends RepresentationModelAssemblerSupport<ValueSet, ServiceBillPaymentModeViewDTO> {
 
    public ServiceBillPaymentModeViewAssembler() {
        super(ValueSetResource.class, ServiceBillPaymentModeViewDTO.class);
    }
 
    
    @Override
    public ServiceBillPaymentModeViewDTO toModel(ValueSet entity) 
    {
    	ServiceBillPaymentModeViewDTO paymentMode = instantiateModel(entity);
         
    	paymentMode.add(linkTo(
                methodOn(ValueSetResource.class)
                .getValueSetId(entity.getId()))
                .withSelfRel());
         
    	paymentMode.setId(entity.getId());
    	paymentMode.setCode(entity.getCode());
        
        paymentMode.setDisplay(entity.getDisplay());
        
        return paymentMode;
    }
    
   /* private List<ServiceModel> toValueSetTypeModel(List<Service> services) {
        if (services.isEmpty())
            return Collections.emptyList();
 
        return services.stream()
                .map(service -> ServiceModel.builder()
                        .id(service.getId())
                        .localtionId(service.getLocaltionId())
                        .organizationId(service.getOrganizationId())
                        .build()
                        .add(linkTo(
                                methodOn(ServiceResource.class)
                                .getServiceId(service.getId())
                                .withSelfRel()))
                .collect(Collectors.toList()));
    }*/

}
