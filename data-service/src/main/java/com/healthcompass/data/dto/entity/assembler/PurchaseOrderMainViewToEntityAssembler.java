package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;


import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.MetaTag;
import com.healthcompass.data.model.MetaTagAttributes;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.MetaTagAttributesViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class PurchaseOrderMainViewToEntityAssembler {
	
private ThreadLocal<LoggedInUser> loggedInUser = new ThreadLocal<LoggedInUser>();
private static class OrderAction {
		
		enum ACTION {REMOVE,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		Order updatedOrder;
		ACTION action;
		
		OrderAction(){
			
		}
		OrderAction(ACTION action, Order updatedOrder){
			this.action=action;
			this.updatedOrder = updatedOrder;
		}
		public Order getUpdatedOrder() {
			return updatedOrder;
		}
		public void setUpdatedOrder(Order updatedOrder) {
			this.updatedOrder = updatedOrder;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		} 
		
	} // end inner class

private static class OrderItemAction {
	
	enum ACTION {REMOVE_FROM_ORDER,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
	OrderItem updatedOrderItem;
	ACTION action;
	
	OrderItemAction(){
		
	}
	OrderItemAction(ACTION action, OrderItem updatedOrderItem){
		this.action=action;
		this.updatedOrderItem = updatedOrderItem;
	}
	public OrderItem getUpdatedOrderItem() {
		return updatedOrderItem;
	}
	public void setUpdatedOrderItem(OrderItem updatedOrderItem) {
		this.updatedOrderItem = updatedOrderItem;
	}
	public ACTION getAction() {
		return action;
	}
	public void setAction(ACTION action) {
		this.action = action;
	}
}	
	private static class InventoryItemAction {
		
		enum ACTION {REMOVE_FROM_ORDER,SET_TO_INACTIVE,CREATE_NEW, UPDATE_EXISTING, DO_NOTHING};
		InventoryItem updatedInventoryItem;
		ACTION action;
		
		InventoryItemAction(){
			
		}
		InventoryItemAction(ACTION action, InventoryItem updatedInventoryItem){
			this.action=action;
			this.updatedInventoryItem = updatedInventoryItem;
		}
		public InventoryItem getUpdatedInventoryItem() {
			return this.updatedInventoryItem;
		}
		public void setUpdatedupdatedInventoryItem(InventoryItem updatedupdatedInventoryItem) {
			this.updatedInventoryItem = updatedInventoryItem;
		}
		public ACTION getAction() {
			return action;
		}
		public void setAction(ACTION action) {
			this.action = action;
		}
	
} // end inner class
	
	public Order toEntity(PurchaseOrderMainViewDTO purchaseOrderMainViewDTO,Order purchaseOrder){
		try { 
		LoggedInUser user = new LoggedInUser();
		 
		 user.setUserId(purchaseOrderMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : purchaseOrderMainViewDTO.getLoggedInUser());
		 loggedInUser.set(user); // thread local for subsequent threads to provide loggged in user
		 
	     
		 ArrayList<InventoryItem> newInventoryItemList = new ArrayList<InventoryItem>();
    
	
		  updateOrder(purchaseOrder, purchaseOrderMainViewDTO.getOrder()); //same for new and edit
		  ArrayList<OrderItem> newOrderItemList = new ArrayList<OrderItem>();
	    	
	    	for(PurchaseOrderItemViewDTO orderItemViewDTO : purchaseOrderMainViewDTO.getOrder().getOrderItems()) {
	    		
	    		OrderItemAction actionOnOrderItem = updateExistingOrderItem(purchaseOrder, orderItemViewDTO.getOrderItemId(),orderItemViewDTO);;
	    		
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.DO_NOTHING) {
	    			continue;
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.REMOVE_FROM_ORDER) {
	    			OrderItem tobeDeletedItem = actionOnOrderItem.getUpdatedOrderItem();
	    			tobeDeletedItem.setOrder(null);
	    			purchaseOrder.getOrderItems().remove(tobeDeletedItem);
	    			continue;
	    			
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.UPDATE_EXISTING) {
	    			OrderItem updatedItem = actionOnOrderItem.getUpdatedOrderItem();
	    			updatedItem.setOrder(purchaseOrder);
	    			// update the corresponding Inventory Item hereInventory Item here
	    			continue;
	    			
	    		}
	    		if(actionOnOrderItem.action == actionOnOrderItem.action.CREATE_NEW) {
	    			OrderItem updatedItem = actionOnOrderItem.getUpdatedOrderItem();
	    			
	    			OrderItem oItem = new OrderItem();
	    			oItem.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    			oItem.setCreatedBy(purchaseOrderMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : purchaseOrderMainViewDTO.getLoggedInUser());
	    			Product product = new Product();
	    			oItem.setProduct(product);
	    			boolean isNewCreated = updateOrderItem(oItem, orderItemViewDTO);
	    			if(isNewCreated) {
	    				oItem.setOrder(purchaseOrder);
	    				newOrderItemList.add(oItem);
	    			}
	    			
	    			//create new Inventory Item here
	    			continue;
	    		}
	    		
	    			
	    		
	    	}
	    	
	    	
	    	
	    	//serviceBill.getServiceBillItems().clear();
  		purchaseOrder.getOrderItems().addAll(newOrderItemList);
  		purchaseOrder.setOrderItems(purchaseOrder.getOrderItems());
  		
  		
  		// remove orphan OrderItems
  		for (OrderItem orderItem : purchaseOrder.getOrderItems()) {
  			
  			if(isOrderItemOrphan(orderItem,purchaseOrderMainViewDTO.getOrder().getOrderItems())){
  				orderItem.setOrder(null);
  				//purchaseOrder.getOrderItems().remove(orderItem);
  				
  			}
  					    
  					   
  		}
  		
  		
  		// Create or Update Inventory items corresponding to each order item
  		for(OrderItem orderItem : purchaseOrder.getOrderItems()) {
  			
			
			
  				InventoryItemAction actionOnInventoryItem = updateExistingInventoryItem(purchaseOrder, orderItem.getProduct().getId(),orderItem.getBatchNumber(),orderItem);
  				//ServiceBillItem updatedItem = 
  				if(actionOnInventoryItem.action == actionOnInventoryItem.action.DO_NOTHING) {
  					continue;
  				}
	    		if(actionOnInventoryItem.action == actionOnInventoryItem.action.REMOVE_FROM_ORDER) {
	    			//InventoryItem tobeDeletedItem = actionOnInventoryItem.getUpdatedInventoryItem();
	    			//tobeDeletedItem.setOrder(null);
	    			//purchaseOrder.getInventoryItems().remove(tobeDeletedItem);
	    			continue; // we will handle remove/orphan case just below this for loop
	    			
	    		}
	    		if(actionOnInventoryItem.action == actionOnInventoryItem.action.UPDATE_EXISTING) {
	    			InventoryItem updatedInventoryItem = actionOnInventoryItem.getUpdatedInventoryItem();
	    			updatedInventoryItem.setOrder(purchaseOrder);
	    			
	    			// update the corresponding Inventory Item hereInventory Item here
	    			continue;
	    			
	    		}
	    		if(actionOnInventoryItem.action == actionOnInventoryItem.action.CREATE_NEW) {
	    			InventoryItem updatedInventoryItem = actionOnInventoryItem.getUpdatedInventoryItem();
	    			
	    			InventoryItem iItem = new InventoryItem();
	    			iItem.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    			iItem.setCreatedBy(orderItem.getCreatedBy());
	    			
	    			
	    			Organization org= new Organization();
	    			org.setId(purchaseOrder.getOrganization().getId());
	    			iItem.setOrganization(org);
	    			
	    			Location loc= new Location();
	    			loc.setId(purchaseOrder.getLocation().getId());
	    			iItem.setLocation(loc);
	    			
	    				    				    	    			
	    			Product inventoryProduct = new Product();
	    			iItem.setProduct(inventoryProduct);
	    			
	    			boolean isNewInventoryItemCreated = updateInventoryItem(iItem, orderItem);
	    			if(isNewInventoryItemCreated) {
	    				iItem.setOrder(purchaseOrder);
	    				newInventoryItemList.add(iItem);
	    			}
	    			
	    			//create new Inventory Item here
	    			continue;
	    		}
			
					
		}
  			
  		
  		purchaseOrder.getInventoryItems().addAll(newInventoryItemList);
  		purchaseOrder.setInventoryItems(purchaseOrder.getInventoryItems());
  		
  		// remove orphan InventoryItems
  		
  		for (InventoryItem inventoryItem : purchaseOrder.getInventoryItems()) {
  			
  			if(isInventoryItemOrphan(inventoryItem,inventoryItem.getId(),purchaseOrder.getId(),purchaseOrderMainViewDTO.getOrder().getOrderItems())){
  				inventoryItem.setOrder(null);
  				
  				
  			}
  					    
  					   
  		}
  	
	    return purchaseOrder;
		}finally {
			loggedInUser.remove();
		}	
			
		}	 //
	  
    	 
	private boolean isOrderItemOrphan(OrderItem orderItem, List<PurchaseOrderItemViewDTO> sourceItems){
			boolean isOrphan = false;
			if(orderItem == null  || sourceItems == null ) {
			    isOrphan=false;
				return isOrphan;
			}
		   
		    
		    if(orderItem.getId() == null) {
		    	isOrphan=false;
		    	return isOrphan;
		    }
			Optional<PurchaseOrderItemViewDTO> optPurchaseOrderItemViewDTO =  sourceItems.stream()
					.filter(purchaseOrderItemView -> (purchaseOrderItemView.getOrderItemId() != null) && (purchaseOrderItemView.getOrderItemId().equals(orderItem.getId()))).findFirst();

					    if(!optPurchaseOrderItemViewDTO.isPresent())  //check this logic where the chilkd
					    	isOrphan=true;
			return isOrphan;
			
		}
	
	private boolean isInventoryItemOrphan(InventoryItem inventoryItem, UUID inventoryId, UUID purchaseOrderId,List<PurchaseOrderItemViewDTO> sourceItems){
		boolean isOrphan = false;
		if(inventoryId == null) {
			isOrphan=false;
			return isOrphan;
		}
		if(purchaseOrderId == null) {
			isOrphan=false;
			return isOrphan;
		}
		if(inventoryItem.getOrder() == null) {
			isOrphan=true;
			return isOrphan;
		}
		UUID existingInventoryOrderId = inventoryItem.getOrder().getId();
		Optional<PurchaseOrderItemViewDTO> optPurchaseOrderItemViewDTO =  sourceItems.stream()
				.filter(purchaseOrderItemView -> ((purchaseOrderItemView.getProduct().getProductId().compareTo(inventoryItem.getProduct().getId())==0)) && (inventoryItem.getBatchNumber().equals(purchaseOrderItemView.getBatchNumber()) && purchaseOrderId.equals(existingInventoryOrderId))).findFirst();

				    if(!optPurchaseOrderItemViewDTO.isPresent())  //check this logic where the chilkd
				    	isOrphan=true;
		return isOrphan;
		
	}
 
	 
	 private boolean updateOrder(Order target, PurchaseOrderViewDTO source) {
		 
		target.setId(source.getPurchaseOrderId());
	    target.getOrganization().setId(source.getOrganizationId()); 
		target.getLocation().setId(source.getLocationId());
		target.getSupplier().setId(source.getSupplierId());
		target.setOrderValue(source.getOrderValue());
		target.setNetOrderValue(source.getNetOrderValue());
		target.setDiscount(source.getDiscount());
		target.setOrderComment(source.getOrderComment());
		target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		target.setUpdatedBy(loggedInUser.get().getUserId());
		return true;
		
 		
	 }
	 
	 public OrderItemAction updateExistingOrderItem(Order puchaseOrder, UUID orderItemId , PurchaseOrderItemViewDTO updatedPurchaseItemViewDTO) {
		 	OrderItemAction orderItemAction = new OrderItemAction();
		    if(puchaseOrder == null  || updatedPurchaseItemViewDTO == null)
		        return new OrderItemAction(orderItemAction.action.DO_NOTHING, null);
		    
		    if(puchaseOrder == null)
		    	return new OrderItemAction(orderItemAction.action.DO_NOTHING, null);
		    
		    if(orderItemId== null) {
		    	
		    		return new OrderItemAction(orderItemAction.action.CREATE_NEW, null);
		    		
		    }

		    Optional<OrderItem> optTargetOrderItem = puchaseOrder.getOrderItems().stream()
		.filter(orderItem -> orderItem.getId().equals(orderItemId)).findFirst();

		    if(!optTargetOrderItem.isPresent())  //check this logic where the chilkd
		    	return new OrderItemAction(orderItemAction.action.CREATE_NEW, null);
		    
		    OrderItem targetOrderItem = optTargetOrderItem.get();
		    
		   

		    // now implement the update function
		    boolean status = updateOrderItem(targetOrderItem, updatedPurchaseItemViewDTO);
		    if(status) {
		    	return new OrderItemAction(orderItemAction.action.UPDATE_EXISTING, targetOrderItem);
		    	//return targetServiceBillItem;
		    	
		    }

		    // if status is true, you also have to call save() on your OrderRepository
		    return new OrderItemAction(orderItemAction.action.DO_NOTHING, null);
		}
        
	 private boolean updateOrderItem(OrderItem target, PurchaseOrderItemViewDTO sourcePurchaseOrderViewItem) {
		 
		 target.setId(sourcePurchaseOrderViewItem.getOrderItemId());
		 target.setBatchNumber(sourcePurchaseOrderViewItem.getBatchNumber());
		 //target.setExpiryDate(sourcePurchaseOrderViewItem.getExpiryDate());
		 target.setExpiryDate(HealthCompassConstants.convertLocalDateToSQLFormat(sourcePurchaseOrderViewItem.getExpiryDate(),HealthCompassConstants.defaultSQLDateFormat));
		 //target.setCentralTaxRate(sourcePurchaseOrderViewItem.getCentralTaxRate());
		// target.setSalesTaxRate(sourcePurchaseOrderViewItem.getSalesTaxRate());
		// target.setVatRate(sourcePurchaseOrderViewItem.getVatRate());
		 //target.setGstRate(sourcePurchaseOrderViewItem.getGstRate());
		 //target.setRate(sourcePurchaseOrderViewItem.getRate()); calculated during save
		 target.setQuantity(sourcePurchaseOrderViewItem.getQuantity());
		 target.setMrp(sourcePurchaseOrderViewItem.getMrp());
		 //target.setDiscountedRate(sourcePurchaseOrderViewItem.getDiscountedRate()); calculated during save
		 target.setDiscount(sourcePurchaseOrderViewItem.getDiscount());
		 target.setNetItemValue(sourcePurchaseOrderViewItem.getNetItemValue());
		 Product prod = new Product();
		 prod.setId(sourcePurchaseOrderViewItem.getProduct().getProductId());
		 target.setProduct(prod);
		 target.setProductName(sourcePurchaseOrderViewItem.getProductName()); // store product name at  order item level
		 target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		 target.setUpdatedBy(loggedInUser.get().getUserId());
		 return true;
		
 		
	 }
	 
	 public InventoryItemAction updateExistingInventoryItem(Order puchaseOrder, Integer productId , String batchNumber,OrderItem updatedOrderItem) {
		 InventoryItemAction inventoryItemAction = new InventoryItemAction();
		    if(puchaseOrder == null  || updatedOrderItem == null)
		        return new InventoryItemAction(inventoryItemAction.action.DO_NOTHING, null);
		    
		    if(puchaseOrder.getInventoryItems() != null &&  puchaseOrder.getInventoryItems().size() == 0)
		    	return new InventoryItemAction(inventoryItemAction.action.CREATE_NEW, null);
		    
		   if(puchaseOrder.getId() == null) {
			   return new InventoryItemAction(inventoryItemAction.action.CREATE_NEW, null);
		   }

		    Optional<InventoryItem> optTargetInventoryItem = puchaseOrder.getInventoryItems().stream()
		.filter(inventoryItem -> (inventoryItem.getProduct().getId().compareTo(productId) == 0) && inventoryItem.getBatchNumber().equals(batchNumber) && (inventoryItem.getOrder() != null && puchaseOrder.getId().equals(inventoryItem.getOrder().getId()))).findFirst();

		    if(!optTargetInventoryItem.isPresent())  //check this logic where the chilkd
		    	return new InventoryItemAction(inventoryItemAction.action.CREATE_NEW, null);
		    
		    if(updatedOrderItem.getOrder() == null ) { // orphan order item so, we remove inventory item also
		    	
		    	return new InventoryItemAction(inventoryItemAction.action.REMOVE_FROM_ORDER, null);
		    }
		    InventoryItem targetInventoryItem = optTargetInventoryItem.get();
		    
		   

		    // now implement the update function
		    boolean status = updateInventoryItem(targetInventoryItem, updatedOrderItem);
		    if(status) {
		    	return new InventoryItemAction(inventoryItemAction.action.UPDATE_EXISTING, targetInventoryItem);
		    	//return targetServiceBillItem;
		    	
		    }

		    // if status is true, you also have to call save() on your OrderRepository
		    return new InventoryItemAction(inventoryItemAction.action.DO_NOTHING, null);
		}
	 
 private boolean updateInventoryItem(InventoryItem target, OrderItem sourceOrderItem) {
		 
		
	    // target.getOrder().setId(sourceOrderItem.getOrder().getId());
	 	 target.getProduct().setId(sourceOrderItem.getProduct().getId());
	 	 target.getOrganization().setId(sourceOrderItem.getOrder().getOrganization().getId());
	 	 target.getLocation().setId(sourceOrderItem.getOrder().getLocation().getId());
	 	 target.setBatchNumber(sourceOrderItem.getBatchNumber());
		 target.setExpiryDate(sourceOrderItem.getExpiryDate());
	 	 target.setAvailableUnits(sourceOrderItem.getQuantity());
		 // check with Arvind on below calulations
		// Double mrpPerUnit = sourceOrderItem.getMrp()/sourceOrderItem.getQuantity(); calculated during save
		// target.setMrpPerUnit(mrpPerUnit);
		// target.setUnitRate(sourceOrderItem.getRate()); // calculated during save
		// calculated during save target.setMaxDiscount(sourceOrderItem.getDiscountedRate());  // Check if it is order item discount or discount rate
		 target.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
 		 target.setUpdatedBy(loggedInUser.get().getUserId());
		 return true;
		 
		
		
 		
	 }
     
}
