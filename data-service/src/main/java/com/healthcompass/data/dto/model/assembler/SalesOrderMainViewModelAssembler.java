package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PurchaseOrderResource;
import com.healthcompass.data.resource.SalesOrderResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class SalesOrderMainViewModelAssembler extends RepresentationModelAssemblerSupport<Order, SalesOrderMainViewDTO> {
 
	
	 @Autowired
	 SalesOrderViewModelAssembler salesOrderViewModelAssembler;
	 
	 public SalesOrderMainViewModelAssembler() {
	        super(SalesOrderResource.class, SalesOrderMainViewDTO.class);
	    }   



	 @Override
	 public SalesOrderMainViewDTO toModel(Order entity) 
  {
 	
 	SalesOrderMainViewDTO salesOrderMainView = new SalesOrderMainViewDTO();
 	
 	salesOrderMainView.setOrganizationId(entity.getOrganization().getId());
 	salesOrderMainView.setOrganizationName(entity.getOrganization().getName());
 	
 	salesOrderMainView.setLocationId(entity.getLocation().getId());
 	salesOrderMainView.setLocationName(entity.getLocation().getName());
 	
 	
 	
 	salesOrderMainView.setOrder(salesOrderViewModelAssembler.toModel(entity));
 	    	    	
     return salesOrderMainView;
 }
    
     
    
 
   
   

}
