package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;

@Component
public class MarketerMainViewToEntityAssembler {
	

	 public Marketer toEntity(MarketerMainViewDTO marketerMainViewDTO, Marketer marketer) 
	    {
	    	
	    boolean isNewMarketer=true;
	    if(marketerMainViewDTO.getMarketer()!= null && marketerMainViewDTO.getMarketer().getId() != null) {
	    	isNewMarketer=false;
	    }
	    if(isNewMarketer) {
	    	Organization org = new Organization(); // set proxy parent
	    	org.setId(marketerMainViewDTO.getOrganizationId());
	    	marketer.setOrganization(org);
	    }
	    
	    marketer.setId(marketerMainViewDTO.getMarketer().getId());
	    marketer.setName(marketerMainViewDTO.getMarketer().getName());
	    marketer.setDescription(marketerMainViewDTO.getMarketer().getDescription());
	    marketer.setActive(marketerMainViewDTO.getMarketer().getActive());
	    if(isNewMarketer){
	    	marketer.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	marketer.setCreatedBy(marketerMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : marketerMainViewDTO.getLoggedInUser());
	    }
	    marketer.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    marketer.setUpdatedBy(marketerMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : marketerMainViewDTO.getLoggedInUser());
	    			
	    	
	        return marketer;
	  }
	 
	 public List<Marketer> toEntityList(MarketerUploadMainViewDTO marketerUploadMainViewDTO) 
	    {
	    List<Marketer> marketerList = new ArrayList<Marketer>();
	    for(MarketerViewDTO marketerView : marketerUploadMainViewDTO.getMarketers()) {
	    	Marketer marketer = new Marketer();
	    	Organization org = new Organization(); // set proxy parent
	    	org.setId(marketerUploadMainViewDTO.getOrganizationId());
	    	marketer.setOrganization(org);
	    	marketer.setId(marketerView.getId());
		    marketer.setName(marketerView.getName());
		    marketer.setDescription(marketerView.getDescription());
		    marketer.setActive(true);
		   	marketer.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		    marketer.setCreatedBy(marketerUploadMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : marketerUploadMainViewDTO.getLoggedInUser());
		    marketer.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		    marketer.setUpdatedBy(marketerUploadMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : marketerUploadMainViewDTO.getLoggedInUser());
		    marketerList.add(marketer);
	    }
	  
	        return marketerList;
	  }
	 
	
        
}
