package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ValueSetResource;

@Component
public class ValueSetModelAssembler extends RepresentationModelAssemblerSupport<ValueSet, ValueSetModel> {
 
    public ValueSetModelAssembler() {
        super(ValueSetResource.class, ValueSetModel.class);
    }
 
    @Autowired
    private ValueSetTypeModelAssembler valueSetTypeModelAssembler;
    @Override
    public ValueSetModel toModel(ValueSet entity) 
    {
        ValueSetModel valueSetModel = instantiateModel(entity);
         
        valueSetModel.add(linkTo(
                methodOn(ValueSetResource.class)
                .getValueSetId(entity.getId()))
                .withSelfRel());
         
        valueSetModel.setId(entity.getId());
        valueSetModel.setCode(entity.getCode());
        valueSetModel.setCommonName(entity.getCommonName());
        valueSetModel.setDefinition(entity.getDefinition());
        valueSetModel.setDisplay(entity.getDisplay());
        valueSetModel.setLevel(entity.getLevel());
        valueSetModel.setSortOrder(entity.getSortOrder());
        valueSetModel.setUserGroup(entity.getUserGroup());
        valueSetModel.setSystem(entity.getSystem());
        //valueSetModel.setValueSetType(valueSetTypeModelAssembler.toModel(entity.getValueSetType()));
        valueSetModel.setIsActive(entity.getIsActive());
        valueSetModel.setCreatedOn(entity.getCreatedOn());
        valueSetModel.setCreatedBy(entity.getCreatedBy());
        valueSetModel.setUpdatedOn(entity.getUpdatedOn());
        valueSetModel.setUpdatedBy(entity.getUpdatedBy());
        return valueSetModel;
    }
     
    @Override
    public CollectionModel<ValueSetModel> toCollectionModel(Iterable<? extends ValueSet> entities) 
    {
        CollectionModel<ValueSetModel> valueSetModels = super.toCollectionModel(entities);
         
        valueSetModels.add(linkTo(methodOn(ValueSetResource.class).getAllValueSets()).withSelfRel());
         
        return valueSetModels;
    }
 
   /* private List<ServiceModel> toValueSetTypeModel(List<Service> services) {
        if (services.isEmpty())
            return Collections.emptyList();
 
        return services.stream()
                .map(service -> ServiceModel.builder()
                        .id(service.getId())
                        .localtionId(service.getLocaltionId())
                        .organizationId(service.getOrganizationId())
                        .build()
                        .add(linkTo(
                                methodOn(ServiceResource.class)
                                .getServiceId(service.getId())
                                .withSelfRel()))
                .collect(Collectors.toList()));
    }*/

}
