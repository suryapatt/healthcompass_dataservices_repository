package com.healthcompass.data.dto.entity.assembler;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class SupplierMainViewToEntityAssembler {
	

	 public Supplier toEntity(SupplierMainViewDTO supplierMainViewDTO, Supplier supplier) 
	    {
	    	
	    boolean isNewSupplier=true;
	    if(supplierMainViewDTO.getSupplier()!= null && supplierMainViewDTO.getSupplier().getId() != null) {
	    	isNewSupplier=false;
	    }
	    if(isNewSupplier) {
	    	Organization org = new Organization(); // set proxy parent
	    	org.setId(supplierMainViewDTO.getOrganizationId());
	    	supplier.setOrganization(org);
	    }
	    
	    supplier.setId(supplierMainViewDTO.getSupplier().getId());
	    supplier.setName(supplierMainViewDTO.getSupplier().getName());
	    supplier.setDescription(supplierMainViewDTO.getSupplier().getDescription());
	    supplier.setSupplierNumber(supplierMainViewDTO.getSupplier().getSupplierNumber());
	    supplier.setActive(supplierMainViewDTO.getSupplier().getActive());
	    if(isNewSupplier){
	    	 
	    	 supplier.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	 supplier.setCreatedBy(supplierMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : supplierMainViewDTO.getLoggedInUser());
	    }
	    supplier.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
   	 	supplier.setUpdatedBy(supplierMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : supplierMainViewDTO.getLoggedInUser());
	    			
	    	
	        return supplier;
	  }
	 
	 public List<Supplier> toEntityList(SupplierUploadMainViewDTO supplierUploadMainViewDTO) 
	    {
	    List<Supplier> supplierList = new ArrayList<Supplier>();
	    for(SupplierViewDTO supplierView : supplierUploadMainViewDTO.getSuppliers()) {
	    	Supplier supplier = new Supplier();
	    	Organization org = new Organization(); // set proxy parent
	    	org.setId(supplierUploadMainViewDTO.getOrganizationId());
	    	supplier.setOrganization(org);
	    	supplier.setId(supplierView.getId());
	    	supplier.setName(supplierView.getName());
	    	supplier.setDescription(supplierView.getDescription());
	    	supplier.setSupplierNumber(supplierView.getSupplierNumber());
	    	supplier.setActive(true);
	    	supplier.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	supplier.setCreatedBy(supplierUploadMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : supplierUploadMainViewDTO.getLoggedInUser());
	    	supplier.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	    	supplier.setUpdatedBy(supplierUploadMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : supplierUploadMainViewDTO.getLoggedInUser());
	    	supplierList.add(supplier);
	    }
	  
	        return supplierList;
	  }
	 
	 
	
	
        
}
