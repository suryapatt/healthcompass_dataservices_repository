package com.healthcompass.data.dto.entity.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.PatientModel;
import com.healthcompass.data.dto.model.PractitionerModel;
import com.healthcompass.data.dto.model.PractitionerRoleModel;
import com.healthcompass.data.dto.model.RateCardModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.Practitioner;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PractitionerRoleResource;



@Component
public class PractitionerEntityAssmebler {
 
	@Autowired
	PractitionerRoleEntityAssmebler practitionerRoleEntityAssembler;
	
    public PractitionerEntityAssmebler() {
       
    }
    
    public Practitioner  toEntity(PractitionerModel practitionerModel,Practitioner practitioner) 
    {
    	if( practitionerModel == null) return null;
        BeanUtils.copyProperties(practitionerModel, practitioner, "practitionerRoles", "_links"); //ignore valueSetType
        
        practitioner.setPractitionerRoles(practitionerRoleEntityAssembler.toEntityCollection(practitionerModel.getPractitionerRoles(),new ArrayList<PractitionerRole>()));
        return practitioner;
    }
    
    public List<Practitioner> toEntityCollection(List<PractitionerModel> practitionerModels,List<Practitioner> practitioners){
    	
    	
    	for(PractitionerModel practitionerModel : practitionerModels ) {
    		practitioners.add(toEntity(practitionerModel, new Practitioner()));
    	}
    	
    	return practitioners;
    }
   

}
