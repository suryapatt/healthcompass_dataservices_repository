package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.ServiceBillHistoryMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillHistoryViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class ServiceBillMainViewModelAsembler extends RepresentationModelAssemblerSupport<ServiceBill, ServiceBillViewMainDTO> {
 
	 @Autowired
	 ServiceBillModelAssembler serviceBillModelAssembler;
	 
	 @Autowired
	 SpecialityViewAssembler specialityViewAssembler;
	 
	 public ServiceBillMainViewModelAsembler() {
	        super(ServiceBillResource.class, ServiceBillViewMainDTO.class);
	    }   
   
  
  
    @Override
    public ServiceBillViewMainDTO toModel(ServiceBill entity) 
     {
    	
    	ServiceBillViewMainDTO serviceBilMainView = new ServiceBillViewMainDTO();
    	
    	 	
         Encounter encounter = entity.getEncounter();
         Patient patient = entity.getPatient();
         PractitionerRole practitionerRole = entity.getPractitionerRole();
        
           	
        serviceBilMainView.setAppointmentNumber(encounter.getAppointment().getId());
    	serviceBilMainView.setEncounterId(encounter.getId());
    	serviceBilMainView.setEncounterNumber(encounter.getEncounterNumber());
    
    	serviceBilMainView.setPatientName(patient.getDecodedName());
    	serviceBilMainView.setPatientId(patient.getId());
    	
    	
    	serviceBilMainView.setPhone(patient.getDecodedPhone());
    	serviceBilMainView.setPractitionerId(practitionerRole.getId());
    	
    	serviceBilMainView.setPractitionerName(practitionerRole.getPractitioner().getDecodedName());
    	                     
    	serviceBilMainView.setSpeciality(specialityViewAssembler.toModel(encounter.getAppointment().getSpeciality()));
    	
    	serviceBilMainView.setLocationId(encounter.getLocation().getId());
    	serviceBilMainView.setLocationName(encounter.getLocation().getName());
    	
    	
    	
    	
    	
    	
    	serviceBilMainView.setOrganizationId(encounter.getOrganization().getId());
    	serviceBilMainView.setOrganizationName(encounter.getOrganization().getName());
    	
    	serviceBilMainView.setServiceBill(serviceBillModelAssembler.toModel(entity));
    	    	    	
        return serviceBilMainView;
    }
    
    public ServiceBillViewMainDTO toModelFromEncounter(Encounter entity) 
    {
   	
   	ServiceBillViewMainDTO serviceBilMainView = new ServiceBillViewMainDTO();
   	
       Encounter encounter =  entity;
       Patient patient = entity.getPatient();
      // PractitionerRole practitionerRole = entity.getParticipant();
        
         
    serviceBilMainView.setAppointmentNumber(encounter.getAppointment().getId());
   	serviceBilMainView.setEncounterId(encounter.getId());
   	serviceBilMainView.setEncounterNumber(encounter.getEncounterNumber());
   
   	
   	serviceBilMainView.setPatientName(patient.getDecodedName());
   	serviceBilMainView.setPatientId(patient.getId());
   	
     	
   	serviceBilMainView.setPhone(patient.getDecodedPhone());
   	
   	//serviceBilMainView.setPractitionerId(practitionerRole.getId());
  
   	
   	//serviceBilMainView.setPractitionerName(practitionerRole.getPractitioner().getDecodedName());
   	                     
   	serviceBilMainView.setSpeciality(specialityViewAssembler.toModel(encounter.getAppointment().getSpeciality()));
   
	serviceBilMainView.setAdmissionTime(encounter.getAdmissionTime());
	serviceBilMainView.setDischargeTime(encounter.getDischargeTime());
   	
   	serviceBilMainView.setLocationId(encounter.getLocation().getId());
   	serviceBilMainView.setLocationName(encounter.getLocation().getName());
   	
    	
   	serviceBilMainView.setOrganizationId(encounter.getOrganization().getId());
   	serviceBilMainView.setOrganizationName(encounter.getOrganization().getName());
   	
   	serviceBilMainView.setServiceBill(null);
   
 
   	
       return serviceBilMainView;
   }
    
     
    @Override
    public CollectionModel<ServiceBillViewMainDTO> toCollectionModel(Iterable<? extends ServiceBill> entities) 
    {
        CollectionModel<ServiceBillViewMainDTO> serviceBillModels = super.toCollectionModel(entities);
         
        //serviceBillModels.add(linkTo(methodOn(ServiceBillResource.class).getAllServiceBills()).withSelfRel());
         
        return serviceBillModels;
    }
 
    public ServiceBillHistoryMainViewDTO toServiceBillHistoryModel(Iterable<? extends ServiceBill> entities,boolean isPatientBillHistory,boolean isProviderHistory) 
    {
       // CollectionModel<ServiceBillHistoryMainViewDTO> serviceBillHistoryModels = super.toCollectionModel(entities);
       ServiceBillHistoryMainViewDTO serviceBillHistoryModels = new ServiceBillHistoryMainViewDTO();
       List<ServiceBillHistoryViewDTO> billItems = new ArrayList<ServiceBillHistoryViewDTO>();
       serviceBillHistoryModels.setServiceBills(billItems);
       
       boolean isPatientOrPractitionerInofSet = false;
       for(ServiceBill serviceBill : entities ) {
    	   if(!isPatientOrPractitionerInofSet) {
    		   if(isPatientBillHistory) {
    			   Patient patient = serviceBill.getPatient();
    			   serviceBillHistoryModels.setPatientName(patient.getDecodedName());
   		           serviceBillHistoryModels.setPatientId(patient.getId());
   		           
    		           
    		   }
    		   if(isProviderHistory) {
    			   
    			   serviceBillHistoryModels.setProviderId(serviceBill.getEncounter().getOrganization().getId());
    			   serviceBillHistoryModels.setProviderName(serviceBill.getEncounter().getOrganization().getName());
   		          
    		   }
    		   
    	   }
    	   ServiceBillHistoryViewDTO  billItem = toHistoryModel(serviceBill) ;
    	   billItems.add(billItem);
    	   
        	
        } 
        return serviceBillHistoryModels;
    }
    
    public ServiceBillHistoryViewDTO toHistoryModel(ServiceBill entity) 
    {
   	
    	ServiceBillHistoryViewDTO serviceBilHistoryView = new ServiceBillHistoryViewDTO();
   	
   	 	
        Encounter encounter = entity.getEncounter();
        Patient patient = entity.getPatient();
        PractitionerRole practitionerRole = entity.getPractitionerRole();
       
          	
        serviceBilHistoryView.setServiceBillId(entity.getId());
   
        serviceBilHistoryView.setPatientName(patient.getDecodedName());
        serviceBilHistoryView.setPatientId(patient.getId());
   	
        serviceBilHistoryView.setPractitionerId(practitionerRole.getId());
   	    serviceBilHistoryView.setPractitionerName("Dr. "+practitionerRole.getPractitioner().getDecodedName());
   	                     
        serviceBilHistoryView.setSpeciality(encounter.getAppointment().getSpeciality().getDisplay());
   	
        serviceBilHistoryView.setLocationId(encounter.getLocation().getId());
        serviceBilHistoryView.setLocationName(encounter.getLocation().getName());
   	
        serviceBilHistoryView.setOrganizationId(encounter.getOrganization().getId());
        serviceBilHistoryView.setOrganizationName(encounter.getOrganization().getName());
   	
        serviceBilHistoryView.setAmountBilled(entity.getAmountBilled());
        serviceBilHistoryView.setAmountDue(entity.getAmountDue());
        serviceBilHistoryView.setAmountPaid(entity.getAmountPaid());
        serviceBilHistoryView.setBillNumber(entity.getBillNumber());
        serviceBilHistoryView.setBillDate(HealthCompassConstants.convertSQLDateTimeStampToLocalDateString(entity.getBillDate()));
       
        serviceBilHistoryView.setAttachmentId(entity.getAttachmentId());
   	
   	
   	    	    	
       return serviceBilHistoryView;
   }
   
   

}
