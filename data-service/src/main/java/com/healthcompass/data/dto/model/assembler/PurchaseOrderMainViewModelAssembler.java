package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.PurchaseOrderResource;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;

@Component
public class PurchaseOrderMainViewModelAssembler extends RepresentationModelAssemblerSupport<Order, PurchaseOrderMainViewDTO> {
 
	 @Autowired
	 PurchaseOrderViewModelAssembler purchaseOrderViewModelAssembler;
	 
	 
	 
	 
	 
	 public PurchaseOrderMainViewModelAssembler() {
	        super(PurchaseOrderResource.class, PurchaseOrderMainViewDTO.class);
	    }   
   
  
  
    @Override
    public PurchaseOrderMainViewDTO toModel(Order entity) 
     {
    	
    	PurchaseOrderMainViewDTO purchaseOrderMainView = new PurchaseOrderMainViewDTO();
    	
    	purchaseOrderMainView.setOrganizationId(entity.getOrganization().getId());
    	purchaseOrderMainView.setOrganizationName(entity.getOrganization().getName());
    	
    	purchaseOrderMainView.setLocationId(entity.getLocation().getId());
    	purchaseOrderMainView.setLocationName(entity.getLocation().getName());
    	
    	purchaseOrderMainView.setLocationId(entity.getLocation().getId());
    	purchaseOrderMainView.setLocationName(entity.getLocation().getName());
    	
    	purchaseOrderMainView.setSupplierId(entity.getSupplier().getId());
    	purchaseOrderMainView.setSupplierName(entity.getSupplier().getName());
    	
    	purchaseOrderMainView.setOrder(purchaseOrderViewModelAssembler.toModel(entity));
    	    	    	
        return purchaseOrderMainView;
    }
    
    
    
     
    
 
   
   

}
