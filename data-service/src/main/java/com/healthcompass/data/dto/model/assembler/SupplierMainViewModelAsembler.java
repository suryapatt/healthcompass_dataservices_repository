package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.HumanName;
import com.healthcompass.data.model.HumanNameType;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Organization;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.ServiceBillItemResource;
import com.healthcompass.data.resource.ServiceBillResource;
import com.healthcompass.data.resource.SupplierResource;
import com.healthcompass.data.resource.ValueSetResource;
import com.healthcompass.data.resource.ValueSetTypeResource;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;

@Component
public class SupplierMainViewModelAsembler extends RepresentationModelAssemblerSupport<Supplier, SupplierMainViewDTO> {
 
	 @Autowired
	 SupplierViewModelAssembler supplierViewModelAssembler;
	 
	 
	 
	 public SupplierMainViewModelAsembler() {
	        super(SupplierResource.class, SupplierMainViewDTO.class);
	    }   
   
  
  
    @Override
    public SupplierMainViewDTO toModel(Supplier entity) 
     {
    	
    	SupplierMainViewDTO supplierMainVieww = new SupplierMainViewDTO();
    	
    	 	
         
    
    	supplierMainVieww.setOrganizationId(entity.getOrganization().getId());
    	supplierMainVieww.setOrganizationName(entity.getOrganization().getName());
    	supplierMainVieww.setSupplier(supplierViewModelAssembler.toModel(entity));
    	    	    	
        return supplierMainVieww;
    }
    
        
    @Override
    public CollectionModel<SupplierMainViewDTO> toCollectionModel(Iterable<? extends Supplier> entities) 
    {
        CollectionModel<SupplierMainViewDTO> suppliers = super.toCollectionModel(entities);
               
         
        return suppliers;
    }
 
    public List<SupplierViewDTO> toSuppliersViewModel(List<Supplier> suppliers) {
		if (suppliers.isEmpty())
			return Collections.emptyList();
		List<SupplierViewDTO> suppliersList = new ArrayList<SupplierViewDTO>();
         for(Supplier supplier : suppliers) {
        	 SupplierViewDTO supplierViewDTO = new SupplierViewDTO();
        	 supplierViewDTO.setId(supplier.getId());
        	 supplierViewDTO.setName(supplier.getName());
        	 supplierViewDTO.setDescription(supplier.getDescription());
        	 supplierViewDTO.setSupplierNumber(supplier.getSupplierNumber());
        	 supplierViewDTO.setActive(supplier.getActive());
        	 suppliersList.add(supplierViewDTO);
         }
		
		return suppliersList;
	
		
				
	}

}
