package com.healthcompass.data.dto.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
//import com.healthcompass.data.dto.model.ValueSetModel.ValueSetModelBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "ValueSetType")
@Relation(collectionRelation = "valueSetTypes", itemRelation = "valueSetType")
@JsonInclude(Include.NON_NULL) //-- enable this if attributes with null values not to be sent to the client
@Component
public class ValueSetTypeModel  extends RepresentationModel<ValueSetTypeModel>{
  
	
	private int id;
	private String referredFrom;
	private String source;
	private boolean isActive;
	private Integer sortOrder;
	private String standardLevel;
	private String name;
	private String definition;
	private String system;
	private List<ValueSetModel> valueSets;
	private java.sql.Timestamp createdOn;
	private String createdBy;
	private java.sql.Timestamp updatedOn;
	private String updatedBy;

	
	
	
}
