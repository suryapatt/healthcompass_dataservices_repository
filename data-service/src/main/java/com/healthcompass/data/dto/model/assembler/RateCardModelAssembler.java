package com.healthcompass.data.dto.model.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import com.healthcompass.data.dto.model.RateCardModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.resource.RateCardResource;
import com.healthcompass.data.view.dto.ServiceBillViewRateCardDTO;


@Component
public class RateCardModelAssembler extends RepresentationModelAssemblerSupport<RateCard, ServiceBillViewRateCardDTO> {
 
	@Autowired
    RateCardCategoryModelAssembler rateCardCategoryModelAssembler;
	
    public RateCardModelAssembler() {
        super(RateCardResource.class, ServiceBillViewRateCardDTO.class);
    }
    
    @Autowired
    LocationModelAssembler locationModelAssembler;
 
    @Override
    public ServiceBillViewRateCardDTO toModel(RateCard entity) 
    {
    	ServiceBillViewRateCardDTO rateCard = instantiateModel(entity);
         
    	rateCard.add(linkTo(
                methodOn(RateCardResource.class)
                .getRateCard(entity.getId()))
                .withSelfRel());
         
    	rateCard.setId(entity.getId());
    	//rateCardModel.setLocation(locationModelAssembler.toModel(entity.getLocation()));
    	rateCard.setRate(entity.getRate());
    	rateCard.setDisplay(entity.getDisplay());
    	rateCard.setDescription(entity.getDescription());
    	
        return rateCard;
    }
     
    @Override
    public CollectionModel<ServiceBillViewRateCardDTO> toCollectionModel(Iterable<? extends RateCard> entities) 
    {
        CollectionModel<ServiceBillViewRateCardDTO> rateCardModels = super.toCollectionModel(entities);
         
        //serviceModels.add(linkTo(methodOn(ServiceResource.class).getAllServices().withSelfRel());
         
        return rateCardModels;
    }
 
    public List<ServiceBillViewRateCardDTO> toRateCardCollectionModel(List<RateCard> rateCards) {
        if (rateCards.isEmpty())
            return Collections.emptyList();
 
        return rateCards.stream()
                .map(rateCard -> ServiceBillViewRateCardDTO.builder()
                        .id(rateCard.getId())
                        .display(rateCard.getDisplay())
                        .description(rateCard.getDescription())
                        .rate(rateCard.getRate())
                        
                        .build()
                        .add(linkTo(
                                methodOn(RateCardResource.class)
                                .getRateCard(rateCard.getId()))
                                .withSelfRel()))
                .collect(Collectors.toList());
    }

}
