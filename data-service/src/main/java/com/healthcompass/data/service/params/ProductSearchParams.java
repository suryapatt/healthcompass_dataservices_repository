package com.healthcompass.data.service.params;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSearchParams {
	
	
	private UUID organizationId;
	private List<Integer> productIds;
	private Integer marketerId;
	private String marketerName;
	private String productName;
	private Boolean active;
	private Map<String,Object> advancedSearchParams;
	
	
	
	
	
   
	

}
