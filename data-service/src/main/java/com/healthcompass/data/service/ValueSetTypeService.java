package com.healthcompass.data.service;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetTypeModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.dto.model.assembler.ValueSetTypeModelAssembler;

import org.springframework.stereotype.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSetType;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.dao.ValueSetTypeDAO;
import com.querydsl.core.types.Predicate;

@Service
public class ValueSetTypeService {
	
	
	@Autowired 
	ServiceBillSearchPredicates preds;
	
	@Autowired
	private ValueSetTypeDAO ValueSetTypeDAO;
		
	@Autowired
	private ValueSetTypeModelAssembler valueSetTypeModelAsembler;
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )	 
	public ValueSetTypeModel getOptionAndOptionValues(String optionName){
		ValueSetType option = ValueSetTypeDAO.getOptionAndOptionValues(optionName);
		return valueSetTypeModelAsembler.toModel(option);
		
	}
	
	
	
	
	
}
