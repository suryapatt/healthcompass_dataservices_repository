package com.healthcompass.data.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.AttachmentDAO;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dao.SupplierDAO;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.SupplierMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.dto.model.assembler.SupplierMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.SupplierViewModelAssembler;

import org.springframework.stereotype.Service;

import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;

import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.util.LoggedInUser;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.validator.SupplierMainViewValidator;
import com.healthcompass.data.validator.SupplierUploadMainViewValidator;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.querydsl.core.types.Predicate;

@Service
public class SupplierServuce {
	
	
		
	@Autowired
	private SupplierDAO supplierDAO;
	
	@Autowired
	SupplierMainViewModelAsembler supplierMainViewModelAsembler;
	
	@Autowired
	SupplierMainViewValidator supplierMainViewValidator;
	
	@Autowired
	SupplierUploadMainViewValidator supplierUploadMainViewValidator;
	
	@Autowired
	SupplierViewModelAssembler supplierViewModelAssembler;
	@Autowired
	SupplierMainViewToEntityAssembler supplierMainViewToEntityAssembler;
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public SupplierMainViewDTO getSupplier(UUID id) {
		
		Optional<Supplier> supplierDBObj = (Optional<Supplier>) supplierDAO.findOne(id);
		Supplier supplier = supplierDBObj.isPresent() ?  supplierDBObj.get() : null;
		if(supplier == null) throw new RecordNotFoundException("Supplier Not found with Id: "+id);
		
		
		SupplierMainViewDTO supplierMainViewDTO = supplierMainViewModelAsembler.toModel(supplier);
		
		
		return supplierMainViewDTO;
				
		
		
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public SupplierMainViewDTO createSupplier(SupplierMainViewDTO supplierMainViewDTO,BindingResult result) {
		
	    
	    	
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		supplierMainViewValidator.customValidate(supplierMainViewDTO, errorsMap,result);
		
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	    }
		
		
		
		
		Supplier supplier = new Supplier();
		
			
		supplierMainViewToEntityAssembler.toEntity(supplierMainViewDTO,supplier) ;
		Supplier persistedSupplierProxy = null;
		try {
			persistedSupplierProxy = supplierDAO.save(supplier);
		}catch(Exception e) {
			     if(e instanceof DataIntegrityViolationException)
				  throw new HealthCompassCustomValidationException("Supplier already exists with same name and organization", result);
			
		}
		
		
		Optional<Supplier> persistedSupplier = supplierDAO.findOne(persistedSupplierProxy.getId());
		
		Supplier newSupplier = persistedSupplier.get();
		
		return supplierMainViewModelAsembler.toModel(newSupplier);
		
    	
    	
    		
				
	}
	

@Transactional(propagation = Propagation.REQUIRED )	
public SupplierMainViewDTO updateSupplier(SupplierMainViewDTO supplierMainViewDTO,BindingResult result) {
		
	    
	Map<String,Object> errorsMap = new HashMap<String,Object>();
	supplierMainViewValidator.customValidate(supplierMainViewDTO, errorsMap,result);
	
	
	if(errorsMap.keySet().size()>0) {
		if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
			throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
		}
		
		
    }
	
	
	Optional<Supplier> optExistingSupplier = supplierDAO.findOne(supplierMainViewDTO.getSupplier().getId());
	Supplier existingDBSupplier  = optExistingSupplier.isPresent() ?  optExistingSupplier.get() : null;
	if(existingDBSupplier == null) throw new RecordNotFoundException("Supplier Not found with Id: "+supplierMainViewDTO.getSupplier().getId());
	supplierMainViewToEntityAssembler.toEntity(supplierMainViewDTO,existingDBSupplier) ;
	Supplier persistedSupplier = supplierDAO.update(existingDBSupplier);
	
	Optional<Supplier> optUpdatedSupplier = supplierDAO.findOne(persistedSupplier.getId());
	
	Supplier updatedSupplier = optUpdatedSupplier.get();
	
	
	SupplierMainViewDTO updatedSupplierMainViewDTO = supplierMainViewModelAsembler.toModel(updatedSupplier);
	
	
	
	return updatedSupplierMainViewDTO;
	
	  		
}






public void deleteSupplier(UUID id) {
	supplierDAO.deleteById(id);
}

@Transactional(propagation = Propagation.REQUIRED )	
public SupplierSummaryOrganizationDTO getSupplierSummaryByOrganization(SupplierSummaryViewSearchParams supplierSummaryViewSearchParams) {
	
     
    
    List<Supplier> suppliers = supplierDAO.findAll(supplierSummaryViewSearchParams);
    List<SupplierViewDTO> supplierDTOList = supplierMainViewModelAsembler.toSuppliersViewModel(suppliers);
	SupplierSummaryOrganizationDTO supplierSummaryOrganizationDTO = new SupplierSummaryOrganizationDTO();
	
	supplierSummaryOrganizationDTO.setSuppliers(supplierDTOList);
	
	for(Supplier supplier : suppliers) {
		supplierSummaryOrganizationDTO.setOrganizationId(supplier.getOrganization().getId());
		supplierSummaryOrganizationDTO.setOrganizationName(supplier.getOrganization().getName());
		break;
	}	
		
	
	return supplierSummaryOrganizationDTO;
	
	

}

@Transactional(propagation = Propagation.REQUIRED )
public SupplierUploadMainViewDTO upLoadSuppliers(MultipartFile file) {
	 BindException be = new BindException(file,"File Upload");
	if (CSVHelper.hasCSVFormat(file)) {
		SupplierUploadMainViewDTO supplierUploadMainViewDTO= null;
		InputStream is = null;
			try {
				is =file.getInputStream();
			}catch(Exception e) {
      	 
				be.addError(new FieldError("Upload File:","File","Cannot read uploaded file"));
				throw new HealthCompassCustomValidationException("Custom Validation", be);
			}
			 supplierUploadMainViewDTO = CSVHelper.csvToSuppliers(is);
	         BindingResult result = new BindException(supplierUploadMainViewDTO,"Suppliers").getBindingResult();
	    	  supplierUploadMainViewValidator.validate(supplierUploadMainViewDTO, result);
	    		if(result.hasErrors()) {
	    			throw new HealthCompassCustomValidationException("Custom Validation", result);
	    		}
	    		
	    		List<Supplier> newSuppliersList = supplierMainViewToEntityAssembler.toEntityList(supplierUploadMainViewDTO);
	    		List<Supplier> persistedSupplierList= supplierDAO.saveAll(newSuppliersList);
	    		List<UUID> persistedIDList = new ArrayList<UUID>();
	    		for(Supplier supplier : persistedSupplierList) {
	    			persistedIDList.add(supplier.getId());
	    		}
	    		SupplierSummaryViewSearchParams supplierSummaryViewSearchParams = new SupplierSummaryViewSearchParams();
	    		supplierSummaryViewSearchParams.setIdList(persistedIDList);
	    		List<Supplier> persistedSuppliersList = supplierDAO.findAll(supplierSummaryViewSearchParams);
	    		
	    		List<SupplierViewDTO> persistedSupplierListViewDTO = supplierMainViewModelAsembler.toSuppliersViewModel(persistedSuppliersList);
	    		supplierUploadMainViewDTO.setSuppliers(persistedSupplierListViewDTO);
	    	    
	    		return supplierUploadMainViewDTO;
	        
	     
	    }else{
	    	 be.addError(new FieldError("Content Type:","File","Invalid Content Type"));
	   			throw new HealthCompassCustomValidationException("Custom Validation", be);
   	    }
	  
 }
	
}
