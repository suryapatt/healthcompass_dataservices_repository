package com.healthcompass.data.service;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.AttachmentDAO;
import com.healthcompass.data.dao.RateCardCategoryDAO;
import com.healthcompass.data.dao.RateCardDAO;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.AttachmentServiceResponse;
import com.healthcompass.data.model.LanguageTranslationServiceRequest;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.GoogleJWTokenGenerator;
import com.healthcompass.data.validator.RateCardLocationValidator;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.querydsl.core.types.Predicate;
import org.springframework.web.client.RestTemplate;



@Service
public class LanguageTranslationService {
	
	
	
	 @Value("${language-translation-service-url-prefix}")
	 public String languageTranslationServiceUrlPrefix;
	 
	 @Value("${language-translation-service-url-suffix}")
	 public String languageTranslationServiceUrlSuffix;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	GoogleJWTokenGenerator googleJWTokenGenerator;
	
	
	
	
	
	
	
	
	
	public Map<String, String> sendToLanguagTranslationService(LanguageTranslationServiceRequest languageTranslationServiceRequest){
		Map<String,String> translationsMap=null;
		try {
			restTemplate = new RestTemplate();
			restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.add("Accept-Encoding","identity"); 
		    googleJWTokenGenerator.checkAndAddAuthorizationHeader(headers);
		    HttpEntity<?> entity = new HttpEntity<>(headers);   // requestHeaders is of HttpHeaders type
		    		    
		    
		    ParameterizedTypeReference<HashMap<String, String>> responseType = 
		               new ParameterizedTypeReference<HashMap<String, String>>() {};
		               RequestEntity<Void> request = RequestEntity.get(URI.create(languageTranslationServiceRequest.getLanguageTranslationServiceURL()))
		                       .accept(MediaType.APPLICATION_JSON).build();
		  
		   UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(languageTranslationServiceRequest.getLanguageTranslationServiceURL()) ;// rawValidURl = http://example.com/hotels
		           
		    UriComponents uriComponents = builder.build().encode(); // encode() is to ensure that characters like {, }, are preserved and not encoded. Skip if not needed.
		    
		    translationsMap = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET,entity, responseType).getBody();
		    
		    
		    
		    	  
		    
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return translationsMap;
		
	}
	
	
	public String getTranslatedTextFromMap(String key,Map<String,String> languageTranslationsMap) {
		String translatedText = "_"+key+"_";
		if(languageTranslationsMap == null ) return key;
		if(languageTranslationsMap.get(key) != null) translatedText = (String) languageTranslationsMap.get(key);
		return translatedText;
	}
	
	
	
}