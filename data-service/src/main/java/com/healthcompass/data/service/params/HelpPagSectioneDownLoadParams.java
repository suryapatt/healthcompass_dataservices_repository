package com.healthcompass.data.service.params;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HelpPagSectioneDownLoadParams {
	
	
	
	
	private Set<String> languageCodeList;
	private Set<String> sectionIdList;
	
	
   
	

}
