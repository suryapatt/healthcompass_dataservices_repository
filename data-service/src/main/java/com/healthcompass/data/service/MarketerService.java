package com.healthcompass.data.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.AttachmentDAO;
import com.healthcompass.data.dao.MarketerDAO;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dao.SupplierDAO;
import com.healthcompass.data.dto.entity.assembler.MarketerMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.SupplierMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.MarketerMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.MarketerViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.dto.model.assembler.SupplierMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.SupplierViewModelAssembler;

import org.springframework.stereotype.Service;

import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;

import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.MarketerMainViewValidator;
import com.healthcompass.data.validator.MarketerUploadMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.validator.SupplierMainViewValidator;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.querydsl.core.types.Predicate;

@Service
public class MarketerService {
	
	
		
	@Autowired
	private MarketerDAO marketerDAO;
	
	@Autowired
	MarketerMainViewModelAsembler marketerMainViewModelAsembler;
	
	@Autowired
	MarketerMainViewValidator marketerMainViewValidator;
	
	@Autowired
	MarketerUploadMainViewValidator marketerUploadMainViewValidator;
	
	@Autowired
	MarketerViewModelAssembler marketerViewModelAssembler;
	@Autowired
	MarketerMainViewToEntityAssembler marketerMainViewToEntityAssembler;
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public MarketerMainViewDTO getMarketer(Integer id) {
		
		Optional<Marketer> marketerDBObj = (Optional<Marketer>) marketerDAO.findOne(id);
		Marketer marketer = marketerDBObj.isPresent() ?  marketerDBObj.get() : null;
		if(marketer == null) throw new RecordNotFoundException("Marketer Not found with Id: "+id);
		
		
		MarketerMainViewDTO marketerMainViewDTO = marketerMainViewModelAsembler.toModel(marketer);
		
		
		return marketerMainViewDTO;
				
		
		
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public MarketerMainViewDTO createMarketer(MarketerMainViewDTO marketerMainViewDTO,BindingResult result) {
		
	     
				
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		marketerMainViewValidator.customValidate(marketerMainViewDTO, errorsMap,result);
		
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	    }
		
		Marketer marketer = new Marketer();
		
			
		marketerMainViewToEntityAssembler.toEntity(marketerMainViewDTO,marketer) ;
		Marketer persistedMarketerProxy = null;
		try {
			persistedMarketerProxy = marketerDAO.save(marketer);
		}catch(Exception e) {
			     if(e instanceof DataIntegrityViolationException)
				  throw new HealthCompassCustomValidationException("Marketer already exists with same name and organization", result);
			
		}
		
		Optional<Marketer> persistedMarketer = marketerDAO.findOne(persistedMarketerProxy.getId());
		
		Marketer newMarketer = persistedMarketer.get();
		
		return marketerMainViewModelAsembler.toModel(newMarketer);
		
    	
    	
    		
				
	}
	
	public boolean isMarketerExistsWithSameName(String name) {
		
		return marketerDAO.checkIfMarketerExistsWithSameName(name);
		
		
	}	
	

@Transactional(propagation = Propagation.REQUIRED )	
public MarketerMainViewDTO updateMarketer(MarketerMainViewDTO marketerMainViewDTO,BindingResult result) {
		
	    
	Map<String,Object> errorsMap = new HashMap<String,Object>();
	marketerMainViewValidator.customValidate(marketerMainViewDTO, errorsMap,result);
	
	
	if(errorsMap.keySet().size()>0) {
		if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
			throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
		}
		
		
    }
	
	Optional<Marketer> optExistingMarketer = marketerDAO.findOne(marketerMainViewDTO.getMarketer().getId());
	Marketer existingDBMarketer  = optExistingMarketer.isPresent() ?  optExistingMarketer.get() : null;
	if(existingDBMarketer == null) throw new RecordNotFoundException("Marketer Not found with Id: "+marketerMainViewDTO.getMarketer().getId());
	marketerMainViewToEntityAssembler.toEntity(marketerMainViewDTO,existingDBMarketer) ;
	Marketer persistedMarketer = marketerDAO.update(existingDBMarketer);
	
	Optional<Marketer> optUpdatedMarketer = marketerDAO.findOne(persistedMarketer.getId());
	
	Marketer updatedMarketer = optUpdatedMarketer.get();
	
	
	MarketerMainViewDTO updatedMarketerMainViewDTO = marketerMainViewModelAsembler.toModel(updatedMarketer);
	
	
	
	return updatedMarketerMainViewDTO;
	
	  		
}






public void deleteMarketer(Integer id) {
	marketerDAO.deleteById(id);
}

@Transactional(propagation = Propagation.REQUIRED )	
public MarketerSummaryOrganizationDTO getMarketerSummaryByOrganization(MarketerSummaryViewSearchParams marketerSummaryViewSearchParams) {
	
     
    
    List<Marketer> marketers = marketerDAO.findAll(marketerSummaryViewSearchParams);
    List<MarketerViewDTO> marketerDTOList = marketerMainViewModelAsembler.toMarketersViewModel(marketers);
    MarketerSummaryOrganizationDTO marketerSummaryOrganizationDTO = new MarketerSummaryOrganizationDTO();
	
    marketerSummaryOrganizationDTO.setMarketers(marketerDTOList);
	
	for(Marketer marketer : marketers) {
		marketerSummaryOrganizationDTO.setOrganizationId(marketer.getOrganization().getId());
		marketerSummaryOrganizationDTO.setOrganizationName(marketer.getOrganization().getName());
		break;
	}	
		
	
	return marketerSummaryOrganizationDTO;
	
	

}

@Transactional(propagation = Propagation.REQUIRED )
public MarketerUploadMainViewDTO upLoadMarketers(MultipartFile file) {
	 BindException be = new BindException(file,"File Upload");
	if (CSVHelper.hasCSVFormat(file)) {
		   MarketerUploadMainViewDTO marketerUploadMainViewDTO= null;
 		   InputStream is = null;
	          try {
	        	  is =file.getInputStream();
	          }catch(Exception e) {
	        	 
	        	  be.addError(new FieldError("Upload File:","File","Cannot read uploaded file"));
	     		  throw new HealthCompassCustomValidationException("Custom Validation", be);
	          }
	          marketerUploadMainViewDTO = CSVHelper.csvToMarketers(is);
	          BindingResult result = new BindException(marketerUploadMainViewDTO,"Marketers").getBindingResult();
	    	  marketerUploadMainViewValidator.validate(marketerUploadMainViewDTO, result);
	    		if(result.hasErrors()) {
	    			throw new HealthCompassCustomValidationException("Custom Validation", result);
	    		}
	    		
	    		List<Marketer> newMarketersList = marketerMainViewToEntityAssembler.toEntityList(marketerUploadMainViewDTO);
	    		List<Marketer> persistedMarketerList= marketerDAO.saveAll(newMarketersList);
	    		List<Integer> persistedIDList = new ArrayList<Integer>();
	    		for(Marketer marketer : persistedMarketerList) {
	    			persistedIDList.add(marketer.getId());
	    		}
	    		MarketerSummaryViewSearchParams marketerSummaryViewSearchParams = new MarketerSummaryViewSearchParams();
	    		marketerSummaryViewSearchParams.setIdList(persistedIDList);
	    		List<Marketer> persistedMarketersList = marketerDAO.findAll(marketerSummaryViewSearchParams);
	    		
	    		List<MarketerViewDTO> persistedMarketerListViewDTO = marketerMainViewModelAsembler.toMarketersViewModel(persistedMarketersList);
	    		marketerUploadMainViewDTO.setMarketers(persistedMarketerListViewDTO);
	    	    
	    		return marketerUploadMainViewDTO;
	        
	     
	    }else{
		    be.addError(new FieldError("Content Type:","File","Invalid Content Type"));
   			throw new HealthCompassCustomValidationException("Custom Validation", be);
   	    }
	  
 }
	
	
	
	
	
	
		
			
}

	

