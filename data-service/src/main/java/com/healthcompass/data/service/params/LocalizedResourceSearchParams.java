package com.healthcompass.data.service.params;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalizedResourceSearchParams {
	
	
	
	private List<String> resourceIds;
	private String languageCode;
	private Integer countryId;
	private String countryCode;
	
	
	
	
	
	
   
	

}
