package com.healthcompass.data.service.params;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceSearchParams {
	
	
	
	private int id;
	private String serviceCode;
	private int service_type_id;
	private UUID organizationId;
	private UUID locationId;
	private UUID practitionarRoleId;
	private double service_rate;
	private List<Integer> idList;
	
   
	

}
