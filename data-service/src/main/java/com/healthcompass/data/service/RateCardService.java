package com.healthcompass.data.service;

import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.RateCardCategoryDAO;
import com.healthcompass.data.dao.RateCardDAO;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.RateCardLocationValidator;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.querydsl.core.types.Predicate;



@Service
public class RateCardService {
	
	
	@Autowired 
	RateCardSearchPredicates preds;
	
	
	@Autowired
	private RateCardModelAssembler rateCardModelAssembler;
	
	@Autowired
	private LocationEntityAssembler LocationEntityAssembler;
	
	
	@Autowired
	RateCardDAO rateCardDAO;
	
	@Autowired
	RateCardCategoryService rateCardCategoryService;
	
	
	@Autowired
	RateCardLocationValidator rateCardLocationValidator;
	
	
	private Predicate toPredicate(RateCardSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isLocationIdEq(params.getLocationId()))
				.and(preds.isActive(true))
				.and(preds.isIdsInList(params.getIdList()));
				
		// @formatter:on
	}
	
	
	
	
	public Optional<RateCardModel> OldfindOne(Integer id) {
		
		
		//return rateCardDAO.OldfindOne(id).map(rateCardModelAssembler::toModel) ;
		return null;
		
		
	}
	
	
	

	public LocationRateCardsViewDTO findAllRateCards(RateCardSearchParams rateCardSearchParams){
		
		return rateCardDAO.findAllRateCards(rateCardSearchParams);
		
	}
	
     public LocationRateCardsViewDTO findAllRateCards(UUID locationId){
		
		return rateCardDAO.findAllRateCards(locationId);
		
	}
	
	public LocationRateCardsViewDTO createOrUpdateRateCardsForALocation(LocationRateCardsViewDTO locationRateCards,BindingResult result){
		
      Map<String,Object> errorsMap = new HashMap<String,Object>();
		
      rateCardLocationValidator.customValidate(locationRateCards, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
		
		
		Location refinedLocationObjectWithNewRateCardCat = rateCardCategoryService.checkAndPersistNewRateCardCategories(locationRateCards);
		UUID locationId  = rateCardDAO.save(locationRateCards,refinedLocationObjectWithNewRateCardCat);
		return rateCardDAO.findAllRateCards(locationId);
	}
	
	
	

	
	public RateCardSummaryOrganizationDTO getRateCardSummaryWithLocations(RateCardViewSummarySearchParams rateCardViewSummarySearchParams) {
		
        return rateCardDAO.getRateCardSummaryWithLocations(rateCardViewSummarySearchParams);
		
	
	}
	
	public boolean deleteLocationRateCards(UUID locationId) {
		return rateCardDAO.deleteLocationRateCards(locationId);
	}
	
	
	
}