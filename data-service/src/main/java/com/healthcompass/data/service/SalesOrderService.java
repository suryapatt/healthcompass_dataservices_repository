package com.healthcompass.data.service;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.healthcompass.data.dao.BillPaymentDAO;
import com.healthcompass.data.dao.ProductDAO;
import com.healthcompass.data.dao.PurchaseOrderDAO;
import com.healthcompass.data.dao.SalesOrderDAO;
import com.healthcompass.data.dao.ServiceBillItemDAO;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ProductListMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.PurchaseOrderMainViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.SalesOrderMainViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.OrderItem;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.InventoryRepository;
import com.healthcompass.data.repository.InventorySummaryRepository;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.SalesOrderInventoryProductSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.CreateNewBillPaymentMainViewValidator;
import com.healthcompass.data.validator.ProductListMainViewValidator;
import com.healthcompass.data.validator.PurchaseOrderMainViewValidator;
import com.healthcompass.data.validator.SaleseOrderMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.SalesOrderMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProductSearchListMainViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProductSearchSummaryListViewDTO;
import com.healthcompass.data.view.dto.SalesOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;

@Service
public class SalesOrderService {
	
	
	
	
	@Autowired
	private SalesOrderDAO salesOrderDAO;
	
	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private  InventorySummaryRepository inventorySummaryRepo;
	
	@Autowired
	private  InventoryRepository inventoryRepo;
	
	
	
	@Autowired 
	SalesOrderMainViewModelAssembler salesOrderMainViewModelAssembler;
	
	@Autowired
	SaleseOrderMainViewValidator salesOrderMainViewValidator;
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public SalesOrderMainViewDTO findOne(UUID id) {
		
		Optional<Order> salesOrder = salesOrderDAO.findOne(id);
		if(salesOrder == null) return null;
		Order order = salesOrder.get();
		SalesOrderMainViewDTO salesOrderMainViewDTO = salesOrderMainViewModelAssembler.toModel(order);
		
		return salesOrderMainViewDTO;
				
		
		
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public SalesOrderMainViewDTO updateSalesOrder(SalesOrderMainViewDTO salesOrderMainViewDTO,BindingResult result) {
			
		
		
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		salesOrderMainViewValidator.customValidate(salesOrderMainViewDTO, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
	    
		
		Order persistedOrder = salesOrderDAO.createOrUpdateSalesOrder(salesOrderMainViewDTO);
	
		//get order id of persisted order. The order obtained above can be a lazily loaded list. Get the new state of the order
				// by retreiving the id and passing that id to DB.
				
		Optional<Order> salesOrder = salesOrderDAO.findOne(persistedOrder.getId());
		
		Order newOrUpdatedSalesOrder = salesOrder.get();		
		SalesOrderMainViewDTO updatedSalesOrderMainViewDTO = salesOrderMainViewModelAssembler.toModel(newOrUpdatedSalesOrder);
		
		
		
		return updatedSalesOrderMainViewDTO;
			
		   
			
			
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public SalesOrderMainViewDTO createSalesOrder(SalesOrderMainViewDTO salesOrderMainViewDTO,BindingResult result) {
			
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		salesOrderMainViewValidator.customValidate(salesOrderMainViewDTO, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
	    
	    
		
		Order persistedOrder = salesOrderDAO.createOrUpdateSalesOrder(salesOrderMainViewDTO);
		
		//get order id of persisted order. The order obtained above can be a lazily loaded list. Get the new state of the order
		// by retreiving the id and passing that id to DB.
		
		Optional<Order> salesOrder = salesOrderDAO.findOne(persistedOrder.getId());
		
		Order newOrUpdatedSalesOrder = salesOrder.get();
		
		SalesOrderMainViewDTO updatedSalesOrderMainViewDTO = salesOrderMainViewModelAssembler.toModel(newOrUpdatedSalesOrder);
		
		//check if the product has reached the re-order level and send a warning message
		
		Set<Integer> prodIdSet = new HashSet<Integer>();
		for(OrderItem orderItem : newOrUpdatedSalesOrder.getOrderItems()) {
			
			 InventoryItem inventoryItem = orderItem.getInventory();
			 prodIdSet.add(inventoryItem.getProduct().getId()); 
		}
		List<InventorySummary> invSummaryForSelectedProducts = new ArrayList<InventorySummary>();
		List<String> inventoryReOrderLevelMessages = new ArrayList<String>();
		if(prodIdSet.size() > 0) {
			   invSummaryForSelectedProducts = inventorySummaryRepo.getInventorySummaryForProductSet(newOrUpdatedSalesOrder.getOrganization().getId(),
					  newOrUpdatedSalesOrder.getLocation().getId(), prodIdSet);
		}
		for (InventorySummary invSummary :invSummaryForSelectedProducts ) {
			if(invSummary.getAvailableUnits().compareTo(invSummary.getProduct().getReOrderLevel()) <=0) {
	    		String reOrderWarMessage = "Product(Name: "+invSummary.getProduct().getName()+" ID: "+invSummary.getProduct().getId()+
	    				" has reached its Re-Order Level! Available quantiry :"+invSummary.getAvailableUnits()+ " Re-order level: "+invSummary.getProduct().getReOrderLevel()+"";
	    		inventoryReOrderLevelMessages.add(reOrderWarMessage);
			}	
		}
		updatedSalesOrderMainViewDTO.setInventoryReOrderLevelMessages(inventoryReOrderLevelMessages);
		
		
		return updatedSalesOrderMainViewDTO;
		//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
		   
			
			
	}
	
	public SalesOrderProductSearchListMainViewDTO findAllInventoryProducts(SalesOrderInventoryProductSearchParams inventoryProductSearchParams){
		
		return salesOrderDAO.findAllInventoryProducts(inventoryProductSearchParams);
		
	}
	
	public SalesOrderProductSearchSummaryListViewDTO getProductSummaryForSalesOrder(SalesOrderInventoryProductSearchParams inventoryProductSearchParams){
		
		return salesOrderDAO.getProductSummaryForSalesOrder(inventoryProductSearchParams);
		
	}
	
	public SalesOrderSummaryListViewDTO getSalesOrderSummarByOrganization(UUID organizationId){
		return salesOrderDAO.getSalesOrderSummaryListByOrganization(organizationId);
	}
	
	public Map<UUID,Integer> getLatestAvailableUnitsFromInventory(List<UUID> inventoryIdList){
		List<Object[]> availableUnits = inventoryRepo.getLatestAvailableUnits(inventoryIdList) ; 
		Map<UUID,Integer> latestAvailableUnits = new HashMap<UUID,Integer>();
		if (availableUnits != null) {
			for(Object[] unitsFromDB : availableUnits) {
				
				UUID id = UUID.fromString((String) unitsFromDB[0]) ;
				Integer units = (Integer) unitsFromDB[1];
				latestAvailableUnits.put(id, units);
			}
			
			
		}
		
		return latestAvailableUnits;
	}
	
	
	
	

		
	
}
