package com.healthcompass.data.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.EncounterDAO;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import org.springframework.stereotype.Service;

import com.healthcompass.data.model.Encounter;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;

import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.querydsl.core.types.Predicate;

@Service
public class EncounterService {
	
	
	
	
	@Autowired
	private EncounterDAO encounterDAO;
	
	@Autowired
	private ServiceBillDAO serviceBillDAO;
	
	@Autowired
	private PractitionerRoleDAOService practitionerRoleDAOService;
	
	
	@Autowired
	ServiceBillMainViewModelAsembler serviceBillMainViewModelAsembler;
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )	 
	public ServiceBillViewMainDTO findOne(UUID id,UUID practitioneRoleId) {
		
		Optional<Encounter> encounterOpt = (Optional<Encounter>) encounterDAO.findOne(id);
		Encounter encounterDBObj = encounterOpt.get();
		ServiceBillViewMainDTO serviceBillViewMainDTO = serviceBillMainViewModelAsembler.toModelFromEncounter(encounterDBObj);
		PractitionerRole practitionerRole = practitionerRoleDAOService.findforEncounter(practitioneRoleId);
		serviceBillViewMainDTO.setPractitionerId(practitionerRole.getId());
		serviceBillViewMainDTO.setPractitionerName(practitionerRole.getPractitioner().getDecodedName());
		Double outStandingPatientBillsAmount = (Double) serviceBillDAO.getOutStandingPendingBillsAmount(encounterDBObj.getPatient().getId());
		serviceBillViewMainDTO.setOutStandingPendingBillsAmount(outStandingPatientBillsAmount);
		return serviceBillViewMainDTO;
				
			
	}
	
	
	}
