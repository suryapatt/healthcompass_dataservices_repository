package com.healthcompass.data.service;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dao.BillPaymentDAO;
import com.healthcompass.data.dao.ServiceBillItemDAO;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.InAppNotificationServiceRequest;
import com.healthcompass.data.model.LanguageTranslationServiceRequest;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.NotificationRequest;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.SmsServiceRequest;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.util.GoogleJWTokenGenerator;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.CreateNewBillPaymentMainViewValidator;
import com.healthcompass.data.validator.PendingBillsSerachValidator;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.querydsl.core.types.Predicate;

@Service
public class BillPaymentService {
	
	
	@Autowired ServiceBillItemSearchPredicates preds;
	
	@Autowired
	private BillPaymentDAO billPaymentDAO;
	
	@Autowired
	CreateNewBillPaymentMainViewValidator createNewBillPaymentMainViewValidator;
	
	@Autowired
	private AttachmentService attachmentService;
	
	@Autowired
	PendingBillsSerachValidator pendingBillsearchValidator;
	
	@Autowired
	InAppNotificationService inAppNotificationService;
	
	@Autowired
	SmsService smsService;
	
	@Autowired
	LanguageTranslationService languageTranslationService;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${hc-notification-service-url}")
	private String healthCompassNotificationServiceURL;
	
	@Autowired
	GoogleJWTokenGenerator googleJWTokenGenerator;
	
	public List<BillPayment> findAll(){
		//return serviceRepository.findAll();
		return null;
	}
	
public BillPaymentsMainViewDTO findAllPatientBillPaymentsByPatientId(Integer patientId){
		
	     BillPaymentsSearchParams billPaymentsSerachParams = new BillPaymentsSearchParams();
	     billPaymentsSerachParams.setPatientId(patientId);
		return billPaymentDAO.findAllPendingPayments(billPaymentsSerachParams);
		
}
	
	public BillPaymentsMainViewDTO findAllPatientBillPayments(BillPaymentsSearchParams billPaymentsSerachParams,BindingResult result){
		/*String phoneNumber = billPaymentsSerachParams.getPhoneNumber(); // we will use the auth_user table to compare the phone number which will be a plain text unlike telecom field in patient table
		 * so we do not need encryption logic
		if(phoneNumber != null && !phoneNumber.isEmpty()) {
			billPaymentsSerachParams.setPhoneNumber(HealthCompassConstants.getEncodeValue("Phone", phoneNumber));
		}*/
		
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		pendingBillsearchValidator.customValidate(billPaymentsSerachParams, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	    }
		
		
		
		BillPaymentsMainViewDTO billPaymentsMainViewDTO = billPaymentDAO.findAllPendingPayments(billPaymentsSerachParams);
		if(billPaymentsMainViewDTO.getBillPayments() !=  null && billPaymentsMainViewDTO.getBillPayments().isEmpty()) {
			
			errorsMap.put("billSearch","No Bills found");
			throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			
		}
		return billPaymentsMainViewDTO;
		
	}
	
     
	public BillPaymentHistoryMainViewDTO findBillPaymentHistoryByBillNumber(String billNumber){
		
	    
		return billPaymentDAO.findBillPaymentHistory(billNumber);
		
}
	
	@Transactional(propagation = Propagation.REQUIRED )
	public List<BillPayment> createBillPaymentFromBillAndSave(ServiceBill serviceBill,Map<String,String> languageTranslationsMap) {
		
		List<BillPayment> billPayments = new ArrayList<BillPayment>();
		// add the details of the current bill payment
		BillPayment newBillPayment = new BillPayment();
		
		// set Patient to the bill payment
		Patient pat = new Patient();
		pat.setId(serviceBill.getPatient().getId());
		
		newBillPayment.setPatient(pat);
		
		newBillPayment.setPaymentAmount(serviceBill.getAmountPaid());
		newBillPayment.setActive(true);
		newBillPayment.setPaymentDate(new java.sql.Timestamp(System.currentTimeMillis()));
		newBillPayment.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		newBillPayment.setCreatedBy(serviceBill.getUpdatedBy());
		newBillPayment.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		newBillPayment.setUpdatedBy(serviceBill.getUpdatedBy());
		
		// set Payment Mode for bill payment
		ValueSet billPaymentMode = new ValueSet();
		
		billPaymentMode.setId(serviceBill.getPaymentMode().getId());
		newBillPayment.setPaymentMode(billPaymentMode);
		// assign the new bill payment child to the parent service bill
		ServiceBill proxyParent = new ServiceBill();
		proxyParent.setBillNumber(serviceBill.getBillNumber());
		proxyParent.setAmountBilled(serviceBill.getAmountBilled());
		newBillPayment.setServiceBill(proxyParent);
		billPayments.add(newBillPayment);
		List<BillPayment> updatedBillPayments =  billPaymentDAO.save(billPayments,false);// no need to update parent service bill as the trigger point is from service bill save
		// the billPayments after the above call will have payment records with generated receipt numbers and new primary keys.
		// Using the bill payment ids above, let us get the newly created bill payments from Database
		ReceiptViewDTO receiptView = billPaymentDAO.getBillPaymentsForReceiptGeneration(updatedBillPayments,false);
		
		
		// let us iterate the list and generate the PDF. Send the pdf to attachment service and update the payments with new attachment ids.
		
		ByteArrayOutputStream baos = generatePDF(receiptView,languageTranslationsMap);
		long size=baos.size();
		byte[] pdfData =  baos.toByteArray();
		//prepare the json data for Attachment service
		AttachmentServiceRequest attachmentServiceRequest = prepareAttachmentServiceRequest(pdfData,size,receiptView);
		//send the pdf to Attachment service and get the new Attachment ID
		UUID attachmentId = attachmentService.sendToAttachmentService(attachmentServiceRequest);
		// we can update multiple bill payments with same attachment id as we will have a single receipt
		List<UUID> primaryKeyList = new ArrayList<UUID>();
		for(BillPaymentReceiptViewDTO billPaymentReceiptViewDTO : receiptView.getBillPayments()) {
    		
			// Update the BillPayment with new attachment id
			
			primaryKeyList.add(billPaymentReceiptViewDTO.getBillPaymentId());
			
    		
		}
		
		attachmentService.updateAttachmentDetails("billing.bill_payments", "attachment_id", attachmentId,primaryKeyList);
		
		
		return updatedBillPayments;
		
	}
			
	public ByteArrayOutputStream generatePDF(ReceiptViewDTO receiptViewDTO,Map<String,String> languageTranslationsMap) {
		
		
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML");
		 
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		 
		
		Context context = new Context();
		Map<String, Object> variables = new HashMap<String, Object>();
		//variables.put("serviceBill", serviceBill);

       //context.setVariables(variables);
		context = populateBillTemplate(receiptViewDTO, context,languageTranslationsMap);
		
		 
		// Get the plain HTML with the resolved ${name} variable!
		String htmloUtput = templateEngine.process("Receipt_Template", context);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			
			
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(htmloUtput);
			renderer.layout();
			renderer.createPDF(baos);
			
			
			System.out.println("size of file:"+baos.size());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return baos;
		
		
		
	}
	
	private Context populateBillTemplate(ReceiptViewDTO receiptViewDTO, Context context,Map<String,String> languageTranslationsMap) {
		
		
		context.setVariable("patientName", receiptViewDTO.getPatientName());
		context.setVariable("organizationName", receiptViewDTO.getOrganizationName());
		context.setVariable("locationName", receiptViewDTO.getLocationName());
		context.setVariable("receiptNumber", receiptViewDTO.getReceiptNumber());
		context.setVariable("currentDate", receiptViewDTO.getPaymentDate());
		context.setVariable("paymentMode", receiptViewDTO.getPaymentMode());
		context.setVariable("totalAmountPaid",  String.format("%.2f%n",receiptViewDTO.getTotalPaymentReceived()));
		context.setVariable("totalOutStanding",  String.format("%.2f%n",receiptViewDTO.getTotalOutStanding()));
		context.setVariable("billPayments", receiptViewDTO.getBillPayments());
			
		// translate labels
		
		context.setVariable("bill_receipt_for",languageTranslationService.getTranslatedTextFromMap("bill_receipt_for",languageTranslationsMap));
		context.setVariable("bill_date_time",languageTranslationService.getTranslatedTextFromMap("bill_date_time",languageTranslationsMap));
		context.setVariable("bill_receipt_no",languageTranslationService.getTranslatedTextFromMap("bill_receipt_no",languageTranslationsMap));
		context.setVariable("bill_mode_of_payment",languageTranslationService.getTranslatedTextFromMap("bill_mode_of_payment",languageTranslationsMap));
		context.setVariable("bill_received_from",languageTranslationService.getTranslatedTextFromMap("bill_received_from",languageTranslationsMap));
		context.setVariable("bill_the_sum_of",languageTranslationService.getTranslatedTextFromMap("bill_the_sum_of",languageTranslationsMap));
		context.setVariable("bill_towards_the_following",languageTranslationService.getTranslatedTextFromMap("bill_towards_the_following",languageTranslationsMap));
		context.setVariable("bill_bill_no",languageTranslationService.getTranslatedTextFromMap("bill_bill_no",languageTranslationsMap));
		context.setVariable("bill_bill_date",languageTranslationService.getTranslatedTextFromMap("bill_bill_date",languageTranslationsMap));
		context.setVariable("bill_payment_due",languageTranslationService.getTranslatedTextFromMap("bill_payment_due",languageTranslationsMap));
		context.setVariable("bill_amount_received",languageTranslationService.getTranslatedTextFromMap("bill_amount_received",languageTranslationsMap));
		context.setVariable("bill_total_outstanding",languageTranslationService.getTranslatedTextFromMap("bill_total_outstanding",languageTranslationsMap));
		context.setVariable("bill_new_amount_due",languageTranslationService.getTranslatedTextFromMap("bill_new_amount_due",languageTranslationsMap));
		
		return context;
	}	
	
	private AttachmentServiceRequest prepareAttachmentServiceRequest (byte[] pdfData,long size,ReceiptViewDTO receiptViewDTO) {
		AttachmentServiceRequest attachmentServiceRequest = new AttachmentServiceRequest();
		try {
		
		/*attachmentServiceRequest.put("contentType", 20503);
		attachmentServiceRequest.put("creation", new java.sql.Timestamp(System.currentTimeMillis()));
		attachmentServiceRequest.put("data", pdfData);
		attachmentServiceRequest.put("Hash", null);
		attachmentServiceRequest.put("language", 1152);
		attachmentServiceRequest.put("size", size);
		attachmentServiceRequest.put("title", "Receipt-"+receiptViewDTO.getReceiptNumber());
		attachmentServiceRequest.put("updatedBy", receiptViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : receiptViewDTO.getLoggedInUser());
		attachmentServiceRequest.put("updatedOn", new java.sql.Timestamp(System.currentTimeMillis()));
		attachmentServiceRequest.put("url", "");*/
		attachmentServiceRequest.setContentType(20503);	// value set type - DataTypes - Valueset - Attachment
		//attachmentServiceRequest.setCreation(new java.sql.Timestamp(System.currentTimeMillis()));
		attachmentServiceRequest.setData(pdfData);
		attachmentServiceRequest.setHash(null);
		attachmentServiceRequest.setLanguage(1152); // value set type - languages, Valueset - english
		attachmentServiceRequest.setSize(size);
		attachmentServiceRequest.setTitle("Receipt-"+receiptViewDTO.getReceiptNumber());
		attachmentServiceRequest.setUpdatedBy(receiptViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : receiptViewDTO.getLoggedInUser());
		attachmentServiceRequest.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
		attachmentServiceRequest.setUrl("");
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return attachmentServiceRequest;
		
		
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public byte[]  createNewBillPayment(CreateNewBillPaymentMainViewDTO createNewBillPaymentMainViewDTO,BindingResult result,String userLocaleLnaguage){
		
		/*createNewBillPaymentMainViewValidator.validate(createNewBillPaymentMainViewDTO, result);
		if(result.hasErrors()) {
			throw new HealthCompassCustomValidationException("Custom Validation", result);
		} */
		
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		createNewBillPaymentMainViewValidator.customValidate(createNewBillPaymentMainViewDTO, errorsMap,result);
		 
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	}
		
		List<BillPayment> billPayments = new ArrayList<BillPayment>();
		UUID patientId=createNewBillPaymentMainViewDTO.getPatientId();
		
		for(CreateNewBillPaymentViewDTO newBillPaymentView : createNewBillPaymentMainViewDTO.getBillPayments())
		{	
			BillPayment newBillPayment = new BillPayment();
		
			// set Patient to the bill payment
			Patient pat = new Patient();
			pat.setId(patientId);
			newBillPayment.setPatient(pat);
			
			newBillPayment.setPaymentAmount(newBillPaymentView.getPaymentAmount());
			newBillPayment.setActive(true);
			newBillPayment.setPaymentDate(new java.sql.Timestamp(System.currentTimeMillis()));
			newBillPayment.setCreatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
			newBillPayment.setCreatedBy(createNewBillPaymentMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : createNewBillPaymentMainViewDTO.getLoggedInUser());
			newBillPayment.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
			newBillPayment.setUpdatedBy(createNewBillPaymentMainViewDTO.getLoggedInUser() == null ? HealthCompassConstants.loggedInUserName : createNewBillPaymentMainViewDTO.getLoggedInUser());
			
			// set Payment Mode for bill payment
			ValueSet billPaymentMode = new ValueSet();
			
			billPaymentMode.setId(newBillPaymentView.getPaymentMode().getId());
			newBillPayment.setPaymentMode(billPaymentMode);
			// assign the new bill payment child to the parent service bill
			ServiceBill proxyParent = new ServiceBill();
			newBillPayment.setServiceBill(proxyParent);
			proxyParent.setAmountBilled(newBillPaymentView.getAmountBilled());
			proxyParent.setAmountDue(newBillPaymentView.getAmountDue());
			proxyParent.setBillNumber(newBillPaymentView.getBillNumber());
			
			billPayments.add(newBillPayment);
	    }
		
		List<BillPayment> updatedBillPayments = billPaymentDAO.save(billPayments,true);//createBillPayments(billPayments,true); // create new payments and update corresponding parent servicce bill with new paid amounts
		// the billPayments after the above call will have payment records with generated receipt numbers and new primary keys.
		// Using the bill payment ids above, let us get the newly created bill payments from Database
		
		ReceiptViewDTO receiptView = billPaymentDAO.getBillPaymentsForReceiptGeneration(updatedBillPayments,true);
		
		
		LanguageTranslationServiceRequest languageTranslationServiceRequest = prepareLanguageTranslationServiceRequest(userLocaleLnaguage);
		Map<String,String> languageTranslationsMap = languageTranslationService.sendToLanguagTranslationService(languageTranslationServiceRequest);
		// let us iterate the list and generate the PDF. Send the pdf to attachment service and update the payments with new attachment ids.
		
		// let us iterate the list and generate the PDF. Send the pdf to attachment service and update the payments with new attachment ids.
		
				ByteArrayOutputStream baos = generatePDF(receiptView,languageTranslationsMap);
				long size=baos.size();
				byte[] pdfData =  baos.toByteArray();
				//prepare the json data for Attachment service
				AttachmentServiceRequest attachmentServiceRequest = prepareAttachmentServiceRequest(pdfData,size,receiptView);
				//send the pdf to Attachment service and get the new Attachment ID
				UUID attachmentId = attachmentService.sendToAttachmentService(attachmentServiceRequest);
				// we can update multiple bill payments with same attachment id as we will have a single receipt
				List<UUID> primaryKeyList = new ArrayList<UUID>();
				for(BillPaymentReceiptViewDTO billPaymentReceiptViewDTO : receiptView.getBillPayments()) {
		    		
					// Update the BillPayment with new attachment id
					
					primaryKeyList.add(billPaymentReceiptViewDTO.getBillPaymentId());
					
		    		
				}
				
				attachmentService.updateAttachmentDetails("billing.bill_payments", "attachment_id", attachmentId,primaryKeyList);
		
		
				//prepare the json data for In App Notification service
				//InAppNotificationServiceRequest inAppNotificationServiceRequest = prepareInAppNotificationServiceRequest(receiptView,attachmentId);
				//send the notification request to InAppNotificationService
				//inAppNotificationService.sendToInAppNotification(inAppNotificationServiceRequest);
				
				//send the notification request to Notification Service, so that sms, email and push notifications are handled
				 /*NotificationRequest sendNotificationRequest = new  NotificationRequest();
				 sendNotificationRequest.setMessageTriggerId(17);
				 HashMap messageKeys = new HashMap<String,String>();
				 messageKeys.put("patient_user_name", receiptView.getUserName());
				 messageKeys.put("patient_first_name", receiptView.getPatientName());
				 messageKeys.put("patient_email", receiptView.getEmail());
				 messageKeys.put("patient_phone_number", receiptView.getPhoneNumber());
				 messageKeys.put("unique_identifier", receiptView.getReceiptNumber());
				 messageKeys.put("payment_amount", String.format("%.2f%n", receiptView.getTotalPaymentReceived()));
				 messageKeys.put("organization_name", receiptView.getOrganizationName());
			     messageKeys.put("location_name", receiptView.getLocationName());
			     messageKeys.put("attachment_link", attachmentService.attachmentServiceUrl+attachmentId+"");
										
				 sendNotificationRequest.setMessageKeys(messageKeys);
				 sendToNotificationService(sendNotificationRequest,"en");*/
				//prepare the json data for sms service
				SmsServiceRequest  smsServiceRequest = prepareSmsServiceRequest(receiptView,attachmentId);
				//send the notification request to InAppNotificationService
				smsService.sendToSmsService(smsServiceRequest);
		//return updatedBillPayments;
		return	pdfData;
		 
		
	}
	
	private LanguageTranslationServiceRequest prepareLanguageTranslationServiceRequest(String localeLanguage) {
		LanguageTranslationServiceRequest languageTranslationServiceRequest = new LanguageTranslationServiceRequest();
		languageTranslationServiceRequest.setLanguageTranslationServiceURL(languageTranslationService.languageTranslationServiceUrlPrefix+localeLanguage+languageTranslationService.languageTranslationServiceUrlSuffix);
		return languageTranslationServiceRequest;
		
		
	}
	
	private InAppNotificationServiceRequest prepareInAppNotificationServiceRequest (ReceiptViewDTO receiptView,UUID attachmentId) {
		InAppNotificationServiceRequest inAppNotificationServiceRequest = new InAppNotificationServiceRequest();
		inAppNotificationServiceRequest.setAction_url(attachmentService.attachmentServiceUrl+attachmentId);
		inAppNotificationServiceRequest.setBody(" Dear "+receiptView.getPatientName()+", your Receipt with Receipt Number:"+receiptView.getReceiptNumber()+" has been generated. Please clcik the link provided to view/download the Receipt");
		inAppNotificationServiceRequest.setMessage_type("INAPP");
		inAppNotificationServiceRequest.setMessage_status("New");
		inAppNotificationServiceRequest.setSender_id("123");
		inAppNotificationServiceRequest.setRecipient_id(Integer.toString(receiptView.getPatientId()));
			
		return inAppNotificationServiceRequest;
		
		
	}

	private SmsServiceRequest prepareSmsServiceRequest(ReceiptViewDTO receiptView,UUID attachmentId) {
		SmsServiceRequest smsServiceRequest = new SmsServiceRequest();
		smsServiceRequest.setPhoneNumber(receiptView.getPhoneNumber());
		smsServiceRequest.setMessage(" Dear "+receiptView.getPatientName()+", your Receipt with Receipt Number:"+receiptView.getReceiptNumber()+" has been generated. Please clcik on "+attachmentService.attachmentServiceUrl+attachmentId+" to view/download the Receipt");
		
		return smsServiceRequest;
		
		
	}
	
	public String  sendToNotificationService(NotificationRequest sendNotificationRequest,String userLanguage){
		   
		   String sendNotificationResponseAsJsonStr = null;
			try {
				restTemplate = new RestTemplate();
			    HttpHeaders sendNotificationRequestHeaders = new HttpHeaders();
			    sendNotificationRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
			    sendNotificationRequestHeaders.add("user-lang", userLanguage);
			    googleJWTokenGenerator.checkAndAddAuthorizationHeader(sendNotificationRequestHeaders);
			    HttpEntity<NotificationRequest> httpRequest = 
			    	      new HttpEntity<NotificationRequest>(sendNotificationRequest, sendNotificationRequestHeaders);
			    ObjectMapper objectMapper = new ObjectMapper();	
			    System.out.println("Calling Notification Service with request:"+sendNotificationRequest.toString()+"");
			    sendNotificationResponseAsJsonStr = 
			    	      restTemplate.postForObject(healthCompassNotificationServiceURL, httpRequest, String.class);
			    	    
			    
			}catch(Exception e) {
				System.out.println("Error from Notifications Service for the the request:"+sendNotificationRequest+"");
				System.out.println("Error Message is:"+e.getMessage()+"");
				// Logger Service
			}
			return sendNotificationResponseAsJsonStr;
		  
	} // 
		
	
	
}
