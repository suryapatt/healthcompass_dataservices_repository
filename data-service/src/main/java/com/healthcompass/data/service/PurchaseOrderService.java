package com.healthcompass.data.service;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.healthcompass.data.dao.BillPaymentDAO;
import com.healthcompass.data.dao.ProductDAO;
import com.healthcompass.data.dao.PurchaseOrderDAO;
import com.healthcompass.data.dao.ServiceBillItemDAO;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ProductListMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.PurchaseOrderMainViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Order;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.CreateNewBillPaymentMainViewValidator;
import com.healthcompass.data.validator.ProductListMainViewValidator;
import com.healthcompass.data.validator.PurchaseOrderMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderItemViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderMainViewDTO;
import com.healthcompass.data.view.dto.PurchaseOrderSummaryListViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;

@Service
public class PurchaseOrderService {
	
	
	
	
	@Autowired
	private PurchaseOrderDAO purchaseOrderDAO;
	
	@Autowired
	private ProductDAO productDAO;
	
	
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	
	
	@Autowired 
	PurchaseOrderMainViewModelAssembler purchaseOrderMainViewModelAssembler;
	
	@Autowired
	PurchaseOrderMainViewValidator purchaseOrderMainViewValidator;
	
	
	@Transactional(propagation = Propagation.REQUIRED )
	public PurchaseOrderMainViewDTO findOne(UUID id) {
		
		Optional<Order> purchaseOrder = purchaseOrderDAO.findOne(id);
		if(purchaseOrder == null) return null;
		Order order = purchaseOrder.get();
		PurchaseOrderMainViewDTO purchaseOrderMainViewDTO = purchaseOrderMainViewModelAssembler.toModel(order);
		
		return purchaseOrderMainViewDTO;
				
		
		
		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public PurchaseOrderMainViewDTO updatePurchaseOrder(PurchaseOrderMainViewDTO purchaseOrderMainViewDTO,BindingResult result) {
			
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		purchaseOrderMainViewValidator.customValidate(purchaseOrderMainViewDTO, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
	    
		
		Order persistedOrder = purchaseOrderDAO.createOrUpdatePurchaseOrder(purchaseOrderMainViewDTO);
	
		//get order id of persisted order. The order obtained above can be a lazily loaded list. Get the new state of the order
				// by retreiving the id and passing that id to DB.
				
		Optional<Order> purchaseOrder = purchaseOrderDAO.findOne(persistedOrder.getId());
		
		Order newOrUpdatedPurchaseOrder = purchaseOrder.get();		
		PurchaseOrderMainViewDTO updatedPurchaseOrderMainViewDTO = purchaseOrderMainViewModelAssembler.toModel(newOrUpdatedPurchaseOrder);
		
		
		
		return updatedPurchaseOrderMainViewDTO;
			
		   
			
			
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public PurchaseOrderMainViewDTO createPurchaseOrder(PurchaseOrderMainViewDTO purchaseOrderMainViewDTO,BindingResult result) {
			
		     
		
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		
		purchaseOrderMainViewValidator.customValidate(purchaseOrderMainViewDTO, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
		Order persistedOrder = purchaseOrderDAO.createOrUpdatePurchaseOrder(purchaseOrderMainViewDTO);
		
		//get order id of persisted order. The order obtained above can be a lazily loaded list. Get the new state of the order
		// by retreiving the id and passing that id to DB.
		
		Optional<Order> purchaseOrder = purchaseOrderDAO.findOne(persistedOrder.getId());
		
		Order newOrUpdatedPurchaseOrder = purchaseOrder.get();		
		PurchaseOrderMainViewDTO updatedPurchaseOrderMainViewDTO = purchaseOrderMainViewModelAssembler.toModel(newOrUpdatedPurchaseOrder);		
				
		
		
		
		
		return updatedPurchaseOrderMainViewDTO;
		//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
		   
			
			
	}
	
	public ProductSummaryOrganizationDTO getOrganizationProductSummary(UUID organizationId){
		return productDAO.findAllProductsByOrganization(organizationId,true); //actice = false as we allow inactive products in the creation of purchase order
	}
	
	public PurchaseOrderSummaryListViewDTO getPurchaseOrderSummarByOrganization(UUID organizationId){
		return purchaseOrderDAO.getPurchaseOrderSummaryListByOrganization(organizationId);
	}

		
	
}
