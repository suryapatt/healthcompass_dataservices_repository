package com.healthcompass.data.service.params;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDetailsSearchParams {
	
	
		
	private UUID organizationId;
	private UUID locationId;
	private Boolean active;
	Integer productId;
	private String productName;
	private List<String> batchNumbers;
	private java.sql.Date expiryDae;
	private Integer availableUnits;
	private String operatorForAvbUnits;	                


}
