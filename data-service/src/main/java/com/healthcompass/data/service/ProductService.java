package com.healthcompass.data.service;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.healthcompass.data.dao.BillPaymentDAO;
import com.healthcompass.data.dao.ProductDAO;
import com.healthcompass.data.dao.ServiceBillItemDAO;
import com.healthcompass.data.dto.entity.assembler.ProductListMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ProductListMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.model.Product;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.ProductSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.CreateNewBillPaymentMainViewValidator;
import com.healthcompass.data.validator.ProductListMainViewValidator;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentReceiptViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ProductListMainViewDTO;
import com.healthcompass.data.view.dto.ProductViewDTO;
import com.healthcompass.data.view.dto.ReceiptViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.querydsl.core.types.Predicate;

@Service
public class ProductService {
	
	
	@Autowired ServiceBillItemSearchPredicates preds;
	
	@Autowired
	private ProductDAO productDAO;
	
	@Autowired 
	ProductListMainViewModelAsembler productListMainViewModelAsembler;
	
	@Autowired 
	ProductListMainViewToEntityAssembler productListMainViewToEntityAssembler;
	
	@Autowired 
	ProductListMainViewValidator productListMainViewValidator;
	
	
		
	public ProductListMainViewDTO findAllProducts(ProductSearchParams productSearchParams){
		
		return productDAO.findAllProducts(productSearchParams);
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public ProductListMainViewDTO updateProducts(ProductListMainViewDTO productListMainViewDTO,BindingResult result) {
			
			    
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		productListMainViewValidator.customValidate(productListMainViewDTO, errorsMap,result);
		
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	    }
		
		
		List<Product> persistedProducts = productDAO.createOrUpdateProducts(productListMainViewDTO);
		
		//get products ids of the persisted products. The product list obtained above can be a lazily loaded list. Get the new state of the products
		// by retreiving their ids and passing that idlist to DB.
		
		List<Integer> persistedProductIdList = new ArrayList<Integer>();
		for (Product product : persistedProducts) {
			persistedProductIdList.add(product.getId());
		}
		
		List<Product> updatedProducts = productDAO.findAllProductsByIds(persistedProductIdList);
		
				
		ProductListMainViewDTO updatedProductListMainViewDTO = productListMainViewModelAsembler.toProductsModel(updatedProducts);
		
		
		
		return updatedProductListMainViewDTO;
		//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
		   
			
			
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public ProductListMainViewDTO createProducts(ProductListMainViewDTO productListMainViewDTO,BindingResult result) {
			
		    
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		productListMainViewValidator.customValidate(productListMainViewDTO, errorsMap,result);
		
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	    }
		
	    
		
		List<Product> persistedProducts = productDAO.createOrUpdateProducts(productListMainViewDTO);
		
		//get products ids of the persisted products. The product list obtained above can be a lazily loaded list. Get the new state of the products
		// by retreiving their ids and passing that idlist to DB.
		
		List<Integer> persistedProductIdList = new ArrayList<Integer>();
		for (Product product : persistedProducts) {
			persistedProductIdList.add(product.getId());
		}
		
		List<Product> updatedProducts = productDAO.findAllProductsByIds(persistedProductIdList);
		
				
		ProductListMainViewDTO updatedProductListMainViewDTO = productListMainViewModelAsembler.toProductsModel(updatedProducts);
		
		
		
		return updatedProductListMainViewDTO;
		//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
		   
			
			
	}
	
	public void deleteProduct(Integer productId) {
		productDAO.deleteProduct(productId);
	}
	
	public List<MetaTagViewDTO> getMetaTags() {
		return productDAO.getMetaTags();
	}

		
	
}
