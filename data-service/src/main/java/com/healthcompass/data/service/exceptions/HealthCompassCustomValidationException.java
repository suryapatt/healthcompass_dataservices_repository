package com.healthcompass.data.service.exceptions;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class HealthCompassCustomValidationException extends RuntimeException {

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BindingResult getErrors() {
		return errors;
	}

	public void setErrors(BindingResult errors) {
		this.errors = errors;
	}

	private String message;
	private BindingResult errors;
	private Map<String,Object> errorsMap  ;
	public HealthCompassCustomValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	public HealthCompassCustomValidationException(String message,BindingResult errors) {
		super(message);
		this.message=message;
		this.errors=errors;
		// TODO Auto-generated constructor stub
	}
	
	public HealthCompassCustomValidationException(String message,BindException errors) {
		super(message);
		this.message=message;
		this.errors=errors;
		// TODO Auto-generated constructor stub
	}
	
	public HealthCompassCustomValidationException(String message,BindingResult errors,Map<String,Object> errorsMap) {
		super(message);
		this.message=message;
		this.errors=errors;
		this.errorsMap=errorsMap;
		// TODO Auto-generated constructor stub
	}

	public Map<String, Object> getErrorsMap() {
		return errorsMap;
	}

	public void setErrorsMap(Map<String, Object> errorsMap) {
		this.errorsMap = errorsMap;
	}
	
}
