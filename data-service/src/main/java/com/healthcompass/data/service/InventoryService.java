package com.healthcompass.data.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.servlet.ServletRequest;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dao.InventoryDAO;
import com.healthcompass.data.dao.InventorySummaryDAO;
import com.healthcompass.data.dao.RateCardCategoryDAO;
import com.healthcompass.data.dao.RateCardDAO;
//import com.healthcompass.data.config.AuthenticationFacade;
import com.healthcompass.data.dto.entity.assembler.LocationEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;


import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.InventoryDetailsListMianViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.InventorySummaryListMianViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.RateCardModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.InventoryItem;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.LanguageTranslationServiceRequest;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.model.RateCardCategory;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.InventorySummaryRepository;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.RateCardCategoryRepository;
import com.healthcompass.data.repository.RateCardRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.RateCardSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.InventoryDetailsSearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.validator.InventoryDetailSearchValidator;
import com.healthcompass.data.validator.RateCardLocationValidator;
import com.healthcompass.data.view.dto.InventoryDetailsListMainViewDTO;
import com.healthcompass.data.view.dto.InventoryDetailsViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryListMainViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewResultsetExtractor;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryResultsetExtractor;
import com.healthcompass.data.view.dto.RateCrdCatDistinctDescResultsetExtractor;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
import com.querydsl.core.types.Predicate;



@Service
public class InventoryService {
	
	
	static String[] PAGE_HEADERS = { "bill_prhanization name", "bill_location_name" ,"inv_product_id", "inv_product_name", "inv_batch_number", "inv_expiry_date","inv_available_unit","inv_unit_rate", "inv_reorde_level", "inv_lead_time_in_daye"};
	
	@Autowired
	InventoryDAO inventoryDAO;
	
	@Autowired
	InventoryDetailSearchValidator inventoryDetailSearchValidator;
	
	@Autowired
	InventoryDetailsListMianViewModelAssembler iInventoryDetailsListMianViewModelAssembler;
	
	@Autowired
	LanguageTranslationService languageTranslationService;
	
	
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public InventoryDetailsListMainViewDTO findAllInventoryDetails(InventoryDetailsSearchParams inventoryDetailsSearchParams,BindingResult result){
		
		inventoryDetailSearchValidator.validate(inventoryDetailsSearchParams, result);
		if(result.hasErrors()) {
			throw new HealthCompassCustomValidationException("Custom Validation", result);
		}
		
		List<InventoryItem> inventoryList = inventoryDAO.findAllInventoryDetails(inventoryDetailsSearchParams);
		List<InventoryDetailsViewDTO> inventoryDetailsDTOList = iInventoryDetailsListMianViewModelAssembler.toInventoryDetailsViewModel(inventoryList);
		InventoryDetailsListMainViewDTO inventoryDetailsListMainViewDTO = new InventoryDetailsListMainViewDTO();
		
		inventoryDetailsListMainViewDTO.setProducts(inventoryDetailsDTOList);
		
		for(InventoryItem inventoryItem : inventoryList) {
			inventoryDetailsListMainViewDTO.setOrganizationId(inventoryItem.getOrganization().getId());
			inventoryDetailsListMainViewDTO.setOrganizationName(inventoryItem.getOrganization().getName());
			inventoryDetailsListMainViewDTO.setLocationId(inventoryItem.getLocation().getId());
			inventoryDetailsListMainViewDTO.setLocationName(inventoryItem.getLocation().getName());
			break;
		}	
		return inventoryDetailsListMainViewDTO;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	
	public  byte[]  downloadInventoryDetals(InventoryDetailsSearchParams inventoryDetailsSearchParams,BindingResult result,String userLanguage){
		
		inventoryDetailSearchValidator.validate(inventoryDetailsSearchParams, result);
		if(result.hasErrors()) {
			throw new HealthCompassCustomValidationException("Custom Validation", result);
		}
		
		byte[] excelData = null;
		List<InventoryItem> inventoryList = inventoryDAO.findAllInventoryDetails(inventoryDetailsSearchParams);
		
		LanguageTranslationServiceRequest languageTranslationServiceRequest = prepareLanguageTranslationServiceRequest(userLanguage);
		Map<String,String> languageTranslationsMap = languageTranslationService.sendToLanguagTranslationService(languageTranslationServiceRequest);
		
		try {
			Workbook workbook = new HSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("InventoryDetails");
			// Header
		    Row headerRow = sheet.createRow(0);
		    CellStyle style = workbook.createCellStyle();  
	       
            Font font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
           
		      for (int col = 0; col < 10; col++) {
		        Cell cell = headerRow.createCell(col);
		       
		        String headerValue = PAGE_HEADERS[col];
		        
		        cell.setCellStyle(style);  // for read only columns during update
		        cell.setCellValue(languageTranslationService.getTranslatedTextFromMap(headerValue,languageTranslationsMap));
		      }
		      
		      
		      int rowIdx = 1;
		         for(InventoryItem inventoryItem : inventoryList) {
				
					 Row row = sheet.createRow(rowIdx++);
					
					  //row.createCell(0).setCellValue(inventoryItem.getOrganization().getId().toString());
					  row.createCell(0).setCellValue(inventoryItem.getOrganization().getName());
					 // row.createCell(2).setCellValue(inventoryItem.getLocation().getId().toString());
					  row.createCell(1).setCellValue(inventoryItem.getLocation().getName());
					 // row.createCell(4).setCellValue(inventoryItem.getId().toString());
					
					  row.createCell(2).setCellValue(inventoryItem.getProduct().getId());
					  row.createCell(3).setCellValue(inventoryItem.getProduct().getName());
					  row.createCell(4).setCellValue(inventoryItem.getBatchNumber());
					  row.createCell(5).setCellValue(HealthCompassConstants.convertSQLDateToLocalDateString(inventoryItem.getExpiryDate()));
					  row.createCell(6).setCellValue(inventoryItem.getAvailableUnits());
					  
					  row.createCell(7).setCellValue(inventoryItem.getUnitRate());
					  
					  
					  
					  row.createCell(8).setCellValue(inventoryItem.getProduct().getReOrderLevel());
					  row.createCell(9).setCellValue(inventoryItem.getProduct().getLeadTime());
		        	
		         }	
					
					
		     
			
			workbook.write(out);
			//return new ByteArrayInputStream(out.toByteArray());
			excelData = out.toByteArray();
		} catch (IOException e) {
		      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
		return excelData;	
		 
		
		
	}
	
	
	private LanguageTranslationServiceRequest prepareLanguageTranslationServiceRequest(String localeLanguage) {
		LanguageTranslationServiceRequest languageTranslationServiceRequest = new LanguageTranslationServiceRequest();
		languageTranslationServiceRequest.setLanguageTranslationServiceURL(languageTranslationService.languageTranslationServiceUrlPrefix+localeLanguage+languageTranslationService.languageTranslationServiceUrlSuffix);
		return languageTranslationServiceRequest;
		
		
	}
	
	
}