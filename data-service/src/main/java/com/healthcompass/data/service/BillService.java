package com.healthcompass.data.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.model.NotificationRequest;
import com.healthcompass.data.dao.AttachmentDAO;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import org.springframework.stereotype.Service;

import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.InAppNotificationServiceRequest;
import com.healthcompass.data.model.LanguageTranslationServiceRequest;
import com.healthcompass.data.model.PractitionerRole;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.SmsServiceRequest;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.PractitionerRoleRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.ServiceBillHistorySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.GoogleJWTokenGenerator;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.util.HealthCompassValidationUtils;
import com.healthcompass.data.validator.GenerateBillMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.view.dto.ServiceBillHistoryMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.querydsl.core.types.Predicate;

@Service
public class BillService {
	
	
	@Autowired 
	ServiceBillSearchPredicates preds;
	
	@Autowired
	private ServiceBillDAO serviceBillDAO;
	
	@Autowired
	private PractitionerRoleRepository practitionerRoleRepository;
	
	@Autowired
	private ServiceBillModelAssembler serviceBillModelAssembler;
	
	@Autowired
	private ServiceBillEntityAssembler serviceBillEntityAssembler;
	
	@Autowired
	ServiceBillMainViewModelAsembler serviceBillMainViewModelAsembler;
	
	@Autowired
	ServiceBillMainViewValidator serviceBillMainViewValidator;
	
	@Autowired
	GenerateBillMainViewValidator generateBillMainViewValidator;
	
	
	
	@Autowired
	BillPaymentService billPaymentService;
	
	@Autowired
	AttachmentService attachmentService;
	
	@Autowired
	InAppNotificationService inAppNotificationService;
	
	@Autowired
	LanguageTranslationService languageTranslationService;
	
	@Autowired
	SmsService smsService;
	
	
	
	@Autowired
	AttachmentDAO attachmentDAO;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${hc-notification-service-url}")
	private String healthCompassNotificationServiceURL;
	
	@Autowired
	GoogleJWTokenGenerator googleJWTokenGenerator;
	
	private Predicate toPredicate(ServiceBillSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isActive(true))
				.and(preds.isIdsInList(params.getIdList())
				.and(preds.isEncounterIdEq(params.getEncounterId()))
				.and(preds.isPatientIdEq(params.getPatientId()))
				.and(preds.isPractitionerRoleIdEq(params.getPractitionarRoleId())));
						
				
		// @formatter:on
	}
	
	public List<ServiceBill> findAll(){
		//return serviceRepository.findAll();
		return null;
	}
	
	@Transactional(propagation = Propagation.REQUIRED )
	public ServiceBillViewMainDTO findOne(UUID id) {
		
		Optional<ServiceBill> serviceBill = serviceBillDAO.findOne(id);
		if(serviceBill == null) return null;
		ServiceBill bill = serviceBill.get();
		ServiceBillViewMainDTO serviceBillViewMainDTO = serviceBillMainViewModelAsembler.toModel(bill);
		Double outStandingPatientBillsAmount = serviceBillDAO.getOutStandingPendingBillsAmount(serviceBillViewMainDTO.getPatientId());
		serviceBillViewMainDTO.setOutStandingPendingBillsAmount(outStandingPatientBillsAmount);
		
		return serviceBillViewMainDTO;
				
		
		
		
	}
	
	public ByteArrayOutputStream generatePDF(ServiceBill serviceBill,Map<String,String> languageTranslationsMap) {
		
				
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML");
		 
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		 
		
		Context context = new Context();
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("serviceBill", serviceBill);

        context.setVariables(variables);
		context = populateBillTemplate(serviceBill, context,languageTranslationsMap);
		
		 
		// Get the plain HTML with the resolved ${name} variable!
		String htmloUtput = templateEngine.process("Bill_Template", context);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			
			
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(htmloUtput);
			renderer.layout();
			renderer.createPDF(baos);
			
			
			System.out.println("size of file:"+baos.size());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return baos;
		
		
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED )	 
	public CollectionModel<ServiceBillViewMainDTO> findAll(ServiceBillSearchParams serviceBillSearchParams,BindingResult result){
		List<ServiceBill> serviceBills = serviceBillDAO.findAll(serviceBillSearchParams);
		if(serviceBills !=  null && serviceBills.isEmpty()) {
			throw new HealthCompassCustomValidationException("No Bills found", result);
		}
		CollectionModel<ServiceBillViewMainDTO> serviceBillMainDTOList = serviceBillMainViewModelAsembler.toCollectionModel(serviceBills);
		for(ServiceBillViewMainDTO serviceBillMainDTO : serviceBillMainDTOList) {
			Double outStandingPatientBillsAmount = serviceBillDAO.getOutStandingPendingBillsAmount(serviceBillMainDTO.getPatientId());
			serviceBillMainDTO.setOutStandingPendingBillsAmount(outStandingPatientBillsAmount);
		}
			
		
		return serviceBillMainDTOList;
		
	}
	@Transactional(propagation = Propagation.REQUIRED )
	public ServiceBillViewMainDTO save(ServiceBillViewMainDTO serviceBillMainViewDTO,BindingResult result) {
		
	    
		
		/*serviceBillMainViewValidator.validate(serviceBillMainViewDTO, result);
		if(result.hasErrors()) {
			throw new HealthCompassCustomValidationException("Custom Validation", result);
		}*/
		
		Map<String,Object> errorsMap = new HashMap<String,Object>();
		
		serviceBillMainViewValidator.customValidate(serviceBillMainViewDTO, errorsMap,result);
		
		if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
		}
		
		ServiceBill persistedServiceBill = serviceBillDAO.save(serviceBillMainViewDTO,false);
		Optional<ServiceBill> serviceBill = serviceBillDAO.getById(persistedServiceBill.getId());
		
		ServiceBill newOrUpdatedServiceBill = serviceBill.get();
		
		return serviceBillMainViewModelAsembler.toModel(newOrUpdatedServiceBill);
		
    	
    	
    	//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
				
	}
	

@Transactional(propagation = Propagation.REQUIRED )	
public ServiceBillViewMainDTO update(ServiceBillViewMainDTO serviceBillMainViewDTO,BindingResult result) {
		
	     // return serviceBillDAO.update(serviceBillMainViewDTO);
	/*serviceBillMainViewValidator.validate(serviceBillMainViewDTO, result);
	if(result.hasErrors()) {
		throw new HealthCompassCustomValidationException("Custom Validation", result);
	}*/
	
	Map<String,Object> errorsMap = new HashMap<String,Object>();
	serviceBillMainViewValidator.customValidate(serviceBillMainViewDTO, errorsMap,result);
	 
	if(errorsMap.keySet().size()>0) {
		if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
			throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
		}
		
		
	}
	
	
	
	ServiceBill persistedServiceBill = serviceBillDAO.save(serviceBillMainViewDTO,false);
	Optional<ServiceBill> serviceBill = serviceBillDAO.getById(persistedServiceBill.getId());
	
	ServiceBill newOrUpdatedServiceBill = serviceBill.get();
	
	
	ServiceBillViewMainDTO updatedServiceBillViewMainDTO = serviceBillMainViewModelAsembler.toModel(newOrUpdatedServiceBill);
	
	
	Double outStandingPatientBillsAmount = (Double) serviceBillDAO.getOutStandingPendingBillsAmount(newOrUpdatedServiceBill.getPatient().getId());
	updatedServiceBillViewMainDTO.setOutStandingPendingBillsAmount(outStandingPatientBillsAmount);
	return updatedServiceBillViewMainDTO;
	//return serviceBillMainViewModelAsembler.toModel(serviceBillDAO.save(serviceBillMainViewDTO));	
	   
		
		
}

@Transactional(propagation = Propagation.REQUIRED )	
//public ServiceBillViewMainDTO generateServiceBill(ServiceBillViewMainDTO serviceBillMainViewDTO,BindingResult result) {
public byte[] generateServiceBill(ServiceBillViewMainDTO serviceBillMainViewDTO,BindingResult result,String userLanguage) {
		
	
    // return serviceBillDAO.update(serviceBillMainViewDTO);
	// remove this after testing
	//Optional<ServiceBill> testBill = serviceBillDAO.getById(serviceBillMainViewDTO.getServiceBill().getServiceBillId());
	//ServiceBill newOrUpdatedServiceBill = testBill.get();
	//ByteArrayOutputStream baos = generatePDF(newOrUpdatedServiceBill);
	//long size=baos.size();
	//byte[] pdfData =  baos.toByteArray();
	
	//System.out.println("size of file:"+baos.size());
	//AttachmentServiceRequest attachmentServiceRequest = prepareAttachmentServiceRequest(pdfData,size,newOrUpdatedServiceBill);
	//UUID attachmentId = attachmentService.sendToAttachmentService(attachmentServiceRequest);
	//attachmentService.updateAttachmentDetails("billing.service_bill", "attachment_id", attachmentId,newOrUpdatedServiceBill.getId());
	//return pdfData;
	// remove until above
	
	// below is actual
	
	//generateBillMainViewValidator.validate(serviceBillMainViewDTO, result);
	Map<String,Object> errorsMap = new HashMap<String,Object>();
	generateBillMainViewValidator.customValidate(serviceBillMainViewDTO, errorsMap,result);
	//if(result.hasErrors()) {
		//throw new HealthCompassCustomValidationException("Custom Validation", result);
	//}   
	 if(errorsMap.keySet().size()>0) {
			if(HealthCompassValidationUtils.checkAndclearErrorMarker(errorsMap, false)) {
				throw new HealthCompassCustomValidationException("Custom Validation", result,errorsMap);
			}
			
			
	}
		
	ServiceBill persistedServiceBill = serviceBillDAO.save(serviceBillMainViewDTO,true);
	Optional<ServiceBill> serviceBill = serviceBillDAO.getById(persistedServiceBill.getId());
	ServiceBill newOrUpdatedServiceBill = serviceBill.get();
	
	
	// create a child bill payment with the bill number generated from above bill. we cannot put this logic in service bill save logic as the order of update service bill parent with bill number nd inert billpayment with new bill number is not gauranteed
	// so we will make sure bill number is saved to Service Bill and them create child BillPayment.// sort of hack
	LanguageTranslationServiceRequest languageTranslationServiceRequest = prepareLanguageTranslationServiceRequest(userLanguage);
	Map<String,String> languageTranslationsMap = languageTranslationService.sendToLanguagTranslationService(languageTranslationServiceRequest);
	
	billPaymentService.createBillPaymentFromBillAndSave(newOrUpdatedServiceBill,languageTranslationsMap);
	
	
	ByteArrayOutputStream baos = generatePDF(newOrUpdatedServiceBill,languageTranslationsMap);
	long size=baos.size();
	byte[] pdfData =  baos.toByteArray();
	//prepare the json data for Attachment service
	AttachmentServiceRequest attachmentServiceRequest = prepareAttachmentServiceRequest(pdfData,size,newOrUpdatedServiceBill);
	//send the pdf to Attachment service and get the new Attachment ID
	UUID attachmentId = attachmentService.sendToAttachmentService(attachmentServiceRequest);
	// Update the bill with new attachment id
	attachmentService.updateAttachmentDetails("billing.service_bill", "attachment_id", attachmentId,newOrUpdatedServiceBill.getId());
	
	
	//prepare the json data for In App Notification service
	//InAppNotificationServiceRequest inAppNotificationServiceRequest = prepareInAppNotificationServiceRequest(newOrUpdatedServiceBill,attachmentId);
	//send the notification request to InAppNotificationService
	//inAppNotificationService.sendToInAppNotification(inAppNotificationServiceRequest);
	
	//send the notification request to Notification Service, so that sms, email and push notifications are handled
	 /*NotificationRequest sendNotificationRequest = new  NotificationRequest();
	 sendNotificationRequest.setMessageTriggerId(16);
	 HashMap messageKeys = new HashMap<String,String>();
	 messageKeys.put("patient_user_name", newOrUpdatedServiceBill.getPatient().getAuthUser().getUserName());
	 messageKeys.put("patient_first_name", newOrUpdatedServiceBill.getPatient().getDecodedName());
	 messageKeys.put("patient_email", newOrUpdatedServiceBill.getPatient().getDecodedEmail());
	 messageKeys.put("patient_phone_number", newOrUpdatedServiceBill.getPatient().getDecodedPhone());
	 messageKeys.put("unique_identifier", newOrUpdatedServiceBill.getBillNumber());
	 messageKeys.put("bill_amount", String.format("%.2f%n", newOrUpdatedServiceBill.getAmountBilled()));
	 messageKeys.put("visit_number", Integer.toString(newOrUpdatedServiceBill.getEncounter().getEncounterNumber()));
     messageKeys.put("organization_name", newOrUpdatedServiceBill.getEncounter().getOrganization().getName());
     messageKeys.put("location_name", newOrUpdatedServiceBill.getEncounter().getLocation().getName());
     messageKeys.put("attachment_link", attachmentService.attachmentServiceUrl+attachmentId+"");
							
	 sendNotificationRequest.setMessageKeys(messageKeys);
	 sendToNotificationService(sendNotificationRequest,"en");*/
	 
	//prepare the json data for sms service
	SmsServiceRequest  smsServiceRequest = prepareSmsServiceRequest(newOrUpdatedServiceBill,attachmentId);
	//send the notification request to InAppNotificationService
	smsService.sendToSmsService(smsServiceRequest);
	
	
	// Return the PDF data to client
	return pdfData;
	
		
		
}



public boolean isBillExistsForPatientEncounterAndPractitioner(UUID patientId,UUID encounterId,UUID practitionerId) {
	
	return serviceBillDAO.checkIfBillExistsForPatientEncounterAndPractitioner(patientId, encounterId, practitionerId);
	
	
}	

public void deleteServiceBill(UUID id) {
	serviceBillDAO.deleteServiceBill(id);
}
	
private Context populateBillTemplate(ServiceBill serviceBill, Context context,Map<String,String> languageTranslationsMap) {
	
	
	context.setVariable("organizationName", serviceBill.getEncounter().getOrganization().getName());
	context.setVariable("billNumber", serviceBill.getBillNumber());
	context.setVariable("locationName", serviceBill.getEncounter().getLocation().getName());
	context.setVariable("admissionTime", HealthCompassConstants.convertSQLDateTimeStampToLocalTimeStampDateString(serviceBill.getEncounter().getAdmissionTime()));
	context.setVariable("dischargeTime", HealthCompassConstants.convertSQLDateTimeStampToLocalTimeStampDateString(serviceBill.getEncounter().getDischargeTime()));
	context.setVariable("billDate", HealthCompassConstants.convertSQLDateTimeStampToLocalTimeStampDateString(serviceBill.getBillDate()));
	context.setVariable("practitionerName", serviceBill.getPractitionerRole().getPractitioner().getDecodedName());
	context.setVariable("encounterNumber", serviceBill.getEncounter().getEncounterNumber());
	context.setVariable("appointmentNumber", serviceBill.getEncounter().getAppointment().getId());
	context.setVariable("specialty", serviceBill.getEncounter().getAppointment().getSpeciality().getDisplay());
	context.setVariable("patientName", serviceBill.getPatient().getDecodedName());
	context.setVariable("patientAge", serviceBill.getPatient().getAge(serviceBill.getPatient().getDecodedDateOfBirth()));
	context.setVariable("patientAddress", serviceBill.getPatient().getDecodedAddress());
	context.setVariable("patientEmail", serviceBill.getPatient().getDecodedEmail());
	context.setVariable("patientPhone", serviceBill.getPatient().getDecodedPhone());
	context.setVariable("paymentMode", serviceBill.getPaymentMode().getCode());
	context.setVariable("serviceBillItems", serviceBill.getServiceBillItems());
	
	context.setVariable("subTotal", String.format("%.2f%n", serviceBill.getSubTotal()));
	context.setVariable("discount", String.format("%.2f%n", serviceBill.getDiscount()));
	context.setVariable("amountBilled", String.format("%.2f%n", serviceBill.getAmountBilled()));
	context.setVariable("amountPaid", String.format("%.2f%n", serviceBill.getAmountPaid()));
	context.setVariable("amountDue", String.format("%.2f%n", serviceBill.getAmountDue()));
	
	// populate translated labels
	
	context.setVariable("bill_organization",languageTranslationService.getTranslatedTextFromMap("bill_organization",languageTranslationsMap));
	context.setVariable("bill_bill_no",languageTranslationService.getTranslatedTextFromMap("bill_bill_no",languageTranslationsMap));
	context.setVariable("bill_location",languageTranslationService.getTranslatedTextFromMap("bill_location",languageTranslationsMap));
	context.setVariable("bill_date_time",languageTranslationService.getTranslatedTextFromMap("bill_date_time",languageTranslationsMap));
	context.setVariable("bill_spaciality",languageTranslationService.getTranslatedTextFromMap("bill_spaciality",languageTranslationsMap));
	context.setVariable("bill_appointment_no",languageTranslationService.getTranslatedTextFromMap("bill_appointment_no",languageTranslationsMap));
	context.setVariable("bill_practtioner_name",languageTranslationService.getTranslatedTextFromMap("bill_practtioner_name",languageTranslationsMap));
	context.setVariable("bill_enclounter_no",languageTranslationService.getTranslatedTextFromMap("bill_enclounter_no",languageTranslationsMap));
	context.setVariable("bill_patient_name",languageTranslationService.getTranslatedTextFromMap("bill_patient_name",languageTranslationsMap));
	context.setVariable("bill_age",languageTranslationService.getTranslatedTextFromMap("bill_age",languageTranslationsMap));
	context.setVariable("bill_address",languageTranslationService.getTranslatedTextFromMap("bill_address",languageTranslationsMap));
	context.setVariable("bill_admission_date_time",languageTranslationService.getTranslatedTextFromMap("bill_admission_date_time",languageTranslationsMap));
	context.setVariable("bill_phone",languageTranslationService.getTranslatedTextFromMap("bill_phone",languageTranslationsMap));
	context.setVariable("bill_discharge_date_time",languageTranslationService.getTranslatedTextFromMap("bill_discharge_date_time",languageTranslationsMap));
	context.setVariable("bill_email",languageTranslationService.getTranslatedTextFromMap("bill_email",languageTranslationsMap));
	context.setVariable("bill_mode_of_payment",languageTranslationService.getTranslatedTextFromMap("bill_mode_of_payment",languageTranslationsMap));
	context.setVariable("bill_particulars",languageTranslationService.getTranslatedTextFromMap("bill_particulars",languageTranslationsMap));
	context.setVariable("bill_quantity",languageTranslationService.getTranslatedTextFromMap("bill_quantity",languageTranslationsMap));
	context.setVariable("bill_unit_price",languageTranslationService.getTranslatedTextFromMap("bill_unit_price",languageTranslationsMap));
	context.setVariable("bill_amount_rs",languageTranslationService.getTranslatedTextFromMap("bill_amount_rs",languageTranslationsMap));
	context.setVariable("bill_discount",languageTranslationService.getTranslatedTextFromMap("bill_discount",languageTranslationsMap));
	context.setVariable("bill_total",languageTranslationService.getTranslatedTextFromMap("bill_total",languageTranslationsMap));
	context.setVariable("bill_amount_paid",languageTranslationService.getTranslatedTextFromMap("bill_amount_paid",languageTranslationsMap));
	context.setVariable("bill_amount_due",languageTranslationService.getTranslatedTextFromMap("bill_amount_due",languageTranslationsMap));
	context.setVariable("bill_sub_total",languageTranslationService.getTranslatedTextFromMap("bill_sub_total",languageTranslationsMap));
	context.setVariable("bill_thnakyou_text",languageTranslationService.getTranslatedTextFromMap("bill_thnakyou_text",languageTranslationsMap));
	
	
	
	return context;
}	



private AttachmentServiceRequest prepareAttachmentServiceRequest (byte[] pdfData,long size,ServiceBill serviceBill) {
	AttachmentServiceRequest attachmentServiceRequest = new AttachmentServiceRequest();
	attachmentServiceRequest.setContentType(20503);	// value set type - DataTypes - Valueset - Attachment
	//attachmentServiceRequest.setCreation(new java.sql.Timestamp(System.currentTimeMillis()));
	attachmentServiceRequest.setData(pdfData);
	attachmentServiceRequest.setHash(null);
	attachmentServiceRequest.setLanguage(1152); // value set type - languages, Valueset - english
	attachmentServiceRequest.setSize(size);
	attachmentServiceRequest.setTitle("Bill-"+serviceBill.getBillNumber());
	attachmentServiceRequest.setUpdatedBy(serviceBill.getUpdatedBy());
	attachmentServiceRequest.setUpdatedOn(new java.sql.Timestamp(System.currentTimeMillis()));
	attachmentServiceRequest.setUrl("");
	
	return attachmentServiceRequest;
	
	
}

private InAppNotificationServiceRequest prepareInAppNotificationServiceRequest (ServiceBill serviceBill,UUID attachmentId) {
	InAppNotificationServiceRequest inAppNotificationServiceRequest = new InAppNotificationServiceRequest();
	inAppNotificationServiceRequest.setAction_url(attachmentService.attachmentServiceUrl+attachmentId);
	inAppNotificationServiceRequest.setBody(" Dear "+serviceBill.getPatient().getDecodedName()+", your bill with Bill Number:"+serviceBill.getBillNumber()+" has been generated. Please clcik the link provided to view/download the Bill");
	inAppNotificationServiceRequest.setMessage_type("INAPP");
	inAppNotificationServiceRequest.setMessage_status("New");
	inAppNotificationServiceRequest.setSender_id("123");
	inAppNotificationServiceRequest.setRecipient_id(Integer.toString(serviceBill.getPatient().getAuthUser().getId()));
		
	return inAppNotificationServiceRequest;
	
	
}

private SmsServiceRequest prepareSmsServiceRequest(ServiceBill serviceBill,UUID attachmentId) {
	SmsServiceRequest smsServiceRequest = new SmsServiceRequest();
	smsServiceRequest.setPhoneNumber(serviceBill.getPatient().getDecodedPhone());
	smsServiceRequest.setMessage(" Dear "+serviceBill.getPatient().getDecodedName()+", your bill with Bill Number:"+serviceBill.getBillNumber()+" has been generated. Please clcik on "+attachmentService.attachmentServiceUrl+attachmentId+" to view/download the Bill");
	
	return smsServiceRequest;
	
	
}

private LanguageTranslationServiceRequest prepareLanguageTranslationServiceRequest(String localeLanguage) {
	LanguageTranslationServiceRequest languageTranslationServiceRequest = new LanguageTranslationServiceRequest();
	languageTranslationServiceRequest.setLanguageTranslationServiceURL(languageTranslationService.languageTranslationServiceUrlPrefix+localeLanguage+languageTranslationService.languageTranslationServiceUrlSuffix);
	return languageTranslationServiceRequest;
	
	
}

@Transactional(propagation = Propagation.REQUIRED )	 
public ServiceBillHistoryMainViewDTO findBillingHistory(ServiceBillHistorySearchParams serviceBillHistorySearchParams){
	boolean isPatientBillHistory = serviceBillHistorySearchParams.getPatientId() != null ? true : false;
	boolean isPractitionerHistory = false;
	boolean isProviderHistory=false;
	PractitionerRole practitionerRole =  null;
	if(!isPatientBillHistory) {
		
		if(serviceBillHistorySearchParams.getPractitionarRoleId() == null && serviceBillHistorySearchParams.getProviderId() !=  null) {
			// search by provider (organization)
			isProviderHistory=true;
			
			Integer doctorCode = Integer.valueOf(HealthCompassConstants.PRACTITIONER_ROLE_DOCTOR_ID);
			UUID organizationId = serviceBillHistorySearchParams.getProviderId();
			List<Object> practitionerRoleIdsAsObject = (List<Object>)  practitionerRoleRepository.getAllDoctorRoleIdsForOrganization(doctorCode,organizationId);
			List<UUID> practitionerRoleIds = new ArrayList<UUID>();
             for(Object pracRoleId : practitionerRoleIdsAsObject) {
				
				UUID id = UUID.fromString((String) pracRoleId) ;
				practitionerRoleIds.add(id);
			}
			if(practitionerRoleIds==null || practitionerRoleIds.isEmpty()) {
				List<ServiceBill> historyModels = new ArrayList<ServiceBill>();
				ServiceBillHistoryMainViewDTO serviceBillHistoryMainDTO = serviceBillMainViewModelAsembler.toServiceBillHistoryModel(historyModels,isPatientBillHistory,isProviderHistory);
				serviceBillHistoryMainDTO.setProviderId(serviceBillHistorySearchParams.getProviderId());
				return serviceBillHistoryMainDTO;
				//practitionerRoleIds = new ArrayList<UUID>();
				//practitionerRoleIds.add(serviceBillHistorySearchParams.getPractitionarRoleId());
				
			}
			serviceBillHistorySearchParams.setPractitionarRoleIds(practitionerRoleIds);
		
		}else {
			// search by practitioner (doctor/others)
			isPractitionerHistory=true;
			Optional<PractitionerRole> optPractitionerRole =  practitionerRoleRepository.findById(serviceBillHistorySearchParams.getPractitionarRoleId());
			if(!optPractitionerRole.isPresent()) return new ServiceBillHistoryMainViewDTO();
			practitionerRole = optPractitionerRole.get();
			List<UUID> practitionerRoleIds = new ArrayList<UUID>();
			Integer doctorCode = Integer.valueOf(HealthCompassConstants.PRACTITIONER_ROLE_DOCTOR_ID);
			if(practitionerRole.getCode().compareTo(doctorCode)==0) { // The practitioner is a doctor
				practitionerRoleIds.add(serviceBillHistorySearchParams.getPractitionarRoleId());
			}else { // get all the practitioner ids who are doctors that are frpm same location of this current practitioner
				UUID practitionerLocationId = practitionerRole.getLocation().getId();
				List<Object> practitionerRoleIdsAsObject = (List<Object>)  practitionerRoleRepository.getAllDoctorRoleIdsForALocation(doctorCode,practitionerLocationId);
				
	             for(Object pracRoleId : practitionerRoleIdsAsObject) {
					
					UUID id = UUID.fromString((String) pracRoleId) ;
					practitionerRoleIds.add(id);
				}
				if(practitionerRoleIds==null || practitionerRoleIds.isEmpty()) {
					practitionerRoleIds = new ArrayList<UUID>();
					practitionerRoleIds.add(serviceBillHistorySearchParams.getPractitionarRoleId());
					
				}
				
			}
			serviceBillHistorySearchParams.setPractitionarRoleIds(practitionerRoleIds);
			if(practitionerRoleIds==null || practitionerRoleIds.isEmpty()) {
				List<ServiceBill> historyModels = new ArrayList<ServiceBill>();
				ServiceBillHistoryMainViewDTO serviceBillHistoryMainDTO = serviceBillMainViewModelAsembler.toServiceBillHistoryModel(historyModels,isPatientBillHistory,isProviderHistory);
				serviceBillHistoryMainDTO.setPractitionerId(serviceBillHistorySearchParams.getPractitionarRoleId());
				return serviceBillHistoryMainDTO;
				
			}
			
			
		}
		
		
	}
	List<ServiceBill> serviceBills = serviceBillDAO.findBillingHistory(serviceBillHistorySearchParams);
	
	
	ServiceBillHistoryMainViewDTO serviceBillHistoryMainDTO = serviceBillMainViewModelAsembler.toServiceBillHistoryModel(serviceBills,isPatientBillHistory,isProviderHistory);
	
	if(!isPatientBillHistory && practitionerRole != null) {
		serviceBillHistoryMainDTO.setPractitionerId(practitionerRole.getId());
		serviceBillHistoryMainDTO.setPractitionerName("Dr. "+practitionerRole.getPractitioner().getDecodedName());
	}
	
	
	return serviceBillHistoryMainDTO;
	
}

public String  sendToNotificationService(NotificationRequest sendNotificationRequest,String userLanguage){
	   
	   String sendNotificationResponseAsJsonStr = null;
		try {
			restTemplate = new RestTemplate();
		    HttpHeaders sendNotificationRequestHeaders = new HttpHeaders();
		    sendNotificationRequestHeaders.setContentType(MediaType.APPLICATION_JSON);
		    sendNotificationRequestHeaders.add("user-lang", userLanguage);
		    googleJWTokenGenerator.checkAndAddAuthorizationHeader(sendNotificationRequestHeaders);
		    HttpEntity<NotificationRequest> httpRequest = 
		    	      new HttpEntity<NotificationRequest>(sendNotificationRequest, sendNotificationRequestHeaders);
		    ObjectMapper objectMapper = new ObjectMapper();	
		    System.out.println("Calling Notification Service with request:"+sendNotificationRequest.toString()+"");
		    sendNotificationResponseAsJsonStr = 
		    	      restTemplate.postForObject(healthCompassNotificationServiceURL, httpRequest, String.class);
		    	    
		    
		}catch(Exception e) {
			System.out.println("Error from Notifications Service for the the request:"+sendNotificationRequest+"");
			System.out.println("Error Message is:"+e.getMessage()+"");
			// Logger Service
		}
		return sendNotificationResponseAsJsonStr;
	  
} // 
	
}
