package com.healthcompass.data.service.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.common.base.MoreObjects;

@ControllerAdvice
@RestController
@Validated
public class CustomizedResponseEntityExceptionHandler 
extends ResponseEntityExceptionHandler{
    
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex,WebRequest request) {

		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
				ex.getMessage(),request.getDescription(false));
		
		return new ResponseEntity(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<Object> handleAllExceptions(RecordNotFoundException ex,WebRequest request) {

		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
				ex.getMessage(),request.getDescription(false));
		
		return new ResponseEntity(exceptionResponse,HttpStatus.NOT_FOUND);
	}
	
	/*@ExceptionHandler(HealthCompassCustomValidationException.class)
	public final ResponseEntity<Object> handleValidationException(Exception ex,WebRequest request) {
		HttpHeaders headers = new HttpHeaders();
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
				ex.getMessage(),request.getDescription(false));
		HttpStatus status = HttpStatus.BAD_REQUEST;
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        HealthCompassCustomValidationException customException = (HealthCompassCustomValidationException) ex;
        //Get all errors
        if(customException.getErrors() == null || !customException.getErrors().hasErrors()) {
        	

            body.put("message", customException.getMessage()); // single message
        }else if (customException.getErrors().getFieldErrors().isEmpty()) {
        	String message = customException.getErrors().getGlobalError().getDefaultMessage();
        	body.put("message", message); // single message
        }else {
        	// list of errors
        	List<String> errors = customException.getErrors()
                    .getFieldErrors()
                    .stream()
                    .map(x -> x.getField()+" : " + x.getDefaultMessage())
                    .collect(Collectors.toList());

            body.put("errors", errors);
        }
        

        return new ResponseEntity<>(body, headers, status);
	}*/
	
	@ExceptionHandler(HealthCompassCustomValidationException.class)
	public final ResponseEntity<Object> handleValidationException(Exception ex,WebRequest request) {
		HttpHeaders headers = new HttpHeaders();
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
				ex.getMessage(),request.getDescription(false));
		HttpStatus status = HttpStatus.BAD_REQUEST;
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        HealthCompassCustomValidationException customException = (HealthCompassCustomValidationException) ex;
        //Get all errors
        if(customException.getErrorsMap() != null && customException.getErrorsMap().size() > 0) {
        	 body.put("errors", customException.getErrorsMap());
        }else if(customException.getErrors() == null || !customException.getErrors().hasErrors()) {
        	

            body.put("message", customException.getMessage()); // single message
        }else if (customException.getErrors().getFieldErrors().isEmpty()) {
        	String message = customException.getErrors().getGlobalError().getDefaultMessage();
        	body.put("message", message); // single message
        }
        

        return new ResponseEntity<>(body, headers, status);
	}
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),
				ex.getBindingResult().toString(),request.getDescription(false));
		
		//return new ResponseEntity(exceptionResponse,HttpStatus.BAD_REQUEST);
		
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getField()+" : " + x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, headers, status);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ex.printStackTrace();
		Map<String, Object> responseJSON = new HashMap<String,Object>();
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		if(InvalidFormatException.class.isAssignableFrom(rootCause.getClass())){
			InvalidFormatException ife = (InvalidFormatException)rootCause;
			List<Reference> refs = ife.getPath();
			for(Reference ref : refs){
				if(!responseJSON.containsKey(ref.getFieldName())){
					if(ref.getFieldName() == null) continue;
					responseJSON.put(ref.getFieldName(), new ArrayList<String>());
				}
				String errorMessage = null;
				if(Number.class.isAssignableFrom(ife.getTargetType())){
					errorMessage = "Invalid numeric value found: For input string: "+ ife.getValue();
				}
				((ArrayList<String>)responseJSON.get(ref.getFieldName())).add(MoreObjects.firstNonNull(errorMessage, ife.getMessage()));
			}
		}
		if(responseJSON.isEmpty()){
			responseJSON.put("Message", rootCause.getMessage());
		}
		return new ResponseEntity<Object>(responseJSON,HttpStatus.BAD_REQUEST);
	}
	
	
	protected ResponseEntity<Object> OldhandleHttpMessageNotReadable(
			HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		//return handleExceptionInternal(ex, null, headers, status, request);
		
		
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        //Get all errors
       /* List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());*/

       // body.put("errors", errors);
        body.put("errors", ex.getLocalizedMessage());

        return new ResponseEntity<>(body, headers, status);
	}
}
