package com.healthcompass.data.service;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.LocationModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.LocationRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.LocationSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.view.dto.LocationDropDownListViewDTO;
import com.healthcompass.data.view.dto.LocationDropDownListViewResultSetExtrator;
import com.querydsl.core.types.Predicate;

@Component
@Transactional
public class LocationDAOService {
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private LocationDropDownListViewResultSetExtrator locationDropDownListViewResultSetExtrator;
	
	
	@Autowired 
	LocationSearchPredicates preds;
	
	@Autowired
	private LocationRepository locationRepository;
	
	
	@Autowired
	private LocationModelAssembler locationModelAssembler;
	
	@Autowired
	private ServiceBillEntityAssembler serviceBillEntityAssembler;
	
	private Predicate toPredicate(LocationSearchParams params) {

		// @formatter:off
		return preds.isIdEq(params.getId())
				.and(preds.isOrganizationIdEq(params.getOrganizationId()))
				.and(preds.isIdsInList(params.getIdList()));
				
		// @formatter:on
	}
	
	
	
	
	public Optional<LocationModel> findOne(UUID id) {
		
		
		return locationRepository.findById(id)
		.map(locationModelAssembler::toModel) ;
		//return serviceBill;
		
		//Optional<User> user = userRepository.findById(id);
		//Optional
		 //Resource<User> resource =  new Resource<User>(user);
		// return resource;
		
	}
	
	public CollectionModel<LocationModel> findAll(LocationSearchParams locationSearchParams){
		List<Location> locations = (List<Location>) locationRepository.findAll(toPredicate(locationSearchParams));
		return locationModelAssembler.toCollectionModel(locations);
		
		
		
	}
	
	
public LocationDropDownListViewDTO getLocationsDropDownByOrganization(UUID organizationId)  {
		
		String LOCATION_DROPDOWN_SEARCH_QUERY= "select org.id as organizationId,org.name as organizationName,loc.id as locationId,loc.name as locationName \r\n" + 
				"	  from project_asclepius.location loc \r\n" + 
				"	  inner join project_asclepius.organization org on loc.managing_organization=org.id " +
				"     where org.id ='" + organizationId+"' and loc.status="+HealthCompassConstants.LOCATION_STATUS_ACTIVE_ID+ " "+
				"   order by locationName asc ";
		
		
		
		
		return (LocationDropDownListViewDTO) jdbcTemplate.query(LOCATION_DROPDOWN_SEARCH_QUERY,locationDropDownListViewResultSetExtrator);
	}
	
	
	
	
	
	
	
}
