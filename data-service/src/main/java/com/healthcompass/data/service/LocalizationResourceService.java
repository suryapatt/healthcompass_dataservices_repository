package com.healthcompass.data.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale.Category;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.healthcompass.data.dao.AttachmentDAO;
import com.healthcompass.data.dao.LocalizationResourceDAO;
import com.healthcompass.data.dao.MarketerDAO;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dao.SupplierDAO;
import com.healthcompass.data.dto.entity.assembler.MarketerMainViewToEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.ServiceBillEntityAssembler;
import com.healthcompass.data.dto.entity.assembler.SupplierMainViewToEntityAssembler;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.assembler.MarketerMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.MarketerViewModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.dto.model.assembler.SupplierMainViewModelAsembler;
import com.healthcompass.data.dto.model.assembler.SupplierViewModelAssembler;

import org.springframework.stereotype.Service;

import com.healthcompass.data.model.AttachmentServiceRequest;
import com.healthcompass.data.model.HelpPage;
import com.healthcompass.data.model.HelpPageDefinition;
import com.healthcompass.data.model.HelpPageDetail;
import com.healthcompass.data.model.HelpPageDetailDefinition;
import com.healthcompass.data.model.HelpPageSection;
import com.healthcompass.data.model.HelpPageSectionDefinition;
import com.healthcompass.data.model.HelpPageSectionDetail;
import com.healthcompass.data.model.HelpPageSectionDetailDefinition;
import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceField;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.LocalizationResourceSection;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.HelpPageDetailRepository;
import com.healthcompass.data.repository.HelpPageRepository;
import com.healthcompass.data.repository.HelpPageSectionRepository;
import com.healthcompass.data.repository.LanguageRepository;
import com.healthcompass.data.repository.LocalizationResourceGroupRepository;
import com.healthcompass.data.repository.LocalizationResourceRepository;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;

import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.HelpPagSectioneDownLoadParams;
import com.healthcompass.data.service.params.HelpPageDownLoadParams;
import com.healthcompass.data.service.params.LocalizedResourceSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.util.ExcelHelper;
import com.healthcompass.data.util.HealthCompassConstants;
import com.healthcompass.data.validator.MarketerMainViewValidator;
import com.healthcompass.data.validator.MarketerUploadMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.validator.SupplierMainViewValidator;
import com.healthcompass.data.view.dto.LocalizationPageViewDTO;
import com.healthcompass.data.view.dto.LocalizationResourcePageResultsetExtractor;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.ProductSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.RateCardSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.ResourceCategoryViewDTO;
import com.healthcompass.data.view.dto.ResourceGroupViewDTO;
import com.healthcompass.data.view.dto.ResourceViewDTO;
import com.healthcompass.data.view.dto.ServiceBillItemViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;
import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.querydsl.core.types.Predicate;

@Service
public class LocalizationResourceService {
	
	
		
	@Autowired
	private LocalizationResourceDAO localizationResourceDAO;
	
	@Autowired
	private LocalizationResourceGroupRepository localizationResourceGroupRepository;
	
	@Autowired
	private HelpPageDetailRepository helpPageDetailRepository;
	
	@Autowired
	private HelpPageRepository helpPageRepository;
	
	@Autowired
	private HelpPageSectionRepository helpPageSectionRepository;
	
	
	@Autowired
	private LanguageRepository languageRepository;
	
	@Autowired
	private  LocalizationResourcePageResultsetExtractor localizationResourcePageResultsetExtractor;
	
	
	static String[] PAGE_HEADERS = { "Language", "S.No.", "Type", "Page ID" ,"Page Name", "Page Def ID", "Page Display Namec", "Page Description" ,"Field Position", "Page Detail ID", "Field Name", "Data_type" ,"Section", "Section ID", "Display", "Page Detail definition ID" ,"Field Display Name","Field Description"};
	static String[] SECTION_HEADERS = { "Language", "S.No.", "Type", "Section ID" ,"Section Name", "Section Def ID", "Section Display Name", "Section Description" ,"Field Position", "Section Detail ID", "Field Name", "Self Referrence","Self Referrence ID", "Display", "Section Detail definition ID" ,"Field Display Name","Field Description"};
	
	
	
	public List<ResourceViewDTO> getLocalizedTextForResources(LocalizedResourceSearchParams searchParams) {
		
		
		return localizationResourceDAO.getLocalizedTextForResources(searchParams);
		
		
	}
	public ResourceViewDTO getLocalizedText(String resourceId) {
		ResourceViewDTO resourceViewDTO = new ResourceViewDTO();
		LocalizedResourceSearchParams searchParams = new LocalizedResourceSearchParams();
		List<String> resourceIds = new ArrayList<String>();
		resourceIds.add(resourceId);
		searchParams.setResourceIds(resourceIds);
		List<ResourceViewDTO> resourceViewDTOs =  localizationResourceDAO.getLocalizedTextForResources(searchParams);
		if(resourceViewDTOs != null && !resourceViewDTOs.isEmpty()) {
			resourceViewDTO =  (ResourceViewDTO) resourceViewDTOs.get(0);
		}
		return resourceViewDTO;
		
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	 
	 public String upLoadHelpPageFields(MultipartFile file) {
		 
	 	 BindException be = new BindException(file,"File Upload");
	 	 
	 	if (ExcelHelper.hasExcelFormat(file)) {
	 		
	 		//LocalizationViewDTO localizationViewDTO= null;
	 		
	 		InputStream is = null;
	 		
	 			try {
	 				is =file.getInputStream();
	 			}catch(Exception e) {
	       	 
	 				be.addError(new FieldError("Upload File:","File","Cannot read uploaded file"));
	 				throw new HealthCompassCustomValidationException("Custom Validation", be);
	 			}
	 			
	 		     
	 		      
	 		      // Parse the excel and form heirarchial structure ReourceGroup > Categories > resources
	 			 List<HelpPage> upLoadedHelpPages = ExcelHelper.excelToHelpPages(is);
	 			 
	 	         // save the uploaded resource groups, categories and resources
	 	    	 List<HelpPage> persistedHelpPage = localizationResourceDAO.saveAllHelpPageFields(upLoadedHelpPages);
	 	    		
	 	    	Set<String> pageIds = new HashSet<String>();
	    		Set<String> languageCodes = new HashSet<String>();
	    		
	    		
	    		for (HelpPage uploadedPage : upLoadedHelpPages) {
	    			
	    			pageIds.add(uploadedPage.getId());
	    			for(HelpPageDefinition pageDef : uploadedPage.getPageDefinitions() ) {
	    				languageCodes.add(pageDef.getLanguage());
	    			}
	    			
	    		}
	    		
	    		//List<LocalizationPageViewDTO> helpPages = localizationResourceDAO.getPersistedResourceGroups(languageIds, resourceIds,resourceGroupCategoryIds, resourceGroupCodes);
	 	    	
	    		byte[] pdfData = null;
	    		
	    		Map<String,Map<String,HelpPage>> helPagesMap = localizationResourceDAO.getPersistedPagesForDisplay(languageCodes,pageIds);
	    		Storage storage = StorageOptions.newBuilder().setProjectId("health-compass-302720").build().getService();
	    		for(String languageCode : helPagesMap.keySet()) {
	    			Map<String,HelpPage> languageMap =  (Map<String,HelpPage>) helPagesMap.get(languageCode);
	    			for(String pageId : languageMap.keySet()) {
	    				HelpPage languagePage = (HelpPage) languageMap.get(pageId);
	    				pdfData = generatePDF(languageCode,languagePage);
	    				BlobId blobId = BlobId.of("health-compass-localization", languageCode+"/"+languagePage.getPageName()+"/");
	     			    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
	     			    storage.create(blobInfo,pdfData);

	     			    System.out.println(
	     			        "File " + languageCode+"/"+languagePage.getPageName()+"/" + " uploaded to bucket ");

	    				
	    				
	    			} // end for each page
	    		} // end for each language
	 	    	
	 	
	 	
	 	}	
	 	
	 	return "Success";
  }

	
	@Transactional(propagation = Propagation.REQUIRED)
	 
	 public String upLoadHelpPageSections(MultipartFile file) {
		 
	 	 BindException be = new BindException(file,"File Upload");
	 	 
	 	if (ExcelHelper.hasExcelFormat(file)) {
	 		
	 		//LocalizationViewDTO localizationViewDTO= null;
	 		
	 		InputStream is = null;
	 		
	 			try {
	 				is =file.getInputStream();
	 			}catch(Exception e) {
	       	 
	 				be.addError(new FieldError("Upload File:","File","Cannot read uploaded file"));
	 				throw new HealthCompassCustomValidationException("Custom Validation", be);
	 			}
	 			
	 		     
	 		      
	 		      // Parse the excel and form heirarchial structure ReourceGroup > Categories > resources
	 			 List<HelpPageSection> upLoadedHelpPageSections = ExcelHelper.excelToHelpPageSections(is);
	 			 
	 	         // save the uploaded resource groups, categories and resources
	 	    	 List<HelpPageSection> persistedHelpPageSections = localizationResourceDAO.saveAllHelpPageSections(upLoadedHelpPageSections);
	 	    		
	 	    	Set<String> sectionIds = new HashSet<String>();
	    		Set<String> languageCodes = new HashSet<String>();
	    		
	    		
	    		for (HelpPageSection uploadedSection : persistedHelpPageSections) {
	    			
	    			sectionIds.add(uploadedSection.getId());
	    			for(HelpPageSectionDefinition secDef : uploadedSection.getSectionDefinitions()) {
	    				languageCodes.add(secDef.getLanguage());
	    			}
	    			
	    		}
	 	    	
	 	       Set<String> pageIds = (Set<String>) helpPageDetailRepository.getDistinctPageIds(sectionIds);
	 	       if(pageIds == null || pageIds.isEmpty()) {
	 	    		return "Success";
	 	       }
	 	       byte[] pdfData = null;
	    		
	    		Map<String,Map<String,HelpPage>> helPagesMap = localizationResourceDAO.getPersistedPagesForDisplay(languageCodes,pageIds);
	    		Storage storage = StorageOptions.newBuilder().setProjectId("health-compass-302720").build().getService();
	    		for(String languageCode : helPagesMap.keySet()) {
	    			Map<String,HelpPage> languageMap =  (Map<String,HelpPage>) helPagesMap.get(languageCode);
	    			for(String pageId : languageMap.keySet()) {
	    				HelpPage languagePage = (HelpPage) languageMap.get(pageId);
	    				pdfData = generatePDF(languageCode,languagePage);
	    				BlobId blobId = BlobId.of("health-compass-localization", languageCode+"/"+languagePage.getPageName()+"/");
	     			    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
	     			    storage.create(blobInfo,pdfData);

	     			    System.out.println(
	     			        "File " + languageCode+"/"+languagePage.getPageName()+"/" + " uploaded to bucket ");

	    				
	    				
	    			} // end for each page
	    		} // end for each language
	 	
	 	}	
	 	
	 	return "Success";
 }
	
	
 
	@Transactional(propagation = Propagation.REQUIRED)
	public String generateHelpPage(String lang,String pgId){
		byte[] pdfData = null;
		Set<String> langSet = new HashSet<String>();
		langSet.add(lang);
		Set<String> pgSet = new HashSet<String>();
		pgSet.add(pgId);
		Map<String,Map<String,HelpPage>> helPagesMap = localizationResourceDAO.getPersistedPagesForDisplay(langSet,pgSet);
		Storage storage = StorageOptions.newBuilder().setProjectId("health-compass-302720").build().getService();
		for(String languageCode : helPagesMap.keySet()) {
			Map<String,HelpPage> languageMap =  (Map<String,HelpPage>) helPagesMap.get(languageCode);
			for(String pageId : languageMap.keySet()) {
				HelpPage languagePage = (HelpPage) languageMap.get(pageId);
				pdfData = generatePDF(languageCode,languagePage);
				BlobId blobId = BlobId.of("health-compass-localization", languageCode+"/"+languagePage.getPageName()+"/");
 			    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
 			    storage.create(blobInfo,pdfData);

 			    System.out.println(
 			        "File " + languageCode+"/"+languagePage.getPageName()+"/" + " uploaded to bucket ");

				
				
			}
		}
		return "success";		
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public byte[] downLoadHelpPageFromDB(HelpPageDownLoadParams helpPageDownLoadParams){
		byte[] excelData = null;
		Set<String> langSet = new HashSet<String>();
		if(helpPageDownLoadParams.getLanguageCodeList() != null && !helpPageDownLoadParams.getLanguageCodeList().isEmpty()) {
			for(String langCode : helpPageDownLoadParams.getLanguageCodeList() ) {
				langSet.add(langCode);
			}
		}
		boolean isAllLanguages = langSet.isEmpty() ||
				langSet.contains("All") ||
				langSet.contains("ALL")||
				langSet.contains("all")||
		         langSet.contains("*");
		
		if(isAllLanguages) {
			List<Object[]> availableLanguageList = (List<Object[]>) helpPageRepository.getAvailablePageLanguages();
			langSet.clear();
			for(Object[] language : availableLanguageList) {
				langSet.add((String) language[0]);
			}
		}
		
		Set<String> pgSet = new HashSet<String>();
		
		if(helpPageDownLoadParams.getPageIdList() != null && !helpPageDownLoadParams.getPageIdList().isEmpty()) {
			for(String pageId : helpPageDownLoadParams.getPageIdList() ) {
				pgSet.add(pageId);
			}
		}
		
		boolean isAllPages = pgSet.isEmpty() ||
				pgSet.contains("All")
				|| pgSet.contains("ALL")
				|| pgSet.contains("all")
				|| pgSet.contains("*");
		
				
		if(isAllPages) {
			List<Object[]> availablePageList = (List<Object[]>) helpPageRepository.getAvailablePageNames();
			pgSet.clear();
			for(Object[] page : availablePageList) {
				pgSet.add((String) page[0]);
			}
		}
		
		
		
		
		Map<String,Map<String,HelpPage>> helpPagesMap = localizationResourceDAO.getPersistedPagesForDownload(langSet,pgSet);
		
		try {
			Workbook workbook = new HSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("Page_Fields");
			// Header
		    Row headerRow = sheet.createRow(0);
		    CellStyle style = workbook.createCellStyle();  
	            // Setting Background color  
            style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());  
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND); 
            //style.setFillPattern(FillPatternType.BIG_SPOTS);  
            Font font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
           
		      for (int col = 0; col < 18; col++) {
		        Cell cell = headerRow.createCell(col);
		       
		        String headerValue = PAGE_HEADERS[col];
		        if(headerValue.equals("Page ID") || headerValue.equals("Page Detail ID") || headerValue.equals("Page Detail definition ID") || headerValue.equals("Page Def ID")) {
		        	 cell.setCellStyle(style);  // for read only columns during update
		        }
		        cell.setCellValue(headerValue);
		      }
		      
		      
				
					
					
		      int rowIdx = 1;
			for(String languageCode : helpPagesMap.keySet()) {
				Map<String,HelpPage> languageMapUnsorted =  (Map<String,HelpPage>) helpPagesMap.get(languageCode);
				Map<String, HelpPage> languageMap = new TreeMap<String, HelpPage>(languageMapUnsorted);
				for (Map.Entry<String, HelpPage> entry : languageMap.entrySet()) {
				//for(String pageId : languageMap.keySet()) {
					String pageId = entry.getKey();
					HelpPage languagePage = (HelpPage) languageMap.get(pageId);
					for(HelpPageDefinition pageDBDef : languagePage.getPageDefinitions()) {
						
						
						for(HelpPageDetail pageDBDet : languagePage.getDetails()) {
							if(pageDBDet.getDetailDefinitions() == null) {
								Row row = sheet.createRow(rowIdx++);
								
								 // Page values
								  row.createCell(0).setCellValue(pageDBDef.getLanguage());
								  row.createCell(1).setCellValue(languagePage.getSerialNumber());
								  row.createCell(2).setCellValue(languagePage.getType());
								  row.createCell(3).setCellValue(languagePage.getId());
								  row.createCell(4).setCellValue(languagePage.getPageName());
								 // Page Def values 
								  row.createCell(5).setCellValue(pageDBDef.getId());
								  row.createCell(6).setCellValue(pageDBDef.getDisplayName());
								  row.createCell(7).setCellValue(pageDBDef.getDescription());
								  
								  // Page Fields
								  row.createCell(8).setCellValue(pageDBDet.getPosition());
								  row.createCell(9).setCellValue(pageDBDet.getId());
								  row.createCell(10).setCellValue(pageDBDet.getFieldName());
								  row.createCell(11).setCellValue(pageDBDet.getDataType());
								  row.createCell(12).setCellValue(pageDBDet.getSectionName());
								  row.createCell(13).setCellValue(pageDBDet.getSectionId());
								  row.createCell(14).setCellValue(pageDBDet.getIsDisplay());
								  
								  // Field definitions
								  row.createCell(15).setCellValue("");
								  row.createCell(16).setCellValue("");
								  row.createCell(17).setCellValue("");
								 
													
							}else {
								for(HelpPageDetailDefinition detDBDef : pageDBDet.getDetailDefinitions()) {
									//create each excel row here
									
									 Row row = sheet.createRow(rowIdx++);
		
									 // Page values
									  row.createCell(0).setCellValue(pageDBDef.getLanguage());
									  row.createCell(1).setCellValue(languagePage.getSerialNumber());
									  row.createCell(2).setCellValue(languagePage.getType());
									  row.createCell(3).setCellValue(languagePage.getId());
									  row.createCell(4).setCellValue(languagePage.getPageName());
									 // Page Def values 
									  row.createCell(5).setCellValue(pageDBDef.getId());
									  row.createCell(6).setCellValue(pageDBDef.getDisplayName());
									  row.createCell(7).setCellValue(pageDBDef.getDescription());
									  
									  // Page Fields
									  row.createCell(8).setCellValue(pageDBDet.getPosition());
									  row.createCell(9).setCellValue(pageDBDet.getId());
									  row.createCell(10).setCellValue(pageDBDet.getFieldName());
									  row.createCell(11).setCellValue(pageDBDet.getDataType());
									  row.createCell(12).setCellValue("");
									  row.createCell(13).setCellValue("");
									  row.createCell(14).setCellValue(pageDBDet.getIsDisplay());
									  
									  // Field definitions
									  row.createCell(15).setCellValue(detDBDef.getId());
									  row.createCell(16).setCellValue(detDBDef.getDisplayName());
									  row.createCell(17).setCellValue(detDBDef.getDescription());
									 
																
									
								}  //  end page details def
							}
							
						}	// end page details
							break; // break after first page definition
					  } // end pageDefinitions
					
				} // end each page
			 } // end each language
			workbook.write(out);
			//return new ByteArrayInputStream(out.toByteArray());
			excelData = out.toByteArray();
		} catch (IOException e) {
		      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
		return excelData;		
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public byte[] downLoadHelpPageSectionFromDB(HelpPagSectioneDownLoadParams helpPageSectionDownLoadParams){
		
		byte[] excelData = null;
		
		Set<String> langSet = new HashSet<String>();
		
		if(helpPageSectionDownLoadParams.getLanguageCodeList() != null && !helpPageSectionDownLoadParams.getLanguageCodeList().isEmpty()) {
			for(String langCode : helpPageSectionDownLoadParams.getLanguageCodeList() ) {
				langSet.add(langCode);
			}
		}
		boolean isAllLanguages = langSet.isEmpty() ||
				langSet.contains("All") ||
				langSet.contains("ALL")||
				langSet.contains("all")||
		        langSet.contains("*");
		
		if(isAllLanguages) {
			List<Object[]> availableLanguageList = (List<Object[]>) helpPageRepository.getAvailablePageLanguages();
			langSet.clear();
			for(Object[] language : availableLanguageList) {
				langSet.add((String) language[0]);
			}
		}
		
		Set<String> secSet = new HashSet<String>();
		
		if(helpPageSectionDownLoadParams.getSectionIdList() != null && !helpPageSectionDownLoadParams.getSectionIdList().isEmpty()) {
			for(String pageId : helpPageSectionDownLoadParams.getSectionIdList() ) {
				secSet.add(pageId);
			}
		}
		
		boolean isAllSections = secSet.isEmpty() ||
				secSet.contains("All")
				|| secSet.contains("ALL")
				|| secSet.contains("all")
				|| secSet.contains("*");
		
				
		if(isAllSections) {
			List<Object[]> availableSectionList = (List<Object[]>) helpPageSectionRepository.getAvailableSectionNames();
			secSet.clear();
			for(Object[] section : availableSectionList) {
				secSet.add((String) section[0]);
			}
		}
		
		
		
		
		
		
		
		
		Map<String,Map<String,HelpPageSection>> sectionsMap = localizationResourceDAO.getPersistedSectionsForDownload(langSet,secSet);
		
		try {
			Workbook workbook = new HSSFWorkbook();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("Page_Sections");
			// Header
		    Row headerRow = sheet.createRow(0);
		   
		    CellStyle style = workbook.createCellStyle();  
	            // Setting Background color  
	        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());  
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND); 
	        //style.setFillPattern(FillPatternType.BIG_SPOTS);  
	        Font font = workbook.createFont();
	        font.setBold(true);
	        style.setFont(font);
          
           
		      for (int col = 0; col < 17; col++) {
		        Cell cell = headerRow.createCell(col);
		       
		        String headerValue = SECTION_HEADERS[col];
		        if(headerValue.equals("Section ID") || headerValue.equals("Section Detail ID") || headerValue.equals("Section Detail definition ID") || headerValue.equals("Section Def ID")) {
		        	 cell.setCellStyle(style);  // for read only columns during update
		        }
		        cell.setCellValue(headerValue);
		      }
		      
		      int rowIdx = 1;
			for(String languageCode : sectionsMap.keySet()) {
				Map<String,HelpPageSection> languageMapUnsorted =  (Map<String,HelpPageSection>) sectionsMap.get(languageCode);
				Map<String, HelpPageSection> languageMap = new TreeMap<String, HelpPageSection>(languageMapUnsorted);
				for (Map.Entry<String, HelpPageSection> entry : languageMap.entrySet()) {
				//for(String sectionId : languageMap.entrySet()) {
					String sectionId = entry.getKey();
					HelpPageSection languagePage = (HelpPageSection) languageMap.get(sectionId);
					for(HelpPageSectionDefinition sectionDBDef : languagePage.getSectionDefinitions()) {
						
						
						for(HelpPageSectionDetail sectionDBDet : languagePage.getSectionDetail()) {
							for(HelpPageSectionDetailDefinition sectionDetDBDef : sectionDBDet.getSectionDetailDefinitions()) {
								Row row = sheet.createRow(rowIdx++);
								
								 // Section values
								  row.createCell(0).setCellValue(sectionDBDef.getLanguage());
								  row.createCell(1).setCellValue(languagePage.getSerialNumber());
								  row.createCell(2).setCellValue(languagePage.getType());
								  row.createCell(3).setCellValue(languagePage.getId());
								  row.createCell(4).setCellValue(languagePage.getSectionName());
								 
								  // Section Def values 
								  row.createCell(5).setCellValue(sectionDBDef.getId());
								  row.createCell(6).setCellValue(sectionDBDef.getDisplayName());
								  row.createCell(7).setCellValue(sectionDBDef.getDescription());
								  
								  // Section Fields
								  row.createCell(8).setCellValue(sectionDBDet.getPosition());
								  row.createCell(9).setCellValue(sectionDBDet.getId());
								  row.createCell(10).setCellValue(sectionDBDet.getFieldName());
								  row.createCell(11).setCellValue(sectionDBDet.getSelfReferrence());
								  row.createCell(12).setCellValue(sectionDBDet.getSelfReferrenceId());
								   row.createCell(13).setCellValue(sectionDBDet.getIsDisplay());
								  
								  // Field definitions
								   row.createCell(14).setCellValue(sectionDetDBDef.getId());
								   row.createCell(15).setCellValue(sectionDetDBDef.getDisplayName());
								   row.createCell(16).setCellValue(sectionDetDBDef.getDescription());
								 
													
							} // end section details def
							
						}	// end section detatilspage details
							break; // break after first section definition
					  } // end sectionDefinitions
					
				} // end each section
			 } // end each language
			workbook.write(out);
			//return new ByteArrayInputStream(out.toByteArray());
			excelData = out.toByteArray();
		} catch (IOException e) {
		      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
		return excelData;		
	}
	
	public byte[]  generatePDF(String languageCode,HelpPage page) {
		
		byte[] pdfData = null;
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML");
		 
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		 
		
		Context context = new Context();
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("page",page);

        context.setVariables(variables);
	//	context = populateBillTemplate(languageCode,page, context);
		
		 
		// Get the plain HTML with the resolved ${name} variable!
		String htmloUtput = templateEngine.process("localization_page_template", context);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			
			
			
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(htmloUtput);
			renderer.layout();
			renderer.createPDF(baos);
			pdfData =  baos.toByteArray();
			
			System.out.println("size of file:"+baos.size());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return pdfData;
		
		
		
	}
	
	private Context populateHelpPageTemplTemplate(String languageCode,  HelpPage page, Context context) {
		
		
		
		context.setVariable("page", page);
		
		return context;
	}	

	
	private LocalizationResourceCategory checkIfCategoryNeedtoBeSavedToBucket(String resourceCategoryId, List<LocalizationResourceCategory> resourceCategories) {
		  
		  if(resourceCategories == null || resourceCategories.isEmpty()) {
			  return null;
		  }
		  
		  Optional<LocalizationResourceCategory> optTargetResourceCategory = resourceCategories.stream()
					.filter(resourceCategory -> resourceCategory.getResourceCategoryId().equals(resourceCategoryId)).findFirst();

					    if(optTargetResourceCategory.isPresent())  //check this logic where the chilkd
					    	
					    {
					    	return optTargetResourceCategory.get();
					    }else{
					    	
					    	
					    	return null;
					    }
	  }	
	
	
	 public byte[]  getHelpPage(String languageCode,String pageId) {
		 
		    Storage storage = StorageOptions.newBuilder().setProjectId("health-compass-302720").build().getService();
		    Blob blob = storage.get(BlobId.of("health-compass-localization", languageCode+"/"+pageId+"/"));
			byte [] helpPage = blob.getContent();
			return helpPage;
			
 }
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	 public Map<String,String>  getAllPageLanguages() {
		 
		    return localizationResourceDAO.getAllPageLanguages();
			
			
     }
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	 public Map<String,String>  getAllSectionLanguages() {
		 
		    return localizationResourceDAO.getAllSectionLanguages();
			
			
     }
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	 public Map<String,String>  getAllPageNames() {
		 
		    return localizationResourceDAO.getAllPageNames();
			
			
     }
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	 public Map<String,String>  getAllSectionNames() {
		 
		    return localizationResourceDAO.getAllSectionNames();
			
			
     }
	
}

	

