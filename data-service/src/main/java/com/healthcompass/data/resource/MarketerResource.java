package com.healthcompass.data.resource;

import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.service.MarketerService;
import com.healthcompass.data.service.SupplierServuce;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.validator.MarketerMainViewValidator;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;



@BasePathAwareController
@RequestMapping("/marketers")
public class MarketerResource {
	
	
	@Autowired
	private MarketerService marketerService;
	
	
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<MarketerMainViewDTO> getMarketer(@PathVariable("id") Integer id) 
	{
		return new ResponseEntity<>(marketerService.getMarketer(id), 
				HttpStatus.OK);
	}
	
	
	
	@PostMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<MarketerMainViewDTO> createMarketer(@RequestBody MarketerMainViewDTO marketerMainViewDTO,BindingResult result)
	{
		MarketerMainViewDTO updatedMarketerMainViewDTOO = marketerService.createMarketer(marketerMainViewDTO,result);
		return ResponseEntity.ok().body(updatedMarketerMainViewDTOO);
	}

	@PutMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<MarketerMainViewDTO> updateMarketer(@RequestBody MarketerMainViewDTO marketerMainViewDTO,BindingResult result)
	{
		MarketerMainViewDTO updatedMarketerMainViewDTO = marketerService.updateMarketer(marketerMainViewDTO, result);
		return ResponseEntity.ok().body(updatedMarketerMainViewDTO);
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object>  deleteMarketer(@PathVariable Integer id) {
		 marketerService.deleteMarketer(id);
		 return ResponseEntity.ok().build();
				
	}
	
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<MarketerSummaryOrganizationDTO> getMarketerSummaryByOrganization(@RequestBody MarketerSummaryViewSearchParams marketerSummaryViewSearchParams) 
	{
		
		return new ResponseEntity<>(marketerService.getMarketerSummaryByOrganization(marketerSummaryViewSearchParams), HttpStatus.OK);
    }
	
	 @PostMapping("/upload")
	  public ResponseEntity<MarketerUploadMainViewDTO> uploadFile(@RequestParam("file") MultipartFile file) {
		 
		 MarketerUploadMainViewDTO marketerUploadMainViewDTO = marketerService.upLoadMarketers(file);
		 return ResponseEntity.ok().body(marketerUploadMainViewDTO);
			
	   

}
}