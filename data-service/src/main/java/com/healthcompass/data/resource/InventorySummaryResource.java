package com.healthcompass.data.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.InventorySummary;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceSearchPredicates;
import com.healthcompass.data.service.InventoryService;
import com.healthcompass.data.service.InventorySummaryService;
import com.healthcompass.data.service.RateCardCategoryService;
import com.healthcompass.data.service.RateCardService;


import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.querydsl.core.types.Predicate;

import jdk.jfr.BooleanFlag;
import lombok.val;

import com.healthcompass.data.service.params.HelpPageDownLoadParams;
import com.healthcompass.data.service.params.InventoryDetailsSearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchParams;
import com.healthcompass.data.service.params.InventorySummarySearchProductParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.SalesOrderInventoryProductSearchParams;
import com.healthcompass.data.service.params.ServiceSearchParams;
import com.healthcompass.data.validator.RateCardLocationValidator;
import com.healthcompass.data.view.dto.InventoryDetailsListMainViewDTO;
import com.healthcompass.data.view.dto.InventorySummaryListMainViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ProductDropDownListViewDTO;
import com.healthcompass.data.view.dto.SalesOrderProductSearchSummaryListViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;


@BasePathAwareController
@RequestMapping("/inventorySummaries")
public class InventorySummaryResource {
	
	
	
	@Autowired
	private InventorySummaryService inventorySummaryService;
	
	@Autowired
	private InventoryService inventoryService;
	
	
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	
	public ResponseEntity<InventorySummaryListMainViewDTO> getInventorySummaries(@RequestBody InventorySummarySearchParams inventorySummarySearchParams,BindingResult result)  {
	
		
		return new ResponseEntity<>(inventorySummaryService.findAllInventorySummary(inventorySummarySearchParams,result), 
		HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/search/advanced/inventoryDetails", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	
	public ResponseEntity<InventoryDetailsListMainViewDTO> getInventoryDetails(@RequestBody InventoryDetailsSearchParams inventoryDetailsSearchParams,BindingResult result)  { 
	
		
		return new ResponseEntity<>(inventoryService.findAllInventoryDetails(inventoryDetailsSearchParams,result), 
		HttpStatus.OK);
	}	
	
	
	 @PostMapping("/summary/download")
	  public  void downloadInventorySummary(@RequestBody InventorySummarySearchParams inventorySummarySearchParams,BindingResult result,HttpServletResponse response,@RequestHeader Map<String, String> headers) throws IOException {
		  
		 String defaultuserLanguage="en_US";
		String userLocaleLnaguage=defaultuserLanguage;
		if(headers.get("user-lang") != null) {
				userLocaleLnaguage= (String) headers.get("user-lang");
		}
		 byte[] excelContent = inventorySummaryService.downloadInventorySummary(inventorySummarySearchParams,result,userLocaleLnaguage);
		 InputStream is = new ByteArrayInputStream(excelContent);
		// response.setContentType("application/octet-stream");
		 response.setContentType("application/vnd.ms-excel");
		 ServletOutputStream outputStream = response.getOutputStream();
		 int chunk = 1024;
		 byte[] buffer = new byte[chunk];
		 int bytesRead = -1;
		 while ((bytesRead = is.read(buffer)) != -1) {
			 outputStream.write(buffer, 0, bytesRead);
			}
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename=inventory_summary.xls";
	        //"Content-Type" : "application/vnd.ms-excel"
	        	
	        response.setHeader(headerKey, headerValue);
		    outputStream.flush();

	       // return new HttpEntity<>(new ByteArrayResource(excelContent), header);
			
	   

    }
	 
	 @PostMapping("/details/download")
	  public  void downloadInventoryDetails(@RequestBody InventoryDetailsSearchParams inventoryDetailsSearchParams,BindingResult result,HttpServletResponse response,@RequestHeader Map<String, String> headers) throws IOException {
		 
		 String defaultuserLanguage="en_US";
			String userLocaleLnaguage=defaultuserLanguage;
			if(headers.get("user-lang") != null) {
					userLocaleLnaguage= (String) headers.get("user-lang");
			}
		 byte[] excelContent = inventoryService.downloadInventoryDetals(inventoryDetailsSearchParams,result,userLocaleLnaguage);
		 InputStream is = new ByteArrayInputStream(excelContent);
		// response.setContentType("application/octet-stream");
		 response.setContentType("application/vnd.ms-excel");
		 ServletOutputStream outputStream = response.getOutputStream();
		 int chunk = 1024;
		 byte[] buffer = new byte[chunk];
		 int bytesRead = -1;
		 while ((bytesRead = is.read(buffer)) != -1) {
			 outputStream.write(buffer, 0, bytesRead);
			}
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename=inventory_details.xls";
	        //"Content-Type" : "application/vnd.ms-excel"
	        	
	        response.setHeader(headerKey, headerValue);
		    outputStream.flush();

	       // return new HttpEntity<>(new ByteArrayResource(excelContent), header);
			
	   

   }
	 @PostMapping("/search/productOptions")
	 public ResponseEntity<ProductDropDownListViewDTO> getDistinctProductsForInventorySummarySearch(@RequestBody InventorySummarySearchProductParams inventorySummarySearchProductParams)  {
			
			return new ResponseEntity<>(inventorySummaryService.getDistinctProductsForInventorySummarySearch(inventorySummarySearchProductParams), 
			HttpStatus.OK);
		}
	
}

	
	


