package com.healthcompass.data.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.OrganizationModel;
import com.healthcompass.data.dto.model.ServiceBillItemModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.dto.model.assembler.OrganizationModelAssembler;
import com.healthcompass.data.dto.model.assembler.ServiceBillItemModelAssembler;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.OrganizationRepository;
import com.healthcompass.data.repository.ServiceBillItemRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillItemSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceSearchPredicates;
import com.healthcompass.data.service.OrganizationDAOService;
import com.healthcompass.data.service.BillItemService;

import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.querydsl.core.types.Predicate;

import lombok.val;

import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.ServiceBillItemSearchParams;
import com.healthcompass.data.service.params.ServiceSearchParams;


@BasePathAwareController
@RequestMapping("/organizations")
public class OrganizationResource {
	
	
	@Autowired OrganizationRepository organizationRepo;
	@SuppressWarnings("rawtypes")
	@Autowired 
	private PagedResourcesAssembler pagedResourcesAssembler;
	
	
	@Autowired
	private OrganizationDAOService organizationDAOService;
	
	@Autowired
	private OrganizationModelAssembler organizationModelAssembler;
	
	
	
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<OrganizationModel> getOrganization(@PathVariable("id") UUID id) 
	{
		return organizationDAOService.findOne(id)
				.map(ResponseEntity::ok) 
				.orElse(ResponseEntity.notFound().build());
	}
	
	
	
	
	
}

	
	


