package com.healthcompass.data.resource;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.BillPayment;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceSearchPredicates;
import com.healthcompass.data.service.BillPaymentService;
import com.healthcompass.data.service.RateCardCategoryService;
import com.healthcompass.data.service.RateCardService;


import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.querydsl.core.types.Predicate;

import jdk.jfr.BooleanFlag;
import lombok.val;

import com.healthcompass.data.service.params.BillPaymentsSearchParams;
import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceSearchParams;
import com.healthcompass.data.validator.CreateNewBillPaymentMainViewValidator;
import com.healthcompass.data.validator.ServiceBillMainViewValidator;
import com.healthcompass.data.view.dto.BillPaymentHistoryMainViewDTO;
import com.healthcompass.data.view.dto.BillPaymentsMainViewDTO;
import com.healthcompass.data.view.dto.CreateNewBillPaymentMainViewDTO;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;


@BasePathAwareController
@RequestMapping("/billPayments")
public class BillPaymentResource {
	
	
	@SuppressWarnings("rawtypes")
	@Autowired 
	private PagedResourcesAssembler pagedResourcesAssembler;
	
	@Autowired
	private RateCardModel rateCardModel;
	
	/*@Autowired
	private RateCardDAOService rateCardDAOService;
	@Autowired
	private RateCardCategoryDAOService rateCardCategoryDAOService;*/
	
	@Autowired
	private BillPaymentService billPaymentService;
	
	@Autowired
	private RateCardCategoryService rateCardCategoryService;
	
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<BillPaymentsMainViewDTO> getPatientBillPaymentsByPatientId(@PathVariable("id") Integer patientId) 
	{
		
		return new ResponseEntity<>(billPaymentService.findAllPatientBillPaymentsByPatientId(patientId), 
				HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	
	public ResponseEntity<BillPaymentsMainViewDTO> getPatientBillPayments(@RequestBody BillPaymentsSearchParams billPaymentsSearchParams,BindingResult result){
	
		
		return new ResponseEntity<>(billPaymentService.findAllPatientBillPayments(billPaymentsSearchParams,result), 
		HttpStatus.OK);
	}
	
	@PostMapping(value = "/printReceipt", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ResponseBody
	public Map<String,byte[]> printReceipt(@Valid @RequestBody  CreateNewBillPaymentMainViewDTO createNewBillPaymentMainViewDTO, BindingResult result,@RequestHeader Map<String, String> headers)
	{
		
		String defaultuserLanguage="en_US";
		String userLocaleLnaguage=defaultuserLanguage;
		if(headers.get("user-lang") != null) {
			userLocaleLnaguage= (String) headers.get("user-lang");
		}
		//List <BillPayment> newlyCreatedBillPayments = billPaymentService.createNewBillPayment(createNewBillPaymentMainViewDTO,result);
		
		byte[] pdfData = billPaymentService.createNewBillPayment(createNewBillPaymentMainViewDTO,result,userLocaleLnaguage);;
		
		Map response = new HashMap<String,byte[]>();
		response.put("data",pdfData);
		return  response;
	
		//return new ResponseEntity<>(billPaymentService.findAllPatientBillPaymentsByPatientId(createNewBillPaymentMainViewDTO.getPatientId()), 
				//HttpStatus.OK);
	
		
		
	}
	
	@GetMapping("/search/paymentHistory/{billNumber}")
	public ResponseEntity<BillPaymentHistoryMainViewDTO> getPaymentHistoryForABillNumber(@PathVariable("billNumber") String billNumber) 
	{
		
		return new ResponseEntity<>(billPaymentService.findBillPaymentHistoryByBillNumber(billNumber), 
				HttpStatus.OK);
	}
	
	
	
	
	
		
}

	
	


