package com.healthcompass.data.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.service.LocalizationResourceService;
import com.healthcompass.data.service.MarketerService;
import com.healthcompass.data.service.SupplierServuce;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.HelpPagSectioneDownLoadParams;
import com.healthcompass.data.service.params.HelpPageDownLoadParams;
import com.healthcompass.data.service.params.LocalizedResourceSearchParams;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.util.ExcelHelper;
import com.healthcompass.data.validator.MarketerMainViewValidator;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerMainViewDTO;
import com.healthcompass.data.view.dto.MarketerSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MetaTagViewDTO;
import com.healthcompass.data.view.dto.ResourceViewDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;
import com.healthcompass.data.view.dto.SupplierViewDTO;



@BasePathAwareController
@RequestMapping("/resources")
public class LocalizationResourceController {
	
	
	@Autowired
	private LocalizationResourceService localizationResourceService;
		
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<List<ResourceViewDTO>> getLocalizedTextForResources(@RequestBody LocalizedResourceSearchParams searchParams) 
	{
		
		return new ResponseEntity<>(localizationResourceService.getLocalizedTextForResources(searchParams), HttpStatus.OK);
    } 
	
	@GetMapping("/search/{id}")
	public ResponseEntity<ResourceViewDTO> getLocalizedText(@PathVariable("id") String id) 
	{
		return new ResponseEntity<>(localizationResourceService.getLocalizedText(id), 
				HttpStatus.OK);
	}
	
	 @PostMapping("/upload/helpPages/fields")
	  public ResponseEntity<String> uploadHelpPageFields(@RequestParam("file") MultipartFile file) {
		 
		String message = localizationResourceService.upLoadHelpPageFields(file);
		 return ResponseEntity.ok().body(message);
			
	   

     }
	 
	 @PostMapping("/upload/helpPages/sections")
	  public ResponseEntity<String> uploadHelpPageSections(@RequestParam("file") MultipartFile file) {
		 
		String message = localizationResourceService.upLoadHelpPageSections(file);
		 return ResponseEntity.ok().body(message);
			
	   

    }
	 
	 @GetMapping("/upload/generateHelpPage/{languageCode}/{pageId}")
	  @ResponseBody
	  public ResponseEntity<String> generateHelpPage (@PathVariable("languageCode") String languageCode,@PathVariable("pageId")  String pageId) {
		 
		 	String message = localizationResourceService.generateHelpPage(languageCode, pageId);
		 	return ResponseEntity.ok().body(message);
			
	
    }
	  
	
	 
	 @GetMapping("/upload/getHelpPage/{languageCode}/{pageName}")
	 @ResponseBody
	  public byte[]   getHelpPage(@PathVariable("languageCode") String languageCode,@PathVariable("pageName")  String pageName, HttpServletResponse response) throws IOException  {
		 
		    
		 
		 byte [] helpPage = localizationResourceService.getHelpPage(languageCode, pageName);
		 
		 //following code is for sending the response as attachment. Currently commmeted. At present we just open an onscreen pdf
		 
		 /*
		 InputStream is = new ByteArrayInputStream(helpPage);
		 response.setContentType("application/octet-stream");
		 ServletOutputStream outputStream = response.getOutputStream();
		 int chunk = 1024;
		 byte[] buffer = new byte[chunk];
		 int bytesRead = -1;
		 while ((bytesRead = is.read(buffer)) != -1) {
			 outputStream.write(buffer, 0, bytesRead);
			}
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename="+languageCode+"_"+pageName+".pdf";
	      
	        response.setHeader(headerKey, headerValue);
		    outputStream.flush();*/
		 return helpPage;
			
    }
	  
	  @PostMapping("/download/helpPage")
	  public  void downloadHelpFile(@RequestBody HelpPageDownLoadParams helpPageDownLoadParams, HttpServletResponse response) throws IOException {
		  
		 byte[] excelContent = localizationResourceService.downLoadHelpPageFromDB(helpPageDownLoadParams);
		 InputStream is = new ByteArrayInputStream(excelContent);
		// response.setContentType("application/octet-stream");
		 response.setContentType("application/vnd.ms-excel");
		 ServletOutputStream outputStream = response.getOutputStream();
		 int chunk = 1024;
		 byte[] buffer = new byte[chunk];
		 int bytesRead = -1;
		 while ((bytesRead = is.read(buffer)) != -1) {
			 outputStream.write(buffer, 0, bytesRead);
			}
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename=HelpPage.xls";
	        //"Content-Type" : "application/vnd.ms-excel"
	        	
	        response.setHeader(headerKey, headerValue);
		    outputStream.flush();

	       // return new HttpEntity<>(new ByteArrayResource(excelContent), header);
			
	   

     }
	 
	  @PostMapping("/download/helpSection")
	  public  void downloadSectionFile(@RequestBody HelpPagSectioneDownLoadParams helpPageSectionDownLoadParams, HttpServletResponse response) throws IOException {
		  
		 byte[] excelContent = localizationResourceService.downLoadHelpPageSectionFromDB(helpPageSectionDownLoadParams);
		 InputStream is = new ByteArrayInputStream(excelContent);
		// response.setContentType("application/octet-stream");
		 response.setContentType("application/vnd.ms-excel");
		 ServletOutputStream outputStream = response.getOutputStream();
		 int chunk = 1024;
		 byte[] buffer = new byte[chunk];
		 int bytesRead = -1;
		 while ((bytesRead = is.read(buffer)) != -1) {
			 outputStream.write(buffer, 0, bytesRead);
			}
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename=HelpSection.xls";
	        //"Content-Type" : "application/vnd.ms-excel"
	        	
	        response.setHeader(headerKey, headerValue);
		    outputStream.flush();

	       // return new HttpEntity<>(new ByteArrayResource(excelContent), header);
			
	   

     }
	  
	    @GetMapping("/getAllPageLanguages")
		public ResponseEntity<Map<String,String>> getAllPageLanguages()  {
		
			return new ResponseEntity<>(
					localizationResourceService.getAllPageLanguages(), 
					HttpStatus.OK);
			
		}
	    
	   
	    
	    @GetMapping("/getAllSectionLanguages")
		public ResponseEntity<Map<String,String>> getAllSectionLanguages()  {
		
			return new ResponseEntity<>(
					localizationResourceService.getAllSectionLanguages(), 
					HttpStatus.OK);
			
		}
	    
	    @GetMapping("/getAllPageNames")
		public ResponseEntity<Map<String,String>> getAllPageNames()  {
		
			return new ResponseEntity<>(
					localizationResourceService.getAllPageNames(), 
					HttpStatus.OK);
			
		}
	    
	    @GetMapping("/getAllSectionNames")
		public ResponseEntity<Map<String,String>> getAllSectionNames()  {
		
			return new ResponseEntity<>(
					localizationResourceService.getAllSectionNames(), 
					HttpStatus.OK);
			
		}
	  
	  
	  
	 

}