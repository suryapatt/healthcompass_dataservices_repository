package com.healthcompass.data.resource;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dao.ServiceBillDAO;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.dto.model.assembler.ServiceBillModelAssembler;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.ServiceBillRepository;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceBillSearchPredicates;
import com.healthcompass.data.search.predicates.ServiceSearchPredicates;
import com.healthcompass.data.service.BillService;

import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.querydsl.core.types.Predicate;

import lombok.val;

import com.healthcompass.data.service.params.ServiceBillHistorySearchParams;
import com.healthcompass.data.service.params.ServiceBillSearchParams;
import com.healthcompass.data.service.params.ServiceSearchParams;
import com.healthcompass.data.view.dto.ServiceBillHistoryMainViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;


@BasePathAwareController
@RequestMapping("/serviceBills")
public class ServiceBillResource {
	
	
	@Autowired ServiceBillRepository serviceBillRepo;
	@SuppressWarnings("rawtypes")
	@Autowired 
	private PagedResourcesAssembler pagedResourcesAssembler;
	
	@Autowired
	private ServiceBillModel serviceBillModel;
	
	@Autowired
	private ServiceBillDAO serviceBillDAO;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private ServiceBillModelAssembler serviceBillModelAssembler;
	
	
	
	//@GetMapping(value="/search/advanced/print/{testParam}")
	//@ResponseBody
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<ServiceBillViewMainDTO> getServiceBill(@PathVariable("id") UUID id) 
	{
		
		/*Locale loc = (Locale) RequestContextUtils.getLocale(request);
		DateFormat dateFormat = DateFormat.getDateInstance(
                DateFormat.DEFAULT, loc);
		String date = dateFormat.format(new Date());
		System.out.println(date);*/
		return new ResponseEntity<>(billService.findOne(id),
				HttpStatus.OK);
		
		/*return billService.findOne(id)
				.map(ResponseEntity::ok) 
				.orElse(ResponseEntity.notFound().build());*/
		
		
	}
	
	
	
	
	
	/*@PostMapping(value = "/search/advanced", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	public ResponseEntity<CollectionModel<ServiceBillModel>> getServiceBillsOld(@RequestBody ServiceBillSearchParams serviceBillParams)  {
	
		
		return new ResponseEntity<>(billService.findAll(serviceBillParams),
				HttpStatus.OK);
	}*/
	
	@PostMapping(value = "/search/advanced", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	public ResponseEntity<CollectionModel<ServiceBillViewMainDTO>> getServiceBills(@RequestBody ServiceBillSearchParams serviceBillParams,BindingResult result){
	
		
		return new ResponseEntity<>(billService.findAll(serviceBillParams,result),
				HttpStatus.OK);
	}
		
	@PostMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<ServiceBillViewMainDTO> createServiceBill(@RequestBody ServiceBillViewMainDTO serviceBillViewMainDTO,BindingResult result)
	{
		
		
		
		ServiceBillViewMainDTO updatedServiceBillViewMainDTO = billService.save(serviceBillViewMainDTO,result);
		return ResponseEntity.ok().body(updatedServiceBillViewMainDTO);
		/*URI location = ServletUriComponentsBuilder.
				fromCurrentRequest().path("/search/{id}").buildAndExpand(updatedServiceBillViewMainDTO.getId()).toUri();
		
		
		return ResponseEntity.created(location).build();*/
		
		
	}
	
	
	@PutMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<ServiceBillViewMainDTO> updateServiceBill(@RequestBody ServiceBillViewMainDTO serviceBillViewMainDTO ,BindingResult result)
	{
		
		
		ServiceBillViewMainDTO updatedServiceBillViewMainDTO = billService.update(serviceBillViewMainDTO,result);
		
		return ResponseEntity.ok().body(updatedServiceBillViewMainDTO);
	}
	
	@PutMapping(value = "/advanced/generateServiceBill", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ResponseBody
	//public ResponseEntity<ServiceBillViewMainDTO> generateServiceBill(@RequestBody ServiceBillViewMainDTO serviceBillViewMainDTO,BindingResult result)
	public Map<String,byte[]> generateServiceBill(@RequestBody ServiceBillViewMainDTO serviceBillViewMainDTO,BindingResult result,@RequestHeader Map<String, String> headers)
	{
		
		
		//ServiceBillViewMainDTO updatedServiceBillViewMainDTO = billService.generateServiceBill(serviceBillViewMainDTO,result);
		String defaultuserLanguage="en_US";
		String userLocaleLnaguage=defaultuserLanguage;
		if(headers.get("user-lang") != null) {
			userLocaleLnaguage= (String) headers.get("user-lang");
		}
		
		byte[] pdfData = billService.generateServiceBill(serviceBillViewMainDTO,result,userLocaleLnaguage);
		
		Map response = new HashMap<String,byte[]>();
		response.put("data",pdfData);
		return  response;
		
		//return ResponseEntity.ok().body(updatedServiceBillViewMainDTO);
	}
	
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteServiceBill(@PathVariable("id") UUID id)
	{
		
		billService.deleteServiceBill(id);
		
		
		return ResponseEntity.ok().build();
	}
	
	@PostMapping(value = "/search/advanced/billingHistory", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	public ResponseEntity<ServiceBillHistoryMainViewDTO> getServiceBillsHistory(@RequestBody ServiceBillHistorySearchParams serviceBillHistoryParams)  {
	
		
		return new ResponseEntity<>(billService.findBillingHistory(serviceBillHistoryParams),
				HttpStatus.OK);
	}
	
	
}

	
	


