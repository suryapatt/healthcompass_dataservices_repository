package com.healthcompass.data.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.dto.model.RateCardModel;

import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ServiceModel;
import com.healthcompass.data.model.Location;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.ServiceRepository;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.search.predicates.ServiceSearchPredicates;
import com.healthcompass.data.service.RateCardCategoryService;
import com.healthcompass.data.service.RateCardService;


import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.querydsl.core.types.Predicate;

import jdk.jfr.BooleanFlag;
import lombok.val;

import com.healthcompass.data.service.params.LocationSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.ServiceSearchParams;
import com.healthcompass.data.validator.RateCardLocationValidator;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.ServiceBillViewMainDTO;


@BasePathAwareController
@RequestMapping("/rateCards")
public class RateCardResource {
	
	
	@SuppressWarnings("rawtypes")
	@Autowired 
	private PagedResourcesAssembler pagedResourcesAssembler;
	
	@Autowired
	private RateCardModel rateCardModel;
	
	/*@Autowired
	private RateCardDAOService rateCardDAOService;
	@Autowired
	private RateCardCategoryDAOService rateCardCategoryDAOService;*/
	
	@Autowired
	private RateCardService rateCardService;
	
	@Autowired
	private RateCardCategoryService rateCardCategoryService;
	
	
	
	/*@InitBinder
	private void initBinder(WebDataBinder binder) {
		if (LocationRateCardsViewDTO.class.equals(binder.getTarget().getClass())) {
	        binder.addValidators(rateCardLocationValidator);
	    }
		
	}*/
	
	@GetMapping("/search/{id}")
	public ResponseEntity<RateCardModel> getRateCard(@PathVariable("id") Integer id) 
	{
		return rateCardService.OldfindOne(id)
				.map(ResponseEntity::ok) 
				.orElse(ResponseEntity.notFound().build());
	}
	
	
	
	
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	
	public ResponseEntity<LocationRateCardsViewDTO> getRateCards(@RequestBody RateCardSearchParams rateCardSearchParams)  {
	
		
		return new ResponseEntity<>(rateCardService.findAllRateCards(rateCardSearchParams), 
		HttpStatus.OK);
	}	
	
	
	@PostMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	
	public ResponseEntity<LocationRateCardsViewDTO> saveRateCards(@RequestBody  LocationRateCardsViewDTO locationRateCards,BindingResult result)
	{
		// This logic need to be moved to a service class and it controller is not right place to be the code here
		// what we are doing here is first we check if there are any new Rate Card Categories need to be created. We will first persist them and get their IDs and set them to the appropriate
		// Rate Cards. If we have all logic in the same RateCardDAOService , since it is transactional, newly createdRate Card Category Ids are not committed until Rate cards are commmited (and order is also not gauranteed)
		// so we are getting foriegn key constraint violation. So for this reason first we commit new rate card categories non transactionally before we call the actual Rate Card DAO service to persist Rate Cards
		
	
		LocationRateCardsViewDTO locationRateCardsViewDTO = rateCardService.createOrUpdateRateCardsForALocation(locationRateCards,result);
		
		return new ResponseEntity<>(locationRateCardsViewDTO, 
				HttpStatus.CREATED);
		
		/*URI location = ServletUriComponentsBuilder.
				fromCurrentRequest().path("/search/{id}").buildAndExpand(persistedLocationWithRateCardsAndCategories.getId()).toUri();
		
		
		return ResponseEntity.created(location).build();*/
		
		
	}
	
	
	
	@RequestMapping(value = "/search/advanced/rateCardSummary", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<Object> getRateCardSummary(@RequestBody  RateCardViewSummarySearchParams rateCardViewSummarySearchParams) 
	{
		/*return rateCardDAOService.getRateCardSummaryWithLocations(rateCardViewSummarySearchParams)
				.map(ResponseEntity::ok) 
				.orElse(ResponseEntity.notFound().build());*/
		
		return new ResponseEntity<Object>(rateCardService.getRateCardSummaryWithLocations(rateCardViewSummarySearchParams), HttpStatus.OK);
    }
	
		
	@RequestMapping(value = "/search/advanced/rateCardSummary/{id}", method = RequestMethod.GET, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<Object> getRateCardSummaryByOrganization(@PathVariable("id") UUID id) 
	{
		/*return rateCardDAOService.getRateCardSummaryWithLocations(rateCardViewSummarySearchParams)
				.map(ResponseEntity::ok) 
				.orElse(ResponseEntity.notFound().build());*/
		RateCardViewSummarySearchParams searchParams = new RateCardViewSummarySearchParams();
		searchParams.setOrganizationId(id);
		return new ResponseEntity<Object>(rateCardService.getRateCardSummaryWithLocations(searchParams), HttpStatus.OK);
    }
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteLocationRateCards(@PathVariable("id") UUID id)
	{
		
		rateCardService.deleteLocationRateCards(id);
		
		
		return new ResponseEntity<Void>(HttpStatus.OK);
		
		
	}
	
}

	
	


