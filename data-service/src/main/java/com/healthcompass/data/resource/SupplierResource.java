package com.healthcompass.data.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.ServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.BasePathAwareController;
//import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.healthcompass.data.dto.model.LocationModel;
import com.healthcompass.data.model.Marketer;
import com.healthcompass.data.model.Supplier;
import com.healthcompass.data.repository.SupplierRepository;
import com.healthcompass.data.service.SupplierServuce;
import com.healthcompass.data.service.exceptions.HealthCompassCustomValidationException;
import com.healthcompass.data.service.exceptions.RecordNotFoundException;
import com.healthcompass.data.service.params.MarketerSummaryViewSearchParams;
import com.healthcompass.data.service.params.RateCardSearchParams;
import com.healthcompass.data.service.params.RateCardViewSummarySearchParams;
import com.healthcompass.data.service.params.SupplierSummaryViewSearchParams;
import com.healthcompass.data.util.CSVHelper;
import com.healthcompass.data.view.dto.LocationRateCardsViewDTO;
import com.healthcompass.data.view.dto.MarketerUploadMainViewDTO;
import com.healthcompass.data.view.dto.MarketerViewDTO;
import com.healthcompass.data.view.dto.SupplierMainViewDTO;
import com.healthcompass.data.view.dto.SupplierSummaryOrganizationDTO;
import com.healthcompass.data.view.dto.SupplierUploadMainViewDTO;



@BasePathAwareController
@RequestMapping("/suppliers")
public class SupplierResource {
	
	
	@Autowired
	private SupplierServuce supplierService;
	
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<SupplierMainViewDTO> getSupplier(@PathVariable("id") UUID id) 
	{
		return new ResponseEntity<>(supplierService.getSupplier(id), 
				HttpStatus.OK);
	}
	
	
	
	@PostMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<SupplierMainViewDTO> createSupplier(@RequestBody SupplierMainViewDTO supplierMainViewDTO,BindingResult result)
	{
		SupplierMainViewDTO updatedSupplierMainViewDTO = supplierService.createSupplier(supplierMainViewDTO,result);
		return ResponseEntity.ok().body(updatedSupplierMainViewDTO);
	}

	@PutMapping(value = "", consumes=MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@JsonIgnoreProperties(ignoreUnknown = true)
	public ResponseEntity<SupplierMainViewDTO> updateSupplier(@RequestBody SupplierMainViewDTO supplierMainViewDTO,BindingResult result)
	{
		SupplierMainViewDTO updatedSupplierMainViewDTO = supplierService.updateSupplier(supplierMainViewDTO, result);
		return ResponseEntity.ok().body(updatedSupplierMainViewDTO);
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object>  deleteSupplier(@PathVariable UUID id) {
		 supplierService.deleteSupplier(id);
		 return ResponseEntity.ok().build();
				
	}
	
	@RequestMapping(value = "/search/advanced", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<SupplierSummaryOrganizationDTO> getSupplierSummaryByOrganization(@RequestBody SupplierSummaryViewSearchParams supplierSummaryViewSearchParams) 
	{
		
		return new ResponseEntity<>(supplierService.getSupplierSummaryByOrganization(supplierSummaryViewSearchParams), HttpStatus.OK);
    }
	
	 @PostMapping("/upload")
	  public ResponseEntity<SupplierUploadMainViewDTO> uploadFile(@RequestParam("file") MultipartFile file) {
		 
		 SupplierUploadMainViewDTO supplierUploadMainViewDTO = supplierService.upLoadSuppliers(file);
		 return ResponseEntity.ok().body(supplierUploadMainViewDTO);
			
	   

}

}
