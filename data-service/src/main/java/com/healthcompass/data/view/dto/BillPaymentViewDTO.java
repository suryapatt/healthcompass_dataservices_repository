package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.healthcompass.data.util.CustomDateDeSerializer;
import com.healthcompass.data.util.CustomDateSerializer;
import com.healthcompass.data.util.CustomDateWithoutTimestampSerializer;
import com.healthcompass.data.util.DecimalJsonSerializer;

import jdk.jfr.BooleanFlag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "billPayments")
@Relation(itemRelation = "billPayment" , collectionRelation = "billPayments")
@Component
public class BillPaymentViewDTO extends RepresentationModel<BillPaymentViewDTO>{
  
	private String billNumber;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@JsonSerialize(using = CustomDateWithoutTimestampSerializer.class)
	private java.sql.Timestamp billDate;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double amountBilled;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double amountPaid;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double amountDue;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@JsonSerialize(using = CustomDateWithoutTimestampSerializer.class)
	private java.sql.Timestamp lastPaidOn;
	private String billPaymentStatus;
	
	
	
	
	
}
