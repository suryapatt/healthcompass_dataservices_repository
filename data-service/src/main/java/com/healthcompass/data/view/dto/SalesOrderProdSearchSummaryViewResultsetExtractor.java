package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.util.HealthCompassConstants;
@Component
public class SalesOrderProdSearchSummaryViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	SalesOrderProductSearchSummaryListViewDTO productListSummaryMainViewDTO = new SalesOrderProductSearchSummaryListViewDTO();
	    	ArrayList<SalesOrderProductSearchProductSummaryViewDTO> productsView = new ArrayList<SalesOrderProductSearchProductSummaryViewDTO>();
	    	List<MetaTagAttributesViewDTO> metaTagAttributesView =  new ArrayList<MetaTagAttributesViewDTO>();
	         boolean isOrganizationInformationSet = false;
	         boolean isProductInfoSet=false;
	         boolean isMetaTagAttributeInfoSet=false;
	         boolean isInventoryIDInfoSet = false;
	         UUID currentInvId=null;
	         UUID newInvId=null;
	         SalesOrderProductSearchProductSummaryViewDTO currentProduct = new SalesOrderProductSearchProductSummaryViewDTO();
	         SalesOrderProductSearchProductSummaryViewDTO newProduct = new SalesOrderProductSearchProductSummaryViewDTO();
	         while (rs.next()) {
	        	 if(!isOrganizationInformationSet) {
	        		 productListSummaryMainViewDTO = new SalesOrderProductSearchSummaryListViewDTO();
	        		 productListSummaryMainViewDTO.setOrganizationId((UUID) rs.getObject("organizationId"));
	        		 productListSummaryMainViewDTO.setOrganizationName(rs.getString("organizationName"));
	        		 productListSummaryMainViewDTO.setLocationId((UUID) rs.getObject("locationId"));
	        		 productListSummaryMainViewDTO.setLocationName(rs.getString("locationName"));
	        		 
	        		 isOrganizationInformationSet=true;		 
	        	 }
	        	 
	        	
	        	 if (currentInvId == null || !currentInvId.equals((UUID) rs.getObject("inventoryId"))){
	        		
	        		// start new inventoryProductItem
	        		 newInvId= (UUID) rs.getObject("inventoryId");
	        		 newProduct = new SalesOrderProductSearchProductSummaryViewDTO();
	        		 newProduct.setInventoryId(newInvId);
	        		 newProduct.setProductId(rs.getInt("productId"));
	        		 newProduct.setBatchNumber(rs.getString("batchNumber"));
	        		 newProduct.setExpiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(rs.getDate("expiryDate")));
	        		 newProduct.setAvailableUnits(rs.getInt("availableUnits"));
	        		 newProduct.setName(rs.getString("productName")+" (Batch-Number : "+rs.getString("batchNumber")+")");
	        		 newProduct.setDescription(rs.getString("productDescription"));
	        		 newProduct.setUnitPrice(rs.getDouble("unitPrice"));
	        		 productsView.add(newProduct);
		        	 currentInvId=newInvId;
		        	 currentProduct=newProduct;
		        	
		        	 
	        	}else {
	        		continue;
	        	}
	        	
	        	
	        	
	        	         
	        }
	         productListSummaryMainViewDTO.setProducts(productsView);
	         return  productListSummaryMainViewDTO;
	    }
	
}
