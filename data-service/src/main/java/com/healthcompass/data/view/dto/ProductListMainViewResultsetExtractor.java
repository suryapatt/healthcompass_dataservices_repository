package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class ProductListMainViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	ProductListMainViewDTO productListMainViewDTO = new  ProductListMainViewDTO();
	    	ArrayList<ProductViewDTO> productsView = new ArrayList<ProductViewDTO>();
	    	List<MetaTagAttributesViewDTO> metaTagAttributesView =  new ArrayList<MetaTagAttributesViewDTO>();
	         boolean isOrganizationInformationSet = false;
	         boolean isProductInfoSet=false;
	         boolean isMetaTagAttributeInfoSet=false;
	         Integer currentProductId=0;
	         Integer newProductId=0;
	         ProductViewDTO currentProduct = new ProductViewDTO();
	         ProductViewDTO newProduct = new ProductViewDTO();
	         while (rs.next()) {
	        	 if(!isOrganizationInformationSet) {
	        		 productListMainViewDTO = new  ProductListMainViewDTO();
	        		 productListMainViewDTO.setOrganizationId((UUID) rs.getObject("organizationId"));
	        		 productListMainViewDTO.setOrganizationName(rs.getString("organizationName"));
	        		 
	        		 isOrganizationInformationSet=true;		 
	        	 }
	        	 
	        	
	        	 if (currentProductId.compareTo(rs.getInt("productId")) != 0){
	        		
	        		// start new product
	        		 newProductId=rs.getInt("productId");
	        		 newProduct = new ProductViewDTO();
	        		 newProduct.setProductId(newProductId);
	        		 newProduct.setMarketerId(rs.getInt("marketerId"));
	        		 newProduct.setMarketerName(rs.getString("marketerName"));
	        		 newProduct.setName(rs.getString("productName"));
	        		 newProduct.setDescription(rs.getString("productDescription"));
	        		 //newProduct.setMoleculeId(rs.getInt("moleculeId"));
	        		// newProduct.setHsnCode(rs.getInt("hsnCode"));
	        		// newProduct.setUnits(rs.getInt("productUnits"));
	        		 //newProduct.setPack(rs.getInt("productPack"));
	        		 //newProduct.setScheduleDrugCode(rs.getString("scheduleDrugCode"));
	        		// newProduct.setGstRate(rs.getDouble("prodGstRate"));
	        		// newProduct.setCentralTaxRate(rs.getDouble("prodCentralTaxRate"));
	        		// newProduct.setSalesTaxRate(rs.getDouble("prodSalesTaxRate"));
	        		 //newProduct.setVatRate(rs.getDouble("prodVatRate"));
	        		 newProduct.setReOrderLevel(rs.getInt("prodReOrderLevel"));
	        		 newProduct.setLeadTime(rs.getInt("prodLeadTime"));
	        		 newProduct.setActive(rs.getBoolean("productActive"));
		        	 metaTagAttributesView = new ArrayList<MetaTagAttributesViewDTO>();
		        	 newProduct.setMetaTagAttributes(metaTagAttributesView);
		        	 productsView.add(newProduct);
		        	 currentProductId=newProductId;
		        	 currentProduct=newProduct;
		        	
		        	 
	        	}
	        	boolean isMetaTagAttributeActive = rs.getBoolean("metaTagAttributeActive");
	        	if(isMetaTagAttributeActive){

	        		MetaTagAttributesViewDTO metaTagAttribute = new MetaTagAttributesViewDTO();
					metaTagAttribute.setActive(isMetaTagAttributeActive);
					metaTagAttribute.setAttributeId(rs.getInt("metaTagAttributeId"));
					metaTagAttribute.setAttrubuteValue(rs.getString("metaTagAttributeValue"));
					MetaTagViewDTO metaTag = new MetaTagViewDTO();
					metaTag.setMetaTagId(rs.getInt("metaTagId"));
					metaTag.setMetaTagName(rs.getString("metaTagName"));
					metaTagAttribute.setMetaTag(metaTag);
					currentProduct.getMetaTagAttributes().add(metaTagAttribute);
						        		
	        	}
	        	
	        	
	        	         
	        }
	         productListMainViewDTO.setProducts(productsView);
	         return  productListMainViewDTO;
	    }
	
}
