package com.healthcompass.data.view.dto;

import java.util.List;
import java.util.UUID;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName("Organization")
@Component
public class RateCardSummaryOrganizationDTO extends RepresentationModel<RateCardSummaryOrganizationDTO>{
	
	private UUID organizationId;
	private String organizationName;
	private List<RateCardSummaryLocationDTO> locationsSummary;

}
