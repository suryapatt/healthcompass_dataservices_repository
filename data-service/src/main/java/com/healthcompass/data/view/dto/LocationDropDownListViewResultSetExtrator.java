package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class LocationDropDownListViewResultSetExtrator implements ResultSetExtractor{
	

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
    	
        LocationDropDownListViewDTO locationDropDownListView = new LocationDropDownListViewDTO();
        List<LocationDropDownViewDTO> locations = new ArrayList<LocationDropDownViewDTO>();
        boolean isOrganizationInofset = false;
        
        LocationDropDownViewDTO newLocation = null;
        while (rs.next()) {
        	if(!isOrganizationInofset) {
        		locationDropDownListView.setOrganizationId((UUID) rs.getObject("organizationId"));;
        		locationDropDownListView.setOrganizationName(rs.getString("organizationName"));
        		   		
        		
        	}
        	newLocation = new LocationDropDownViewDTO();   	 
        	newLocation.setId((UUID) rs.getObject("locationId"));
        	newLocation.setName(rs.getString("locationName"));
        	locations.add(newLocation);
	        	
	        	 
        	}
            locationDropDownListView.setLocations(locations);
        	
           return  locationDropDownListView;
    }
	
}
