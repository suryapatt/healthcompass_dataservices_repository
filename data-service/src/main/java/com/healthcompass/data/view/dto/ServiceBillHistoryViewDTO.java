package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.healthcompass.data.dto.model.RateCardModel;
import com.healthcompass.data.dto.model.ServiceBillModel;
import com.healthcompass.data.dto.model.ValueSetModel;
import com.healthcompass.data.model.Service;
import com.healthcompass.data.model.ServiceBill;
import com.healthcompass.data.model.ServiceBillItem;
import com.healthcompass.data.model.ValueSet;
import com.healthcompass.data.util.CustomDateDeSerializer;
import com.healthcompass.data.util.CustomDateSerializer;
import com.healthcompass.data.util.DecimalJsonSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "ServiceBill")
@Relation(itemRelation = "serviceBill",collectionRelation =  "serviceBill")
@Component
public class ServiceBillHistoryViewDTO  extends RepresentationModel<ServiceBillHistoryViewDTO>{
  
	private UUID serviceBillId;
	private UUID patientId;
	private String patientName;
	private String practitionerName;
	private UUID practitionerId;
	private UUID organizationId;
	private String organizationName;
	private String speciality;
	private UUID locationId;
	private String locationName;
	
	
	private String billNumber;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
	//@JsonSerialize(using = DateTime.class) @jsondeserialize(using = timestampdeserializer.class) private timestamp createdon; 
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	//@JsonSerialize(using = CustomDateSerializer.class)
	//@JsonDeserialize(using = CustomDateDeSerializer.class)
	private String billDate;
	
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double amountBilled;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	
	private Double amountPaid;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double amountDue;
	private UUID attachmentId;

	
	
	
	
	
	


	
	
	
}
