package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.healthcompass.data.util.DecimalJsonSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "SalesOrderItems")
@Relation(itemRelation = "saleOrderItems",collectionRelation =  "orderItems")
@Component
public class InventorySummaryViewDTO extends RepresentationModel<InventorySummaryViewDTO>{
  
	private UUID inventorySummaryId;
	private UUID organizationId;
	private String organizationName;
	private UUID locationId;
	private String locationName;
	private Integer productId;
	private String productName;
	private Integer availableUnits;
	private Integer reOrderLevel;
	private Integer leadTime;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double unitRate;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
