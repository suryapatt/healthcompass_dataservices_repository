package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.healthcompass.data.util.DecimalJsonSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "SalesOrderItems")
@Relation(itemRelation = "saleOrderItems",collectionRelation =  "orderItems")
@Component
public class SalesOrderItemViewDTO extends RepresentationModel<SalesOrderItemViewDTO>{
  
		
	private UUID orderItemId;
	private String batchNumber;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
	private String expiryDate;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double rate;
	private SalesOrderInventoryItemViewDTO inventory;
	private SalesOrderProductViewDTO product;
	private String productName;
	private Integer quantity;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double itemValue;
	//private Double gstRate;
	//private Double salesTaxRate;
	//private Double centralTaxRate;
	//private Double vatRate;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double discount;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double discountedRate;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double netItemValue;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
