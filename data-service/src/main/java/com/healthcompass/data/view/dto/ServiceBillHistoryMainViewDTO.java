package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.healthcompass.data.util.CustomDateDeSerializer;
import com.healthcompass.data.util.CustomDateSerializer;
import com.healthcompass.data.util.CustomDateWithoutTimestampSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "ServiceBillHistoryView")
@Relation(collectionRelation = "ServiceBillHistoryView")
@Component
public class ServiceBillHistoryMainViewDTO extends RepresentationModel<ServiceBillHistoryMainViewDTO>{
  
		
	private UUID patientId;
	private String patientName;
	private String practitionerName;
	private UUID practitionerId;
	private UUID providerId;
	private String providerName;
	private List<ServiceBillHistoryViewDTO> serviceBills;
	
	
	
	
	
	
	
	
	
	
}
