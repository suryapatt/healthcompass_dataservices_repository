package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.model.RateCardCategory;
@Component
public class RateCrdCatDistinctDescResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	       Map<String ,RateCardCategory> rateCardDistinctDescMap = new HashMap<String,RateCardCategory>(); 
	       
	         while (rs.next()) {
	        	  RateCardCategory rateCardCatInDB = new RateCardCategory();
	        	 rateCardCatInDB.setId(rs.getInt("id"));
	        	 String desc = rs.getString("description");
	        	 rateCardCatInDB.setDescription(desc);
	        	 rateCardDistinctDescMap.put(desc,rateCardCatInDB);
	        	 
	         
	        }
	         return  rateCardDistinctDescMap;
	    }
	
}
