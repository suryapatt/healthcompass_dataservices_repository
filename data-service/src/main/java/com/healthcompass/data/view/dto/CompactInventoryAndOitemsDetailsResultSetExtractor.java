package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.util.CompactInventoryItemsAndOrderItemsCollector;
import com.healthcompass.data.util.CompactInventoryItemsCollector;

@Component
public class CompactInventoryAndOitemsDetailsResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	Map<Integer,List<CompactInventoryItemsAndOrderItemsCollector>> compactInventoryDetails = new HashMap<Integer,List<CompactInventoryItemsAndOrderItemsCollector>>();
	        
	         while (rs.next()) {
	        	 CompactInventoryItemsAndOrderItemsCollector item = new CompactInventoryItemsAndOrderItemsCollector();
	        	 item.setInventoryId((UUID) rs.getObject("inventoryId")); 
	        	 item.setBatchNumber(rs.getString("batchNumber"));
	        	 Integer prodId = rs.getInt("productId");
	        	 item.setProductId(prodId);
	        	 
	        	 Integer availableUnits = rs.getInt("availableUnits");
	        	 Double unitRate = rs.getDouble("unitRate");
	        	 Double inventoryValue=availableUnits*unitRate;
	        	 
	        	 item.setProductAvailableUnits(availableUnits);
	        	 item.setProductUnitRate(unitRate);
	        	 item.setInventoryValue(inventoryValue);
	        	 item.setOrderItemQuantity(rs.getInt("itemQuantity"));
	        	 
	        	 if(compactInventoryDetails.containsKey(prodId)) {
	        		 ((List<CompactInventoryItemsAndOrderItemsCollector>) compactInventoryDetails.get(prodId)).add(item);
	        		 
	        	 }else {
	        		 List<CompactInventoryItemsAndOrderItemsCollector> itemList = new ArrayList<CompactInventoryItemsAndOrderItemsCollector>();
	        		 itemList.add(item);
	        		 compactInventoryDetails.put(item.getProductId(),itemList);
	        		 
	        		 
	        	 }
	        	 
	        	
	        	 
	        	 
	         
	        }
	         
	        return  compactInventoryDetails;
	    }
	
}
