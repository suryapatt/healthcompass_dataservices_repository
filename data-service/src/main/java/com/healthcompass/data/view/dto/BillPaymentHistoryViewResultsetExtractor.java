package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class BillPaymentHistoryViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	BillPaymentHistoryMainViewDTO BillPaymentHistoryMainViewDTO = null;
	    	ArrayList<BillPaymentHistoryViewDTO> biilPaymentsHistoryView = new ArrayList<BillPaymentHistoryViewDTO>();
	         boolean isAggregatedInformationSet = false;
	         while (rs.next()) {
	        	 if(!isAggregatedInformationSet) {
	        		 BillPaymentHistoryMainViewDTO = new  BillPaymentHistoryMainViewDTO();
	        		 BillPaymentHistoryMainViewDTO.setBillId((UUID) rs.getObject("billId"));
	        		 BillPaymentHistoryMainViewDTO.setBillNumber(rs.getString("billNumber"));
	        		 
	        		 BillPaymentHistoryMainViewDTO.setBillDate(rs.getTimestamp("billDate"));
	        		 BillPaymentHistoryMainViewDTO.setAmountBilled(rs.getDouble("amountBilled"));
	        		 isAggregatedInformationSet=true;		 
	        	 }
	        	 BillPaymentHistoryViewDTO billPaymentHistory = new BillPaymentHistoryViewDTO();
	        	 billPaymentHistory.setBillPaymentId((UUID) rs.getObject("billPaymentId"));
	        	 billPaymentHistory.setBillNumber(rs.getString("billNumber"));
	        	 billPaymentHistory.setPaymentAmount(rs.getDouble("paymentAmount"));
	        	 billPaymentHistory.setPaymentDate(rs.getTimestamp("paymentDate"));
	        	 billPaymentHistory.setPaymentMode(rs.getString("paymentMode"));
	        	 billPaymentHistory.setReceiptNumber(rs.getString("receiptNumber"));
	        	 billPaymentHistory.setAttachmentId((UUID)rs.getObject("attachmentId"));
	        	
	        	 biilPaymentsHistoryView.add(billPaymentHistory);
	        	 
	        	 
	         
	        }
	         BillPaymentHistoryMainViewDTO.setBillPaymentHistory(biilPaymentsHistoryView);
	        return  BillPaymentHistoryMainViewDTO;
	    }
	
}
