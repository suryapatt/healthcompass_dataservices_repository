package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.healthcompass.data.model.Marketer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "language")
@Relation(itemRelation = "language")
@Component
public class LocalizationPageViewDTO extends RepresentationModel<LocalizationPageViewDTO>{
  
		
	
	private String languageCode;
	private Integer languageId;
	private List<ResourceGroupViewDTO> resourceGroups;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
