package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.healthcompass.data.model.Marketer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "products")
@Relation(itemRelation = "products",collectionRelation =  "products")
@Component
public class ProductViewDTO extends RepresentationModel<ProductViewDTO>{
  
		
	private Integer productId;
	private Integer marketerId;
	private String marketerName;
	private String name;
	private String description;
	//private Integer manufacturerId;
	//private Integer moleculeId;
	//private Integer hsnCode;
	//private Integer pack;
	//private Integer units;
	//private String scheduleDrugCode;
	//private Double gstRate;
	//private Double salesTaxRate;
	//private Double centralTaxRate;
	//private Double vatRate;
	private Integer reOrderLevel;
	private Integer leadTime;
	private Boolean active;	
	
	private List<MetaTagAttributesViewDTO> metaTagAttributes;
	
	
	
	
	
	
	
	
	
	
	
}
