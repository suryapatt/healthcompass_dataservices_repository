package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;

import jdk.jfr.BooleanFlag;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.healthcompass.data.util.CustomDateDeSerializer;
import com.healthcompass.data.util.CustomDateSerializer;
import com.healthcompass.data.util.CustomDateWithoutTimestampSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "ServiceBillView")
@Relation(collectionRelation = "ServiceBillView")
@Component
public class ServiceBillViewMainDTO extends RepresentationModel<ServiceBillViewMainDTO>{
  
		
	private UUID patientId;
	private String patientName;
	private UUID encounterId;
	private Integer appointmentNumber;
	private Integer encounterNumber;
	private String practitionerName;
	private UUID practitionerId;
	private UUID organizationId;
	private String organizationName;
	private SpecialityViewDTO speciality;
	private String phone;
	private UUID locationId;
	private String locationName;
	@JsonSerialize(using = CustomDateSerializer.class)
	private java.sql.Timestamp admissionTime;
	@JsonSerialize(using = CustomDateSerializer.class)
	private java.sql.Timestamp dischargeTime;
	private Double outStandingPendingBillsAmount;
	private ServiceBillViewDTO serviceBill;
	private String loggedInUser;
	
	
	
	
	
	
	
	
	
}
