package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class SalesOrderSummaryListViewResultextractor implements ResultSetExtractor{
	

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
    	
        SalesOrderSummaryListViewDTO salesOrderSummaryListView = new SalesOrderSummaryListViewDTO();
        List<SalesOrderSummaryViewDTO> salesOrders = new ArrayList<SalesOrderSummaryViewDTO>();
        boolean isOrganizationInofset = false;
        
        SalesOrderSummaryViewDTO newSO = null;
        while (rs.next()) {
        	if(!isOrganizationInofset) {
        		salesOrderSummaryListView.setOrganizationId((UUID) rs.getObject("organizationId"));;
        		salesOrderSummaryListView.setOrganizationName(rs.getString("organizationName"));
        		   		
        		
        	}
        	newSO = new SalesOrderSummaryViewDTO();   	 
        	newSO.setLocationId((UUID) rs.getObject("locationId"));
        	newSO.setLocationName(rs.getString("locationName"));
        	newSO.setSalesOrderValue(rs.getDouble("salesOrderValue"));
        	newSO.setSalesOrderId((UUID) rs.getObject("salesOrderId"));
        	newSO.setSalesOrderNumber(rs.getString("salesOrderNumber"));
        	newSO.setSalesOrderDate(rs.getTimestamp("salesOrderDate"));
        	salesOrders.add(newSO);
	        	
	        	 
        	}
        salesOrderSummaryListView.setSalesOrders(salesOrders);
        	
           return  salesOrderSummaryListView;
    }
	
}
