package com.healthcompass.data.view.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import com.healthcompass.data.dto.model.ServiceModel.ServiceModelBuilder;
import com.healthcompass.data.model.RateCard;
import com.healthcompass.data.util.DecimalJsonSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Component
public class ReceiptViewDTO implements Serializable {
  
 	
	private Integer patientId;
	private String patientName;
	private UUID organizationId;
	private String organizationName;
	private UUID locationId;
	private String locationName;
	private String paymentDate;
	private String phoneNumber;
	private String email;
	private String userName;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	//private java.sql.Timestamp paymentDate;
	private String receiptNumber;
	private String paymentMode;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double totalPaymentReceived;
	@JsonSerialize(using=DecimalJsonSerializer.class)
	private Double totalOutStanding;
	List<BillPaymentReceiptViewDTO> billPayments;
	private String loggedInUser;
	
	
	
	
	
}
