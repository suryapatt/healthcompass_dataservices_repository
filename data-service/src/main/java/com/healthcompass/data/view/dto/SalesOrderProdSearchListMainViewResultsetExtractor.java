package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.util.HealthCompassConstants;
@Component
public class SalesOrderProdSearchListMainViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	SalesOrderProductSearchListMainViewDTO productListMainViewDTO = new SalesOrderProductSearchListMainViewDTO();
	    	ArrayList<SalesOrderProductSearchProductViewDTO> productsView = new ArrayList<SalesOrderProductSearchProductViewDTO>();
	    	List<MetaTagAttributesViewDTO> metaTagAttributesView =  new ArrayList<MetaTagAttributesViewDTO>();
	         boolean isOrganizationInformationSet = false;
	         boolean isProductInfoSet=false;
	         boolean isMetaTagAttributeInfoSet=false;
	         boolean isInventoryIDInfoSet = false;
	         UUID currentInvId=null;
	         UUID newInvId=null;
	         SalesOrderProductSearchProductViewDTO currentProduct = new SalesOrderProductSearchProductViewDTO();
	         SalesOrderProductSearchProductViewDTO newProduct = new SalesOrderProductSearchProductViewDTO();
	         while (rs.next()) {
	        	 if(!isOrganizationInformationSet) {
	        		 productListMainViewDTO = new  SalesOrderProductSearchListMainViewDTO();
	        		 productListMainViewDTO.setOrganizationId((UUID) rs.getObject("organizationId"));
	        		 productListMainViewDTO.setOrganizationName(rs.getString("organizationName"));
	        		 productListMainViewDTO.setLocationId((UUID) rs.getObject("locationId"));
	        		 productListMainViewDTO.setLocationName(rs.getString("locationName"));
	        		 
	        		 isOrganizationInformationSet=true;		 
	        	 }
	        	 
	        	
	        	 if (currentInvId == null || !currentInvId.equals((UUID) rs.getObject("inventoryId"))){
	        		
	        		// start new inventoryProductItem
	        		 newInvId= (UUID) rs.getObject("inventoryId");
	        		 newProduct = new SalesOrderProductSearchProductViewDTO();
	        		 newProduct.setInventoryItem(new SalesOrderInventoryItemViewDTO());
	        		 newProduct.getInventoryItem().setInventoryId(newInvId);
	        		 newProduct.getInventoryItem().setBatchNumber(rs.getString("batchNumber"));
	        		 newProduct.getInventoryItem().setExpiryDate(HealthCompassConstants.convertSQLDateToLocalDateString(rs.getDate("expiryDate")));
	        		 newProduct.getInventoryItem().setUnitRate(rs.getDouble("unitRate"));
	        		 newProduct.getInventoryItem().setAvailableUnits(rs.getInt("availableUnits"));
	        		 
	        		 newProduct.setMarketerId(rs.getInt("marketerId"));
	        		 newProduct.setMarketerName(rs.getString("marketerName"));
	        		 newProduct.setProductId(rs.getInt("productId"));
	        		 newProduct.setName(rs.getString("productName"));
	        		 newProduct.setDescription(rs.getString("productDescription"));
	        		 newProduct.setGstRate(rs.getDouble("prodGstRate"));
	        		 newProduct.setCentralTaxRate(rs.getDouble("prodCentralTaxRate"));
	        		 newProduct.setSalesTaxRate(rs.getDouble("prodSalesTaxRate"));
	        		 newProduct.setVatRate(rs.getDouble("prodVatRate"));
	        		 metaTagAttributesView = new ArrayList<MetaTagAttributesViewDTO>();
		        	 newProduct.setMetaTagAttributes(metaTagAttributesView);
		        	 productsView.add(newProduct);
		        	 currentInvId=newInvId;
		        	 currentProduct=newProduct;
		        	
		        	 
	        	}
	        	boolean isMetaTagAttributeActive = rs.getBoolean("metaTagAttributeActive");
	        	if(isMetaTagAttributeActive){

	        		MetaTagAttributesViewDTO metaTagAttribute = new MetaTagAttributesViewDTO();
					metaTagAttribute.setActive(isMetaTagAttributeActive);
					metaTagAttribute.setAttributeId(rs.getInt("metaTagAttributeId"));
					metaTagAttribute.setAttrubuteValue(rs.getString("metaTagAttributeValue"));
					MetaTagViewDTO metaTag = new MetaTagViewDTO();
					metaTag.setMetaTagId(rs.getInt("metaTagId"));
					metaTag.setMetaTagName(rs.getString("metaTagName"));
					metaTagAttribute.setMetaTag(metaTag);
					currentProduct.getMetaTagAttributes().add(metaTagAttribute);
						        		
	        	}
	        	
	        	
	        	         
	        }
	         productListMainViewDTO.setProducts(productsView);
	         return  productListMainViewDTO;
	    }
	
}
