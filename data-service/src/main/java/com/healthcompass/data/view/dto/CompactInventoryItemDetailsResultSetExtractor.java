package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.util.CompactInventoryItemsCollector;

@Component
public class CompactInventoryItemDetailsResultSetExtractor implements ResultSetExtractor{
	
	
	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    
	    	Map<Integer,List<CompactInventoryItemsCollector>> compactInventoryDetails = new HashMap<Integer,List<CompactInventoryItemsCollector>>();
	        
	         while (rs.next()) {
	        	 CompactInventoryItemsCollector item = new CompactInventoryItemsCollector();
	        	 item.setInventoryId((UUID) rs.getObject("inventoryId")); 
	        	 item.setBatchNumber(rs.getString("batchNumber"));
	        	 Integer prodId = rs.getInt("productId");
	        	 item.setProductId(prodId);
	        	 
	        	 Integer availableUnits = rs.getInt("availableUnits");
	        	 Double unitRate = rs.getDouble("unitRate");
	        	 Double inventoryValue=availableUnits*unitRate;
	        	 
	        	 item.setProductAvailableUnits(availableUnits);
	        	 item.setProductUnitRate(unitRate);
	        	 item.setInventoryValue(inventoryValue);
	        	 
	        	 if(compactInventoryDetails.containsKey(prodId)) {
	        		 ((List<CompactInventoryItemsCollector>) compactInventoryDetails.get(prodId)).add(item);
	        		 
	        	 }else {
	        		 List<CompactInventoryItemsCollector> itemList = new ArrayList<CompactInventoryItemsCollector>();
	        		 itemList.add(item);
	        		 compactInventoryDetails.put(item.getProductId(),itemList);
	        		 
	        		 
	        	 }
	        	 
	        	
	        	 
	        	 
	         
	        }
	         
	        return  compactInventoryDetails;
	    }
	
}
