package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

@Component
public class RateCardSummaryResultsetExtractor implements ResultSetExtractor {
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	       RateCardSummaryOrganizationDTO organizationSumary = null;
	         boolean isOrganizationSet = false;
	         while (rs.next()) {
	        	 if(!isOrganizationSet) {
	        		 organizationSumary = new  RateCardSummaryOrganizationDTO();
	        		 organizationSumary.setOrganizationId((UUID) rs.getObject("organization_id"));
	        		 organizationSumary.setOrganizationName(rs.getString("organization_name"));
	        		 organizationSumary.setLocationsSummary(new ArrayList<RateCardSummaryLocationDTO>());
	        		 isOrganizationSet=true;		 
	        	 }
	        	 RateCardSummaryLocationDTO locationSummary = new RateCardSummaryLocationDTO();
	        	 locationSummary.setLocationId((UUID) rs.getObject("location_id"));
	        	 locationSummary.setLocationName(rs.getString("location_name"));
	        	Integer rateCardCount = rs.getInt("count_ratecard");
	        	if(rateCardCount == null) rateCardCount=0;
	        	 locationSummary.setRateCardCount(rateCardCount);
	        	 organizationSumary.getLocationsSummary().add(locationSummary);
	         
	        }
	        return  organizationSumary;
	    }
	
}
