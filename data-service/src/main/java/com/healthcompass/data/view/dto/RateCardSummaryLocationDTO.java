package com.healthcompass.data.view.dto;

import java.util.UUID;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "Location")
@Relation(collectionRelation = "locationsSummary")
@Component
public class RateCardSummaryLocationDTO  extends RepresentationModel<RateCardSummaryLocationDTO>
{
	
	private UUID locationId;
	private String locationName;
	private Integer rateCardCount;

}
