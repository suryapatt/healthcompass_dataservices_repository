package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class ProductSummaryOrganizationViewResultSetExtrator implements ResultSetExtractor{
	

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
    	
        ProductSummaryOrganizationDTO productSummaryOrganization = new ProductSummaryOrganizationDTO();
        List<ProductSummaryViewDTO> products = new ArrayList<ProductSummaryViewDTO>();
        boolean isOrganizationInofset = false;
        
        ProductSummaryViewDTO newProduct = null;
        while (rs.next()) {
        	if(!isOrganizationInofset) {
        		productSummaryOrganization.setOrganizationId((UUID) rs.getObject("organizationId"));;
        		productSummaryOrganization.setOrganizationName(rs.getString("organizationName"));
        		   		
        		
        	}
        		 newProduct = new ProductSummaryViewDTO();   	 
	        	 newProduct.setMarketerId(rs.getInt("marketerId"));
	        	 newProduct.setMarketerName(rs.getString("marketerName"));
        		 newProduct.setProductId(rs.getInt("productId"));
        		 newProduct.setName(rs.getString("productName")+" (Marketer : "+newProduct.getMarketerName()+")");
        		 newProduct.setDescription(rs.getString("productDescription"));
        		 products.add(newProduct);
	        	
	        	 
        	}
        productSummaryOrganization.setProducts(products);
        	
         return  productSummaryOrganization;
    }
	
}
