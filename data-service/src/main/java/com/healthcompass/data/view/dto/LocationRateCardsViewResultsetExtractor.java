package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class LocationRateCardsViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	       LocationRateCardsViewDTO locationRateCards = null;
	         boolean isLocationSet = false;
	         while (rs.next()) {
	        	 if(!isLocationSet) {
	        		 locationRateCards = new  LocationRateCardsViewDTO();
	        		 locationRateCards.setLocationId((UUID) rs.getObject("locationId"));
	        		 locationRateCards.setLocationName(rs.getString("locationName"));
	        		 locationRateCards.setRateCards(new ArrayList<LocationRateCardViewDTO>());
	        		 isLocationSet=true;		 
	        	 }
	        	 LocationRateCardViewDTO rateCard = new LocationRateCardViewDTO();
	        	 rateCard.setId(rs.getInt("id"));
	        	 rateCard.setDisplay(rs.getString("display"));
	        	 rateCard.setDescription(rs.getString("description"));
	        	 rateCard.setRate(rs.getDouble("rate"));
	        	 rateCard.setActive(rs.getBoolean("active"));
	        	 LocationRateCardsCategoryViewDTO rateCardCat = new LocationRateCardsCategoryViewDTO();
	        	 rateCardCat.setRateCardCategoryId(rs.getInt("rateCardCategoryId"));
	        	 rateCardCat.setRateCardCategoryCode(rs.getString("rateCardCategoryCode"));
	        	 rateCardCat.setRateCardCatgeoryDescription(rs.getString("rateCardCategoryDescription"));
	        	 rateCardCat.setActive(rs.getBoolean("active"));
	        	 rateCard.setRateCardCategory(rateCardCat);
	        	 
	        	 locationRateCards.getRateCards().add(rateCard);
	         
	        }
	        return  locationRateCards;
	    }
	
}
