package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.model.LocalizationResource;
import com.healthcompass.data.model.LocalizationResourceCategory;
import com.healthcompass.data.model.LocalizationResourceGroup;
import com.healthcompass.data.util.HealthCompassConstants;
@Component
public class LocalizationResourcePageResultsetExtractor implements ResultSetExtractor{
	
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	List<LocalizationPageViewDTO> helpPages = new ArrayList<LocalizationPageViewDTO>();
	    	
	         Integer currentLanguageId=null;
	         String currentResourceGroupCode=null;
	         String currentResourceCategoryId=null;
	         String currentResourceId=null;
	         String currentResourceSectionId=null;
	         LocalizationPageViewDTO page = new LocalizationPageViewDTO();
	         List<ResourceGroupViewDTO> resourceGroups = new ArrayList<ResourceGroupViewDTO>();
	         List<ResourceCategoryViewDTO> resourceCategories = new ArrayList<ResourceCategoryViewDTO>();
	         List<ResourceViewDTO> resources = new ArrayList<ResourceViewDTO>();
	         
	         List<ResourceSectionViewDTO> resourceSections = new ArrayList<ResourceSectionViewDTO>();
	         List<ResourceFieldViewDTO> resourceFields = new ArrayList<ResourceFieldViewDTO>();
	         
	         ResourceGroupViewDTO resourceGroup = null;
	         ResourceCategoryViewDTO resourceCategory = null;
	         ResourceViewDTO resource = null;
	         ResourceSectionViewDTO resourceSection = null;
	         ResourceFieldViewDTO resourceField = null;
	         
	         
	         while (rs.next()) {
	        	     Integer langId = rs.getInt("languageId");
	        	     if(currentLanguageId == null || currentLanguageId.compareTo(langId) !=0) {
	        	    	 page = new LocalizationPageViewDTO();
	        	    	 helpPages.add(page);
	        	    	 page.setLanguageCode(rs.getString("languageCode"));
	        	    	 page.setLanguageId(langId);
	        	    	 resourceGroups = new ArrayList<ResourceGroupViewDTO>();
	        	    	 page.setResourceGroups(resourceGroups);
	        	    	
	        	    	 currentLanguageId = langId;
	        	     }
	        	     String resourceGroupCode = rs.getString("resourceGroupCode");
	        	     if(currentResourceGroupCode == null || !currentResourceGroupCode.equals(resourceGroupCode)) {
	        	    	 resourceGroup = new ResourceGroupViewDTO();
	        	    	 resourceGroups.add(resourceGroup);
	        	    	 resourceGroup.setResourceGroupCode(resourceGroupCode);
	        	    	 resourceGroup.setResourceGroupName(rs.getString("resourceGroupName"));
	        	    	 resourceCategories = new ArrayList<ResourceCategoryViewDTO>();
	        	    	 resourceGroup.setResourceCategories(resourceCategories);
	        	    	 currentResourceGroupCode = resourceGroupCode;
	        	    	
	        	     }
	        	     String resourceCategoryId = rs.getString("resourceCategoryId");
	        	     if(currentResourceCategoryId == null || !currentResourceCategoryId.equals(resourceCategoryId)) {
	        	    	 resourceCategory = new ResourceCategoryViewDTO();
	        	    	 resourceCategories.add(resourceCategory);
	        	    	 resourceCategory.setResourceCategoryId(resourceCategoryId);
	        	    	 resourceCategory.setResourceCategoryCode(rs.getString("resourceCategoryCode"));
	        	    	 resourceCategory.setResourceCategoryName(rs.getString("resourceCategoryName"));
	        	    	 resources = new ArrayList<ResourceViewDTO>();
	        	    	 resourceCategory.setResources(resources);
	        	    	 currentResourceCategoryId = resourceCategoryId;
	        	    	
	        	     }
	        	     
	        	     String resourceId = rs.getString("resourceId");
	        	     if(currentResourceId == null || !currentResourceId.equals(resourceId)) {
	        	    	 resource = new ResourceViewDTO();
	        	    	 resources.add(resource);
	        	    	 resource.setResourceId(resourceId);
	        	    	 resource.setResourceCode(rs.getString("resourceCode"));
	        	    	 resource.setResourceName(rs.getString("resourceName"));
	        	    	 resourceSections = new ArrayList<ResourceSectionViewDTO>();
	        	    	 resource.setSections(resourceSections);
	        	    	 currentResourceId = resourceId;
	        	    	
	        	     }
	        	     
	        	     String sectionId = rs.getString("resourceSectionId");
	        	     if(currentResourceSectionId == null || !currentResourceSectionId.equals(sectionId)) {
	        	    	 resourceSection = new ResourceSectionViewDTO();
	        	    	 resourceSections.add(resourceSection);
	        	    	 resourceSection.setResourceSectionId(sectionId);
	        	    	 resourceSection.setResourceSectionCode(rs.getString("resourceSectionCode"));
	        	    	 resourceSection.setResourceSectionName(rs.getString("resourceSectionName"));
	        	    	 resourceSection.setResourceSectionPosition(rs.getInt("resourceSectionPosition"));
	        	    	 resourceSection.setResourceId(resourceId);
	        	    	 
	        	    	 resourceFields = new ArrayList<ResourceFieldViewDTO>();
	        	    	 resourceSection.setFields(resourceFields);
	        	    	 currentResourceSectionId = sectionId;
	        	    	
	        	     }
	        	     resourceField = new ResourceFieldViewDTO();
	        	     resourceFields.add(resourceField);
	        	     resourceField.setResourceFieldId(rs.getString("resourceFieldId"));
	        	     resourceField.setText(rs.getString("Text"));
	        	     resourceField.setResourceFieldName(rs.getString("resourceFieldName"));
	        	     resourceField.setLanguageCode(rs.getString("languageCode"));
	        	     resourceField.setResourceFieldPosition(rs.getInt("fieldPosition"));
	      
	        }
	        
	         return  helpPages;
	    }
	
}
