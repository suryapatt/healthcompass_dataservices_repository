package com.healthcompass.data.view.dto;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
//import com.healthcompass.data.dto.model.ServiceModel.ServiceModelBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonRootName(value = "valueset")
@Relation(collectionRelation = "valueSets", itemRelation = "valueSet")
//@JsonInclude(Include.NON_NULL) -- enable this if attributes with null values not to be sent to the client
@Component
public class ValueSetViewDTO extends RepresentationModel<ValueSetViewDTO>{
  
	
	private int id;
	private String userGroup;
	private Integer level;
	private String code;
	private String system;
	private String display;
	private String definition;
	private Integer sortOrder;
	private String isActive;
	private String commonName;
	//private ValueSetTypeModel valueSetType;
	private java.sql.Timestamp createdOn;
	private String createdBy;
	private java.sql.Timestamp updatedOn;
	private String updatedBy;
	

	
	
	
}
