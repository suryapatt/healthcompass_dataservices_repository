package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.model.Patient;
import com.healthcompass.data.util.HealthCompassConstants;
@Component
public class BillPaymentsMainViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	BillPaymentsMainViewDTO billPaymentsMainViewDTO = new  BillPaymentsMainViewDTO();
	    	ArrayList<BillPaymentViewDTO> biilPaymentsView = new ArrayList<BillPaymentViewDTO>();
	         boolean isPatientInformationSet = false;
	         while (rs.next()) {
	        	 if(!isPatientInformationSet) {
	        		 
	        		 billPaymentsMainViewDTO.setPatientId((UUID) rs.getObject("patientId"));
	        		 String encodedPatientName = rs.getString("patientName");
	        		 String decodedPatientName=null;
	        		 try {
	        			//create a dummy Patient object to get the decoded bane.
	        			 Patient dummy = new Patient();
		        		 dummy.setName(encodedPatientName);
		        		 decodedPatientName = HealthCompassConstants.getDecodedValue("Patient Name",dummy.getCompositeName().getText());
		        		 
	        		 }catch(Exception e) {
	        			 e.printStackTrace();
	        			 decodedPatientName = encodedPatientName;
	        		 }
	        		 
	        		 billPaymentsMainViewDTO.setPatientName(decodedPatientName);
	        		 billPaymentsMainViewDTO.setBillPayments(new ArrayList<BillPaymentViewDTO>());
	        		 isPatientInformationSet=true;		 
	        	 }
	        	 BillPaymentViewDTO billPayment = new BillPaymentViewDTO();
	        	 billPayment.setBillNumber(rs.getString("billNumber"));
	        	 billPayment.setBillDate(rs.getTimestamp("billDate"));
	        	 //billPayment.setBillDate(HealthCompassConstants.convertSQLDateTimeStampToLocalTimeStampDate(rs.getTimestamp("billDate"), HealthCompassConstants.defaultSQLTimesStampDateFormat));
	        	 billPayment.setBillDate(rs.getTimestamp("billDate"));
	        	 billPayment.setAmountBilled(rs.getDouble("amountBilled"));
	        	 billPayment.setAmountPaid(rs.getDouble("amountPaid"));
	        	 billPayment.setAmountDue(rs.getDouble("amountDue"));
	        	 billPayment.setLastPaidOn(rs.getTimestamp("lastPaidOn"));
	        	 billPayment.setBillPaymentStatus(rs.getString("paymentStatus"));
	        	 biilPaymentsView.add(billPayment);
	        	 
	        	 
	         
	        }
	         billPaymentsMainViewDTO.setBillPayments(biilPaymentsView);
	        return  billPaymentsMainViewDTO;
	    }
	
}
