package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class LocalizedTextResultSetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	List<ResourceViewDTO> resourceViewDTOs = new  ArrayList<ResourceViewDTO>();
	    	
	         while (rs.next()) {
	        	    ResourceViewDTO   resourceViewDTO = new ResourceViewDTO();   		
	        		resourceViewDTO.setId(rs.getInt("id"));
	        		resourceViewDTO.setResourceId(rs.getString("resourceId"));
	        	//	resourceViewDTO.setLanguageCode(rs.getString("languageCode"));
	        		//resourceViewDTO.setLanguage(rs.getString("language"));
	        		//resourceViewDTO.setCountryCode(rs.getString("countryCode"));
	        		//resourceViewDTO.setCountry(rs.getString("country"));
	        		//resourceViewDTO.setText(rs.getString("text"));
	        		resourceViewDTOs.add(resourceViewDTO);
	        	         
	        }
	       
	         return  resourceViewDTOs;
	    }
	
}
