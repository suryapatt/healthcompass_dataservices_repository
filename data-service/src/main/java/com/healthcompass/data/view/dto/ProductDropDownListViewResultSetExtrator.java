package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class ProductDropDownListViewResultSetExtrator implements ResultSetExtractor{
	

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
    	
        ProductDropDownListViewDTO productDropDownListView = new ProductDropDownListViewDTO();
        List<ProductDropDownViewDTO> products = new ArrayList<ProductDropDownViewDTO>();
        boolean isOrganizationInofset = false;
        
        ProductDropDownViewDTO product = null;
        while (rs.next()) {
        	
        	product = new ProductDropDownViewDTO();   	 
        	product.setId(rs.getInt("id"));
        	product.setName(rs.getString("name")+" (Marketer : "+rs.getString("marketerName")+")");
        	products.add(product);
	        	
	        	 
        	}
        productDropDownListView.setProducts(products);
        	
           return  productDropDownListView;
    }
	
}
