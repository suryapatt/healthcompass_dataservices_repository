package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

@Component
public class PatientOutStandingBillsAmountRestltSetExtractor implements ResultSetExtractor {
	

	    public Double extractData(ResultSet rs) throws SQLException, DataAccessException {
	         Double pendingAmount = 0.0;
	         while (rs.next()) {
	        	 pendingAmount = rs.getDouble("outStandingPendingBillsAmount");    
	        }
	        return  pendingAmount;
	    }
	
}
