package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.healthcompass.data.model.ContactPointType;
import com.healthcompass.data.model.Patient;
import com.healthcompass.data.util.HealthCompassConstants;
@Component
public class ReceiptViewResultsetExtractor implements ResultSetExtractor{
	

	    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	    	ReceiptViewDTO receiptViewDTO = null;
	    	ArrayList<BillPaymentReceiptViewDTO> biilPaymentReceiptViewDTO = new ArrayList<BillPaymentReceiptViewDTO>();
	         boolean isAggregatedInformationSet = false;
	         while (rs.next()) {
	        	 if(!isAggregatedInformationSet) {
	        		 receiptViewDTO = new  ReceiptViewDTO();
	        		 receiptViewDTO.setOrganizationId((UUID) rs.getObject("organizationId"));
	        		 receiptViewDTO.setOrganizationName(rs.getString("organizationName"));
	        		 receiptViewDTO.setLocationId((UUID) rs.getObject("locationId"));
	        		 receiptViewDTO.setLocationName(rs.getString("locationName"));
	        		 receiptViewDTO.setPatientId(rs.getInt("patientId"));
	        		 Patient pat = new Patient();
	        		 pat.setName(rs.getString("patientName"));
	        		 pat.setPhone(rs.getString("phoneNumber"));
	        		 receiptViewDTO.setPhoneNumber(pat.getDecodedPhone());
	        		 receiptViewDTO.setEmail(pat.getDecodedEmail());
	        		 receiptViewDTO.setUserName(rs.getString("userName"));
	        		 receiptViewDTO.setPatientName(pat.getDecodedName());
	        		 receiptViewDTO.setReceiptNumber(rs.getString("receiptNumber"));
	        		        		 
	        		 receiptViewDTO.setPaymentMode(rs.getString("paymentMode"));
	        		 java.sql.Timestamp curDate = new java.sql.Timestamp(System.currentTimeMillis());
	        		 receiptViewDTO.setPaymentDate(HealthCompassConstants.convertSQLDateTimeStampToLocalDateString(curDate));

	        		// receiptViewDTO.setPaymentDate(new java.sql.Timestamp(System.currentTimeMillis()));
	        		 receiptViewDTO.setTotalPaymentReceived(rs.getDouble("totalPaymentReceived"));
	        		 receiptViewDTO.setTotalOutStanding(rs.getDouble("totalOutstanding"));
	        		 receiptViewDTO.setBillPayments(biilPaymentReceiptViewDTO);
	        		 isAggregatedInformationSet=true;		 
	        	 }
	        	 BillPaymentReceiptViewDTO billPayment = new BillPaymentReceiptViewDTO();
	        	 billPayment.setBillPaymentId((UUID) rs.getObject("billPaymentId"));
	        	 billPayment.setBillNumber(rs.getString("billNumber"));
	        	// billPayment.setBillDate(rs.getTimestamp("billDate"));
	        	 java.sql.Timestamp billDate = rs.getTimestamp("billDate");
        		 billPayment.setBillDate(HealthCompassConstants.convertSQLDateTimeStampToLocalDateString(billDate));
	        	 billPayment.setPaymentAmount(rs.getDouble("paymentAmount"));
	        	 billPayment.setFinalDue(rs.getDouble("finalDue"));
	        	 billPayment.setOriginalDue(0.0);
	        	 biilPaymentReceiptViewDTO.add(billPayment);
	        	 
	        	 
	         
	        }
	        /* Patient patient = new Patient(); 
	         patient.setName(receiptViewDTO.getPatientName()); // just for decoding the name obtained from DB we are using Patient Object here.
    		 receiptViewDTO.setPatientName(patient.getDecodedName());*/
	         receiptViewDTO.setBillPayments(biilPaymentReceiptViewDTO);
	        return  receiptViewDTO;
	    }
	
}
