package com.healthcompass.data.view.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
@Component
public class PurchaseOrderSummaryListViewResultextractor implements ResultSetExtractor{
	

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
    	
        PurchaseOrderSummaryListViewDTO purchaseOrderSummaryListView = new PurchaseOrderSummaryListViewDTO();
        List<PurchaseOrderSummaryViewDTO> purchaseOrders = new ArrayList<PurchaseOrderSummaryViewDTO>();
        boolean isOrganizationInofset = false;
        
        PurchaseOrderSummaryViewDTO newPO = null;
        while (rs.next()) {
        	if(!isOrganizationInofset) {
        		purchaseOrderSummaryListView.setOrganizationId((UUID) rs.getObject("organizationId"));;
        		purchaseOrderSummaryListView.setOrganizationName(rs.getString("organizationName"));
        		   		
        		
        	}
        	newPO = new PurchaseOrderSummaryViewDTO();   	 
        	newPO.setLocationId((UUID) rs.getObject("locationId"));
        	newPO.setLocationName(rs.getString("locationName"));
        	newPO.setSupplierId((UUID) rs.getObject("supplierId"));
        	newPO.setSupplierName(rs.getString("supplierName"));
        	newPO.setPurchaseOrderId((UUID) rs.getObject("purchaseOrderId"));
        	newPO.setPurchaseOrderNumber(rs.getString("purchaseOrderNumber"));
        	newPO.setPurchaseOrderDate(rs.getTimestamp("purchaseOrderDate"));
        	purchaseOrders.add(newPO);
	        	
	        	 
        	}
        purchaseOrderSummaryListView.setPurchaseOrders(purchaseOrders);
        	
           return  purchaseOrderSummaryListView;
    }
	
}
